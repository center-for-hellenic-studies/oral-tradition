<?php
/**
 * @internal    never define functions inside callbacks.
 *              these functions could be run multiple times; this would result in a fatal error.
 */

/**
 * custom option and settings
 */
function orpheus_settings_init()
{
    // register a new setting for "orpheus options" page
    register_setting('orpheus', 'orpheus_options');

    // register a new section in the "orpheus options" page
		/*
    add_settings_section(
        'orpheus_section_developers',
        __('Instagram settings for orpheus.', 'orpheus'),
        'orpheus_section_developers_cb',
        'orpheus'
    );
		*/
}

/**
 * register our orpheus_settings_init to the admin_init action hook
 */
add_action('admin_init', 'orpheus_settings_init');

/**
 * top level menu
 */
function orpheus_options_page()
{
    // add top level menu page
    add_menu_page(
        'Configure MPC Options',
        'MPC Options',
        'manage_options',
        'orpheus',
        'orpheus_options_page_html'
    );
}

/**
 * register our orpheus_options_page to the admin_menu action hook
 */
add_action('admin_menu', 'orpheus_options_page');

/**
 * top level menu:
 * callback functions
 */
function orpheus_options_page_html()
{
    // check user capabilities
    if (!current_user_can('manage_options')) {
        return;
    }

    // add error/update messages

    // check if the user have submitted the settings
    // wordpress will add the "settings-updated" $_GET parameter to the url
    if (isset($_GET['settings-updated'])) {
        // add settings saved message with the class of "updated"
        add_settings_error('orpheus_messages', 'orpheus_message', __('Settings Saved', 'orpheus'), 'updated');
    }

    // show error/update messages
    settings_errors('orpheus_messages');
    ?>
    <div class="wrap">
        <h1><?= esc_html(get_admin_page_title()); ?></h1>
        <form action="options.php" method="post">
            <?php
            // output security fields for the registered setting "orpheus"
            settings_fields('orpheus');
            // output setting sections and their fields
            // (sections are registered for "orpheus", each field is registered to a specific section)
            do_settings_sections('orpheus');
            // output save settings button
            submit_button('Save Settings');
            ?>
        </form>
    </div>
    <?php
}
