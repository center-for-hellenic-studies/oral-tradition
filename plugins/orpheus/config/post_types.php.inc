<?php
/**
 * @package WordPress
 * @subpackage Orpheus
 *
 * Register necessary custom post types
 *
 */

add_action( 'init', 'create_post_type' );
function create_post_type() {
  /*
  register_post_type( 'issue',
    array(
      'labels' => array(
        'name' => __( 'Issues' ),
      ),
    'supports' => array( 'title', 'author', ),
    'public' => true,
    'has_archive' => true,
    'description' => 'An issue of the journal',
    'map_meta_cap' => true,
    'taxonomies' => array( 'category', 'post_tag' ),
    )
  );
  register_post_type( 'item',
    array(
      'labels' => array(
        'name' => __( 'Gallery Item' ),
      ),
    'supports' => array( 'title', 'editor', 'thumbnail', ),
    'public' => true,
    'has_archive' => true,
    'description' => 'Item such as a photograph or video from the orphe.us collection Gallery (not the collections with Harvard Library)',
    'map_meta_cap' => true,
    'taxonomies' => array( 'category', 'post_tag' ),
    'capability_type' => "item"
    )
  );
  register_post_type( 'collection',
    array(
      'labels' => array(
        'name' => __( 'Collection' ),
      ),
    'supports' => array( 'title', 'thumbnail', ),
    'public' => true,
    'has_archive' => true,
    'description' => 'A collection to link to from the homepage',
    'map_meta_cap' => true,
    'taxonomies' => array( 'category', 'post_tag' ),
    'capability_type' => 'collection',
    )
  );
  register_post_type( 'song',
    array(
      'labels' => array(
        'name' => __( 'Song' ),
      ),
    'supports' => array( 'title', 'editor', 'thumbnail', ),
    'public' => true,
    'has_archive' => true,
    'description' => 'A song in the orphe.us collection (may be transcribed)',
    'map_meta_cap' => true,
    'taxonomies' => array( 'category', 'post_tag' ),
    'capability_type' => "song"
    )
  );
  register_post_type( 'transcription',
    array(
      'labels' => array(
        'name' => __( 'Transcription' ),
      ),
    'supports' => array( 'title', 'author', ),
    'public' => true,
    'has_archive' => true,
    'description' => 'A transcription of a song in the orphe.us collection',
    'map_meta_cap' => true,
    'taxonomies' => array( 'category', 'post_tag' ),
    'capability_type' => "transcription"
    )
  );
  */
}
