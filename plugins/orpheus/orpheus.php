<?php

/**
 * @package WordPress
 * @subpackage Orpheus
 *
 * Plugin Name: orphe.us
 * Description: Plugin associated with the orphe.us website
 * Version: 1.0.0
 * Author: Archimedes Digital
 * Author URI: https://archimedes.digital
 */

define('ORPHEUS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

// require settings
require_once( ORPHEUS_PLUGIN_DIR .'config/settings.php.inc');
require_once( ORPHEUS_PLUGIN_DIR .'config/post_types.php.inc');
require_once( ORPHEUS_PLUGIN_DIR .'config/taxonomies.php.inc');

// require other specific plugin functionalities
require_once( ORPHEUS_PLUGIN_DIR .'inc/ajax.php.inc');
require_once( ORPHEUS_PLUGIN_DIR .'inc/options-page.php.inc');
require_once( ORPHEUS_PLUGIN_DIR .'inc/shortcodes.php.inc');
