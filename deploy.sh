#!/bin/bash
rsync -avz --progress . lukehollis@dev.oraltradition.org:~/webapps/dev_oraltradition/wp-content/
rsync -avz --progress . lukehollis@oraltradition.org:~/webapps/oraltradition/wp-content/
