import React from 'react';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';

// component
import HeaderInternal from './HeaderInternal';
import configureStore from '../../../store/configureStore';

describe('HeaderInternal', () => {
	it('renders correctly', () => {

		const wrapper = shallow(
			<Provider store={configureStore()}>
				<HeaderInternal />
			</Provider>
		);
		expect(wrapper).toBeDefined();
	});
});
