/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'react-apollo';
import $ from 'jquery';
import autoBind from 'react-autobind';

import IconSearch from '@material-ui/icons/Search';
import IconAccountCircle from '@material-ui/icons/AccountCircle';
import IconLaunch from '@material-ui/icons/Launch';
import IconClose from '@material-ui/icons/Close';
import IconAdd from '@material-ui/icons/Add';
import IconDeleteOutline from '@material-ui/icons/DeleteOutline';


import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Grow from '@material-ui/core/Grow';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { withStyles } from '@material-ui/core/styles';

import ItemCollectionsModalContainer from '../../../modules/items/containers/ItemCollectionsModalContainer';
import ItemDeleteModalContainer from '../../../modules/items/containers/ItemDeleteModalContainer';

import OrpheusLogo from '../../common/OrpheusLogo';
import { unselectAll } from '../../../modules/items/actions';

import './HeaderInternal.css';

var classNames = require('classnames');

function headerScrolled(e) {
	showHideScrollHint($(e.currentTarget));
}

function showHideScrollHint($headerInternalDefault) {
	if (
		$headerInternalDefault[0].scrollWidth -
			$headerInternalDefault.scrollLeft() ==
		$headerInternalDefault.outerWidth()
	) {
		console.log('hide');
		$headerInternalDefault
			.parent()
			.find('.headerInternalScrollHintRight')
			.removeClass('-show');
	} else {
		console.log('show');
		$headerInternalDefault
			.parent()
			.find('.headerInternalScrollHintRight')
			.addClass('-show');
	}
}

window.addEventListener('resize', headerResize);
window.onload = headerResize;
function headerResize(e) {
	var $headerInternalDefault = $('.headerInternalDefault');
	if (
		$headerInternalDefault.prop('scrollWidth') >
		$headerInternalDefault.parent().prop('scrollWidth')
	) {
		// If header isn't completely visible, show scroll hint.
		$headerInternalDefault
			.parent()
			.find('.headerInternalScrollHintRight')
			.addClass('-show');
	} else {
		$headerInternalDefault
			.parent()
			.find('.headerInternalScrollHintRight')
			.removeClass('-show');
	}
}

const styles = theme => ({
	root: {
		display: 'flex',
	},
	paper: {
		marginRight: theme.spacing.unit * 2,
	},
});

class HeaderInternal extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			secondaryMenuVisible: false,
			collectionsModalOpen: false,
			deleteModalOpen: false,

		};
		autoBind(this);
	}

	componentDidMount() {
		document.addEventListener('keydown', this.handleKeydown, false);
	}

	componentWillUnmount() {
		document.removeEventListener('keydown', this.handleKeydown, false);
	}

	handleKeydown(event) {
		if (event.keyCode === 27) {
			this.props.dispatchUnselectAll();
		}
	}

	handleToggle(e) {
		const { currentTarget } = e;

		this.setState(state => ({
			accountAnchorEl: currentTarget,
			secondaryMenuVisible: !state.secondaryMenuVisible,
		}));

		// TODO (daniel) figure out why the dropdown is translated above the menu and remove this hack
		setTimeout(()=>{
			$('.headerInternalSecondaryNav').css('margin-top', '240px');
		}, 300)

		$(e.currentTarget)
			.closest('.headerInternal')
			.addClass('-headerInternalPopup');
	}

	handleClose() {
		this.setState({
			secondaryMenuVisible: false,
		});
	}

	handleCollectionModalClose() {
		this.setState({
			collectionsModalOpen: false,
		});
	}

	handleCollectionModalOpen() {
		this.setState({
			collectionsModalOpen: true,
		});
	}


	handleDeleteModalClose() {
		this.setState({
			deleteModalOpen: false,
		});
	}

	handleDeleteModalOpen() {
		this.setState({
			deleteModalOpen: true,
		});
	}

	handleSearchKeyDown(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			event.stopPropagation();

			this.props.submitSearch();
		}
	}

	render() {
		const {
			title,
			handleUpdateSearch,
			itemsCount,
			articlesCount,
			collectionsCount,
			selectedItemIds,
		} = this.props;
		const { secondaryMenuVisible } = this.state;
		const showSelectionHeader =
			this.props.match && this.props.match.path === '/dashboard';

		var classes = classNames(this.props.className, {
			headerInternal: true,
			'-showSelectionHeader':
				showSelectionHeader && selectedItemIds && selectedItemIds.length,
		});

		return (
			<div className="headerInternalWrapper">
				<div className={classes}>
					<ItemCollectionsModalContainer
						open={this.state.collectionsModalOpen}
						handleClose={this.handleCollectionModalClose}
						handleOpen={this.handleCollectionModalOpen}
						selectedItemIds={showSelectionHeader ? selectedItemIds : []}
					/>
					<ItemDeleteModalContainer
						open={this.state.deleteModalOpen}
						handleClose={this.handleDeleteModalClose}
						handleOpen={this.handleDeleteModalOpen}
						selectedItemIds={showSelectionHeader ? selectedItemIds : []}
						dispatchUnselectAll={this.props.dispatchUnselectAll}
					/>

					<div className="headerInternalScrollHint headerInternalScrollHintLeft" />
					<div className="headerInternalScrollHint headerInternalScrollHintRight" />
					<div className="headerInternalDefault" onScroll={headerScrolled}>
						<div className="headerInternalSite">
							<a href="//orphe.us" className="headerInternalSiteLogo">
								<OrpheusLogo />
							</a>
							<a href="/dashboard" className="headerInternalSiteArchiveTitle">
								{title && title.length ? <span>{title}</span> : ''}
							</a >
						</div>
						<div className="headerInternalSearch">
							<IconSearch />
							<input
								id="header_internal_search"
								type="search"
								placeholder="Search archive"
								onChange={handleUpdateSearch}
								onKeyDown={this.handleSearchKeyDown}
								value={this.props.searchInputValue}
							/>
						</div>

						{!this.props.userId && (
							<ul className="headerInternalNav">
								<li>
									<Button
										className="headerInternalButton headerInternalButtonUpload"
										variant="contained"
										size="large"
										color="primary"
										to="/login"
										component={a}
									>
										Login
									</Button>
								</li>
							</ul>
						)}
						{!this.props.authState && (
							<ul className="headerInternalNav">
								<li
									className={
										window.location.pathname === '/dashboard/' ? '-active' : ''
									}
								>
									<a href="/dashboard/">Archive</a>
								</li>

								{itemsCount && articlesCount ? (
									<li>
										<a href="/items">Items</a>
									</li>
								) : (
									''
								)}
								{itemsCount && articlesCount ? (
									<li className="headerExternalNavLinkHideOnMobile">
										<a href="/posts">Posts</a>
									</li>
								) : (
									''
								)}
								<li
									className={`headerExternalNavLinkHideOnMobile ${
										window.location.pathname === '/settings/' ? '-active' : ''
									}`}
								>
									<a href="/settings">Settings</a>
								</li>
								<li>
									<IconButton
										className="headerInternalButton headerInternalButtonUser"
										aria-label="Account"

										aria-owns={
											secondaryMenuVisible ? 'menu-list-user-grow' : undefined
										}
										aria-haspopup="true"
										onClick={this.handleToggle}
									>
										<IconAccountCircle />
									</IconButton>
									<Popper
										open={secondaryMenuVisible}
										anchorEl={this.state.accountAnchorEl}
										placement={"bottom"}
										transition
										disablePortal
									>
										{({ TransitionProps, placement }) => (
											<Grow
												{...TransitionProps}
												id="menu-list-user-grow"

											>
												<Paper className="headerInternalSecondaryNav" >
													<ClickAwayListener onClickAway={this.handleToggle}>
														<MenuList>
															<li>
																<MenuItem
																	component={Link}
																	onClick={this.handleClose}
																	to="/profile"
																>
																	Profile
																</MenuItem>
															</li>
															<Divider />
															<li>
																<MenuItem
																	component={'a'}
																	onClick={() => {
																		this.handleClose();
																		this.props.startDriftInteraction();
																	}}
																>
																	Help
																</MenuItem>
															</li>
															<li>
																<MenuItem
																	component={Link}
																	onClick={this.handleClose}
																	to="/logout"
																>
																	Log out
																</MenuItem>
															</li>
														</MenuList>
													</ClickAwayListener>
												</Paper>
											</Grow>
										)}
									</Popper>
								</li>
								<li>
									<IconButton
										className="headerInternalButton headerInternalButtonLaunch"
										aria-label="Launch"
										to={
											this.props.match.path === '/items' &&
											this.props.location.pathname.includes('/edit')
												? this.props.location.pathname.split('/edit')[0]
												: '/'
										}
										target="_blank"
									>
										<IconLaunch />
									</IconButton>
								</li>
								<li>
									<Button
										className="headerInternalButton headerInternalButtonUpload"
										variant="contained"
										size="large"
										color="primary"
										to="/items/create"
									>
										Add
									</Button>
								</li>
							</ul>
						)}
					</div>
					<div className="headerInternalSelection">
						<Paper
							className="headerInternalSelectionPaper"
							elevation={3}
							square={true}
						>
							<IconButton
								className="headerInternalSelectionClose"
								aria-label="Close"
								disableRipple
								onClick={this.props.dispatchUnselectAll}
							>
								<IconClose />
							</IconButton>
							<Typography
								className="headerInternalSelectionCount"
								variant="body1"
								component="span"
							>
								{selectedItemIds.length} selected
							</Typography>
							<div className="headerInternalSelectionActions">

								<IconButton
									onClick={this.handleCollectionModalOpen}
									className="headerInternalSelectionEdit"
									aria-label="Add to Collection"
									disableRipple
								>
									<IconAdd color="primary" />
								</IconButton>

								<IconButton
									onClick={this.handleDeleteModalOpen}
									className="headerInternalSelectionDelete"
									aria-label="Delete"
									disableRipple
								>
									<IconDeleteOutline color="primary" />
								</IconButton>


							</div>
						</Paper>
					</div>
				</div>
			</div>
		);
	}
}

HeaderInternal.propTypes = {
	dark: PropTypes.bool,
	title: PropTypes.string,
	handleUpdateSearch: PropTypes.func,
	selectedItemIds: PropTypes.array,
	dispatchUnselectAll: PropTypes.func,
};

HeaderInternal.defaultProps = {
	title: null,
};

const mapStateToProps = state => ({
	selectedItemIds: state.items.selectedItemIds,
});

const mapDispatchToProps = dispatch => ({
	dispatchUnselectAll: () => {
		dispatch(unselectAll());
	},
});

export default compose(
	connect(
		mapStateToProps,
		mapDispatchToProps
	),
	withStyles(styles)
)(HeaderInternal);
