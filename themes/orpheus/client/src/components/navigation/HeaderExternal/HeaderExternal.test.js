import React from 'react';
import { shallow } from 'enzyme';

// component
import HeaderExternal from './HeaderExternal';

describe('HeaderExternal', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<HeaderExternal />);
		expect(wrapper).toBeDefined();
	});
});
