/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import autoBind from 'react-autobind';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import IconMoreHoriz from '@material-ui/icons/MoreHoriz';
import IconSearch from '@material-ui/icons/Search';
import IconArrowBack from '@material-ui/icons/ArrowBack';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import HeaderExternalOrpheusMain from './HeaderExternalOrpheusMain';
import OrpheusLogo from '../../common/OrpheusLogo';

import getCurrentArchiveHostname from '../../../lib/getCurrentArchiveHostname';

import './HeaderExternal.css';

class HeaderExternal extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			secondaryMenuVisible: false,
			searchVisible: false,
		};
		autoBind(this);
	}

	componentDidMount() {
		document.addEventListener('keydown', this.handleDocumentKeyDown);
	}

	componentWillUnmount() {
		document.removeEventListener('keydown', this.handleDocumentKeyDown);
	}

	handleDocumentKeyDown(event) {
		if (event.keyCode === 27) {
			event.preventDefault();
			event.stopPropagation();

			this.collapseSearch();
		}
	}

	handleSearchKeyDown(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			event.stopPropagation();

			this.props.submitSearch();
		}
	}

	expandSearch() {
		this.setState(
			{
				searchVisible: true,
			},
			() => {
				$('#header_external_search').focus();
			}
		);
	}

	collapseSearch() {
		this.setState({
			searchVisible: false,
		});
	}

	goBack() {
		this.collapseSearch();
		this.props.history.goBack();
	}

	handleToggle() {
		this.setState({
			secondaryMenuVisible: !this.state.secondaryMenuVisible,
		});
	}

	handleClose() {
		this.setState({
			secondaryMenuVisible: false,
		});
	}

	render() {
		const {
			headerTheme,
			title,
			classes,
			handleUpdateSearch,
			itemsCount,
			articlesCount,
			userId,
			userIsAdmin,
			searchInputValue,
		} = this.props;

		const { searchVisible } = this.state;

		const _classes = classes || [];
		const isOnSearchRoute = window.location.pathname === '/search/';

		// if no archive hostname is found, use Orpheus Main Header
		const hostname = getCurrentArchiveHostname();
		if (!hostname || !hostname.length) {
			return <HeaderExternalOrpheusMain {...this.props} />;
		}

		_classes.push('headerExternal');

		if (headerTheme === 'dark') {
			_classes.push('-dark');
		}

		if (isOnSearchRoute) {
			_classes.push('-onSearchRoute');
		} else if (searchVisible) {
			_classes.push('-showSearch');
		}

		const isItemDetail =
			this.props.match.path === '/items' && !this.props.match.isExact;

		return (
			<React.Fragment>
				<div className={_classes.join(' ')}>
					<div className="headerExternalDefault">
						<div className="headerExternalSite">
							<a href="//orphe.us" className="headerExternalSiteLogo">
								<OrpheusLogo />
							</a>
							{title && title.length ? (
								<Link to="/" className="headerExternalSiteArchiveTitle">
									<span>{title}</span>
								</Link>
							) : (
								''
							)}
						</div>
						<ul className="headerExternalNav">
							{/*
							<li>
								<Link to="/explore">Explore</Link>
							</li>
							*/}
							{getCurrentArchiveHostname() === "mindthegap.orphe.us" ?
								<li className="headerExternalNavLinkHideOnMobile">
									<Link to="/search/?Type=Person">People</Link>
								</li>
							: ''}
							{getCurrentArchiveHostname() === "mindthegap.orphe.us" ?
								<li className="headerExternalNavLinkHideOnMobile">
									<Link to="/search?Type=Item">Archival Items</Link>
								</li>
							:
								<li className="headerExternalNavLinkHideOnMobile">
									<Link to="/search">Items</Link>
								</li>
							}
							{/*
							<li className="headerExternalNavLinkHideOnMobile">
								<Link to="/about">About</Link>
							</li>
							*/}
							<li>
								<IconButton
									buttonRef={node => {
										this.anchorEl = node;
									}}
									aria-owns={open ? 'menu-list-grow' : undefined}
									aria-haspopup="true"
									onClick={this.handleToggle}
									className="headerExternalButton"
								>
									<IconMoreHoriz />
								</IconButton>
								<Popper
									open={this.state.secondaryMenuVisible}
									anchorEl={this.anchorEl}
									transition
									disablePortal
								>
									{({ TransitionProps, placement }) => (
										<Grow
											{...TransitionProps}
											id="menu-list-grow"
											style={{
												transformOrigin:
													placement === 'bottom'
														? 'center top'
														: 'center bottom',
											}}
										>
											<Paper>
												<ClickAwayListener onClickAway={this.handleClose}>
													<MenuList className="headerExternalSecondaryNav">
														{!userId ? (
															<li>
																<MenuItem
																	component={Link}
																	onClick={this.handleClose}
																	to="/login"
																>
																	Log in
																</MenuItem>
															</li>
														) : (
															''
														)}
														{/*/
														<MenuItem
															component={Link}
															onClick={this.handleClose}
															to="/signup"
														>
															Sign up
														</MenuItem>
														*/}
														<li>
															<MenuItem
																component="a"
																onClick={() => {
																	this.handleClose();
																	this.props.startDriftInteraction();
																}}
															>
																Help
															</MenuItem>
														</li>
														{userId && (
															<li>
																<MenuItem
																	component={Link}
																	onClick={this.handleClose}
																	to="/logout"
																>
																	Log out
																</MenuItem>
															</li>
														)}
													</MenuList>
												</ClickAwayListener>
											</Paper>
										</Grow>
									)}
								</Popper>
							</li>
							{itemsCount ? (
								<li>
									<button
										className="headerExternalButton headerExternalButtonSearch"
										type="button"
										onClick={this.expandSearch}
									>
										<IconSearch />
									</button>
								</li>
							) : (
								''
							)}
							{userId &&
								userIsAdmin &&
								(isItemDetail ? (
									<li>
										<Link to={this.props.location.pathname + 'edit'}>
											<Button
												className="headerExternalButton headerExternalButtonManage"
												variant="contained"
												size="large"
												color="primary"
											>
												Edit
											</Button>
										</Link>
									</li>
								) : (
									<li>
										<Link to="/dashboard/">
											<Button
												className="headerExternalButton headerExternalButtonManage"
												variant="contained"
												size="large"
												color="primary"
											>
												Manage
											</Button>
										</Link>
									</li>
								))}
						</ul>
					</div>
					<div
						className="headerExternalSearch"
						style={{ visibility: searchVisible ? 'visible' : 'hidden' }}
					>
						<div className="headerExternalSearchContent">
							<button id="header_external_search_back" onClick={this.goBack}>
								<IconArrowBack />
							</button>
							<input
								id="header_external_search"
								type="search"
								placeholder="Search"
								onChange={handleUpdateSearch}
								onKeyDown={this.handleSearchKeyDown}
								value={searchInputValue}
							/>
						</div>
						<div
							className="headerExternalSearchBackground"
							onClick={this.collapseSearch}
						></div>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

HeaderExternal.propTypes = {
	articlesCount: PropTypes.number,
	collectionsCount: PropTypes.number,
	handleUpdateSearch: PropTypes.func,
	headerTheme: PropTypes.string,
	itemsCount: PropTypes.number,
	submitSearch: PropTypes.func.isRequired,
	title: PropTypes.string,
};

HeaderExternal.defaultProps = {
	title: null,
};

export default HeaderExternal;
