import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import _s from 'underscore.string';

import IconMoreHoriz from '@material-ui/icons/MoreHoriz';
import IconSearch from '@material-ui/icons/Search';
import IconArrowBack from '@material-ui/icons/ArrowBack';
import Tooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import OrpheusLogo from '../../common/OrpheusLogo';

import './HeaderExternal.css';

function expandSearch(history) {
	$(document.body).addClass('-showSearch');
	$('#header_external_search').focus();
	// history.push('/items');
}

function collapseSearch(e) {
	$(document.body).removeClass('-showSearch');
}

const HeaderExternal = (props) => {

	const {
		headerTheme,
		title,
		classes,
		history,
		handleUpdateSearch,
	} = props;

	const _classes = classes || [];

	_classes.push('headerExternal');

	if (headerTheme == "dark") {
		_classes.push('-dark');
	}

	return (
		<div>
			<div className={_classes.join(' ')}>
				<div className="headerExternalDefault">
					<Link className="headerExternalSite" to="/">
						<OrpheusLogo />
						<span>Orpheus</span>
					</Link>
					<ul className="headerExternalNav">
						{/*
						<li>
							<Link to="/explore">Explore</Link>
						</li>
						*/}
						{/*
						<li>
							<button className="headerExternalButton headerExternalButtonMore">
								<IconMoreHoriz />
								// Implement using https://material-ui.com/demos/tooltips/
							</button>
						</li>
						*/}
						<li className="headerExternalNavLinkHideOnMobile">
							<Link to="/login">Log in</Link>
						</li>
					</ul>
				</div>
				<div className="headerExternalSearch">
					<div className="headerExternalSearchContent">
						<button id="header_external_search_back" onClick={collapseSearch}>
							<IconArrowBack />
						</button>
						<input
							id="header_external_search"
							type="search"
							placeholder="Search"
							onChange={handleUpdateSearch}
						/>
					</div>
					<div className="headerExternalSearchBackground" onClick={collapseSearch}></div>
				</div>
			</div>
		</div>
	);
};

HeaderExternal.propTypes = {
	headerTheme: PropTypes.string,
	title: PropTypes.string,
	handleUpdateSearch: PropTypes.func,
};

HeaderExternal.defaultProps = {
	title: "Orpheus",
};

export default HeaderExternal;
