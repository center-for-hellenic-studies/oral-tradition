/**
 * @prettier
 */

import React from 'react';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';

// component
import Header, { Header as BareHeader } from './Header';

import configureStore from '../../../store/configureStore';

describe('Header', () => {
	it('renders correctly', () => {
		const wrapper = shallow(
			<Provider store={configureStore()}>
				<Header />
			</Provider>
		);
		expect(wrapper).toBeDefined();
	});

	describe('submitSearch()', () => {
		it('pushes to history if `pathname` does not contain "/dashboard/" or "/search/"', () => {
			const history = {
				location: {
					pathname: 'foo',
					search: '',
				},
				push: jest.fn(),
			};
			const wrapper = shallow(<BareHeader history={history} />);
			wrapper.instance().submitSearch();

			expect(history.push).toHaveBeenCalled();
		});

		it('replaces history if `pathname` contains "/search/"', () => {
			const history = {
				location: {
					pathname: '/search/',
					search: '',
				},
				replace: jest.fn(),
			};
			const wrapper = shallow(<BareHeader history={history} />);
			wrapper.instance().submitSearch();

			expect(history.replace).toHaveBeenCalled();
		});
	});
});
