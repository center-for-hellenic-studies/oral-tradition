import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'react-apollo';
import autoBind from 'react-autobind';
import qs from 'qs-lite';

// components
import HeaderExternal from '../HeaderExternal';
import HeaderInternal from '../HeaderInternal';

// api
import projectQuery from '../../../modules/archives/graphql/queries/detail';

// redux
import { toggleDriftHelp } from '../../../actions';

const shouldUseHeaderInternal = (userId, project) => {
	let isHeaderInternal = false;
	const internalPaths = ['/dashboard/', '/settings/', '/profile/'];

	if (!userId || !project) {
		// don't show internal header if not logged in
		return false;
	}

	if (internalPaths.indexOf(window.location.pathname) >= 0) {
		isHeaderInternal = true;
	}

	if (
		window.location.pathname.endsWith('/edit/')
		|| window.location.pathname.endsWith('/create/')
	) {
		isHeaderInternal = true;
	}

	// if user is logged in but unauthorized
	if (!project.users.some(projectUser => (projectUser.user && projectUser.user._id === userId))) {
		isHeaderInternal = false;
	}

	return isHeaderInternal;
};


class Header extends React.Component {

	constructor(props) {
		super(props);
		const query = qs.parse(window.location.search.replace('?', ''));
		let searchInputValue = '';

		if (query.search) {
			searchInputValue = query.search;
		}

		this.state = {
			searchInputValue,
		};
		autoBind(this);
	}

	submitSearch(event){
		const { history } = this.props;
		const { location } = history;
		const query = qs.parse(location.search.replace('?', ''));
		query.search = this.state.searchInputValue;
		let { pathname } = location;

		if (['/dashboard/', '/search/'].indexOf(pathname) < 0) {
			pathname = '/search/';

			history.push({
				search: qs.stringify(query),
				pathname,
			});
		} else {
			history.replace({
				search: qs.stringify(query),
				pathname,
			});
		}
	}

	handleUpdateSearch(e) {
		this.setState({
			searchInputValue: e.target.value,
		});
	}


	render() {
		const { userId, forceExternalHeader } = this.props;

		let project;
		if (this.props.projectQuery) {
			project = this.props.projectQuery.project;
		}

		return (
			<React.Fragment>
				{shouldUseHeaderInternal(userId, project) && !forceExternalHeader ?
					<HeaderInternal
						{...this.props}
						userId={userId}
						handleUpdateSearch={this.handleUpdateSearch}
						searchInputValue={this.state.searchInputValue}
						submitSearch={this.submitSearch}
					/>
					:
					<HeaderExternal
						{...this.props}
						title={project ? project.title : ''}
						userId={userId}
						handleUpdateSearch={this.handleUpdateSearch}
						searchInputValue={this.state.searchInputValue}
						submitSearch={this.submitSearch}
					/>
				}
			</React.Fragment>
		);
	}
}

Header.propTypes = {
	userId: PropTypes.string,
	headerTheme: PropTypes.string,
	title: PropTypes.string,
};

Header.defaultProps = {
	userId: null,
	title: null,
};

const mapStateToProps = state => ({
	userId: state.auth.userId,
});

const mapDispatchToProps = dispatch => ({
	startDriftInteraction: () => {
		dispatch(toggleDriftHelp(true));
	},
});

export default compose(
	connect(
		mapStateToProps,
		mapDispatchToProps,
	),
	projectQuery,
)(Header);

export { Header };
