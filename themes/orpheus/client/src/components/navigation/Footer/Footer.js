import React from 'react';
import PropTypes from 'prop-types';

import './Footer.css';

const Footer = ({ title, isArchive }) => {
	const now = new Date();
	const year = now.getFullYear();

	return (
		<section className="footer">
			<div className="sticky-reactnode-boundary"></div>
			<div className="footerCopyright">
        © {title} {year}
			</div>
			<div className="footerLinks">
				<ul>
					{!isArchive &&
						<li>
							Created by&nbsp;
							<a
								href="https://archimedes.digital"
								target="_blank"
								rel="noopener noreferrer"
							>
									Archimedes Digital
							</a>
						</li>
					}
					<li>
						<a href="/terms">
							Terms of use
						</a>
					</li>
					{/*
						<li>
						<a href="https://angel.co/archimedes-digital/jobs" target="_blank" rel="noopener noreferrer">
							Careers
						</a>
					</li>
					<li>
						<a href="https://blog.archimedes.digital/" target="_blank" rel="noopener noreferrer">
							Blog
						</a>
					</li>
					*/}
				</ul>
			</div>
		</section>
	);
}


Footer.propTypes = {
	title: PropTypes.string,
};

Footer.defaultProps = {
	title: 'Orpheus',
};

export default Footer;
