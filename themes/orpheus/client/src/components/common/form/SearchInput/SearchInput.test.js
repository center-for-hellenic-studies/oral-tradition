import React from 'react';
import { shallow } from 'enzyme';

// component
import SearchInput from './SearchInput';

describe('SearchInput', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<SearchInput />);
		expect(wrapper).toBeDefined();
	});
});
