import React from 'react';
import { shallow } from 'enzyme';

// component
import Dashboard from './Dashboard';

describe('Dashboard', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<Dashboard />);
		expect(wrapper).toBeDefined();
	});
});
