import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

import ItemEditorUploader from '../../../modules/dashboard/components/ItemEditorUploader';
import PageMeta from '../../../components/common/PageMeta';
import FacetedCards from '../FacetedCards';

import './Dashboard.css';

const Dashboard = (props) => {

	const {
		classes,
		empty,
		loading,
		uploading,
		uploadProgress,
		anonymous
	} = props;

	const _classes = classnames(classes, 'dashboard', {
		'-empty': empty,
		'-loading': loading,
		'-uploading': uploading
	});

	if (loading) {
		return (
			<div className={_classes}>
				<FacetedCards
					isAdmin={true}
					loading
				/>
			</div>
		);
	}

	return (
		<div className={_classes}>
			<PageMeta
				pageTitle={props.archiveTitle ? `Dashboard — ${props.archiveTitle}`: 'Dashboard'}
			/>
			{ uploading &&
				<div className="dashboardUploading">
					<Paper className="dashboardUploadingPaper" elevation={3}>
						<Typography variant="body1" component="p">
							Uploading files...
						</Typography>
						<LinearProgress
							variant="determinate"
							value={uploadProgress}
						/>
					</Paper>
				</div>
			}
			{(empty) ?
				<React.Fragment>
					<div className="dashboardEmpty">
						{!props.files || !props.files.length ?
							<React.Fragment>
								<div className="dashboardEmptyInstructions">
									<Typography variant="h1">
										Alright!
									</Typography>
                  <Typography variant="h3" style={{fontSize: '2rem', }}>
                    Now let's add some items.
                  </Typography>
                  <br />
									<Typography variant="body1" component="p" paragraph={true}>
										A little word on archives and items.
									</Typography>
									<Typography variant="body1" component="p" paragraph={true}>
										Your archive is made up of items. These items can be anything: archaeological artefacts, archival photographs, journal entries, public records—whatever composes your archive really!
									</Typography>
									<Typography variant="body1" component="p" paragraph={true}>
										<strong>Start your archive by <Link to="/items/create">adding the first item</Link>.</strong>
									</Typography>
								</div>
								<div className="dashboardEmptyQuote">
									<Typography variant="body1" component="p" className="dashboardEmptyQuoteText">
										“Let us save what remains: not by vaults and locks which fence them from the public eye and use in consigning them to the waste of time, but by such a multiplication of copies, as shall place them beyond the reach of accident.”
										<span>― Thomas Jefferson</span>
									</Typography>
									<Typography variant="caption" component="p" className="dashboardEmptyQuoteImageCredits">
										<a href="https://unsplash.com/photos/GWOTvo3qq7U" target="_blank" rel="noopener noreferrer">Image credits</a>
									</Typography>
									<div className="dashboardEmptyQuoteImage">
										<img src="/images/unsplash-GWOTvo3qq7U.jpg" width="1500" height="1500" />
									</div>
								</div>
							</React.Fragment>
							: ''}
					</div>
				</React.Fragment>
				:
				<div>
					<FacetedCards
						isAdmin={props.isAdmin}
						selectable
					/>
					{props.anonymous &&
						<div className="dashboardAnonymous">
							<div className="dashboardAnonymousContent">
								{props.anonymousSignedUp ? (
									<div>
										<p className="dashboardAnonymousContentParagraph">You're in! One last thing.</p>
										<Typography variant="caption" className="dashboardAnonymousContentCaption"> We've just sent you an email to [TODO: EMAIL GOES HERE]. Please click the link in the email to verify your email address. Then you're fully up and running 🚀</Typography>
									</div>
								) : (
									<div>
										<p className="dashboardAnonymousContentParagraph">Liking Orpheus? Don't lose your uploaded items.</p>
										<Typography variant="caption" className="dashboardAnonymousContentCaption">Sign up now to save them. It's free to join.</Typography>
									</div>
								)}
								<div>
									{props.anonymousSignedUp ? (
										<Button variant="contained" className="dashboardAnonymousContentButton">Got it</Button>
									) : (
										<Button variant="contained">Sign up for free</Button>
									)}
								</div>
							</div>
						</div>
					}
				</div>
			}
		</div>
	);
};

Dashboard.propTypes = {
	empty: PropTypes.bool,
	loading: PropTypes.bool,
	uploading: PropTypes.bool,
	anonymous: PropTypes.bool,
	isAdmin: PropTypes.bool,
	uploadProgress: PropTypes.number,
};

Dashboard.defaultProps = {
	uploadProgress: 0,
	isAdmin: false,
};

export default Dashboard;
