import React from 'react';
import { shallow } from 'enzyme';

// component
import TimelineFilter from './TimelineFilter';

describe('TimelineFilter', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<TimelineFilter />);
		expect(wrapper).toBeDefined();
	});
});
