import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import ProfileContainer from '../../../modules/users/containers/ProfileContainer';
import ProfileArchivesContainer from '../../../modules/users/containers/ProfileArchivesContainer';
import ArchiveEditorContainer from '../../../modules/archives/containers/ArchiveEditorContainer';
import ArchivePeopleContainer from '../../../modules/archives/containers/ArchivePeopleContainer';
import ArchivePlanContainer from '../../../modules/archives/containers/ArchivePlanContainer';
import PageMeta from '../PageMeta';


import './Settings.css';


// Styles from https://material-ui.com/demos/expansion-panels/ examples in SCSS instead of inline


const Settings = (props) => {

	const {
		classes,
	} = props;

	const _classes = classes || [];

	_classes.push('settings');

	return (
		<div className={_classes.join(' ')}>
			<PageMeta
				pageTitle="Settings"
			/>



			<ExpansionPanel>
				<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
					<p className="expandMoreHeading">Archive Settings</p>
					<label className="expandMoreSecondaryHeading">Archive name, teaser, URL, visibility</label>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>
					<ArchiveEditorContainer />
				</ExpansionPanelDetails>
			</ExpansionPanel>


      <ExpansionPanel>
				<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
					<p className="expandMoreHeading">My Archives</p>
					<label className="expandMoreSecondaryHeading">Create or edit archives</label>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>
					<ProfileArchivesContainer />
				</ExpansionPanelDetails>
			</ExpansionPanel>

      {/*
    	<ExpansionPanel>
				<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
					<p className="expandMoreHeading">Members</p>
					<label className="expandMoreSecondaryHeading">Invite and manage other people to help manage your archive</label>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>
					<ArchivePeopleContainer />
				</ExpansionPanelDetails>
			</ExpansionPanel>
      */}

			<ExpansionPanel>
				<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
					<p className="expandMoreHeading">Plan</p>
					<label className="expandMoreSecondaryHeading">Manage your archive's subscription</label>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>
					<ArchivePlanContainer />
				</ExpansionPanelDetails>
			</ExpansionPanel>
		</div>
	);
}

Settings.propTypes = {
};

export default Settings;
