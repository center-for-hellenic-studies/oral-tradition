import React from 'react';
import { shallow } from 'enzyme';

// component
import Cover from './Cover';

describe('Cover', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<Cover />);
		expect(wrapper).toBeDefined();
	});
});
