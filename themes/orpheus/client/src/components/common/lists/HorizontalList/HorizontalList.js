import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import $ from 'jquery';

import CollectionCard from '../../cards/CollectionCard';


import './HorizontalList.css';


const HorizontalList = (props) => {

	const _classes = [];
	_classes.push('horizontalList');

	if (props.loading) {
		_classes.push('-loading');
	}

	if (props.fullWidth) {
		_classes.push('-fullWidth');
	}

	if (props.className) {
		_classes.push(props.className);
	}

	if (!props.horizontalListItems.length && !props.loading) {
		return null;
	}

	return (
		<div className={_classes.join(' ')}>
			{!props.loading ?
				<Typography variant="overline" className="horizontalListTitle">
					{props.horizontalListItems.length} {props.title}
				</Typography>
				: ''}
			{/*
			<div className="horizontalListButton">
				<Button href="/horizontalListItems">
					View All
				</Button>
			</div>
			*/}
			<div className="horizontalListItems">
				{props.horizontalListItems.map(horizontalListItem => (
					<CollectionCard
						loading={props.loading}
						compact={props.compact}
						type={props.type}
						key={horizontalListItem._id}
						className="horizontalListItem"
						{...horizontalListItem}
					/>
				))}
			</div>
		</div>
	);
};

HorizontalList.propTypes = {
	horizontalListItems: PropTypes.array,
	loading: PropTypes.bool,
	fullWidth: PropTypes.bool,
	className: PropTypes.string,
	compact: PropTypes.bool,
	type: PropTypes.string,
};

HorizontalList.defaultProps = {
	horizontalListItems: [],
};

export default HorizontalList;
