import React from 'react';
// import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';

import HeaderExternal from '../../navigation/HeaderExternal';
import Footer from '../../navigation/Footer';
import Card from '../cards/Card';

import './Post.css';


const Post = (props) => {

	const {
	} = props;

	return (
		<div className="post">
			<div className="postHeader">
				<div className="postHeaderImage">
					<figure>
						<img src="https://source.unsplash.com/IO4mKHe3uLo/1600x900" alt="" width="1600" height="900" />
						<Typography variant="caption" component="figcaption">
							Image caption with <a href="https://unsplash.com/photos/_JgUBSKY3vk" target="_blank" rel="noopener noreferrer">credits</a>.
						</Typography>
					</figure>
				</div>
			</div>
			<div className="postContent">
				<div className="postContentHeader">
					<h1>Post Title</h1>
					<div className="itemContentHeaderMetadata">
						<ul>
							<li>
								<Typography variant="caption">
									Written by
								</Typography>
								<Typography variant="body2">
									Name Surname
								</Typography>
							</li>
							<li>
								<Typography variant="caption">
									Date
								</Typography>
								<Typography variant="body2">
									April 1, 2019
								</Typography>
							</li>
						</ul>
					</div>
				</div>
				<div className="postContentMain">
					<p>Post text ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.</p>
					<figure>
						<img src="https://source.unsplash.com/7zwSf_Lb93g/900x1300" alt="" width="900" height="1300" />
						<Typography variant="caption" component="figcaption">Image caption with <a href="https://unsplash.com/photos/7zwSf_Lb93g" target="_blank" rel="noopener noreferrer">credits</a>.</Typography>
					</figure>
					<p>Fermentum dui faucibus in ornare quam viverra. Quam nulla porttitor massa id neque aliquam. Dapibus ultrices in iaculis nunc sed augue lacus viverra vitae. Odio ut sem nulla pharetra diam sit. A iaculis at erat pellentesque. Suspendisse in est ante in nibh mauris cursus mattis molestie. Pharetra convallis posuere morbi leo urna molestie at elementum. Faucibus turpis in eu mi bibendum. Eget nunc lobortis mattis aliquam faucibus purus in massa tempor. Aliquam ut porttitor leo a diam sollicitudin tempor id eu. Interdum varius sit amet mattis vulputate enim nulla aliquet porttitor. Justo eget magna fermentum iaculis eu non diam phasellus. Velit aliquet sagittis id consectetur purus ut faucibus pulvinar elementum. Molestie a iaculis at erat pellentesque adipiscing commodo. Maecenas volutpat blandit aliquam etiam. Dui sapien eget mi proin sed libero enim sed. Ultricies leo integer malesuada nunc vel risus commodo viverra.</p>
					<figure>
						<img src="https://source.unsplash.com/0J6cTw0V2lE/1500x1000" alt="" width="1500" height="1000" />
						<Typography variant="caption" component="figcaption">Image caption with <a href="https://unsplash.com/photos/0J6cTw0V2lE" target="_blank" rel="noopener noreferrer">credits</a>.</Typography>
					</figure>
					<p>Fermentum dui faucibus in ornare quam viverra. Quam nulla porttitor massa id neque aliquam. Dapibus ultrices in iaculis nunc sed augue lacus viverra vitae. Odio ut sem nulla pharetra diam sit. A iaculis at erat pellentesque. Suspendisse in est ante in nibh mauris cursus mattis molestie. Pharetra convallis posuere morbi leo urna molestie at elementum. Faucibus turpis in eu mi bibendum. Eget nunc lobortis mattis aliquam faucibus purus in massa tempor. Aliquam ut porttitor leo a diam sollicitudin tempor id eu. Interdum varius sit amet mattis vulputate enim nulla aliquet porttitor. Justo eget magna fermentum iaculis eu non diam phasellus. Velit aliquet sagittis id consectetur purus ut faucibus pulvinar elementum. Molestie a iaculis at erat pellentesque adipiscing commodo. Maecenas volutpat blandit aliquam etiam. Dui sapien eget mi proin sed libero enim sed. Ultricies leo integer malesuada nunc vel risus commodo viverra.</p>
					<Card
						title = "Title of item"
						textShort = "Description text goes here and gets capped at a max number of characters. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis."
						assetImageSrc = "http://placeowl.com/1500"
						assetImageWidth = "2000"
						assetImageHeight = "2000"
						assetImageAlt = "Owl"
						to=""
						metadata = {[
							{ metaLabel: 'Author', metaValue: 'Name Surname' },
							{ metaLabel: 'Label lorem', metaValue: 'Linked attribute' },
							{ metaLabel: 'Label', metaValue: 'Attribute' },
							{ metaLabel: 'Dimensions', metaValue: '130 x 48 cm' },
							{ metaLabel: 'Culture', metaValue: 'Chinese' },
							{ metaLabel: 'Style', metaValue: 'Shang' },
							{ metaLabel: 'Label that is really long goes here', metaValue: 'Long text goes here, and goes on and on and on and on...' },
						]}
					/>
					<p>Fermentum dui faucibus in ornare quam viverra. Quam nulla porttitor massa id neque aliquam. Dapibus ultrices in iaculis nunc sed augue lacus viverra vitae. Odio ut sem nulla pharetra diam sit. A iaculis at erat pellentesque. Suspendisse in est ante in nibh mauris cursus mattis molestie. Pharetra convallis posuere morbi leo urna molestie at elementum. Faucibus turpis in eu mi bibendum. Eget nunc lobortis mattis aliquam faucibus purus in massa tempor. Aliquam ut porttitor leo a diam sollicitudin tempor id eu. Interdum varius sit amet mattis vulputate enim nulla aliquet porttitor. Justo eget magna fermentum iaculis eu non diam phasellus. Velit aliquet sagittis id consectetur purus ut faucibus pulvinar elementum. Molestie a iaculis at erat pellentesque adipiscing commodo. Maecenas volutpat blandit aliquam etiam. Dui sapien eget mi proin sed libero enim sed. Ultricies leo integer malesuada nunc vel risus commodo viverra.</p>
				</div>
			</div>
		</div>
	);
};

export default Post;
