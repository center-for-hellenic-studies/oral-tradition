/**
 * @prettier
 */

import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ProfileContainer from '../../../modules/users/containers/ProfileContainer';
import PageMeta from '../PageMeta';

import './Profile.css';

// Styles from https://material-ui.com/demos/expansion-panels/ examples in SCSS instead of inline

const Profile = props => {
	const { classes } = props;

	const _classes = classes || [];

	_classes.push('settings');

	return (
		<div className={_classes.join(' ')}>
			<PageMeta pageTitle="Profile" />
			{/*
			<ExpansionPanel>
				<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
					<p className="expandMoreHeading">Bookmarks</p>
					<label className="expandMoreSecondaryHeading">List of all items and annotations that you have saved</label>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>
					<Typography>
						No bookmarks found.
					</Typography>
				</ExpansionPanelDetails>
			</ExpansionPanel>
			<ExpansionPanel>
				<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
					<p className="expandMoreHeading">Annotations</p>
					<label className="expandMoreSecondaryHeading">List of all annotations that you have contributed</label>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>
					<Typography>
						No annotations found.
					</Typography>
				</ExpansionPanelDetails>
			</ExpansionPanel>
			*/}
			<ExpansionPanel>
				<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
					<p className="expandMoreHeading">Profile</p>
					<label className="expandMoreSecondaryHeading">
						Email, password, bio
					</label>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>
					<ProfileContainer />
				</ExpansionPanelDetails>
			</ExpansionPanel>
		</div>
	);
};

export default Profile;
