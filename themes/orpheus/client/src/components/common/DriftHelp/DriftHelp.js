import React from 'react';
import Drift from 'react-driftjs'
import autoBind from 'react-autobind';
import { connect } from 'react-redux';

import { toggleDriftHelp } from '../../../actions';


/* http://youmightnotneedjquery.com/#each */
const forEachElement = (selector, fn) => {
	const elements = document.querySelectorAll(selector);
	for (let i = 0; i < elements.length; i++)
		fn(elements[i], i);
}

class DriftHelp extends React.Component {

	constructor(props) {
		super(props);
		autoBind(this);
	}

	componentDidMount() {
  	if (window) {
  		// Start drift interaction
  		const drift = window.drift;
  		drift.on("ready", (api) => {
  			api.startInteraction({ interactionId: "760590", goToConversation: true }) /* <-- your playbook's interaction ID here */
				forEachElement('.widget-container', (el) => {
					el.addEventListener('click', this.handleClose);
				});
  		});
  	}
	}

	handleClose(e, a, b,) {
		// TODO: handle hiding widget
		this.props.toggleDriftHelp();
	}


	render() {
		return (
			<Drift
				id="drift-button"
				appId="sn3ixba6pnzr"
			/>
		);
	}

}

const mapStateToProps = state => ({
	helpVisible: state.help.visible,
});

const mapDispatchToProps = dispatch => ({
	toggleDriftHelp: () => {
		dispatch(toggleDriftHelp(false));
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(DriftHelp);
