import React from 'react';
import PropTypes from 'prop-types';
import IconClose from '@material-ui/icons/Close';
import $ from 'jquery';

import './Modal.css';

function closeModal(e) {
	$(document.body).removeClass('js-showing-modal');
}

function closeOnBackgroundClick(e) {
	if($(e.target).closest('.modalInner').length != 1) {
		$(document.body).removeClass('js-showing-modal');
	}
}

const Modal = (props) => {

	const {
		children,
		classes,
		show,
		loading,
	} = props;

	const _classes = classes || [];

	_classes.push('modal');

	if (loading) {
		_classes.push('-loading');
	}

	if (show) {
		document.body.classList.add('js-showing-modal');
		return (
			<div
				className={`modal ${classes.join(' ')}`}
				onClick={closeOnBackgroundClick}
			>
				<div
					className="modalClose"
					onClick={closeModal}
				>
					<IconClose />
				</div>
				<div className="modalInner">
					<div>
						{[children]}
					</div>
				</div>
			</div>
		);
	} else {
		document.body.classList.remove('js-showing-modal');
	}
	return null;
};

Modal.propTypes = {
	closeModal: PropTypes.func.isRequired,
	classes: PropTypes.arrayOf(PropTypes.string),
	children: PropTypes.element,
	show: PropTypes.bool,
};

Modal.defaultProps = {
	show: false,
	classes: [],
};

export default Modal;
