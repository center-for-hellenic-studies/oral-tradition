import React from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import Sticky from 'react-stickynode';

import Header from '../../navigation/Header';
import Footer from '../../navigation/Footer';
import List from '../lists/List';
import CustomTheme from '../../../lib/muiTheme';
import Filters from '../Filters';
import ItemListContainer from '../../../modules/items/containers/ItemListContainer';
import ArchiveFiltersContainer from '../../../modules/archives/containers/ArchiveFiltersContainer';

import './FacetedCards.css';


class FacetedCards extends React.Component {
	constructor(props) {
		super(props);
	}

	render () {
		const _classes = this.props.classes || [];

		_classes.push('facetedCards');

		if (this.props.loading) {
			_classes.push('-loading');
		}

		return (
			<div className={_classes.join(' ')}>
				<div className="facetedCardsContent">
					<Sticky
						className="facetedCardsContentFilters"
						activeClass="-sticky"
						bottomBoundary=".sticky-reactnode-boundary"
						enabled
					>
						<div className="facetedCardsContentFiltersInner">
							{this.props.loading ? (
								<ArchiveFiltersContainer loading />
							) : (
								<ArchiveFiltersContainer />
							)}
						</div>
					</Sticky>
					<div className="facetedCardsContentCards">
						{this.props.items ?
							<List
								loading={this.props.loading}
								selectable={this.props.selectable}
								items={this.props.items}
								isAdmin={this.props.isAdmin}
							/>
						:
							<ItemListContainer
								loading={this.props.loading}
								selectable={this.props.selectable}
								isAdmin={this.props.isAdmin}
							/>
						}
					</div>
				</div>
			</div>
		);
	}
}

FacetedCards.propTypes = {
	theme: PropTypes.string,
	selectable: PropTypes.bool,
	isAdmin: PropTypes.bool,

}

FacetedCards.defaultProps = {
	theme: '',
};


export default FacetedCards;
