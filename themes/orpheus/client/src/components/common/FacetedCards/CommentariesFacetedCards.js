import React from 'react';
import PropTypes from 'prop-types';
import Sticky from 'react-stickynode';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import Header from '../../navigation/Header';

import Footer from '../../navigation/Footer';
import List from '../lists/List';
import CustomTheme from '../../../lib/muiTheme';
import Filters from '../Filters';
import ItemListContainer from '../../../modules/commentaries/containers/ItemListContainer';
import ArchiveFiltersContainer from '../../../modules/commentaries/containers/CommentariesFiltersContainer';
import qs from 'qs-lite';

import { MuiThemeProvider } from '@material-ui/core';


class FacetedCards extends React.Component {
	constructor(props) {
		super(props);
	}

	render () {
		const query = qs.parse(window.location.search.replace('?', ''));

		const _classes = this.props.classes || [];

		_classes.push('facetedCards');

		if (this.props.loading) {
			_classes.push('-loading');
		}

		const commentaries = [{
			title: "A Homer Commentary in Progress",
			textShort: "Gregory Nagy, Leonard Muellner, Doug Frame, and 10 others",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit ame",
			type: "commentaries",
			slug: "homer",
			_id: "oq84gbl",
			itemsCount: "5,276 comments",
			files: [{
				name: "hector.jpg",
				type: "image/jpeg",
			}],
			originalLanguage: 'English',
			type: 'commentary',
			tags: ['Homer', 'Epics', 'In Progress'],
			authors: ['Gregory Nagy', 'Leonard Muellner']

		}, {
			title: "A Pindar Commentary in Progress",
			textShort: "Gregory Nagy and Maša Culumovic",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit ame",
			type: "commentaries",
			slug: "pindar",
			_id: "4t98ogu",
			itemsCount: "1,489 comments",
			files: [{
				name: "ajax_achilles_3.jpg",
				type: "image/jpeg",
			}],
			originalLanguage: 'English',
			type: 'commentary',
			tags: ['Pindar', 'In Progress'],
			authors: ['Gregory Nagy', 'Maša Culumovic']

		}, {
			title: "A Pausanias Commentary in Progress",
			textShort: "Gregory Nagy, Carolyn Higbie, Greta Hawes, and 2 others",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit ame",
			type: "commentaries",
			slug: "pausanias",
			_id: "38of00m",
			itemsCount: "255 comments",
			files: [{
				name: "iris.jpg",
				type: "image/jpeg",
			}],
			originalLanguage: 'English',
			type: 'commentary',
			tags: ['Pausanias', 'In Progress'],
			authors: ['Gregory Nagy', 'Carolyn Higbie', 'Greta Hawes']

		}, {
			title: "A Herodotus Commentary in Progress",
			textShort: "Gregory Nagy, Leonard Muellner, and Doug Frame",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit ame",
			type: "commentaries",
			slug: "herodotus",
			_id: "31di93a",
			itemsCount: "312 comments",
			files: [{
				name: "sirens.jpg",
				type: "image/jpeg",
			}],
			originalLanguage: 'English',
			type: 'commentary',
			tags: ['Pausanias', 'In Progress'],
			authors: ['Gregory Nagy', 'Leonard Muellner', 'Doug Frame']

		}, {
			title: "Example Commentary",
			textShort: "Gregory Nagy, Leonard Muellner, and Doug Frame",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit ame",
			type: "commentaries",
			slug: "herodotus",
			_id: "31di93a",
			itemsCount: "3,012 comments",
			files: [{
				name: "o_and_p.jpg",
				type: "image/jpeg",
			}],
			originalLanguage: 'Greek',
			type: 'commentary',
			tags: ['Example'],
			authors: ['Gregory Nagy', 'Leonard Muellner', 'Doug Frame']
		}, {
			title: "Example Commentary II",
			textShort: "Gregory Nagy, Leonard Muellner, and Doug Frame",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit ame",
			type: "commentaries",
			slug: "herodotus",
			_id: "31di93a",
			itemsCount: "312 comments",
			files: [{
				name: "odysseus.jpg",
				type: "image/jpeg",
			}],
			originalLanguage: 'Greek',
			type: 'commentary',
			tags: ['Example'],
			authors: ['Gregory Nagy', 'Leonard Muellner', 'Doug Frame']
		}];

		let filterLookup = {};
		filterLookup.authors =  {
			name: 'Authors',
			type: 'text',
			values: []
		}
		filterLookup.originalLanguage =  {
			name: 'Original Language',
			type: 'text',
			values: []
		}
		filterLookup.tags =  {
			name: 'Tags',
			type: 'text',
			values: []
		}

		let filteredItems = []
		commentaries.forEach( (commentary) => {
			let matchesFilters = true;
			if (!filterLookup.originalLanguage.values.includes(commentary.originalLanguage)) {
				filterLookup.originalLanguage.values.push(commentary.originalLanguage)
			}
			if (commentary.authors) {
				commentary.authors.forEach((author) => {
					if (!filterLookup.authors.values.includes(author)) {
						filterLookup.authors.values.push(author)
					}
				})
			}
			if (commentary.tags) {
				commentary.tags.forEach((tag) => {
					if (!filterLookup.tags.values.includes(tag)) {
						filterLookup.tags.values.push(tag)
					}
				})
			}

			if (query) {
				if (query['Original Language'] && commentary.originalLanguage !== query['Original Language']) {
					matchesFilters = false;
				}
				if (query['Authors'] ) {
					matchesFilters = false;
					commentary.authors.forEach((author) => {
						if (query['Authors'].split('+').includes(author)) {
							matchesFilters = true;
						}
					})
				}
				if (query['Tags'] ) {
					matchesFilters = false;
					commentary.tags.forEach((tag) => {
						if (query['Tags'].split('+').includes(tag)) {
							matchesFilters = true;
						}
					})
				}
			}
			if (matchesFilters) {
				filteredItems.push(commentary)
			}
		})

		let relevantFiltersLookup = {};
		relevantFiltersLookup.authors =  {
			name: 'Authors',
			type: 'text',
			values: []
		}
		relevantFiltersLookup.originalLanguage =  {
			name: 'Original Language',
			type: 'text',
			values: []
		}
		relevantFiltersLookup.tags =  {
			name: 'Tags',
			type: 'text',
			values: []
		}
		filteredItems.forEach( (commentary) => {
			if (!relevantFiltersLookup.originalLanguage.values.includes(commentary.originalLanguage)) {
				relevantFiltersLookup.originalLanguage.values.push(commentary.originalLanguage)
			}
			if (commentary.authors) {
				commentary.authors.forEach((author) => {
					if (!relevantFiltersLookup.authors.values.includes(author)) {
						relevantFiltersLookup.authors.values.push(author)
					}
				})
			}
			if (commentary.tags) {
				commentary.tags.forEach((tag) => {
					if (!relevantFiltersLookup.tags.values.includes(tag)) {
						relevantFiltersLookup.tags.values.push(tag)
					}
				})
			}

			
		})


		const filters = Object.keys(filterLookup).map((filter_name) => {
			return filterLookup[filter_name]
		})
		const relevantFilters = Object.keys(relevantFiltersLookup).map((filter_name) => {
			return relevantFiltersLookup[filter_name]
		})
		return (
			<div className={_classes.join(' ')}>
                <h5>Commentaries  <NavigateNextIcon style={{position: 'relative', top: '5px'}}/></h5>
                <h2>All Commentaries</h2>
				<div className="facetedCardsContent">
					<Sticky
						className="facetedCardsContentFilters"
						activeClass="-sticky"
						bottomBoundary=".sticky-reactnode-boundary"
						enabled
					>
						<div className="facetedCardsContentFiltersInner">
							{false && this.props.loading ? (
								<ArchiveFiltersContainer loading />
							) : (
								<ArchiveFiltersContainer filters={filters} relevantFilters={relevantFilters} />
							)}
						</div>
					</Sticky>
					<div className="facetedCardsContentCards">
						{this.props.items ?
							<List
								loading={this.props.loading}
								selectable={this.props.selectable}
								items={this.props.items}
								isAdmin={this.props.isAdmin}
							/>
						:
							<ItemListContainer
								loading={this.props.loading}
								selectable={this.props.selectable}
								isAdmin={this.props.isAdmin}
								items = {filteredItems}
							/>
						}
					</div>
				</div>
			</div>
		);
	}
}

FacetedCards.propTypes = {
	theme: PropTypes.string,
	selectable: PropTypes.bool,
	isAdmin: PropTypes.bool,

}

FacetedCards.defaultProps = {
	theme: '',
};


export default FacetedCards;
