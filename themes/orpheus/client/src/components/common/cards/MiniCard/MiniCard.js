/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import './MiniCard.css';

const MiniCard = props => {
	let itemUrl = `/${props.type}/${props._id}/${props.slug}`;

	if (props.subdomain) {
		itemUrl = `//${props.subdomain}.orphe.us${itemUrl}`;
	}

	let file;
	const _classes = [];
	_classes.push('miniCard');
	let imageUrl;
	// let mediaUrl;

	if (props.files && props.files.length) {
		file = props.files[0];
		const fileType = file.type || '';
		const isImage = fileType.slice(0, fileType.indexOf('/')) === 'image';

		if (isImage) {
			imageUrl = `//iiif.orphe.us/${file.name}/full/full/0/default.jpg`;
		} else {
			// TODO: handle media
			// mediaUrl = `https://s3.amazonaws.com/iiif-orpheus/${file.name}`;
		}
	}

	if (props.loading) {
		_classes.push('-loading');
	}

	if (props.compact) {
		_classes.push('-compact');
	}

	if (props.className) {
		_classes.push(props.className);
	}

	const content = (
		<React.Fragment>
			<div
				className="miniCardBackground"
				style={{
					backgroundImage: `url(${imageUrl})`,
				}}
			/>
			<div className="miniCardBackgroundGradient" />
			<div className="miniCardText">
				{props.title && <label>{props.title}</label>}
				{props.author && <span className="caption">{props.author}</span>}
			</div>
		</React.Fragment>
	);

	return (
		<Paper elevation={3} className={_classes.join(' ')}>
			{props.subdomain ? (
				<a href={itemUrl}>{content}</a>
			) : (
				<Link to={itemUrl}>{content}</Link>
			)}
		</Paper>
	);
};

MiniCard.propTypes = {
	_id: PropTypes.string,
	author: PropTypes.string,
	className: PropTypes.string,
	compact: PropTypes.bool,
	description: PropTypes.string,
	files: PropTypes.array,
	loading: PropTypes.bool,
	slug: PropTypes.string,
	subdomain: PropTypes.string,
	title: PropTypes.string,
	type: PropTypes.string,
};

MiniCard.defaultProps = {
	type: 'items',
};

export default MiniCard;
