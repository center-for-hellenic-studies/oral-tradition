import React from 'react';
import { shallow } from 'enzyme';

// component
import MediaViewer from './MediaViewer';


describe('MediaViewer', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<MediaViewer files={[{}]} />);
		expect(wrapper).toBeDefined();
	});
});
