import React from 'react';
import { shallow } from 'enzyme';

// component
import SelectedFilters from './SelectedFilters';

describe('SelectedFilters', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<SelectedFilters />);
		expect(wrapper).toBeDefined();
	});
});
