import React from 'react';
import { shallow } from 'enzyme';

// component
import Item from './Item';

describe('Item', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<Item to="/"/>);
		expect(wrapper).toBeDefined();
	});
});
