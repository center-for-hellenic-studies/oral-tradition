import React from 'react';
import { shallow } from 'enzyme';

// component
import Collection from './Collection';

describe('Collection', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<Collection />);
		expect(wrapper).toBeDefined();
	});
});
