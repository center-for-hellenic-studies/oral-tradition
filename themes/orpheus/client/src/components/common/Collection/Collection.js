import React from 'react';
import PropTypes from 'prop-types';

import Cover from '../Cover';
import FacetedCards from '../FacetedCards';
import PageMeta from '../PageMeta';


import './Collection.css';


const Collection = (props) => {

	const _classes = props.classes || [];

	_classes.push('collection');

	if (props.loading) {
		_classes.push('-loading');
	}

	return (
		<div className={_classes.join(' ')}>
			{props.title ?
				<PageMeta
					pageTitle={props.title}
					titleAppendix={`${props.archiveTitle} — Orpheus`}
				/>
				:
				<PageMeta
					pageTitle={`Collection in ${props.archiveTitle}`}
				/>
			}
			<Cover
				className="collectionHeader"
				title={props.title}
				lead={props.description}
				imageUrl={props.coverImage}
			/>
			<FacetedCards
				className="collectionContent"
				items={props.items}
			/>
		</div>
	);
}

Collection.propTypes = {
	classes: PropTypes.array,
	items: PropTypes.array,
	loading: PropTypes.bool,
};

Collection.defaultProps = {
	items: [],
};

export default Collection;
