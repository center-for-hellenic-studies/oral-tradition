/**
 * @prettier
 */
import React from 'react';
import { Provider } from 'react-redux';
import { mount, shallow } from 'enzyme';

import EmbedBlock from './EmbedBlock';
import configureStore from '../../../../../store/configureStore';

describe('EmbedBlock', () => {
  it('renders', () => {
    const wrapper = shallow(
      <Provider store={configureStore()}>
        <EmbedBlock block={{ getKey: noop }} url="url.com" />
      </Provider>
    );

    expect(wrapper).toBeDefined();
  });

  describe('renderRemoveButton()', () => {
    it('renders if the user is logged in and on the edit page', () => {
      window.history.pushState({}, 'renderRemoveButton()', '/edit/');

      const wrapper = mount(
        <Provider store={configureStore({ auth: { userId: 'userId' } })}>
          <EmbedBlock block={{ getKey: noop }} url="url.com" />
        </Provider>
      );

      expect(wrapper.find('.embedBlockRemove')).toHaveLength(1);
    });

    it('renders if the user is logged in and on the create page', () => {
      window.history.pushState({}, 'renderRemoveButton()', '/create/');

      const wrapper = mount(
        <Provider store={configureStore({ auth: { userId: 'userId' } })}>
          <EmbedBlock block={{ getKey: noop }} url="url.com" />
        </Provider>
      );

      expect(wrapper.find('.embedBlockRemove')).toHaveLength(1);
    });

    it('does not render if the user is not logged in', () => {
      window.history.pushState({}, 'renderRemoveButton()', '/edit/');

      const wrapper = mount(
        <Provider store={configureStore()}>
          <EmbedBlock block={{ getKey: noop }} url="url.com" />
        </Provider>
      );

      expect(wrapper.find('.embedBlockRemove')).toHaveLength(0);
    });

    it('does not render if not on the item edit or create page', () => {
      window.history.pushState({}, 'renderRemoveButton()', '/not-edit/');

      const wrapper = mount(
        <Provider store={configureStore({ auth: { userId: 'userId' } })}>
          <EmbedBlock block={{ getKey: noop }} url="url.com" />
        </Provider>
      );

      expect(wrapper.find('.embedBlockRemove')).toHaveLength(0);
    });
  });
});
