import React from 'react';
import { shallow } from 'enzyme';

import ImageBlock from './ImageBlock';

describe('ImageBlock', () => {
	it('renders correctly', () => {
		const wrapper = shallow(
			<ImageBlock />
		);
		expect(wrapper).toBeDefined();
	});
});
