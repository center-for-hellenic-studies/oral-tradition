import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

const issueDetailQuery = graphql(gql`
  query issueDetailQuery($name: String){
    issue(name: $name){
			id
			name
			title
      articles {
        id
        title
        content
        pdf {
          id
          fileURI
        }
				authors {
					name
				}
        files {
          id
          fileURI
        }
      }
      customFields {
        id
        issue_id
        name
        value
      }
    }
  }
`, {
	options: (props) => {
		let name;
		if (props.match && props.match.params) {
			name = props.match.params.name;
		}
		return {
			variables: {
				name,
			}
		};
	},
	name: 'issueDetailQuery',
	props: props => ({
		issue: props.issueDetailQuery.issue,
		loading: props.issueDetailQuery.loading,
	}),
});


export default issueDetailQuery;
