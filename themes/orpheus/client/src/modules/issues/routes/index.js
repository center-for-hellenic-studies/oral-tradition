/**
 * @prettier
 */

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import PostView from '../components/PostView';

const Routes = () => (
	<Route path={`${match.url}/:slug`} component={PostView} />
);

export default Routes; 
