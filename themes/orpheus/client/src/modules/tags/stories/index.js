import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';

// components
import SearchTags from '../components/SearchTags';
import Tag from '../components/Tag';
import Tags from '../components/Tags';

import { mockTags, mockTag } from '../mocks';

storiesOf('Tags', module)
	.addDecorator(StoryRouter())
	.add('SearchTags', () => (
		<SearchTags tags={mockTags()}/>
	))
	.add('Tag', () => (
		<Tag tag={mockTag()} />
	))
	.add('Tags', () => (
		<Tags tags={mockTags()} />
	));
