import faker from 'faker';
import mockList from '../../../mocks/list';

const tag = () => faker.lorem.word();
const tags = (rangeLow, rangeHigh) => (mockList(tag, rangeLow, rangeHigh));


export default tag;
export {
	tag as mockTag, tags as mockTags,
};
