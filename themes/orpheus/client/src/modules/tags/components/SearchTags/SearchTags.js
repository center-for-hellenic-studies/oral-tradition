import React from 'react';
import { Link } from 'react-router-dom';
import qs from 'qs-lite';


import './SearchTags.css';


const SearchTags = ({ tags }) => (
	<div className="searchTags">
		{tags.map(tag => {
			let tagParams = [];
			const query = qs.parse(window.location.search.replace('?', ''));
			query.page = 1;

			if (query.tags) {
				tagParams = query.tags.split(',');
			}

			const isActive = tagParams.indexOf(tag) >= 0;

			if (isActive) {
				tagParams.splice(tagParams.indexOf(tag), 1);
			} else {
				tagParams.push(tag);
			}

			if (tagParams.length) {
				query.tags = tagParams.join(',');
			} else {
				delete query.tags;
			}

			return (
				<Link
					className={`
            tag searchTag
            ${isActive ? 'searchTagActive' : ''}
          `}
					key={tag}
					to={{
      			pathname: '/search',
						query,
      		}}
      	>
					{tag}
				</Link>
			);
		})}

	</div>
);


export default SearchTags;
