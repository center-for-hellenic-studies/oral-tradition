import React from 'react';
import { shallow } from 'enzyme';

import SearchTags from './SearchTags';

describe('SearchTags', () => {
	it('renders', () => {
		const wrapper = shallow(<SearchTags tags={[]} />);

		expect(wrapper).toBeDefined();
	});
});
