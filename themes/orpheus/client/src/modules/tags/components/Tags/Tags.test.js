import React from 'react';
import { shallow } from 'enzyme';

import Tags from './Tags';

describe('Tags', () => {
	it('renders', () => {
		const wrapper = shallow(<Tags />);

		expect(wrapper).toBeDefined();
	});
});
