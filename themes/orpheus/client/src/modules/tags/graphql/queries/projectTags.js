import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';


const query = gql`
	query projectQuery($hostname: String) {
		project(hostname: $hostname) {
	    _id
      tags
		}
	}
`;

const projectQuery = graphql(query, {
	name: 'projectQuery',
	options: ({ params }) => ({
		variables: {
			hostname: getCurrentArchiveHostname(),
		}
	}),
});

export default projectQuery;
