/*
 * action types
 */
export const TOGGLE_ITEM_SELECTED = 'TOGGLE_ITEM_SELECTED';
export const UNSELECT_ALL = 'UNSELECT_ALL';


/*
 * action creators
 */
export const toggleItemSelected = (itemId) => ({
	type: TOGGLE_ITEM_SELECTED,
	itemId,
});

export const unselectAll = () => ({
	type: UNSELECT_ALL,
});
