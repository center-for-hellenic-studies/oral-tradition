import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import InfiniteScroll from 'react-infinite-scroller';
import qs from 'qs-lite';
import { compose } from 'react-apollo';
import ReactTimeout from 'react-timeout'
import Button from '@material-ui/core/Button';

import CardLoadingItem from '../../../../components/common/cards/CardLoadingItem';
import List from '../../../../components/common/lists/List';
import itemListQuery from '../../graphql/queries/list';

import './ItemListContainer.css';


class ItemListContainer extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);

		this.state = {
			isThrottled: false,
		};

	}

	clearFilters() {
		this.props.history.push(window.location.pathname);
	}

	async handleLoadMore(hasMoreToLoad) {
		const { isThrottled } = this.state;

		if (!hasMoreToLoad) {
			return false;
		}

		if (!isThrottled) {
			await this.setState({
				isThrottled: true,
			});

			const query = qs.parse(window.location.search.replace('?', ''));

			if (query.page) {
				query.page = parseInt(query.page, 10) + 1;
			} else {
				query.page = 2;
			}

			await this.props.history.push({
				search: qs.stringify(query),
			});

			this.props.setTimeout(() => {
				this.setState({
					isThrottled: false,
				})
			}, 1000);
		}
	}

	render() {
		let items = [];
		let itemsCount = 0;
		let hasMoreToLoad = true;
		if (
			this.props.itemListQuery
			&& this.props.itemListQuery.project
			&& this.props.itemListQuery.project.items
			&& this.props.itemListQuery.project.items.length
		) {
			items = this.props.itemListQuery.project.items;
			itemsCount = this.props.itemListQuery.project.itemsCount;
		}
		hasMoreToLoad = !(itemsCount <= items.length);

		const selectable = this.props.match.path === '/dashboard';

		if (items.length == 0 && (this.props.itemListQuery.loading || this.props.forceLoading)) {
			return <List loading />
		}

		if (items.length > 0 && (this.props.itemListQuery.loading || this.props.forceLoading)) {
			return (
				<React.Fragment>
					<List
						items={items}
  					handleSelect={this.handleSelect}
	  				selectable={selectable}
					/>
					<div className="listItem" key="x"><CardLoadingItem /></div>
				</React.Fragment>
			);
		}

		if (!items || !items.length) {
			return (
				<div className="itemListContainer -no-results">
					<p>
						No items found with matching filters.
					</p>
					<Button
						onClick={this.clearFilters}
						size="large"
						variant="outlined"
					>
						Clear filters
					</Button>
				</div>
			);
		}

		return (
			<React.Fragment>
				<InfiniteScroll
					pageStart={0}
					loadMore={this.handleLoadMore.bind(this, hasMoreToLoad)}
					hasMore={hasMoreToLoad}
					loader={<div className="listItem" key="x"><CardLoadingItem /></div>}
				>
					<List
						items={items}
						handleSelect={this.handleSelect}
						isAdmin={this.props.isAdmin}
						selectable={selectable}
					/>
				</InfiniteScroll>
			</React.Fragment>
		);
	}
}

ItemListContainer.propTypes = {
	itemListQuery: PropTypes.object,
	selectable: PropTypes.bool,
	handleSelect: PropTypes.func,
	forceLoading: PropTypes.bool,
	isAdmin: PropTypes.bool,

};

export default compose(
	itemListQuery,
	ReactTimeout,
)(ItemListContainer);
