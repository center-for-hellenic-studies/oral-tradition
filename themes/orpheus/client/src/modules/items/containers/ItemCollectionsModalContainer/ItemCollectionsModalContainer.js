/**
 * @prettier
 */

import React from 'react';
import { compose } from 'react-apollo';
import autoBind from 'react-autobind';
import { withSnackbar } from 'notistack';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Checkbox from '@material-ui/core//Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';

import CollectionCreator from '../../../collections/components/CollectionCreator';
import collectionCreateMutation from '../../../collections/graphql/mutations/create';

// graphql
import itemListQuery from '../../graphql/queries/list';
import itemUpdateMutation from '../../graphql/mutations/update';
import projectCollectionsQuery from '../../graphql/queries/projectCollectionsQuery';

function onlyUnique(value, index, self) {
	return self.indexOf(value) === index;
}

class ItemCollectionsModalContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			open: false,
			collections: [],
			selectedCollections: [],
			submitting: false,
		};
		autoBind(this);
	}

	handleNewCollection(title) {
		const { collectionCreate } = this.props;
		collectionCreate({ title })
			.then(response => {
				let collections = this.state.collections;
				collections.push({ _id: response.data.collectionCreate._id, title });
				this.setState({ collections });
			})
			.catch(err => {
				console.error(err);
			});
	}

	componentDidMount() {
		if (
			this.props.projectCollectionsQuery &&
			this.props.projectCollectionsQuery.project &&
			this.props.projectCollectionsQuery.collections
		) {
			this.setState({
				collections: this.props.projectCollectionsQuery.project.collections,
			});
		}
	}

	componentWillReceiveProps(nextProps) {
		if (
			(!this.state.collections || !this.state.collections.length) &&
			nextProps.projectCollectionsQuery &&
			nextProps.projectCollectionsQuery.project &&
			nextProps.projectCollectionsQuery.project.collections
		) {
			this.setState({
				collections: nextProps.projectCollectionsQuery.project.collections,
			});
		}
	}

	handleCheckboxChange(event, checked) {
		let selectedCollections = this.state.selectedCollections;
		if (checked) {
			selectedCollections.push(event.target.value);
		} else {
			selectedCollections = selectedCollections.filter(
				id => id !== event.target.value
			);
		}
		this.setState({ selectedCollections });
	}

	closeModal() {
		this.setState({ selectedCollections: [] });
		this.props.handleClose();
	}

	addItemsToCollections() {
		this.setState({ submitting: true });
		const { itemUpdate } = this.props;
		const project = this.props.itemListQuery.project;

		let itemUpdates = this.props.selectedItemIds.map(item_id => {
			let item = project.items.filter(item => item._id === item_id)[0];
			let collectionIds = item.collectionId
				.concat(this.state.selectedCollections)
				.filter(onlyUnique);

			return itemUpdate({
				_id: item_id,
				collectionId: collectionIds,
				projectId: project._id,
			});
		});

		Promise.all(itemUpdates)
			.then(() => {
				this.props.handleClose();
				this.props.enqueueSnackbar('Successfully added!', {
					variant: 'success',
					autoHideDuration: 5000,
				});
			})
			.catch(err => {
				console.log('promis errors: ', err);
				this.props.enqueueSnackbar('Error adding items, please try again', {
					variant: 'error',
					autoHideDuration: 5000,
				});
			});
	}

	render() {
		let collectionsInputs = this.state.collections.map((collection, i) => {
			return (
				<div key={`collection_${i}`}>
					<FormControlLabel
						control={
							<Checkbox
								name={`collectionId[${i}]`}
								label={collection.title}
								value={collection._id}
								onChange={this.handleCheckboxChange}
							/>
						}
						label={collection.title}
					/>
				</div>
			);
		});

		return (
			<Dialog open={this.props.open} onClose={this.closeModal}>
				<DialogTitle
					id="customized-dialog-title"
					onClose={this.props.handleClose}
				>
					Add to Collection
				</DialogTitle>
				<DialogContent>
					<CollectionCreator handleNewCollection={this.handleNewCollection} />
					{collectionsInputs}
				</DialogContent>
				<DialogActions>
					<Button onClick={this.closeModal}>Cancel</Button>
					<Button
						onClick={this.addItemsToCollections}
						color="primary"
						variant="contained"
						disabled={this.state.submitting}
					>
						Save
					</Button>
				</DialogActions>
			</Dialog>
		);
	}
}

export default withSnackbar(
	compose(
		itemListQuery,
		itemUpdateMutation,
		projectCollectionsQuery,
		collectionCreateMutation
	)(ItemCollectionsModalContainer)
);
