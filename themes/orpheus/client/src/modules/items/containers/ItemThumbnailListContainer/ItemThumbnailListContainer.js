import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import InfiniteScroll from 'react-infinite-scroller';
import qs from 'qs-lite';
import { compose } from 'react-apollo';
import ReactTimeout from 'react-timeout'
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import ImageIcon from '@material-ui/icons/Image';

import CardLoadingItem from '../../../../components/common/cards/CardLoadingItem';
import List from '../../../../components/common/lists/List';
import itemListQuery from '../../graphql/queries/list';

import './ItemThumbnailListContainer.css';


class ItemThumbnailListContainer extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);

		this.state = {
			isThrottled: false,
		};

	}

	clearFilters() {
		this.props.history.push(window.location.pathname);
	}

	async handleLoadMore(hasMoreToLoad) {
		const { isThrottled } = this.state;

		if (!hasMoreToLoad) {
			return false;
		}

		if (!isThrottled) {
			await this.setState({
				isThrottled: true,
			});

			const query = qs.parse(window.location.search.replace('?', ''));

			if (query.page) {
				query.page = parseInt(query.page, 10) + 1;
			} else {
				query.page = 2;
			}

			await this.props.history.push({
				search: qs.stringify(query),
			});

			this.props.setTimeout(() => {
				this.setState({
					isThrottled: false,
				})
			}, 1000);
		}
	}

	render() {
		let items = [];
		let itemsCount = 0;
		let hasMoreToLoad = true;
		if (
			this.props.itemListQuery
			&& this.props.itemListQuery.project
			&& this.props.itemListQuery.project.items
			&& this.props.itemListQuery.project.items.length
		) {
			items = this.props.itemListQuery.project.items;
			itemsCount = this.props.itemListQuery.project.itemsCount;
		}
		hasMoreToLoad = !(itemsCount <= items.length);

		const selectable = this.props.match.path === '/dashboard';

		if (items.length == 0 && (this.props.itemListQuery.loading || this.props.forceLoading)) {
			return null
		}

		if (!items || !items.length) {
			return '';
		}

		let files = []
		items.forEach((i) => {
			i.files.forEach((f) => {
				if (f.type.indexOf('image') === 0) {
					f.item_id = i._id;
					f.title = i.title;
					f.slug = i.slug;
					files.push(f);
				}
			})
		})

		// if there are no files on any items, don't display the block until we handle items without images
		if (!files.length) {
			return null;
		}

		return (
			<React.Fragment>
				<Typography variant="h2" className="archiveSectionTitle">
					Items
				</Typography>
				<div className='ItemThumbnailListContainer'>
					<div className='list-info'>
						<Typography variant="overline" className='itemsCountTitle'>
							{itemsCount} Item{itemsCount > 1 ? 's': ''}
						</Typography>
						<Button color="primary" href='/search' className="viewAllButton">
			        View all
			      </Button>
					</div>
					<div className='image-wrapper'>
						{files.map((file, i) => (
							<Link to={`/items/${file.item_id}/${file.slug}/`}  key={`${file.item_id}-${i}`} alt={file.title} title={file.title}>
								<img src={`//iiif.orphe.us/${file.name}/full/,200/0/default.jpg`} alt={file.title}/>
							</Link>
						))}
					</div>
				</div>
			</React.Fragment>
		);
	}
}

ItemThumbnailListContainer.propTypes = {
	itemListQuery: PropTypes.object,
	selectable: PropTypes.bool,
	handleSelect: PropTypes.func,
	forceLoading: PropTypes.bool,
	isAdmin: PropTypes.bool,

};

export default compose(
	itemListQuery,
	ReactTimeout,
)(ItemThumbnailListContainer);
