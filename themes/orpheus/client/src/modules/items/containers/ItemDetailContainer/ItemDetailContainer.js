/**
 * @prettier
 */

import React from 'react';
import { compose } from 'react-apollo';
import autoBind from 'react-autobind';

import NotFound from '../../../../modules/main/components/NotFound';

// components
import Item from '../../../../components/common/Item';

// graphql
import itemListQuery from '../../graphql/queries/list';
import itemDetailQuery from '../../graphql/queries/detail';
import itemRemoveMutation from '../../graphql/mutations/remove';

class ItemDetailContainer extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	handleRemove(itemId) {
		const { itemRemove, history } = this.props;

		itemRemove(itemId)
			.then(response => {
				history.replace('/items');
			})
			.catch(err => {
				console.error(err);
			});
	}

	render() {
		let item = [];
		let userIsAdmin = false;
		let archiveTitle = '';
		let loading = false;

		if (this.props.itemQuery) {
			if (this.props.itemQuery.loading) {
				loading = this.props.itemQuery.loading;
			}

			if (this.props.itemQuery.project) {
				item = this.props.itemQuery.project.item;
				archiveTitle = this.props.itemQuery.project.title;
				userIsAdmin = this.props.itemQuery.project.userIsAdmin;
				if (!item) {
					return <NotFound />;
				}
			}
		}

		return (
			<Item
				{...item}
				loadingMedia={loading}
				loadingText={loading}
				archiveTitle={archiveTitle}
				userIsAdmin={userIsAdmin}
				handleRemove={this.handleRemove}
			/>
		);
	}
}

export default compose(
	itemListQuery,
	itemDetailQuery,
	itemRemoveMutation
)(ItemDetailContainer);
