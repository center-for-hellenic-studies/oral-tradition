/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import { arrayMove } from 'react-sortable-hoc';
import { compose } from 'react-apollo';
import { convertToRaw } from 'draft-js';
import { withSnackbar } from 'notistack';
import { Prompt } from 'react-router';
import CircularProgress from '@material-ui/core/CircularProgress';

import ItemEditor from '../../components/ItemEditor';
import ItemLoadingPlaceholder from '../../components/ItemLoadingPlaceholder';
import itemListQuery from '../../graphql/queries/list';
import itemDetailQuery from '../../graphql/queries/detail';
import itemCreateMutation from '../../graphql/mutations/create';
import itemUpdateMutation from '../../graphql/mutations/update';
import itemRemoveMutation from '../../graphql/mutations/remove';
import collectionCreateMutation from '../../../collections/graphql/mutations/create';

import renderWhileLoading from '../../../../lib/renderWhileLoading';
import store from '../../../../store';

const removeTypename = o => {
	delete o.__typename;

	return o;
};

class ItemEditorContainer extends React.Component {
	static propTypes = {
		collectionCreate: PropTypes.func,
		history: PropTypes.object,
		itemCreate: PropTypes.func,
		itemQuery: PropTypes.shape({
			project: PropTypes.shape({
				collections: PropTypes.array,
				item: PropTypes.shape({
					description: PropTypes.string,
					files: PropTypes.array,
				}),
				tags: PropTypes.array,
				title: PropTypes.string,
			}),
		}),
		itemUpdate: PropTypes.func,
	};

	constructor(props) {
		super(props);
		autoBind(this);

		this.state = {
			files: [],
			newCollections: [],
			isUploadingFiles: false,
			isSubmitting: false,
			unsavedEdits: false,
		};
	}

	componentDidMount() {
		if (
			(!this.state.files || !this.state.files.length) &&
			this.props.itemQuery &&
			this.props.itemQuery.project &&
			this.props.itemQuery.project.item &&
			this.props.itemQuery.project.item.files
		) {
			this.setState({
				files: this.props.itemQuery.project.item.files,
			});
		}
	}

	reportUnsavedEdits() {
		this.setState({ unsavedEdits: true });
	}

	handleNewCollection(title) {
		const { collectionCreate } = this.props;
		collectionCreate({ title })
			.then(response => {
				let newCollections = this.state.newCollections;
				newCollections.push({ _id: response.data.collectionCreate._id, title });
				this.setState({ newCollections });
				// history.replace('/collections/');
			})
			.catch(err => {
				console.error(err);
			});
	}

	handleCancel() {
		this.props.history.goBack();
	}

	handleSubmit(_values) {
		this.setState({ unsavedEdits: false, isSubmitting: true }, () => {
			const { itemCreate, itemUpdate, history } = this.props;
			const _files = this.state.files;
			const tags = _values.tags ? _values.tags.slice() : [];
			const values = Object.assign({}, _values);
			if (!_values.collectionId) {
				values.collectionId = [];
			}

			// remove non-input values
			delete values.__typename;
			delete values.comments;
			delete values.commentsCount;
			delete values.files;
			delete values.manifest;

			const { editorState } = store.getState().editor;
			values.description = JSON.stringify(
				convertToRaw(editorState.getCurrentContent())
			);
			values.private = values.privacy === 'private';

			delete values.privacy;

			// consolidate tag label/values
			values.tags = [];
			tags.forEach(tag => {
				values.tags.push(tag.value);
			});

			// sanitize metadata
			const metadata =
				values.metadata.map(removeTypename).map((mm) => {
					if (mm.type === 'media' || mm.type === 'item') {
						let _mm = Object.assign({}, mm);
						_mm.value = JSON.stringify(mm.value);
						return _mm;
					}

					return mm;
				}) ||
				[];
			// const relatedItems = values.relatedItems.map(removeTypename);

			// set metadata
			values.metadata = metadata.filter((md) => md);

			delete values.relatedItems;

			let isUploadingFiles = false;
			// sanitize files
			const files = [];
			_files.forEach(_file => {
				if (!_file.path) {
					isUploadingFiles = true;
				}
				if (_file && _file.annotations) {
					_file.annotations = _file.annotations.map(d => {
						return {
							x: d.x,
							y: d.y,
							order: d.order,
							content: d.content,
						};
					});
				}

				const file = Object.assign({}, _file);
				delete file.__typename;
				files.push(file);
			});

			if (
				values.metadata.length == 1
				&& values.metadata[0]
				&& Object.keys(values.metadata[0]).length == 0
			) {
				// bugfix to prevent empty metadata field from erroring itemCreate
				values.metadata = [];
			}

			// // guarantee default type on metadata
			metadata.forEach(datum => {
				if (datum && !datum.type || datum && typeof datum.type === 'undefined') {
					datum.type = 'text';
				}
			});

			if (isUploadingFiles) {
				this.props.enqueueSnackbar('Please wait until all files are uploaded', {
					variant: 'error',
					autoHideDuration: 5000,
				});
			} else {
				// create or update
				let message = (<div><CircularProgress /> Saving ...</div>)
				const savingSnackbarKey = this.props.enqueueSnackbar( 'Saving...', {
					variant: 'default',
				});

				if ('_id' in values) {
					itemUpdate(values, files)
						.then(response => {
							this.props.closeSnackbar(savingSnackbarKey);
							this.props.enqueueSnackbar('Saved', {
								variant: 'success',
								autoHideDuration: 5000,
							});
							this.setState({isSubmitting: false});
							history.replace('/dashboard/');
						})
						.catch(err => {
							this.props.closeSnackbar(savingSnackbarKey);
							this.props.enqueueSnackbar('Error saving', {
								variant: 'error',
								autoHideDuration: 5000,
							});
							this.setState({isSubmitting: false});

							console.error(err);
						});
				} else {
					itemCreate(values, files)
						.then(response => {
							this.props.closeSnackbar(savingSnackbarKey);
							this.props.enqueueSnackbar('Saved', {
								variant: 'default',
								autoHideDuration: 5000,
							});
							this.setState({isSubmitting: false});
							history.replace('/dashboard/');
						})
						.catch(err => {
							this.props.closeSnackbar(savingSnackbarKey);
							this.props.enqueueSnackbar('Error saving', {
								variant: 'error',
								autoHideDuration: 5000,
							});
							this.setState({isSubmitting: false});
							console.error(err);
						});
				}
			}
		});
	}

	handleRemove(itemId) {
		const { itemRemove, history } = this.props;

		itemRemove(itemId)
			.then(response => {
				history.replace('/dashboard/');
			})
			.catch(err => {
				console.error(err);
			});
	}

	handleAddFiles(files) {
		if (this.state.uploadingSnackbarKey) {
			this.props.closeSnackbar(this.state.uploadingSnackbarKey);
		}
		const message = (<span>Uploading...</span>)
		const uploadingSnackbarKey = this.props.enqueueSnackbar( message, {
			variant: 'default',
			persist: true
		});
		const newFiles = this.state.files ? this.state.files.slice() : [];
		files.forEach(file => {
			newFiles.push(file);
		});
		this.setState({
			files: newFiles,
			isUploadingFiles: true,
			uploadingSnackbarKey: uploadingSnackbarKey
		});
	}

	addFile(file) {
		const files = this.state.files.slice();

		files.push(file);
		this.setState({
			files,
		});
	}

	removeFile(index, a, b, c) {
		const files = this.state.files.slice();
		files.splice(index, 1);
		this.setState({
			files,
		});
	}

	onSortEnd({ oldIndex, newIndex }) {
		this.setState({
			files: arrayMove(this.state.files, oldIndex, newIndex),
		});
	}

	isFinishedUploading(file) {
		if (this.state.files) {
			let paths = this.state.files.map(f => f.path);
			if (
				this.state.files.length - paths.filter(p => p !== undefined).length < 2
			) {
				if (this.state.isUploadingFiles) {
					this.setState({ isUploadingFiles: false });
					this.props.closeSnackbar(this.state.uploadingSnackbarKey);
					this.props.enqueueSnackbar('Files uploaded', {
						variant: 'success',
						autoHideDuration: 5000,
					});
				}

			}
		}
	}

	updateFile(index, file) {
		const files = this.state.files.slice();

		this.isFinishedUploading(file);

		if (!index) {
			files.some((f, i) => {
				if (f._id == file._id) {
					index = i;
					return;
				}
			});
		}
		files[index] = file;
		this.setState({
			files,
		});
	}

	render() {
		const { itemQuery } = this.props;
		const { files } = this.state;
		let item = { title: '', metadata: [{ type: 'text', value: '' }] };
		const tagOptions = [];
		let collections = [];
		let archiveTitle = '';

		if (itemQuery && itemQuery.project) {
			// set item to be edited
			item = Object.assign({}, itemQuery.project.item);
			if (item.tags) {
				const _tags = item.tags.slice();
				item.tags = [];
				_tags.forEach(tag => {
					item.tags.push({
						value: tag,
						label: tag,
					});
				});
			}

			// set project tags
			if (itemQuery.project.tags) {
				itemQuery.project.tags.forEach(tag => {
					tagOptions.push({
						value: tag,
						label: tag,
					});
				});
			}

			archiveTitle = itemQuery.project.title;
			item.privacy = item.private ? 'private' : 'public';

			collections = itemQuery.project.collections.concat(
				this.state.newCollections
			);
		}
		if (itemQuery.project.item && itemQuery.project.item.metadata) {
			item.relatedItems = itemQuery.project.item.metadata.filter(
				m => m.type === 'item'
			);
		} else {
			item.relatedItems = [];
		}

		return (
			<React.Fragment>
				<Prompt
					when={this.state.unsavedEdits}
					message="You have unsaved changes, are you sure you want to leave?"
				/>
				<ItemEditor
					addFile={this.addFile}
					archiveTitle={archiveTitle}
					collections={collections}
					files={files}
					handleAddFiles={this.handleAddFiles}
					handleCancel={this.handleCancel}
					handleNewCollection={this.handleNewCollection}
					initialValues={item}
					isUploadingFiles={this.state.isUploadingFiles}
					isSubmitting={this.state.isSubmitting}
					item={item}
					metadata={item ? item.metadata : []}
					onRemove={this.handleRemove}
					onSortEnd={this.onSortEnd}
					onSubmit={this.handleSubmit}
					removeFile={this.removeFile}
					reportUnsavedEdits={this.reportUnsavedEdits}
					tagOptions={tagOptions}
					updateFile={this.updateFile}
				/>
			</React.Fragment>
		);
	}
}

export default withSnackbar(
	compose(
		itemCreateMutation,
		itemUpdateMutation,
		itemRemoveMutation,
		itemDetailQuery,
		itemListQuery,
		collectionCreateMutation,
		renderWhileLoading('itemQuery', ItemLoadingPlaceholder)
	)(ItemEditorContainer)
);
