/**
 * @prettier
 */

import React from 'react';
import { compose } from 'react-apollo';
import autoBind from 'react-autobind';
import { withSnackbar } from 'notistack';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Checkbox from '@material-ui/core//Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import IconPriorityHigh from '@material-ui/icons/PriorityHigh';

import CollectionCreator from '../../../collections/components/CollectionCreator';
import collectionCreateMutation from '../../../collections/graphql/mutations/create';

// graphql
import itemListQuery from '../../graphql/queries/list';
import itemRemoveMutation from '../../graphql/mutations/remove';

class ItemDeleteModalContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			open: false,
			submitting: false,
		};
		autoBind(this);
	}

	closeModal() {
		this.props.handleClose();
	}

	deleteItems() {
		this.setState({ submitting: true });
		const { itemRemove } = this.props;
		const project = this.props.itemListQuery.project;
		let itemRemoves = this.props.selectedItemIds.map(item_id => {
			return itemRemove(item_id);
		});

		Promise.all(itemRemoves)
			.then(() => {
				this.props.handleClose();
				this.props.enqueueSnackbar('Successfully deleted!', {
					variant: 'success',
					autoHideDuration: 5000,
				});
				this.setState({submitting: false});
				this.props.dispatchUnselectAll();
			})
			.catch(err => {
				if (err.message.includes('Boolean cannot')) {
					// todo daniel fix hack, either refetch item query or else fix api error
					this.props.handleClose();
					this.props.enqueueSnackbar('Successfully deleted!', {
						variant: 'success',
						autoHideDuration: 5000,
					});
					this.props.dispatchUnselectAll();
					window.location.href='/dashboard'
				} else {
					this.props.enqueueSnackbar('Error deleting items, please try again', {
						variant: 'error',
						autoHideDuration: 5000,
					});
				}


				this.setState({submitting: false});
			});
	}

	render() {
		const pluralizeItem = (this.props.selectedItemIds.length > 1);
		return (
			<Dialog open={this.props.open} onClose={this.closeModal}>
				<DialogTitle
					id="customized-dialog-title"
					onClose={this.props.handleClose}
				>
					{`Are you sure you want to delete ${this.props.selectedItemIds.length} ${pluralizeItem ? 'items' : 'item'}?`}
				</DialogTitle>
				<DialogContent>
					<p>Once deleted there is no way to recover {`${pluralizeItem ? 'these items' : 'this item'}`}</p>
				</DialogContent>
				<DialogActions>
					<Button onClick={this.closeModal}>Cancel</Button>
					<Button
						onClick={this.deleteItems}
						color="primary"
						variant="contained"
						disabled={this.state.submitting}
					>
						Delete Permanently
					</Button>
				</DialogActions>
			</Dialog>
		);
	}
}

export default withSnackbar(
	compose(
		itemListQuery,
		itemRemoveMutation,
		collectionCreateMutation
	)(ItemDeleteModalContainer)
);
