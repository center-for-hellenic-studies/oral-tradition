import React from 'react';
import { Route, Switch } from 'react-router';

// layouts
import ArchiveLayout from '../../archives/layouts/ArchiveLayout';

// components
import ArchiveContainer from '../../archives/containers/ArchiveContainer';
import ItemEditorContainer from '../containers/ItemEditorContainer';
import ItemDetailContainer from '../containers/ItemDetailContainer';

// auth
import requireAuth from '../../../routes/requireAuth';

const AuthLayout = requireAuth(ArchiveLayout);


const AuthItems = ({ match }) => (
	<AuthLayout>
		<Switch>
			<Route
				path={match.path}
				component={ArchiveContainer}
				exact
			/>
			<Route
				path={`${match.path}/create`}
				component={ItemEditorContainer}
				exact
			/>
			<Route
				path={`${match.path}/:id`}
				component={ItemDetailContainer}
				exact
			/>
			<Route
				path={`${match.path}/:id/:slug`}
				component={ItemDetailContainer}
				exact
			/>
			<Route
				path={`${match.path}/:id/:slug/edit`}
				component={ItemEditorContainer}
			/>
		</Switch>
	</AuthLayout>
);

export default (
	<Route
		path="/items" component={AuthItems} />
);
