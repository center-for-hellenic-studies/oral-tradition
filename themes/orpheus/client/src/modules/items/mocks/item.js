import faker from 'faker';
import titleize from 'underscore.string/titleize';
import slugify from 'underscore.string/slugify';
import _ from 'underscore';
import mockList from '../../../mocks/list';

import { mockFiles } from '../../../mocks/files';

const mockMetadatum = () => {
	return {
		type: 'text',
		label: titleize(faker.lorem.words()),
		value: faker.lorem.words(),
	};
}

const mockMetadata = (rangeLow, rangeHigh) => (mockList(mockMetadatum, rangeLow, rangeHigh));

const mockItem = () => {
	const item = {};

	const title = faker.lorem.words();
	const tags = [];
	const nTags = _.random(0, 12);

	for (let i = 0; i < nTags; i++) {
		tags.push(faker.lorem.word());
	}

	item._id = faker.random.uuid();
	item.title = titleize(title);
	item.archiveTitle = titleize(faker.lorem.words());
	item.slug = slugify(title);
	item.description = `${faker.lorem.sentences()}`;
	item.author = `${faker.name.findName()}`;
	item.tags = tags;

	item.files = mockFiles(1, 100);
	item.metadata = mockMetadata(0, 10);

	return item;
};

export default mockItem;
