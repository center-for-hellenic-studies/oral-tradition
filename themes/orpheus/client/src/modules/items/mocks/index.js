import item from './item';
import mockList from '../../../mocks/list';

const items = (rangeLow, rangeHigh) => (mockList(item, rangeLow, rangeHigh));

export default item;
export { item as mockItem, items as mockItems };
