import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import faker from 'faker';
import titleize from 'underscore.string/titleize';
import capitalize from 'underscore.string/capitalize';
import withReduxForm from 'redux-form-storybook';

// components
import ItemEditor from '../components/ItemEditor';
import Item from '../../../components/common/Item';
import MainLayout from '../../../layouts/MainLayout';

// decorator
import {
	ApolloDecorator, ReduxDecorator, MuiThemeDecorator
} from '../../../stories/decorators';

// mocks
import { mockItem, mockItems } from '../mocks';

// Item
storiesOf('Features/Item/View', module)
	.addDecorator(ApolloDecorator)
	.addDecorator(ReduxDecorator)
	.addDecorator(StoryRouter())
	.addDecorator(MuiThemeDecorator)
	.addDecorator(withReduxForm)
	.addParameters({ options: { sortStoriesByKind: true } })
	.add('Default', () => (
		<MainLayout>
			<Item
				{...mockItem()}
			/>
		</MainLayout>
	))
	.add('Loading', () => (
		<MainLayout>
			<Item
				loadingMedia
				loadingText
				{...mockItem()}
			/>
		</MainLayout>
	));
storiesOf('Features/Item/Add', module)
	.addDecorator(ApolloDecorator)
	.addDecorator(ReduxDecorator)
	.addDecorator(StoryRouter())
	.addDecorator(MuiThemeDecorator)
	.addDecorator(withReduxForm)
	.addParameters({ options: { sortStoriesByKind: true } })
	.add('Default', () => (
		<div style={{padding: '40px'}}>
			<div>
				<h6>Default Item</h6>
				<ItemEditor />
			</div>
		</div>
	))
	.add('Saving', () => (
		<div style={{padding: '40px'}}>
			<div>
				<h6>Saving Item</h6>
				// TODO
			</div>
		</div>
	))
	.add('Error', () => (
		<div style={{padding: '40px'}}>
			<div>
				<h6>Error Item</h6>
				// TODO
			</div>
		</div>
	));
