import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';


const itemCreate = gql`
mutation itemCreate($hostname: String!, $item: ItemInputType!, $files: [FileInputType]) {
	itemCreate(hostname: $hostname, item: $item, files: $files) {
		_id
		title
		slug
		description
	}
}
`;

const itemCreateMutation = graphql(itemCreate, {
	props: params => ({
		itemCreate: (item, files, hostname) => params.itemCreateMutation({
			variables: {
				item,
				files,
				hostname: hostname || getCurrentArchiveHostname(),
			},
		}),
	}),
	name: 'itemCreateMutation',
	options: {
		refetchQueries: [ 'countsQuery', 'projectQuery', 'itemQuery', 'itemListQuery', 'projectFilterQuery'],
	},
});

export default itemCreateMutation;
