import { graphql } from 'react-apollo';
import gql from 'graphql-tag';


import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';


const itemRemove = gql`
	mutation itemRemove($id: String!, $hostname: String!) {
	itemRemove(_id: $id, hostname: $hostname) {
		result
	}
}
`;


// todo update refetch
const itemRemoveMutation = graphql(itemRemove, {
	props: params => ({
		itemRemove: id => params.itemRemoveMutation({
			variables: {
				id,
				hostname: getCurrentArchiveHostname(),
			},
		}),
	}),
	name: 'itemRemoveMutation',
	options: {
		refetchQueries: ['itemQuery', 'itemListQuery', 'projectFilterQuery', 'projectQuery'],
	},
});

export default itemRemoveMutation;
