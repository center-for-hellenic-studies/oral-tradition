import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';


const query = gql`
	query projectCollectionsQuery($hostname: String) {
		project(hostname: $hostname) {
	    _id
			collections {
				_id
				title
				slug
			}
		}
	}
`;

const projectCollectionsQuery = graphql(query, {
	name: 'projectCollectionsQuery',
	options: ({ params }) => ({
		variables: {
			hostname: getCurrentArchiveHostname(),
		}
	}),
});

export default projectCollectionsQuery;
