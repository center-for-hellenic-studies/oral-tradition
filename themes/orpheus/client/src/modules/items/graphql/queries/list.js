import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import qs from 'qs-lite';

import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';


const query = gql`
	query itemListQuery($hostname: String, $textsearch: String, $filter: [FieldInputType], $tags: [String], $offset: Int, $limit: Int) {
		project(hostname: $hostname) {
	    _id
			title
			items(textsearch: $textsearch, filter: $filter, tags: $tags, offset: $offset, limit: $limit) {
				_id
				title
				slug
				description
				tags
				collectionId
				files {
					_id
					type
					name
				}
			}
			itemsCount(textsearch: $textsearch, filter: $filter, tags: $tags)

			files {
				_id
				type
				name
			}
		}
	}
`;

const itemListQuery = graphql(query, {
	name: 'itemListQuery',
	options: (props) => {
	  const query = qs.parse(window.location.search.replace('?', ''));
		let offset = 0;
	  let limit = 42;
		let textsearch;
		let filter;
		let tags;

		if (props.limit) {
			limit = props.limit;
		}

	  if (query.page) {
	    limit = query.page * limit;
	  }

		if (query.search) {
			textsearch = query.search;
		}

		if (query.Tags) {
			tags = query.Tags;
		}

		if (query.dateFields) {
			query.dateFields.split('+').forEach((fieldTitle) => {
				delete query[fieldTitle+'_Max'];
				delete query[fieldTitle+'_Min'];
			})
			delete query['dateFields'];
		}

		for (let key in query) {
			if (['page', 'search', 'Tags'].indexOf(key) < 0) {
				if (!filter) {
					filter = [];
				}

				filter.push({
					name: key,
					values: query[key].replace('%20', ' ').split('+'),
				});
			}
		}

		// exception for storybook
		if (window.location.host === 'localhost:9009') {
			filter = null;
		}

		return {
			variables: {
				hostname: getCurrentArchiveHostname(),
				textsearch,
				tags,
				filter,
				offset,
				limit,
			},
		};
	},
});

export default itemListQuery;
