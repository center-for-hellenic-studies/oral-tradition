import React from 'react';
import { shallow } from 'enzyme';

// component
import MetadataFieldMapInput from './MetadataFieldMapInput';


describe('MetadataFieldMapInput', () => {
	it.skip('renders correctly', () => {

		const wrapper = shallow(<MetadataFieldMapInput input={{ onChange: noop }} />);
		expect(wrapper).toBeDefined();
	});
});
