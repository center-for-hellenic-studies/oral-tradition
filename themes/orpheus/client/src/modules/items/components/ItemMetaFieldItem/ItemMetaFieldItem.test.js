/**
 * @prettier
 */

import React from 'react';
import { shallow } from 'enzyme';

// component
import ItemMetaFieldItem from './ItemMetaFieldItem';

describe('ItemMetaFieldItem', () => {
	it('renders correctly', () => {
		const wrapper = shallow(<ItemMetaFieldItem />);
		expect(wrapper).toBeDefined();
	});

	it('handles JSON-string items', () => {
		const label = 'label';
		const value = '[{ "_id": "bar" }]';

		expect(() =>
			shallow(<ItemMetaFieldItem label={label} value={value} />)
		).not.toThrow();
	});

	it('handles plain id-string items', () => {
		const label = 'label';
		const value = 'bar';

		expect(() =>
			shallow(<ItemMetaFieldItem label={label} value={value} />)
		).not.toThrow();
	});
});
