/**
 * @prettier
 */

import React from 'react';
import { shallow } from 'enzyme';

import ItemRelatedInput from './ItemRelatedInput';

describe('ItemRelatedInput', () => {
	it('renders correctly', () => {
		const wrapper = shallow(<ItemRelatedInput />);
		expect(wrapper).toBeDefined();
	});
});
