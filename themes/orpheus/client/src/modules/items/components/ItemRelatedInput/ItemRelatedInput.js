/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import autoBind from 'react-autobind';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';

import './ItemRelatedInput.css';

const relatedItemsQuery = gql`
	query relatedItemsQuery($hostname: String!, $offset: Int, $limit: Int) {
		project(hostname: $hostname) {
			items(offset: $offset, limit: $limit) {
				_id
				title
			}
		}
	}
`;

const formatItems = (items = []) =>
	items.map(i => ({ label: i.title, type: 'item', value: i._id }));

const handleBlur = input => () => {
	input.onBlur(input.value);
};

ItemRelatedInput.propTypes = {
	input: PropTypes.object,
};

function ItemRelatedInput(props) {
	let input = props.input;
	const _handleBlur = handleBlur(input);
	let value;
	if (input && input.value) {
		value = input.value;
	}
	if (typeof value === 'string') {
		try {
			value = JSON.parse(value);
		} catch (e) {}
	}
	return (
		<Query
			query={relatedItemsQuery}
			variables={{
				hostname: getCurrentArchiveHostname(),
				offset: 0,
				limit: 10000,
			}}
			fetchPolicy="no-cache"
		>
			{({ loading, error, data, fetchMore }) => {

				return (
					<div className="itemRelatedInput">
						<Select
							isMulti
							onBlur={_handleBlur}
							onChange={input.onChange}
							options={
								loading
									? []
									: formatItems(data && data.project && data.project.items)
							}
							placeholder="Select related items"
							textFieldProps={{
								label: 'Label',
								InputLabelProps: {
									shrink: true,
								},
							}}
							value={value}
						/>
					</div>
				);
			}}
		</Query>
	);
}

export default ItemRelatedInput;
