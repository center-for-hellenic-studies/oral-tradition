/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

import MetadataField from '../MetadataField';

import './MetadataFields.css';

const notItem = f => f.type !== 'item';

const renderField = (name, index, fields, reportUnsavedEdits) => {
	// the FieldArray API is legitimately the worst. The first
	// argument to `fields.map` is not a field but the field's name.
	// To get the field's values, we need to call `fields.get(index)`
	const field = fields.get(index);
	const { type = 'text', value = null } = field;

	let performValidation = index > 0;

	if (index == 0) {
		if (value || field.label) {
			performValidation = true;
		}
	}

	return (
		<MetadataField
			field={field}
			key={name}
			name={name}
			index={index}
			type={type}
			initialValue={value}
			handleRemove={() => fields.remove(index)}
			performValidation={performValidation}
			reportUnsavedEdits={reportUnsavedEdits}
		/>
	);
};

MetadataFields.propTypes = {
	// fields is an object onto which redux-form grafts some array-like
	// functions (e.g., `map` below)
	fields: PropTypes.object,
};

function MetadataFields({ fields, reportUnsavedEdits }) {
	if (fields.length === 0) {
		// `fields.push()` causes a re-render. See:
		// https://redux-form.com/8.2.2/docs/api/fieldarray.md/#-code-fields-push-value-any-function-code-
		// (We want a re-render if there are no fields, so this is fine.)
		fields.push({});
	}

	return (
		<div id="itemEditorMetadata">
			{fields.map( (f, i) => {
				return renderField(f, i, fields, reportUnsavedEdits)
			})}
			<Button
				variant="contained"
				onClick={() => {
					fields.push({});
				}}
			>
				Add New Field
			</Button>
		</div>
	);
}

export default MetadataFields;
