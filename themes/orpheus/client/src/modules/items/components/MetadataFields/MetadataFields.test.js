import React from 'react';
import { shallow } from 'enzyme';

// component
import MetadataFields from './MetadataFields';


describe('MetadataFields', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<MetadataFields fields={{ map: noop }} />);
		expect(wrapper).toBeDefined();
	});
});
