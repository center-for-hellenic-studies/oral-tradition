/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import autoBind from 'react-autobind';
import { arrayMove } from 'react-sortable-hoc';
import { TextField } from 'redux-form-material-ui';

import ItemEditorUploader from '../../../dashboard/components/ItemEditorUploader';
import ItemSelectorField from '../../../dashboard/components/ItemSelectorField';
import MetadataFieldMapInput from '../MetadataFieldMapInput';
import ItemRelatedInput from '../ItemRelatedInput';

import MediaViewer from '../../../../components/common/MediaViewer/';

const ENTER_KEY = 13;

const formatPlaceValue = v => {
	let value;
	try {
		value = JSON.parse(v);
	} catch (e) {
		value = v;
	}

	return value;
};

const renderMediaInput = (props) => {
	let { input, label, ...rest } = props;
	let files = input.value || [];

	if (typeof files === 'string') {
		try {
			files = JSON.parse(files);
		} catch (e) {}
	}
	return (
		<div>
		<MediaViewer files={files || []} />
		<ItemEditorUploader
			label={label}
			files={files}
			handleAddFiles={(new_files) => {
				let _files = files.concat(new_files);
				input.onChange(_files);
				props.updateFiles(_files);
			}}
			updateFile={(index, file)=>{
				files[index] = file;
				input.onChange(files);
				props.updateFiles(files);
			}}
			{ ...rest}
			/>

			</div>
	)
}


const normalizePlaceValue = value => value && JSON.stringify(value);


const CUSTOM_FIELD_TYPES = ['media', 'place', 'text', 'item'];

class MetadataFieldValueInput extends React.Component {
	static propTypes = {
		field: PropTypes.object,
		initialValue: PropTypes.any,
		itemQuery: PropTypes.object,
		type: PropTypes.oneOf(['date', 'media', 'number', 'place', 'text', 'item']),
	};

	static defaultProps = {
		field: null,
		type: 'text',
	};

	constructor(props) {
		super(props);

		this.state = {
			files: false
		};

		autoBind(this);
	}

	handleKeyDown(e) {
		// Prevent the form from submitting if a user presses ENTER
		// while editing metadata
		if (e.keyCode === ENTER_KEY) {
			e.preventDefault();
			e.stopPropagation();
		}
	}

	componentDidMount() {
		let type = this.props.type;

		if (type && type === 'media') {
			let files = this.props.initialValue;;
			if (typeof initialValue === 'string') {
				try {
					files = JSON.parse(initialValue);
				} catch (e) {}
			}
			this.setState({files: files});
		}
	}

	render() {
		const { name, type } = this.props;

		if (CUSTOM_FIELD_TYPES.includes(type)) {
			return this[`_${type}Field`]();
		}

		return (
			<Field
				name={name}
				type={type}
				component="input"
				onKeyDown={this.handleKeyDown}
				placeholder="Value"
			/>
		);
	}

	_mediaField() {
		const { field, name, initialValue, reportUnsavedEdits } = this.props;
		let files = [];
		if (typeof initialValue === 'string') {
			try {
				files = JSON.parse(initialValue);
			} catch (e) {}
		}

		return (
			<Field
				component={renderMediaInput}
				field={field}
				initialValue={files}
				name={name}
				reportUnsavedEdits={reportUnsavedEdits}
				updateFiles={(files)=>{
					this.setState({files: files})
				}}
			/>
		);
	}

	_itemField() {
		const { field, name, initialValue, reportUnsavedEdits } = this.props;

		let items = [];
		if (typeof initialValue === 'string') {
			try {
				items = JSON.parse(initialValue);
			} catch (e) {}
		}

		return (<Field
			field={field}
			name={name}
			component={ItemRelatedInput}
			type="select-multi"
		/>)
	}

	_placeField() {
		const { field, name, initialValue } = this.props;
		let value = null;

		try {
			value = JSON.parse(initialValue);
		} catch (e) {}

		return (
			<Field
				component={MetadataFieldMapInput}
				format={formatPlaceValue}
				field={field}
				initialValue={value}
				name={name}
				normalize={normalizePlaceValue}
			/>
		);
	}

	_textField() {
		const { name, type } = this.props;

		return (
			<Field
				component={TextField}
				fullWidth
				name={name}
				placeholder="Value"
				type={type}
			/>
		);
	}
}

export default MetadataFieldValueInput;
