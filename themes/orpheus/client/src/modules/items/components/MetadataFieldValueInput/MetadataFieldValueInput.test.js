import React from 'react';
import { shallow } from 'enzyme';

// component
import MetadataFieldValueInput from './MetadataFieldValueInput';


describe('MetadataFieldValueInput', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<MetadataFieldValueInput name="test[0]" />);
		expect(wrapper).toBeDefined();
	});
});
