import React from 'react';
import { shallow } from 'enzyme';

// component
import ItemMetaFieldMap from './ItemMetaFieldMap';


describe('ItemMetaFieldMap', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<ItemMetaFieldMap value="{}" label="testMap" />);
		expect(wrapper).toBeDefined();
	});
});
