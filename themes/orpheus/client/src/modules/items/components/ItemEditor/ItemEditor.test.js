import React from 'react';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';

// component
import ItemEditor from './ItemEditor';

import { mockItem } from '../../mocks'
import configureStore from '../../../../store/configureStore';

describe('ItemEditor', () => {
	it('renders correctly', () => {

		const wrapper = shallow(
			<Provider store={configureStore()}>
				<ItemEditor item={mockItem()}  />
			</Provider>
		);
		expect(wrapper).toBeDefined();
	});
});
