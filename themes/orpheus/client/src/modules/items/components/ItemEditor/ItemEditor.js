/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
	ContentState,
	EditorState,
	convertFromHTML,
	convertFromRaw,
} from 'draft-js';
import { Field, FieldArray, reduxForm } from 'redux-form';

import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import MenuItem from '@material-ui/core/MenuItem';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import IconExpandMore from '@material-ui/icons/ExpandMore';

// components
import OrpheusEditor from '../../../orpheus-editor/components/Editor';
import MediaViewer from '../../../../components/common/MediaViewer';
// import MediaViewer from '@bit/archimedes_digital.orpheus.media-viewer';

import PageMeta from '../../../../components/common/PageMeta';
import MetadataFields from '../MetadataFields';
import ItemCollectionsInput from '../ItemCollectionsInput';
import ItemRelatedInput from '../ItemRelatedInput';

import decorators from '../../../orpheus-editor/components/decorators';
import editorActions from '../../../orpheus-editor/actions';

import './ItemEditor.css';

const createEditorState = rawEditorStateString => {
	if (!rawEditorStateString) return EditorState.createEmpty(decorators);

	let content;
	try {
		const json = JSON.parse(rawEditorStateString);
		content = convertFromRaw(json);
	} catch (e) {
		console.error(
			`Failed to parse description: ${e}. Trying to treat it as plain text.`
		);
		const blocks = convertFromHTML(`<p>${rawEditorStateString}</p>`);
		content = ContentState.createFromBlockArray(
			blocks.contentBlocks,
			blocks.entityMap
		);
	}
	return content
		? EditorState.createWithContent(content, decorators)
		: EditorState.createEmpty(decorators);
};

const renderTitle = ({
	input,
	label,
	type,
	meta: { touched, error, warning },
}) => (
	<div>
		<input
			{...input}
			className="itemEditorFormTitle--field-input"
			placeholder="Title"
			type={type}
		/>
		{touched &&
			((error && <span className="error">{error}</span>) ||
				(warning && <span className="warning">{warning}</span>))}
	</div>
);

const renderSelect = ({
	input: inputFromField,
	label,
	labelWidth,
	meta: { touched, error },
	children,
	...rest
}) => (
	<FormControl error={touched && error} fullWidth variant="outlined">
		<TextField
			{...inputFromField}
			{...rest}
			fullWidth
			input={<OutlinedInput fullWidth labelWidth={labelWidth} />}
			inputProps={{
				name: 'privacy',
				id: 'privacy-input',
			}}
			label={label}
			select
			variant="outlined"
		>
			{children}
		</TextField>
		{touched && error ? <FormHelperText>{error}</FormHelperText> : null}
	</FormControl>
);

const validateTitle = v => (v ? undefined : 'Please provide a title.');

class Description extends React.Component {
	static propTypes = {
		description: PropTypes.string,
		editorState: PropTypes.object.isRequired,
		reportUnsavedEdits: PropTypes.func,
		setEditorState: PropTypes.func.isRequired,
	};

	static defaultProps = {
		editorState: EditorState.createEmpty(decorators),
		reportUnsavedEdits: () => {},
		setEditorState: () => {},
	};

	componentDidMount() {
		const { description, setEditorState } = this.props;
		const editorState = createEditorState(description);

		setEditorState(editorState);
	}

	render() {
		const { editorState } = this.props;

		return (
			<div>
				<OrpheusEditor
					editorState={editorState}
					handleChange={this._handleChange}
					placeholder="Description"
				/>
			</div>
		);
	}

	_handleChange = editorState => {
		const { reportUnsavedEdits, setEditorState } = this.props;

		reportUnsavedEdits();
		setEditorState(editorState);
	};
}

const mapStateToProps = state => ({
	editorState: state.editor.editorState,
});

const mapDispatchToProps = {
	setEditorState: editorActions.setEditorState,
};

const ConnectedDescription = connect(
	mapStateToProps,
	mapDispatchToProps
)(Description);

class ItemEditorForm extends React.Component {
	static propTypes = {
		item: PropTypes.object,
		files: PropTypes.array,
	};

	render() {
		const {
			addFile,
			archiveTitle,
			collections,
			files,
			handleAddFiles,
			handleCancel,
			handleNewCollection,
			handleSubmit,
			isUploadingFiles,
			isSubmitting,
			item,
			onSortEnd,
			privacy,
			removeFile,
			reportUnsavedEdits,
			updateFile,
		} = this.props;
		const verb = (item.title || '').length > 0 ? 'Edit' : 'Create';

		return (
			<div className="itemEditor">
				<PageMeta pageTitle={`${verb} Item`} titleAppendix={archiveTitle} />
				<form
					className="itemEditorForm"
					onSubmit={handleSubmit}
					onChange={reportUnsavedEdits}
				>
					<div className="itemEditorFormInputOuter itemEditorFormTitleOuter">
						<Field
							component={renderTitle}
							data-lpignore="true"
							name="title"
							placeholder="Title"
							type="text"
							validate={validateTitle}
						/>
					</div>

					<div className="itemEditorHeader">
						<MediaViewer
							files={files}
							addFile={addFile}
							handleAddFiles={handleAddFiles}
							removeFile={removeFile}
							onSortEnd={onSortEnd}
							updateFile={updateFile}
							isUploadingFiles={isUploadingFiles}
							editMode={true}
							thumbnailSize={1200}
						/>
					</div>

					<ConnectedDescription
						description={item.description}
						reportUnsavedEdits={reportUnsavedEdits}
					/>

					<div className="itemEditorFormPrivacyOuter">
						<Field
							component={renderSelect}
							label="Privacy"
							name="privacy"
							value={privacy}
							variant="outlined"
						>
							<MenuItem value="private">Private</MenuItem>
							<MenuItem value="public">Public</MenuItem>
						</Field>
					</div>

					<div className="itemEditorExpansionPanels">
						<ExpansionPanel key="Metadata">
							<ExpansionPanelSummary expandIcon={<IconExpandMore />}>
								<Typography className="expansionPanelSummary--heading">
									Properties
								</Typography>
								<Typography className="expansionPanelSummary--secondary-heading">
									Attributes and tags
								</Typography>
							</ExpansionPanelSummary>
							<ExpansionPanelDetails>
								<FieldArray name="metadata" component={MetadataFields} reportUnsavedEdits={reportUnsavedEdits} />
							</ExpansionPanelDetails>
						</ExpansionPanel>

						<ExpansionPanel key="Collections">
							<ExpansionPanelSummary expandIcon={<IconExpandMore />}>
								<Typography className="expansionPanelSummary--heading">
									Collections
								</Typography>
								<Typography className="expansionPanelSummary--secondary-heading">
									{item.collectionId && item.collectionId.length > 0
										? `Belongs to ${item.collectionId.length} collection${
												item.collectionId.length > 1 ? 's' : ''
										  }`
										: 'Does not belong to any collections'}
								</Typography>
							</ExpansionPanelSummary>
							<ExpansionPanelDetails>
								<ItemCollectionsInput
									item={item}
									collections={collections}
									handleNewCollection={handleNewCollection}
								/>
							</ExpansionPanelDetails>
						</ExpansionPanel>

					</div>

					<div className="itemEditorFormInputOuterFormSubmit">
						<Button onClick={handleCancel}>Cancel</Button>
						<Button
							type="submit"
							className="itemEditorButton"
							color="primary"
							variant="contained"
							disabled={isSubmitting || isUploadingFiles}
						>
							Save
						</Button>
					</div>
				</form>
			</div>
		);
	}
}

export default reduxForm({
	form: 'ItemEditor',
	enableReinitialize: true,
})(ItemEditorForm);
