/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import { Field } from 'redux-form';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { TextField } from 'redux-form-material-ui';

import { required, maxLength } from '../../../../lib/formHelpers';
import MetadataFieldValueInput from '../MetadataFieldValueInput';

import './MetadataField.css';

const maxLength200 = maxLength(200);

const renderTypeSelect = ({
	input,
	label,
	meta: { touched, error },
	children,
	...rest
}) => {
	return (
		<Select {...input} {...rest} value={input.value || 'text'}>
			{children}
		</Select>
	);
};

class MetadataField extends React.Component {
	static propTypes = {
		field: PropTypes.object,
		handleRemove: PropTypes.func,
		handleUpdateMetadata: PropTypes.func,
		initialValue: PropTypes.string,
		name: PropTypes.string,
		performValidation: PropTypes.bool,
		// the 'item' and 'collection' types are essentially no-ops, because they're
		// handled elsewhere
		type: PropTypes.oneOf(['date', 'item', 'media', 'number', 'place', 'text']),
	};

	static defaultProps = {
		initialValue: '',
		type: 'text',
	};

	constructor(props) {
		super(props);

		this.state = {
			type: props.type,
		};

		autoBind(this);
	}

	render() {
		const {
			field,
			handleRemove,
			handleUpdateMetadata,
			initialValue,
			name,
			type,
			performValidation,
			reportUnsavedEdits,
		} = this.props;

	
		return (
			<div className="itemEditorMetadataField">
				<div className="itemEditorMetadataFieldInput itemEditorMetadataFieldLabelInput">
					<Field
						name={`${name}.label`}
						type="text"
						component={TextField}
						placeholder="Label"
						validate={[performValidation ? required : () => {}, maxLength200]}
						fullWidth
					/>
				</div>
				<div className="itemEditorMetadataFieldInput itemEditorMetadataFieldTypeInput">
					<Field name={`${name}.type`} component={renderTypeSelect}>
						<MenuItem value="text">Text</MenuItem>
						<MenuItem value="number">Number</MenuItem>
						<MenuItem value="date">Date</MenuItem>
						<MenuItem value="place">Place</MenuItem>
						<MenuItem value="media">Media</MenuItem>
						<MenuItem value="item">Item</MenuItem>

					</Field>
				</div>
				<div className="itemEditorMetadataFieldInput itemEditorMetadataFieldValueInput">
					<MetadataFieldValueInput
						field={field}
						name={`${name}.value`}
						type={type}
						initialValue={initialValue}
						reportUnsavedEdits={reportUnsavedEdits}
					/>
				</div>
				<div className="itemEditorMetadataFieldInput itemEditorMetadataFieldRemove">
					<IconButton aria-label="Remove metadata" onClick={handleRemove}>
						<CloseIcon
							color={
								typeof handleRemove === 'function' ? 'inherit' : 'disabled'
							}
						/>
					</IconButton>
				</div>
			</div>
		);
	}
}

export default MetadataField;
