import React from 'react';
import { shallow } from 'enzyme';

// component
import MetadataField from './MetadataField';


describe('MetadataField', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<MetadataField />);
		expect(wrapper).toBeDefined();
	});
});
