import React from 'react';
import { shallow } from 'enzyme';

// component
import ItemMetaFields from './ItemMetaFields';


describe('ItemMetaFields', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<ItemMetaFields />);
		expect(wrapper).toBeDefined();
	});
});
