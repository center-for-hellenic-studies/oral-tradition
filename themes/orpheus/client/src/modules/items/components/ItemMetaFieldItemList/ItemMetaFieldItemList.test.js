import React from 'react';
import { shallow } from 'enzyme';

// component
import ItemMetaFieldItemList from './ItemMetaFieldItemList';


describe('ItemMetaFieldItemList', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<ItemMetaFieldItemList items={[]} />);
		expect(wrapper).toBeDefined();
	});
});
