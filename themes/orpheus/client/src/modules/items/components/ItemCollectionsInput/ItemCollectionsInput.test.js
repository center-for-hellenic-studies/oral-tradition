import React from 'react';
import { shallow } from 'enzyme';

// component
import ItemCollectionsInput from './ItemCollectionsInput';

import configureStore from '../../../../store/configureStore';

describe('ItemEditor', () => {
	it('renders correctly', () => {

		const wrapper = shallow(
			<ItemCollectionsInput   />
		);
		expect(wrapper).toBeDefined();
	});
});
