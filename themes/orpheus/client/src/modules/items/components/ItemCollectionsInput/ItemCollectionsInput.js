import React from 'react';
import Checkbox from '@material-ui/core//Checkbox';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Field } from "redux-form";
import CollectionCreator from '../../../collections/components/CollectionCreator'

import './ItemCollectionsInput.css';

const renderCheckboxGroup = ({ name, options,  input, meta, ...custom}) => {
  let $options = options.map((option, i) => (
    <div key={i}>
    <FormControlLabel
      control={
        <Checkbox
          name={`collectionId[${i}]`}
          defaultChecked={input.value.indexOf(option.value) !== -1}
          label={option.label}
          onChange={(e, checked) => {
            let newValue = [...input.value];
            if (checked){
              newValue.push(option.value);
            } else {
              newValue.splice(newValue.indexOf(option.value), 1);
            }
            return input.onChange(newValue);
          }}
          {...custom}
        />
      }
      label={option.label}
    />
    </div>
  ));
  return (
    <div>
      {$options}
    </div>
  );
};

const ItemCollectionsInput = (props) => (
  <div className="itemCollectionsInput">
    <CollectionCreator handleNewCollection={props.handleNewCollection} />
    <fieldset>
       <Field
         name="collectionId"
         component={renderCheckboxGroup}
         options={(props.collections ? props.collections.map((c)=> {return {label: c.title, value: c._id}} ) : []) }
       />
     </fieldset>
  </div>
);

export default ItemCollectionsInput;
