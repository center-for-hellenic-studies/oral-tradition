import React from 'react';
import { shallow } from 'enzyme';

// component
import ItemMetaFieldMedia from './ItemMetaFieldMedia';


describe('ItemMetaFieldMedia', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<ItemMetaFieldMedia />);
		expect(wrapper).toBeDefined();
	});
});
