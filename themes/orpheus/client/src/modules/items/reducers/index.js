import * as types from '../actions';

const initialState = {
	selectedItemIds: [],
};

export default (state = initialState, action) => {
	switch (action.type) {
	case types.TOGGLE_ITEM_SELECTED: {
		const selectedItemIds = state.selectedItemIds.slice();
		const itemToToggleIndex = selectedItemIds.indexOf(action.itemId);

		if (itemToToggleIndex < 0) {
			selectedItemIds.push(action.itemId);
		} else {
			selectedItemIds.splice(itemToToggleIndex, 1);
		}

		return {
			...state,
			selectedItemIds,
		};
	}
	case types.UNSELECT_ALL: {
		return {
			...state,
			selectedItemIds: [],
		};
	}
	default:
		return state;
	}
};
