import React from 'react';
import { compose } from 'react-apollo';
import { getAuthedUserQuery } from '../graphql/queries/users';
import UserDropdown from '../components/UserDropdown';

export default compose(
	getAuthedUserQuery,
)(UserDropdown);
