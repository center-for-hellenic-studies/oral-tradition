import React from 'react';
import { compose } from 'react-apollo';
import { settingQueryByTenantId } from '../graphql/queries/settings';
import Publications from '../components/Publications';

export default compose(
	settingQueryByTenantId,
)(Publications);
