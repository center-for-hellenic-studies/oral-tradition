import React from 'react';
import { compose } from 'react-apollo';
import { usersGetPublicById } from '../graphql/queries/users';
import AnnotationIconToggle from '../components/AnnotationIconToggle';

export default compose(
	usersGetPublicById,
)(AnnotationIconToggle);
