import React from 'react';
import Toggle from 'material-ui/Toggle';
import Cookies from 'js-cookie';
import $ from 'jquery';

const styles = {
	block: {
		maxWidth: 250,
	},
	toggle: {
		marginBottom: 16,
	},
	thumbOff: {
		backgroundColor: '#eee',
	},
	trackOff: {
		backgroundColor: '#aaa',
	},
	thumbSwitched: {
		backgroundColor: '#fff',
	},
	trackSwitched: {
		backgroundColor: '#eee',
	},
	labelStyle: {
		color: '#fff',
		fontFamily: '"Helvetica Neue", Times, sans-serif',
		fontWeight: '300',
		letterSpacing: '1.1px',
	},
};

class AnnotationsToggle extends React.Component {
	constructor(props) {
		super(props);
		let legacyMode = Cookies.get('legacyMode');

		if (legacyMode === 'true') {
			legacyMode = true;
		} else {
			legacyMode = false;
		}

		if (typeof legacyMode === 'undefined') {
			Cookies.set('legacyMode', false);
			legacyMode = false;
		}

		this.state = {
			annotationsOn: !legacyMode,
		};
	}

	onToggle = (e, isInputChecked) => {
		if (isInputChecked) {
			$('body').addClass('readingEnvironment');
			$('.mainDisplay .rightDisplay').addClass('readingContent').addClass('readingContent--primary');
			Cookies.set('legacyMode', false);
			this.setState({
				annotationsOn: true,
			});
		} else {
			$('body').removeClass('readingEnvironment');
			$('.mainDisplay .rightDisplay').removeClass('readingContent').removeClass('readingContent--primary');
			Cookies.set('legacyMode', true);
			this.setState({
				annotationsOn: false,
			});
		}
	}

	render = () => {
		return (
			<div className="annotationsToggle">
				<Toggle
					label="Annotations"
					style={styles.toggle}
					thumbStyle={styles.thumbOff}
					trackStyle={styles.trackOff}
					thumbSwitchedStyle={styles.thumbSwitched}
					trackSwitchedStyle={styles.trackSwitched}
					labelStyle={styles.labelStyle}
					defaultToggled={this.state.annotationsOn}
					onToggle={this.onToggle.bind(this)}
				/>
			</div>
		);
	}
}
export default AnnotationsToggle;
