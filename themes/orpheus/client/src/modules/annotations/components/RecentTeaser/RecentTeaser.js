import React from 'react';
import PropTypes from 'prop-types';

import Utils from '../../lib/utils';


class RecentTeaser extends React.Component {
	static propTypes = {
		recentItem: PropTypes.object.isRequired,
	}

	render = () => {
		const { recentItem } = this.props;

		return (
			<a
				className="recentTeaser"
				href={recentItem.link}
			>
				<span className="recentTeaserTitle">
					{recentItem.author}, {Utils.trunc(recentItem.title, 30)}
				</span>
				<span className="recentTeaserChapter">
					{Utils.trunc(recentItem.subtitle, 30)}
				</span>
			</a>
		);
	}
}

export default RecentTeaser;
