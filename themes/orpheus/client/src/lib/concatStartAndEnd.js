const concatStartAndEnd = ({
	start,
	startOffset,
	end,
	endOffset,
}) => (`${start}-${startOffset}-${end}-${endOffset}`);


export default concatStartAndEnd;
