/**
 * @prettier
 */

/**
 * Define helpers for verification of form inputs
 */

export const required = value => (value ? undefined : 'Required'); // eslint-disable-line

export const maxLength = max => (
	value // eslint-disable-line
) =>
	value && value.length > max ? `Must be ${max} characters or less` : undefined; // eslint-disable-line

export const validUrl = value =>
	value &&
	!/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/i.test(
		value
	)
		? 'Invalid URL'
		: undefined;

export const validTwitter = value =>
	value && !/^[a-zA-Z0-9_]{1,15}$/i.test(value)
		? 'Invalid Twitter username'
		: undefined;

export const validEmail = value =>
	value &&
	!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i.test(
		value.toLowerCase()
	)
		? 'Invalid email address'
		: undefined;
