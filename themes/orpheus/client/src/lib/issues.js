const issues = {
  "data": {
    "issues": [
            {
              "id": 1,
              "name": "33i",
              "title": "Volume 33, Issue 1",
              "articles": [{
                  "id": 1,
                  "title": "Oral Tradition and the Dead Sea Scrolls",
                  "ecompanionhtml": null,
                  "authors": [{
                    "name": "Shem Miller"
                  }],
                  "files": [{
                    "id": 8312,
                    "title": "oral_tradition_volume_33_issue_1.jpg",
                    "fileURI": "//journal.oraltradition.org/wp-content/uploads/oral_tradition_volume_33_issue_1.jpeg",
                    "caption": "Fig. 1. Naxi dongba script for the creation hero, Coqsseileel’ee."
                  }]
                },
                {
                  "id": 1,
                  "title": "Oral Tradition and the Dead Sea Scrolls",
                  "ecompanionhtml": null,
                  "authors": [{
                    "name": "Shem Miller"
                  }],
                  "files": []
                },
                {
                  "id": 1,
                  "title": "An Examination of the Poetics of Tibetan Secular Oratory: An A mdo Tibetan Wedding Speech",
                  "ecompanionhtml": null,
                  "authors": [{
                    "name": "Timothy Thurston"
                  }],
                  "files": []
                },
                {
                  "id": 1,
                  "title": "Magic Questions: The Rhetoric of Authority in South Slavic Epic Song",
                  "ecompanionhtml": null,
                  "authors": [{
                    "name": "Milan Vidaković"
                  }],
                  "files": []
                },
              ]
            },
      {
        "id": 1,
        "name": "32i",
        "title": "Volume 32, Issue 1",
        "articles": [
          {
            "id": 8320,
            "title": "<em>Eall-feala Ealde Sæge</em>: Poetic Performance and  “The Scop's Repertoire” in Old English Verse",
            "content": "<p>This essay identifies “The Scop’s Repertoire” as an Old English traditional theme. The theme associates the making of verse with three motifs: <em>copiousness</em>, <em>orality</em>, and <em>antiquity</em>. With close analogues in Old Saxon, Old and Middle High German, and Old Norse poetry, “The Scop’s Repertoire” originates in an oral Germanic tradition of versification. The theme thus sheds light on the myth of the oral poet, who is either depicted as divinely inspired or as bearer of tradition.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Paul Battles"
              },
              {
                "name": "Charles D. Wright"
              }
            ],
            "files": []
          },
          {
            "id": 8310,
            "title": "Between the Oral and the Literary: The Case of the Naxi Dongba Texts",
            "content": "<p>This essay considers the orality of ritual texts written in the Naxi dongba script from southwest China. Historically, the inherent orality of these texts has been largely ignored in favor of seeing them as a kind of visual “hieroglyphics.” Here, a case will be made that the Naxi texts represent an intermediary stage between the “oral” and the “written,” questioning the existence of a stark divide between orality and literacy.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Duncan Poupard"
              }
            ],
            "files": [
              {
                "id": 8312,
                "title": "Poupard-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Poupard-1.jpg",
                "caption": "Fig. 1. Naxi dongba script for the creation hero, Coqsseileel’ee."
              },
              {
                "id": 8313,
                "title": "Poupard-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Poupard-2.jpg",
                "caption": "Fig. 2. Naxi dongba script for tiger."
              },
              {
                "id": 8314,
                "title": "Poupard-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Poupard-3.jpg",
                "caption": "Fig. 3. Naxi creation myth from the sacrifice to the wind ceremony."
              },
              {
                "id": 8315,
                "title": "Poupard-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Poupard-4.jpg",
                "caption": "Fig. 4. Story episode: “Coqsseileel’ee shoots the magpie”."
              },
              {
                "id": 8316,
                "title": "Poupard-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Poupard-5.jpg",
                "caption": "Fig. 5. Coqsseileel’ee shoots the magpie, <em>Delivering the souls of the dead</em>."
              },
              {
                "id": 8317,
                "title": "Poupard-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Poupard-6.jpg",
                "caption": "Fig. 6. A dongba writes about his ailing hand."
              },
              {
                "id": 8318,
                "title": "Poupard-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Poupard-7.jpg",
                "caption": "Fig. 7. The dongba wishes good fortune on the household."
              },
              {
                "id": 8319,
                "title": "Poupard-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Poupard-8.jpg",
                "caption": "Fig. 8. An example of vertical composition."
              }
            ]
          },
          {
            "id": 8308,
            "title": "The Fairy-Seers of Eastern Serbia: Seeing Fairies—Speaking through Trance",
            "content": "<p>The fairy-seers of Southeastern Europe are generally women who are able to communicate with the invisible world. They claim to see women-like creatures and transmit messages from them. Sometimes they fell into a trance-like state in order to establish a communication. During this process the fairy-seers can prophesy future events. They bring messages to the living from the behalf of their deceased relatives. This essay is about two of such women from the Vlach community of Eastern Serbia.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Maria Vivod"
              }
            ],
            "files": []
          },
          {
            "id": 8306,
            "title": "A New Approach to the Classification of Gaelic Song",
            "content": "This essay illustrates the importance of songs and singing in traditional Gaelic society, revisits earlier attempts to classify Gaelic song, and examines whether or not the effort of constructing a classification system is still worthwhile. Finally, it examines the place and role occupied by singing in Gaelic in our own time, and in particular with the contextual and aesthetic changes that have shaped the performance of Gaelic song in today’s commercially-driven “world music” environment.",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Virginia Blankenhorn"
              }
            ],
            "files": []
          },
          {
            "id": 8304,
            "title": "Oral Features of the Qur’ān Detected in Public Recitation",
            "content": "<p>This essay examines textual features of the Qur’ān that may emerge more prominently as a result of listening to it, features that might enhance insight gained during slow or silent reading sessions. Comparison with ancient Greek oral works, such as Homer, and an examination of Classical memory methodologies provide support for some of the oral features identified. In particular, a series of linguistic devices regarding structure, meaning, diction, syntax, and sound are sampled.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mary Knight"
              }
            ],
            "files": []
          },
          {
            "id": 8294,
            "title": "A Pebble Smoothed by Tradition: Lines 607-61 of <em>Beowulf</em> as a Formulaic Set-piece",
            "content": "<p>In this essay Drout and Smith use new “lexomic” methods of computer-assisted statistical analysis to identify a concentration of unusual lexical, metrical, grammatical, and formulaic features in lines 607-61 of <em>Beowulf</em>, a scene in which Queen Wealhtheow passes the cup of friendship to the assembled warriors. Although the passage contains a number of proper names, the authors demonstrate that it is highly formulaic and adaptable, and conclude that the <em>Beowulf</em>-poet had an unwritten, highly traditional source for these lines.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Michael D. C. Drout"
              },
              {
                "name": "Leah Smith"
              }
            ],
            "files": [
              {
                "id": 8296,
                "title": "Drout_Smith-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Drout_Smith-1.jpg",
                "caption": "Fig. 1. Dendrogram of the A-Scribe Portion of <em>Beowulf</em> when the poem is divided into segments. “Wealhtheow’s Cup-Bearing” is included in Segment C."
              },
              {
                "id": 8297,
                "title": "Drout_Smith-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Drout_Smith-2.jpg",
                "caption": "Fig. 2. Dendrogram of the A-Scribe Portion of <em>Beowulf</em> when the poem is divided into segments. “Wealhtheow’s Cup-Bearing” is moved to segment E."
              },
              {
                "id": 8298,
                "title": "Drout_Smith-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Drout_Smith-3.jpg",
                "caption": "Fig. 3. Frequency of conjunctive <em>siþþan</em> in <em>Beowulf</em> in a rolling window of 20 lines."
              },
              {
                "id": 8299,
                "title": "Drout_Smith-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Drout_Smith-4.jpg",
                "caption": "Fig. 4. Frequency of Kaluza’s Law verses in a rolling window of 25 lines."
              },
              {
                "id": 8300,
                "title": "Drout_Smith-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Drout_Smith-5.jpg",
                "caption": "Fig. 5. Frequency of Ss/Sx verse types in <em>Beowulf</em> in a rolling-window of 50 lines."
              },
              {
                "id": 8301,
                "title": "Drout_Smith-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Drout_Smith-6.jpg",
                "caption": "Fig. 6. Average frequency of alliterating non-displaced finite verbs in <em>Beowulf</em> in a rolling-window of 50 half-lines. Horizontal scale is in half-lines."
              },
              {
                "id": 8302,
                "title": "Drout_Smith-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Drout_Smith-7.jpg",
                "caption": "Fig. 7. Frequency of repeated formulas in <em>Beowulf</em> in a rolling window of 20 lines."
              },
              {
                "id": 8303,
                "title": "Drout_Smith-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/32i/full/Drout_Smith-8.jpg",
                "caption": "Fig. 8. Ratio of þ to ð in a rolling window of 25 lines."
              }
            ]
          }
        ],
        "customFields": [
          {
            "id": 1,
            "issue_id": "1",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 2,
            "issue_id": "1",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/32i/00_32.1cover.pdf"
          },
          {
            "id": 3,
            "issue_id": "1",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/32i/00_32.1cover.jpg"
          },
          {
            "id": 4,
            "issue_id": "1",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/32i/00_32.1fm.pdf"
          },
          {
            "id": 5,
            "issue_id": "1",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/32i/08_32.1bm.pdf"
          },
          {
            "id": 6,
            "issue_id": "1",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/32i/01_32.1.pdf"
          },
          {
            "id": 7,
            "issue_id": "1",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/32i/oraltradition_3_.1.zip"
          },
          {
            "id": 8,
            "issue_id": "1",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/32i/00_32.1complete.pdf"
          },
          {
            "id": 9,
            "issue_id": "1",
            "name": "Volume",
            "value": "32"
          },
          {
            "id": 10,
            "issue_id": "1",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 11,
            "issue_id": "1",
            "name": "Date",
            "value": "2018-03-31"
          },
          {
            "id": 705,
            "issue_id": "1",
            "name": "URN",
            "value": ""
          }
        ]
      },
      {
        "id": 10,
        "name": "31ii",
        "title": "Volume 31, Issue 2: Parallelism in Verbal Art and Performance",
        "articles": [
          {
            "id": 8364,
            "title": "Parallelism in Verbal Art and Performance: An Introduction",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": " Frog"
              },
              {
                "name": "Lotte Tarkka"
              }
            ],
            "files": []
          },
          {
            "id": 8360,
            "title": "Remembering and Recreating Origins: The Transformation of a Tradition of Canonical Parallelism among the Rotenese of Eastern Indonesia",
            "content": "<p>This paper examines and illustrates key oral traditions of canonical parallelism among the Rotenese of eastern Indonesia. The Rotenese are concerned with maintaining a knowledge of origins and the poets who perpetuate this knowledge are known for their ability to recount long chants that reveal these origins. On Rote, a traditional ritual canon of origins that recounts relations between Sun and Moon and the Lords of the Sea has now been complemented by a Christian canon based on the appropriation of Biblical knowledge.  This Christian canon strictly adheres to the paired lexicon and rules of composition of the traditional canon, but has developed its own appropriate lexicon of theological pairings. Another, lesser canon has also begun to emerge to recount the celebration of national origins. All of these canons now form part of a continuing body of creative traditions expressed in the paired language of parallelism.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "James J. Fox"
              }
            ],
            "files": [
              {
                "id": 8362,
                "title": "fox-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/fox-1.jpg",
                "caption": "Fig. 1. The domains of Rote."
              },
              {
                "id": 8363,
                "title": "fox-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/fox-2.jpg",
                "caption": "Fig 2. A tentative dialect map of Rote (note: Dialects II and III are closer to one another than they are to any other dialects)."
              }
            ]
          },
          {
            "id": 8354,
            "title": "“Word upon a Word”: Parallelism, Meaning, and Emergent Structure in Kalevala-meter Poetry",
            "content": "<p>This essay treats parallelism as a means for articulating and communicating meaning in performance. Rather than a merely stylistic and structural marker, parallelism is discussed as an expressive and cognitive strategy for the elaboration of notions and cognitive categories that are vital in the culture and central for the individual performers. The essay is based on an analysis of short forms of Kalevala-meter poetry from Viena Karelia: proverbs, aphorisms, and lyric poetry. In the complex system of genres using the same poetic meter parallelism transformed genres and contributed to the emergence of cohesive and finalized performances.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lotte Tarkka"
              }
            ],
            "files": [
              {
                "id": 8356,
                "title": "tarkka-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/tarkka-1.jpg",
                "caption": "Fig. 1. Anni Lehtonen (1866-1943). SKS KRA photo collection, Paulaharju 5413.9."
              },
              {
                "id": 8357,
                "title": "tarkka-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/tarkka-2.jpg",
                "caption": "Fig. 2. Anni Lehtonen’s string of proverbs on the intertwining notions of poverty, sorrow and song make up a continuation of the poem The Self Wouldn’t (Fig. 3) (SKS KRA. Paulaharju c)9614-9620. 1915). With the red vertical line Paulaharju marked the proverbs, phrases, and poems that Anni performed spontaneously, without the collector’s inducement. Photo by author."
              },
              {
                "id": 8358,
                "title": "tarkka-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/tarkka-3.jpg",
                "caption": "Fig. 3. The Self Wouldn’t (SKS KRA. Paulaharju c)9611-13. 1915). The poem is preceded by proverbs and a couplet with end-rhyme contemplating on the causes for singing; and followed by the singer’s comment on the profound personal meaningfulness of the poem."
              },
              {
                "id": 8359,
                "title": "1A013.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/1A013.mp3",
                "caption": "Fig. 4. Reconstruction of the sung performance of Anni Lehtonen’s The Widow’s Song, Kati Kallio and Heidi Haapoja."
              }
            ]
          },
          {
            "id": 8352,
            "title": "“The Language of Gods”: The Pragmatics of Bilingual Parallelism in Ritual Ch’orti’ Maya Discourse",
            "content": "<p>In this study I investigate the discursive function of parallelism in the ritual speech of Ch’orti’ Maya. Specifically, I examine the exploitation of the dual lexicons of Ch’orti’ Mayan and Spanish in the production of parallel structures. Ch’orti’ ritual speech is almost universally constructed in parallelistic fashion, accomplishing at once a near hypnotic cadence when performed, while also serving various pragmatic functions. I detail the dynamic breadth of what I refer to as <em>bilingual parallelism</em>, i.e., parallelism that involves the pairing of synonymous terms from different languages in a distich. The effective use of parallelistic speech is said by the Ch’orti’ to be an imitation of the speech patterns of the gods themselves, thereby further explaining its importance in ceremonial contexts when speaking to gods and otherworld beings.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Kerry Hull"
              }
            ],
            "files": []
          },
          {
            "id": 8350,
            "title": "Parallelism and the Composition of Oral Narratives in Banda Eli",
            "content": "<p>While parallelism is easily recognizable as the source for various literary tropes, it is also important as a resource for the speakers’ dialogic engagement with the patterns of interaction and experience: they embody part of their linguistic habitus. This article explores the forms of parallelism found in a variety of speech and narrative genres in Bandanese, an Eastern Indonesian minority language with about 5,000 speakers. Bandanese abounds with parallel expressions in which speakers use part-whole relations based on social and cultural classifications to construct totalizing cognitive and value statements. At the same time, Bandanese poetics is more than just evidence of an integrated cultural world. The article analyzes interactions between tropes based on repetition and parallelism to suggest that speakers and narrators use them to create a resonance between immediate rhetorical effects and larger recognized aesthetic positions in their folk categories. A prominent example of such resonance is the use of parallelism in eloquent, public speech. When speakers use the lexical contrast between Bandanese and the regional or national majority language as a source of parallel expressions, they draw from an aesthetic in which powerful speech resonates with past and future dialogue with outsiders. Recent scholarship on parallelism and repetition encourages us to recognize that they produce potential dialogic relations on a larger scale than that of single utterances. This approach can produce valuable insights into possibilities for innovation in and revitalization of Bandanese and other minority languages threatened by demographic change and loss of use in their former domains.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Timo Kaartinen"
              }
            ],
            "files": []
          },
          {
            "id": 8345,
            "title": "Parallelism and Musical Structures in Ingrian and Karelian Oral Poetry",
            "content": "<p>Listening to historical oral poetry usually means listening to archival sound recordings with no possibility to ask questions or compare performances by one singer in different performance arenas. Yet, when a greater number of recordings from different singers and by different collectors is available, the comparison of these performances has the potential to reveal some locally shared understandings on the uses of poetic registers. In the present article, this setting is applied to examine the relationships of textual parallelism and musical structures in Kalevala-metric oral songs recorded from two Finnic language areas, Ingria and Karelia.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Kati Kallio"
              }
            ],
            "files": [
              {
                "id": 8347,
                "title": "kallio-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/kallio-1.jpg",
                "caption": "Fig. 1. The beginning of a song on the mythical journey of Lemminkäini, performed by Nasti Huotarin’i from Akonlahti village, Archangel Karelia. Sound recording made in 1938 in a refugee camp."
              },
              {
                "id": 8348,
                "title": "kallio-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/kallio-2.jpg",
                "caption": "Fig. 2. Lines 10-19 of the song on the mythical journey of Lemminkäini, performed by Siitari Karjalaini from Vuokkiniemi village, Archange Karelia. Sound recording made in 1922."
              },
              {
                "id": 8349,
                "title": "kallio-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/kallio-3.jpg",
                "caption": "Fig. 3. First lines of the song on the boat trip of the old sage Väinämöinen and the creation of the first Kantele-instrument, performed by Anni Kiriloff from Aajuolahti village, Archangel Karelia. Sound recording made in 1923 in a refugee camp."
              }
            ]
          },
          {
            "id": 8343,
            "title": "Poetic Parallelism and Working Memory",
            "content": "<p>A widespread kind of parallelism is a relation between sections of text such that each resembles the other in linguistic form, or in lexical meaning, or in both form and meaning.  In poetry, this kind of parallelism can be systematic, and when it is, it holds between two adjacent sections. The new claim of this article is that these sections are short enough for the whole parallel pair to be held in working memory (in the episodic buffer).  Parallelism thus shares a property with the other added forms of poetry - meter, rhyme and alliteration - that it holds over material which can be held as a whole section (such as line or couplet) in working memory.  I conclude by suggesting that processing the parallel pair in working memory brings advantages to the poetry: an emotional effect from contrastive valence, an epistemic effect from the fluency heuristic, and the production of metaphorical meaning.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Nigel Fabb"
              }
            ],
            "files": []
          },
          {
            "id": 8341,
            "title": "Parallelism in the Hanvueng: A Zhuang Verse Epic  from West-Central Guangxi in Southern China",
            "content": "<p>Zhuang is a Tai-Kadai language spoken in southern China. Parallelism is ubiquitous in Zhuang poetry and song,in ritual texts, and in a range of oral genres. Curiously, this salient fact has generally escaped the notice of scholars writing on the subject of Zhuang poetics. This article looks specifically at the phenomenon of parallelism in one particular Zhuang ritual text from west-central Guangxi. This is the <em>Hanvueng</em>, a long verse narrative that is recited at rituals intended to deal with cases of unnatural death and serious family quarrels, especially feuding between brothers. I provide a general description of the role of song and parallel verse in Zhuang oral culture. I next present a typology of poetic lines and passages exhibiting strict parallelism and quasi-parallelism, and also look at the rhetorical and rhythmical uses of non-parallel lines. As a second step in this investigation, I re-analyse these typological categories in terms of the recitation soundscape as it unfolds in real time and in ritual performance. This second step brings us back from an objectivist account to a variety of emic perspectives, and allows us to see more clearly the rhetorical and emotive power generated by the ongoing narration – and its artistry – for a range of participants within the ritual space.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "David Holm"
              }
            ],
            "files": []
          },
          {
            "id": 8339,
            "title": "“Said a Word, Uttered Thus”: Structures and Functions of Parallelism in Arhippa Perttunen’s Poems",
            "content": "<p>Verse parallelism is one of the most distinctive features of a Finnic tradition of oral poetry, which is called “kalevalaic poetry” in Finland or “regilaul” in Estonia. This essay presents grammatical and semantic principles and patterns according to which parallel verses are composed, and introduces a statistical analysis of parallelism in the repertoire of one singer of these poems. Verse parallelism is considered a constitutive feature which, alongside the meter and alliteration, defines this register. As the first line has normally the full referential power of a proposition, parallel lines add to this power and deepen and enrich description in the discourse.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jukka Saarinen"
              }
            ],
            "files": []
          },
          {
            "id": 8337,
            "title": "Parallelism and Orders of Signification (Parallelism Dynamics I)",
            "content": "<p>This essay sets out an approach to parallelism in verbal art as a semiotic phenomenon that can operate at multiple orders (or levels) of signification. It examines parallelism in the sounds through which words are communicated, in language communicated by those sounds, in symbols or minimal units of narration communicated through language, and then in more complex units of narration communicated through those symbols or units. Attention is given to how these different levels of parallelism interrelate and may diverge, while revealing that parallelism at all of these levels reflects a single semiotic phenomenon.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": " Frog"
              }
            ],
            "files": []
          },
          {
            "id": 8333,
            "title": "Parallelism in Karelian Laments",
            "content": "<p>Karelian laments are performed by women during a ritual – funerals, weddings, and recruiting ceremonies – and were once commonly used in other contexts of everyday life. Laments are works of a special kind of improvisation. They were created during the performance process in relation to a concrete situation, drawing upon traditional language, stylistic means and traditional themes. Among the main stylistic features of the Karelian lament poetry is an extensive use of different types of parallelism. This paper discusses parallelism in laments as one of the central conventional organizational parameters of the performance. Increased use of parallelism in Karelian laments was meaningful as an indicator of significance and emphasis. It is also possible to use parallelism to address the relationship between verbal art and experienced reality, a form of parallelism that would be connected to understandings of its ritual efficacy.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Eila Stepanova"
              }
            ],
            "files": [
              {
                "id": 8335,
                "title": "stepanova-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/stepanova-1.jpg",
                "caption": "Fig 1. Finnic linguistic-cultural areas."
              },
              {
                "id": 8336,
                "title": "stepanova-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/stepanova-2.jpg",
                "caption": "Fig 2. Regions of the Karelian Lament Tradition."
              }
            ]
          },
          {
            "id": 8331,
            "title": "Prayers for the Community: Parallelism and Performance in San Juan Quiahije Eastern Chatino",
            "content": "<p>La<sup>42</sup> qin<sup>4</sup> kchin<sup>4</sup> or ‘Prayers for the Community’ are supplications spoken by elders, traditional authorities, and virtuoso Chatino speakers from Oaxaca, Mexico. Chatino prayers are composed of varied, and complex forms of parallelism, repetition, and formulaic expressions. Units of meaning in these prayers are developed and presented in semantically and syntactically related stanzas consisting of any number of verses, including couplets, triplets, or quatrains. Chatino supplications achieve poetic tension, imagery, and metaphor through the extensive use of formulaic expressions, which are conventionally paired parallel words and phrases. These well established units of the poetic lexicon are part of the collective knowledge of the community. Formulaic expressions make extensive use of positional and existential predicates, making them challenging to translate into English or any Western languages.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Hilaria Cruz"
              }
            ],
            "files": []
          },
          {
            "id": 8326,
            "title": "Parallelism in Arandic Song-Poetry",
            "content": "<p>The ceremonial song-poetry performed by Arandic people of central Australia is characterized by parallelism of sound, form and meaning in both auditory and visual modalities.  Parallelism, in all its manifestations, operates at multiple levels of the hierarchically structured poetic form. In the period Arandic people call the <em>Altyerre</em>, “Dreaming,” ancestral spirit-beings created the land and laid the lore through actions and song. This included the creation of women’s song-poetry called <em>awelye</em>. <em>Awelye</em> is sung in group unison as a series of many short verses that relate to each group’s inherited estate lands, their ancestors, and to the ceremonial performance itself. Actions that mirror the meaning of the verses accompany the singing, such as painting designs on the body, placing a ritual object in the ground, and dancing. This paper considers the role of parallelism in the poetic function of language (Jakobson 1987), and facilitates the merging of the everyday realm with that of the performer’s ancestors, which Stanner so aptly translates as the “everywhen.”</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Myfany Tuprin"
              }
            ],
            "files": [
              {
                "id": 8328,
                "title": "turpin-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/turpin-1.jpg",
                "caption": "Fig. 1. Lena Ngal and Rosie Ngwarray performing the accompanying dance for the Alyawarr Antarrengeny verse “The headbands made it glisten / The shimmering horizon” (Turpin and Ross 2013:33)."
              },
              {
                "id": 8329,
                "title": "turpin-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/turpin-2.jpg",
                "caption": "Fig. 2. Map showing the Arandic varieties and two neighboring subgroups that have similar song-poetry (Ngumpin-Yapa and Western Desert)."
              },
              {
                "id": 8330,
                "title": "turpin-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31ii/full/turpin-3.jpg",
                "caption": "Fig. 3. Stages within an awelye performance. Songs that accompany actions are shaded, and their relative size gives an approximate indication of their length in performance."
              }
            ]
          },
          {
            "id": 8324,
            "title": "Twin Constellations: Parallelism and Stance in Stand-Up Comedy",
            "content": "<p>This paper addresses the interrelations between poetic parallelism and interactional stance-taking in stand-up comedy by examining commercially edited recordings of stand-up routines performed by two contemporary comics. Methodologically, the article suggests a heuristic distinction between 1) an approach to parallelism as a textual and rhetorical device based on sequential repetition of units of expression, and 2) a more positional or symbolic orientation that conceptualizes parallelism as a higher-order structural and functional principle. It is concluded that both types rely on iconic mappings across co-textual signs. The flexibility of parallelism is simultaneously proposed as affording diversity on the level of discursive presentation.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Antti Lindfors"
              }
            ],
            "files": []
          },
          {
            "id": 8322,
            "title": "Multimedial Parallelism in Ritual Performance (Parallelism Dynamics II)",
            "content": "<p>This article approaches parallelism as a semiotic phenomenon that can operate across verbal art and other media in performance. It presents an approach to different media and the uniting performance mode as construing “metered frames.” Multimedial parallelism is analyzed as a phenomenon resulting from the coordination of expressions in relation to these frames to form members of parallel groups. The focus is on rituals that involve interaction with the unseen world. Discussion of parallelism between speech and empirical aspects of performance extends to the potential for presumed parallelism between speech and unseen objects, agents, and forces. John Miles Foley’s concept of “performance arena” is extended to performers’ and audiences’ perceptions and expectations about “reality” in ritual performance. The mapping of otherworld locations and cosmology onto empirical spaces in performance is also discussed.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": " Frog"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 12,
            "issue_id": "10",
            "name": "Subtitle",
            "value": "Parallelism in Verbal Art and Performance"
          },
          {
            "id": 13,
            "issue_id": "10",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31ii/00_31.2cover.pdf"
          },
          {
            "id": 14,
            "issue_id": "10",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31ii/00_31.2cover.jpg"
          },
          {
            "id": 15,
            "issue_id": "10",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31ii/00_31.2fm.pdf"
          },
          {
            "id": 16,
            "issue_id": "10",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31ii/17_31.2bm.pdf"
          },
          {
            "id": 17,
            "issue_id": "10",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31ii/01_31.2.pdf"
          },
          {
            "id": 18,
            "issue_id": "10",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31ii/oraltradition_31_2.zip"
          },
          {
            "id": 19,
            "issue_id": "10",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31ii/00_31.2complete.pdf"
          },
          {
            "id": 20,
            "issue_id": "10",
            "name": "Volume",
            "value": "31"
          },
          {
            "id": 21,
            "issue_id": "10",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 22,
            "issue_id": "10",
            "name": "Date",
            "value": "2017-10-31"
          }
        ]
      },
      {
        "id": 24,
        "name": "31i",
        "title": "Volume 31, Issue 1",
        "articles": [
          {
            "id": 8377,
            "title": "Diachronic Homer and a Cretan <em>Odyssey</em>",
            "content": "<p>This essay explores the kaleidoscopic world of Homer and Homeric poetry from a combined diachronic and synchronic perspective. Linguists refer to “synchronic” as a given structure that exists in a given time and space, and “diachronic” as a structure that evolves through time. Nagy argues that from a diachronic perspective, the structure known as Homeric poetry can be viewed as an “evolving medium.” From a diachronic perspective, Homeric poetry is not only an evolving medium of oral poetry, but it is also a medium that actually views itself diachronically. In other words, Homeric poetry demonstrates aspects of its own evolution. A case in point is “the Cretan <em>Odyssey</em>”—or, better, “a Cretan <em>Odyssey</em>”—as reflected in the “lying tales” of Odysseus in the <em>Odyssey</em>. The author claims that these tales give the medium an opportunity to open windows into an <em>Odyssey</em> that is otherwise unknown. In the alternate universe of this “Cretan <em>Odyssey</em>,” the adventures of Odysseus take place in the exotic context of Minoan-Mycenaean civilization.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Gregory Nagy"
              }
            ],
            "files": [
              {
                "id": 8379,
                "title": "Nagy-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-1.jpg",
                "caption": "Fig. 1. As we will see, this image of sea-bound Crete will help visualize the importance of the sea for conceptualizing the Minoan Empire."
              },
              {
                "id": 8380,
                "title": "Nagy-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-2.jpg",
                "caption": "Fig. 2. Il Bucintoro on Ascension Day, c. 1780–1790, Francesco Guardi."
              },
              {
                "id": 8381,
                "title": "Nagy-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-3.jpg",
                "caption": "Fig. 3. Il Bucintoro at the Molo on Ascension Day, c. 1732. Canaletto."
              },
              {
                "id": 8382,
                "title": "Nagy-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-4.jpg",
                "caption": "Fig. 4. Il Bucintoro, the Doge’s ship of state, accompanied by a flotilla., c. 1609, Giacomo Franco."
              },
              {
                "id": 8383,
                "title": "Nagy-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-5.jpg",
                "caption": "Fig. 5. Theran fresco at Akrotiri showing flotilla; c. 1600 BCE."
              },
              {
                "id": 8384,
                "title": "Nagy-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-6.jpg",
                "caption": "Fig. 6. Detail from Theran fresco at Akrotiri, c. 1600 BCE."
              },
              {
                "id": 8385,
                "title": "Nagy-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-7.jpg",
                "caption": "Fig. 7. The Doge Sebastiano Ziani disembarking from il Bucintoro at the Convent of Charity. Miniature, 16th Century."
              },
              {
                "id": 8386,
                "title": "Nagy-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-8.mp4",
                "caption": "Fig. 8. A Venezia lo “sposalizio del mare,” video posted June 1, 2014."
              },
              {
                "id": 8387,
                "title": "Nagy-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-9.jpg",
                "caption": "Fig. 9. Bishop of Ravenna throws a ring into the Adriatic Sea as part of lo Sposalizio del Mare."
              },
              {
                "id": 8388,
                "title": "Nagy-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-10.jpg",
                "caption": "Fig. 10. Theran fresco at Akrotiri showing flotilla."
              },
              {
                "id": 8389,
                "title": "Nagy-11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-11.jpg",
                "caption": "Fig. 11. Detail from Theran fresco at Akrotiri, c. 1600 BCE."
              },
              {
                "id": 8390,
                "title": "Nagy-12.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-12.jpg",
                "caption": "Fig. 12. Detail from a fresco painting in Room 4 of the West House."
              },
              {
                "id": 8391,
                "title": "Nagy-13.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-13.jpg",
                "caption": "Fig. 13, sent to me by my friend Costas Tzagarakis, shows an assortment of garlands on sale in a marketplace. The flowers that make up the garland in this case are sempreviva (in Venetian Italian, it means “eternally alive”; the local Greeks think it is a local Greek word). The locale is Cythera. For more on this flower and on its symbolism, see Tzagarakis (2011)."
              },
              {
                "id": 8392,
                "title": "Nagy-14.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-14.jpg",
                "caption": "Fig.14. A votive offering or <em>tama</em>, showing <em>stephana</em> or “wedding garlands.”"
              },
              {
                "id": 8393,
                "title": "Nagy-15.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-15.jpg",
                "caption": "Fig. 15. Another votive offering or <em>tama</em>, showing <em>stephana</em> or “wedding garlands.”"
              },
              {
                "id": 8394,
                "title": "Nagy-16.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-16.jpg",
                "caption": "Fig. 16. From a fresco found at Hagia Triadha in Crete. Reconstruction by Mark Cameron, p. 96 of the catalogue Fresco: A Passport into the Past, 1999 (for an expanded citation, see the Bibliography below)."
              },
              {
                "id": 8395,
                "title": "Nagy-17.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-17.jpg",
                "caption": "Fig. 17. Detail from a fresco found at Hagia Triadha in Crete. Reconstruction by Mark Cameron, p. 96 of the catalogue <em>Fresco: A Passport into the Past</em>, 1999."
              },
              {
                "id": 8396,
                "title": "Nagy-18.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Nagy-18.jpg",
                "caption": "Fig. 18. Painting on a lekythos attributed to the Pan Painter, dated around 470 BCE (Taranto IG 4545). The line drawing, presented in rollout mode, is by Tina Ross."
              }
            ]
          },
          {
            "id": 8375,
            "title": "The Tale of Meleager in the <em>Iliad</em>",
            "content": "<p>This essay employs narratology and oral theory in a close reading of Phoenix’s tale of the Kalydonian hero, Meleager, in Book 9 of the <em>Iliad</em>. The author aims to clarify the function of this embedded narrative within the Homeric epic. Phoenix compares Achilles to Meleager, and the crux of the analogy—angry withdrawal from battle—has tempted some in the past to suppose that a pre-Homeric epic about an angry Meleager was the source for the <em>Iliad</em>’s angry Achilles. But since most ancient narratives about Meleager do not feature withdrawal from battle, Homerists today have more generally concluded that Phoenix invents Meleager’s withdrawal in order to pursue his analogy. Though Burgess essentially subscribes to this conclusion, analysis of the poetics of Phoenix’s narrative have often been misguided. This essay explores the traditionality of Phoenix’s story and its narratological construction in the Homeric epic. The main goal is to better calibrate the significance of the <em>Iliad</em>’s version of the story of Meleager. The issue is relevant to how the <em>Iliad</em> employs material from outside its boundaries, including the Epic Cycle.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jonathan Burgess"
              }
            ],
            "files": []
          },
          {
            "id": 8373,
            "title": "The Transformation of Cyavana: A Case Study in Narrative Evolution",
            "content": "<p>The assessment of possible genetic relationships between pairs of proposed narrative parallels currently relies on subjective conventional wisdom-based criteria. This essay presents an attempt at categorizing patterns of narrative evolution through the comparison of variants of orally-composed, fixed-text Sanskrit tales. Systematic examination of the changes that took place over the developmental arc of <em>The Tale of Cyavana</em> offers a number of insights that may be applied to the understanding of the evolution of oral narratives in general. An evidence-based exposition of the principles that govern the process of narrative evolution could provide more accurate diagnostic tools for evaluating narrative parallels.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Emily West"
              }
            ],
            "files": []
          },
          {
            "id": 8370,
            "title": "Beginning from the End: Strategies of Composition in Lyrical Improvisation with End Rhyme",
            "content": "<p>This essay examines the basic principles of constructing improvised verses with end rhyme in three contemporary cultures: <em>mandinadhes</em>, Mallorcan <em>gloses</em>, and Finnish freestyle rap. This study is based on ethnographic interviews, in which improvisers analyze their methods of composition. This knowledge is complemented by a textual analysis of examples of performances in the given traditions. Sykäri shows that competent improvisers master complex cognitive methods when they create their lines that end with the poetic device of end rhyme, and in particular when they structure the discourse so that the strong arguments are situated at the end of the structural unit of composition. This “reversed” method witnesses a tendency to use parallel phonic patterns in a way that is largely the opposite of those employed with semantic (or canonical) parallelism.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Venla Sykäri"
              }
            ],
            "files": [
              {
                "id": 8372,
                "title": "Sykari-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Sykari-1.jpg",
                "caption": "Fig. 1. The <em>glosadors</em> Mateu Matas “Xurí,” Maribel Servera, and Blai Salom performing at a <em>glosada</em>-event in Mallorca, January 23, 2015."
              }
            ]
          },
          {
            "id": 8366,
            "title": "“Hear the Tale of the Famine Year”: Famine Policy, Oral Traditions, and the Recalcitrant Voice of the Colonized in Nineteenth-Century India",
            "content": "<p>This essay considers the appearance of British colonial representations of Indian oral traditions in administrative documents concerning famine relief policies, as an example of the connection between colonial ethnography on the one hand and forms of disciplinary control on the other. Raheja analyzes eleven Hindi and Punjabi famine songs recorded in colonial texts, songs that addressed issues of hunger, famine, “custom,” and famine policy in tones of either compliance or critique. The singers of these songs occupied various locations within local hierarchies and with respect to the colonial state; she reads their words as historically situated, reflective, heterogeneous, and diversely positioned memory and critique. Raheja argues that while compliant native voices in a few of these songs were characterized as “the voice of the people” and entextualized in colonial documents to create an illusion of Indian consent to colonial rule, other voices–the voices of remonstrance and lament–were erased or marginalized or criminalized, to contribute to that same illusion.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Gloria Goodwin Raheja"
              }
            ],
            "files": [
              {
                "id": 8368,
                "title": "Raheja-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Raheja-1.jpg",
                "caption": "Fig. 1. Engraving from <em>Illustrated London News</em> of February 21, 1874, based on William Simpson’s 1866 ink and sepia wash sketch entitled “The Famine in India.”"
              },
              {
                "id": 8369,
                "title": "Raheja-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/31i/full/Raheja-2.jpg",
                "caption": "Fig. 2. William Simpson, “The Ganges Canal, Roorkee, Saharanpur District.”"
              }
            ]
          }
        ],
        "customFields": [
          {
            "id": 23,
            "issue_id": "24",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 24,
            "issue_id": "24",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31i/00_31.1cover.pdf"
          },
          {
            "id": 25,
            "issue_id": "24",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31i/00_31.1cover.jpg"
          },
          {
            "id": 26,
            "issue_id": "24",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31i/00_31.1fm.pdf"
          },
          {
            "id": 27,
            "issue_id": "24",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31i/07_31.1bm.pdf"
          },
          {
            "id": 28,
            "issue_id": "24",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31i/01_31.1.pdf"
          },
          {
            "id": 29,
            "issue_id": "24",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31i/oraltradition_31_1.zip"
          },
          {
            "id": 30,
            "issue_id": "24",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/31i/00_31.1complete.pdf"
          },
          {
            "id": 31,
            "issue_id": "24",
            "name": "Volume",
            "value": "31"
          },
          {
            "id": 32,
            "issue_id": "24",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 33,
            "issue_id": "24",
            "name": "Date",
            "value": "2017-03-31"
          }
        ]
      },
      {
        "id": 39,
        "name": "30ii",
        "title": "Volume 30, Issue 2: Authoritative Speech in the Himalayas",
        "articles": [
          {
            "id": 8473,
            "title": "Words of Truth: Authority and Agency in Ritual and Legal Speeches in the Himalayas",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Anne de Sales"
              },
              {
                "name": "Marie Lecomte-Tilouine"
              }
            ],
            "files": []
          },
          {
            "id": 8471,
            "title": "Authoritative Modes of Speech in a Central Himalayan Ritual",
            "content": "<p>The authoritative character of an utterance depends not only on the social identity of the speaker, but also on the aesthetic power of the style of speaking. Using Jakobson’s six functions of language as a framework, this paper presents two modes of speech realized in rituals of divine possession in the Central Himalayan region. The ritual forefronts two sources of authoritative language—a socially recognized master of ceremonies and an incarnate divinity—each of which has his or her own social source of authority, but also rely on marked forms of speech to solidify their role and convey their messages in an effective way.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Leavitt"
              }
            ],
            "files": []
          },
          {
            "id": 8457,
            "title": "The Untouchable Bard as Author of his Royal Patron: A Social Approach to Oral Epic Poetry in Western Nepal",
            "content": "<p>The article explores the complex relationship between a bard and his patron in western Nepal, that links together individuals situated at opposite extremes of the social hierarchy of the caste organization. During the bardic performance, their relationship reverses the caste hierarchy by bestowing authority to speak in the royal patron's name to an untouchable bard. The perpetuation of a fully oral bardic tradition, as well as its specific social setting in western Nepal, presents a rare opportunity to examine in all its complexity such a relationship within its context of enunciation and its wider social context. The article draws a sociological portrayal of the bard in his relation to his patron, explores the form of his art, and examines a new oral composition—an embryonic epic of the People’s War waged by the Maoist party in Nepal between 1996 and 2006—for a final discussion of the nature of the bardic contract, and the distribution of prestige it involves.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Marie Lecomte-Tilouine"
              }
            ],
            "files": [
              {
                "id": 8459,
                "title": "lecomte-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-1.jpg",
                "caption": "Fig. 1. A bard from Bajhang district holding his <em>huḍko</em> hourglass drum."
              },
              {
                "id": 8460,
                "title": "lecomte-video1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-video1.mp4",
                "caption": "Video 1. Late Damai, who was considered as the greatest bard and magician of Achham district, teaching his art to his grandson. Séance filmed in October 2007."
              },
              {
                "id": 8461,
                "title": "lecomte-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-2.jpg",
                "caption": "Fig. 2. The bards of Sera, Achham district, and their choristers."
              },
              {
                "id": 8462,
                "title": "lecomte-video2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-video2.mp4",
                "caption": "Video 2. The bards of Sera, Achham district, performing the opening dance of the bardic séance, on stage, Kathmandu valley, 2009."
              },
              {
                "id": 8463,
                "title": "lecomte-video3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-video3.mp4",
                "caption": "Video 3. Gome Damai, performing the epic of Kashiram, Dullu, Dailekh district, 2000."
              },
              {
                "id": 8464,
                "title": "lecomte-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-3.jpg",
                "caption": "Fig. 3. The bard’s skirt reaching the horizontal."
              },
              {
                "id": 8465,
                "title": "lecomte-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-4.jpg",
                "caption": "Fig. 4. The bard’s royal posture. Patal, Achham district, 2007."
              },
              {
                "id": 8466,
                "title": "lecomte-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-5.jpg",
                "caption": "Fig. 5: Sample of a text written in the bards’ cryptic alphabet."
              },
              {
                "id": 8467,
                "title": "lecomte-video4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-video4.mp4",
                "caption": "Video 4. Harka Bahadur Dholi, with his elder brother and his father (in bardic dress) performing the People’s epic, Sera, Achham district, 2007."
              },
              {
                "id": 8468,
                "title": "lecomte-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-8.jpg",
                "caption": "Map 1: Repartition of the bards in the districts of western Nepal. Black dots: lower status bards who perform with their wives. (Bajura and Kalikot have not yet been explored)."
              },
              {
                "id": 8469,
                "title": "lecomte-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-6.jpg",
                "caption": "Fig. 6. Bard from Thalara, Bajhang district (locally called <em>bhārate</em>, from the term <em>bhārat</em>, epic), 2014."
              },
              {
                "id": 8470,
                "title": "lecomte-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/lecomte-7.jpg",
                "caption": "Fig. 7. A low status bard from Ningala Saini, Baitadi district, performing with his wife, 2014."
              }
            ]
          },
          {
            "id": 8453,
            "title": "The Sources of Authority for Shamanic Speech: Examples from the Kham-Magar of Nepal",
            "content": "<p>This article tries to identify the sources of authority that allow the ritual specialists of the Kham-Magar community to act as its spokespersons with invisible partners and say the truth. The author partly challenges Bourdieu’s vision that ritual techniques such as ritual language are mainly techniques of domination. She explores, rather, the truth conditions of shamanic speech and the pragmatic effects of the ritual use of language, including a complex definition of the performer.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Anne de Sales"
              }
            ],
            "files": [
              {
                "id": 8455,
                "title": "desales-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/desales-1.jpg",
                "caption": "Fig. 1. A Kham-Magar shaman in full gear. The shaman is singing during the consecration of a neophyte. He wears a crown of female pheasant feathers on the head, and skins of animals are attached to his costume in the back. April 2006."
              },
              {
                "id": 8456,
                "title": "desales-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/desales-2.jpg",
                "caption": "Fig. 2. The blessing of the shamans by their main master. The master shaman put a protective soot mark on the forehead of his disciples before singing. April 2006."
              }
            ]
          },
          {
            "id": 8451,
            "title": "Meaning, Intention, and Responsibility in Rai Divinatory Discourse",
            "content": "<p>In speech act theory, as developed in ordinary language philosophy, the intentions of a speaker are regarded as decisive for the communication of meaning. This view, however, has been criticized by linguistic anthropologists as being culture-bound: in other cultural contexts with other notions of personhood the situations may be quite different. One case in point is that of divination, or divinatory speech, when the speaker is only the vehicle of another, higher authority. This paper examins the crucial part of a shamanic session (<em>cintā</em>) among the Rai in eastern Nepal, when the shaman (<em>jhā̃kri</em>) performs a divination (<em>bakhyāune</em>), diagnosing the state of the client household in Nepali language. The shaman is possessed by several divinities (such as Aitabare, Molu Sikari), who answer questions posed by the household and lineage elders. This raises a number of analytic issues: Who is held responsible for the diagnosis? What kind of language and imagery are used? And how is the meaning of the diagnosis—often expressed in ambivalent terms—established? The interpretation of textual and performative characteristics shows a more elaborate model of speech act participants is required to understand the complex agency involved. The divinities do have intentions, though it remains a collaborative task to read them properly.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Martin Gaenszle"
              }
            ],
            "files": []
          },
          {
            "id": 8431,
            "title": "The House of Letters: Musical Apprenticeship among the Newar Farmers (Kathmandu Valley, Nepal)",
            "content": "<p>This article explores the principles of musical discourse among the farmers of the Kathmandu Valley as revealed through the teaching of the <em>dhimay</em> drum. During ritual apprenticeship, the transmission of a corpus of musical compositions based on mimetic syllables that the discourse of authority of the masters expresses. Compositions played during religious processions originate from these syllables, which imitate the sounds of the drum. In addition, <em>dhimay</em> drum apprenticeship is inextricably linked to that of acrobatics, which includes the handling of a bamboo pole. This essay discusses the nature of this musical language in its traditional context, as well as its recent transformations in Newar society.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Franck Bernède"
              }
            ],
            "files": [
              {
                "id": 8433,
                "title": "bernede-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-1.jpg",
                "caption": "Fig. 1. Nāsadyaḥ. Sundari coka. Royal Palace, Patan."
              },
              {
                "id": 8434,
                "title": "bernede-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-2.jpg",
                "caption": "Fig. 2. <em>Boddhisatva</em> Padmanṛtyeśvara. Mural painting in the vestibule of the temple of Śāntipur, Svayambhūnāth, Kathmandu."
              },
              {
                "id": 8435,
                "title": "bernede-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-3.jpg",
                "caption": "Fig. 3 Nāsaḥ pvāḥ. Temple of Wangaḥ Duchenani, Kathmandu."
              },
              {
                "id": 8436,
                "title": "bernede-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-4.jpg",
                "caption": "Fig. 4 <em>Yantra</em> of Nāsadyaḥ."
              },
              {
                "id": 8437,
                "title": "bernede-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-5.jpg",
                "caption": "Fig. 5. <em>Nāsaḥ dhimay pūjā</em> at Tanani twāḥ, Kathmandu."
              },
              {
                "id": 8438,
                "title": "bernede-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-6.jpg",
                "caption": "Fig. 6. Singhinī and Vyāghrinī. Detail of the “Rāto Matsyendranātha Temple” paubhā, Rubin Museum of Art. C 2006-42.2 (Har 89010)."
              },
              {
                "id": 8439,
                "title": "bernede-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-7.jpg",
                "caption": "Fig. 7. Singhinī and Vyāghrinī. Detail of the “Rāto Matsyendranātha Temple” paubhā, Rubin Museum of Art. C 2006-42.2 (Har 89010)."
              },
              {
                "id": 8440,
                "title": "bernede-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-8.jpg",
                "caption": "Fig. 8. <em>Mā dhimay</em>. Seto Matsyendranātha <em>jātrā</em>."
              },
              {
                "id": 8441,
                "title": "bernede-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-9.jpg",
                "caption": "Fig. 9. <em>Yalepvaḥ dhimay</em>. Wotu twāḥ."
              },
              {
                "id": 8442,
                "title": "bernede-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-10.jpg",
                "caption": "Fig. 10. <em>Mā dhimay</em> drums decorated with <em>svastika</em>. Seto Matsyendranātha <em>jātrā</em>, Kathmandu."
              },
              {
                "id": 8443,
                "title": "bernede-11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-11.jpg",
                "caption": "Fig. 11. <em>ākhāḥ cheṃ</em> of Wotu twāḥ, Kathmandu."
              },
              {
                "id": 8444,
                "title": "bernede-12.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-12.jpg",
                "caption": "Fig. 12. Masters and students, Oṁ Bāhāḥ twāḥ."
              },
              {
                "id": 8445,
                "title": "bernede-13.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-13.jpg",
                "caption": "Fig. 13. Masters and students, Oṁ Bāhāḥ twāḥ."
              },
              {
                "id": 8446,
                "title": "bernede-14.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-14.jpg",
                "caption": "Fig. 14. <em>Māḥ thanegu</em>. Detail of the paubhā Dīpaṃkara Buddha, Rubin Museum."
              },
              {
                "id": 8447,
                "title": "bernede-video1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-video1.mp4",
                "caption": "House of Letters: Musical Apprenticeship among the Newar Farmers."
              },
              {
                "id": 8448,
                "title": "bernede-15.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-15.jpg",
                "caption": "Fig. 15. Raising of the <em>dhunyā</em> pole (<em>māḥ thanegu</em>), Oṁ Bāhāḥ twāḥ."
              },
              {
                "id": 8449,
                "title": "bernede-16.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-16.jpg",
                "caption": "Fig. 16. Closing ritual of the <em>dhimay syanegu</em>. Consecration of the drums and chanting of an auspicious song (mangalam mye)."
              },
              {
                "id": 8450,
                "title": "bernede-17.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/bernede-17.jpg",
                "caption": "Fig. 17. Jyāpuni of Tanani twāḥ playing the <em>yalepvaḥ dhimay</em>. Procession to the temple of Pacali Bhairava."
              }
            ]
          },
          {
            "id": 8415,
            "title": "Imparting and (Re-)Confirming Order to the World: Authoritative Speech Traditions and Socio-political Assemblies in Spiti, Upper Kinnaur, and Purang in the Past and Present",
            "content": "<p>This study focuses on speech traditions and socio-political assemblies in the Tibetan-speaking area of the Spiti Valley in the Northwest Indian state of Himachal Pradesh. Important comparative material is drawn from field research in the adjacent areas of Upper Kinnaur in Himachal Pradesh and from Purang County in the Ngari Prefecture of the Tibet Autonomous Region of China. In accordance with the structural setting, contexts, and functions of these assemblies, where performances of authoritative speeches usually take place, three categories of formal or authoritative speech tradition are identified: those with a primarily state-related political function that occurred in ancient periods, mainly in royal dynastic contexts; the context of community politics, associated mainly with local village contexts in modern times; and, finally, occasions in which mythological and religious functions are foregrounded—settings that may concern either village or monastic Buddhist contexts. Based on the use of written and oral sources (audio-visual recordings made in the field), and the application of social-anthropological and historical methods, selected historical and contemporary examples of such authoritative speech traditions are discussed and analyzed. These include, for example, authoritative speech (<em>molla</em>; <em>mol ba</em> in written Tibetan) at a wedding ceremony, and an oracular soliloquy made by the trance-medium of a local protective goddess in Tabo Village in Spiti Valley.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Christian Jahoda"
              }
            ],
            "files": [
              {
                "id": 8417,
                "title": "jahoda-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-1.jpg",
                "caption": "Fig. 1. Map of Historical Western Tibet."
              },
              {
                "id": 8418,
                "title": "jahoda-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-2.jpg",
                "caption": "Fig. 2. Wall paintings of rows of deities (upper registers) and historical figures (lower registers), Tabo monastery, Entry Hall, south wall."
              },
              {
                "id": 8419,
                "title": "jahoda-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-3.jpg",
                "caption": "Fig. 3. Nagarāja (Na ga ra dza), Yeshe Ö (Ye shes ’od), Devarāja (De ba ra dza) and other historical figures, Tabo monastery, Entry Hall, south wall."
              },
              {
                "id": 8420,
                "title": "jahoda-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-4.jpg",
                "caption": "Fig. 4. Wall paintings of rows historical figures, Tabo monastery, Entry Hall, north wall."
              },
              {
                "id": 8421,
                "title": "jahoda-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-5.jpg",
                "caption": "Fig. 5. Assembly of village people, Nako, Upper Kinnaur; celebration of Phingri (Bang ri?) festival."
              },
              {
                "id": 8422,
                "title": "jahoda-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-7.jpg",
                "caption": "Fig. 7: Beginning of a <em>mol ba</em> text, Spiti valley."
              },
              {
                "id": 8423,
                "title": "jahoda-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-7.jpg",
                "caption": "Fig. 7: Beginning of a <em>mol ba</em> text, Spiti valley."
              },
              {
                "id": 8424,
                "title": "jahoda-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-8.jpg",
                "caption": "Fig. 8: <em>Mes mes bu chen</em> during the performance of a play (<em>roam that</em>), Pin valley, Spiti."
              },
              {
                "id": 8425,
                "title": "jahoda-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-9.jpg",
                "caption": "Fig. 9: <em>Mes mes bu chen</em> during the recitation of a <em>roam that</em> story, Tabo monastery, Spiti."
              },
              {
                "id": 8426,
                "title": "jahoda-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-10.jpg",
                "caption": "Fig. 10. Performance of ritual speech songs by a lay village priest; manuscript detail, Sherken festival, Pooh, Upper Kinnaur."
              },
              {
                "id": 8427,
                "title": "jahoda-11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-11.jpg",
                "caption": "Fig. 11. Dabla (dGra lha) trance medium, <em>dog ra</em> ground, Sherken place, Sherken festival."
              },
              {
                "id": 8428,
                "title": "jahoda-12.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-12.jpg",
                "caption": "Fig. 12. Dorje Chenmo (rDo rje chen mo) trance medium performing a ritual speech (<em>mol ba</em>), Tabo, lower Spiti valley."
              },
              {
                "id": 8429,
                "title": "jahoda-13.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-13.jpg",
                "caption": "Fig. 13. Trance medium of the protectress Dorje Chenmo (rDo rje chen mo), Namkhan Festival, Tabo."
              },
              {
                "id": 8430,
                "title": "jahoda-14.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30ii/full/jahoda-14.jpg",
                "caption": "Fig. 14. Protectress Dorje Chenmo (rDo rje chen mo), Namtong festival, Khorchag monastery, Purang."
              }
            ]
          },
          {
            "id": 8413,
            "title": "The Authority of Law and the Production of Truth in India",
            "content": "<p>The paper addresses how evidentiary truth is constructed during a court hearing in a District Court of Himachal Pradesh, in Northern India. The paper focuses on how multiple narratives of the facts are produced through the judicial process, and how they are validated or invalidated from a legal point of view. By relying on a case study, the author shows how the power of language in a trial situation does not always rely on rhetorical skills, but, rather, on specific procedural rules that determine the evidentiary value of the witness’ statements. In the present case, the witness’s replies, expressed in simple terms, are sufficient to invalidate the prosecutor’s case. The paper shows that this is not due to the witness’s linguistic ability, but to the primacy accorded to procedure: it is precisely this primacy that renders the witness’ spoken words effective, regardless of their veracity.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Daniela Berti"
              }
            ],
            "files": []
          },
          {
            "id": 8411,
            "title": "Engendering Minorities in Nepal: The Authority of Legal Discourse and the Production of Truth",
            "content": "<p>Based on an anthropological analysis of Public Interest Litigation filed at the Supreme Court of Nepal on behalf of LGBTI rights, this article looks at the way in which invisible and unmentionable individuals were turned into an official minority, whose rights were to be enshrined in the constitution of Nepal (2015). It explores the mechanisms through which the performative utterance of the court shaped social realities in a context of conflicting public meanings, by establishing the “truth” about them. It shows, in particular, how the authority of the legal discourse was enlisted by activists and deployed by the Nepalese Supreme Court—through the judicial operations of codification, normalization, and institutionalization, in order to introduce new gender and sexual categories—and thus, to institutionalize a “new” minority based on these categories. It also analyzes how the overlapping authorities of scientific medical discourse, the international juridical framework, and the language of rights concur in empowering the activists’ claims and structuring the parallel discourse of the judges. Finally, it suggests that these authorities also concur, problematically, in defining the morphology of the “new” LGBTI minority and its official representations in a way that does not necessarily reflect local perceptions and self-representations.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Barbara Berardi Tadié"
              }
            ],
            "files": []
          },
          {
            "id": 8409,
            "title": "Authority, Status, and Caste Markers in Everyday Village Conversations: the Example of Eastern Nepal",
            "content": "<p>This study sets out to detect the various markers that express forms of caste and community belonging, and more generally, hierarchies in the language used in ordinary social interactions in villages in the hills of eastern Nepal. In this remote mountainous area, bilingualism is resilient among the Himalayan communities. The accents and syntactic variations of the Nepali, the language of inter-community relations, and more generally, of social relations, are gradually being replaced by a standard Nepali, taught in school, where differences become imperceptible. The study addresses the complex use of terms of address and honorific pronouns in common Nepali and focuses on the language spoken by the local headmen, notables, and politicians. It reflects the evolution of the standard of authority that, within several generations, has shifted from virile dominance to a more formal one based on moderation and restraint.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Pustak Ghimire"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 78,
            "issue_id": "39",
            "name": "Subtitle",
            "value": "Authoritative Speech in the Himalayas"
          },
          {
            "id": 79,
            "issue_id": "39",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30ii/00_30.2cover.pdf"
          },
          {
            "id": 80,
            "issue_id": "39",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30ii/30.2cover.jpg"
          },
          {
            "id": 81,
            "issue_id": "39",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30ii/00_30.2fm.pdf"
          },
          {
            "id": 82,
            "issue_id": "39",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30ii/12_30.2bm.pdf"
          },
          {
            "id": 83,
            "issue_id": "39",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30ii/01_30.2.pdf"
          },
          {
            "id": 84,
            "issue_id": "39",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30ii/oraltradition_30_2.zip"
          },
          {
            "id": 85,
            "issue_id": "39",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30ii/30.2complete.pdf"
          },
          {
            "id": 86,
            "issue_id": "39",
            "name": "Volume",
            "value": "30"
          },
          {
            "id": 87,
            "issue_id": "39",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 88,
            "issue_id": "39",
            "name": "Date",
            "value": "2016-10-31"
          }
        ]
      },
      {
        "id": 49,
        "name": "30i",
        "title": "Volume 30, Issue 1",
        "articles": [
          {
            "id": 8489,
            "title": "The Ch’orti’ Maya Myths of Creation",
            "content": "<p>The Ch’orti’ Maya of southern Guatemala have a rich mythology regarding the creation of the world and the human race.  In the last century, internal and external forces (religious and secular) weakened the transmission of oral narratives containing elements of their creation mythology. Hull draws information from all known Ch’orti’ sources relating to the Ch’orti’ Maya view of the creation and early destruction of the world and demonstrates that the disparate, surviving details of the Ch’orti’ creation myth provide a coherent narrative that continues to inform cultural practices today.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Kerry Hull"
              }
            ],
            "files": [
              {
                "id": 8491,
                "title": "Hull-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30i/full/Hull-1.jpg",
                "caption": "The “dawn” glyph comprised the sky and earth signs connected at one edge with the sun wedged between."
              }
            ]
          },
          {
            "id": 8486,
            "title": "Traditional Poetry in Contemporary Senegal: A Case Study of Wolof <em>Kasak</em> Songs",
            "content": "<p>Lo addresses Wolof <em>kasak</em> songs in Senegal. He examines the contextual frame in which they are performed, the system of values they convey, and aesthetic qualities embedded in them. This essay examines changes in poetic form as well as factors that have contributed to the contemporary diminishment of this genre in Wolof society. Lo claims that this traditional poetic culture, despite the disappearance of the cultural institution that gave birth to it, is being transformed, revived, and recuperated in novel forms informed by modern urban realities.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Cheikh Tidiane Lo"
              }
            ],
            "files": [
              {
                "id": 8488,
                "title": "Lo-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30i/full/Lo-1.mp4",
                "caption": "Example of neo-traditional revalorization of <em>kasak</em>."
              }
            ]
          },
          {
            "id": 8484,
            "title": "The Silent Debate Over the Igor Tale",
            "content": "<p>Mann develops an interpretation of the <em>Slovo o polka Igoreve</em> that focuses on its composer's expectations and its audience's perceptions of its discourse. He argues that passages which to modern readers seem difficult or \"obscure\" were readily comprehensible to 12th- and 13th-century audiences who were thoroughly familiar with the  epic’s traditional framework. \nHe points to stylistic features that most likely originated within an oral formulaic tradition and reviews arguments proffered by scholars who insist that the Igor Tale was composed by a highly literate author.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Robert Mann"
              }
            ],
            "files": []
          },
          {
            "id": 8482,
            "title": "James Weldon Johnson and the <em>Speech Lab Recordings</em>",
            "content": "<p>On December 24, 1935, James Weldon Johnson read thirteen of his poems at Columbia University, in a recording session engineered by Columbia Professor of Speech George W. Hibbitt and Barnard colleague Professor W. Cabell Greet, pioneers in the field that became sociolinguistics. Interested in American dialects, Greet and Hibbitt used early sound recording technologies to preserve dialect samples. In the same lab where they recorded T.S. Eliot, Gertrude Stein, and others, James Weldon Johnson read a selection of poems that included several from his seminal collection <em>God’s Trombones</em> and some dialect poems. Mustazza has digitized these and made them publicly available in the PennSound archive. In this essay, Mustazza contextualizes the collection, considering the recordings as sonic inscriptions alongside their textual manifestations. He argues that the collection must be heard within the frames of its production conditions—especially its recording in a speech lab—and that the sound recordings are essential elements in an hermeneutic analysis of the poems. He reasons that the poems’ original topics are reframed and refocused when historicized and contextualized within the frame of <em>The Speech Lab Recordings</em>.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Chris Mustazza"
              }
            ],
            "files": []
          },
          {
            "id": 8477,
            "title": "A New Algorithm for Extracting Formulas from  Poetic Texts and Formulaic Density of Russian <em>Bylinas</em>",
            "content": "<p>The problem of identifying formulas in poetic texts played an important role in folkloristics and several other fields, such as medieval literary studies, for more than fifty-odd years. Currently, there is no consensus as to what constitutes a formula or how the formulaic density of a given text should be computed. Nikolayev essays responds to these questions with a strict formal definition of a formula and a fast algorithm for computing formulaic density in any poetic text in any language (provided it is separated into lines and words). A case study of formulaic density in a corpus of Russian <em>bylinas</em> is marshaled to illustrate the methodology.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Dmitry Nikolayev"
              }
            ],
            "files": [
              {
                "id": 8479,
                "title": "Nikolaev-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30i/full/Nikolaev-1.jpg",
                "caption": "The difference in the mean formulaic density for the corpus computed with 4-symbol keys and 5-symbol keys (full words, essentially) does not exceed 2.5% (vertical bars indicate standard deviation)."
              },
              {
                "id": 8480,
                "title": "Nikolaev-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30i/full/Nikolaev-2.jpg",
                "caption": "An arc diagram of formulaic connections between the lines of the bylina <em>Dobrynya and the dragon</em>. Points on the horizontal axis represent the lines of the text arranged from left to right, and the arcs connect the lines having the same formulae."
              },
              {
                "id": 8481,
                "title": "Nikolaev-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/30i/full/Nikolaev-3.jpg",
                "caption": "A histogram of formulaic densities of the texts of <em>bylinas</em> in the corpus. The full list of values can be found in Appendix I."
              }
            ]
          },
          {
            "id": 8475,
            "title": "A Model of Defiance: Reimagining the Comparative Analysis of Concealed Discourse in Text",
            "content": "<p>This paper proposes that diverse oral-traditional cultures may use similar processes to conceal expressions of political or social subversion in a text. It introduces a new model to help identify concealed expressions of resistance and compare the processes involved in their disguise. Designed to tease out the relationships between the oral-derived text and the oral-traditional environment, the model frames the disguise process according to three principles: articulation, by which a text hides secondary meaning through its use of diction and syntax; construction, by which a text incorporates hidden meaning within its narrative or textual structure; and diversion, by which a text directs audience attention away from subversive meaning by focusing on other elements. With examples from the Babylonian Talmud, Homer’s <em>Odyssey</em>, and Maria Edgeworth’s <em>Castle Rackrent</em>, the paper demonstrates how these principles may be articulated.\nBecause the new model frames disguise processes rather than individual expressions of resistance, its value is not limited to the context of defiance, but should be viewed as a general tool for the systematic, comparative analysis of texts, enabling nuanced examination of cultural features that may be difficult to discern from textual sources.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jillian G. Shoichet"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 89,
            "issue_id": "49",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 90,
            "issue_id": "49",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30i/00_30.1cover.pdf"
          },
          {
            "id": 91,
            "issue_id": "49",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30i/30.1cover.jpg"
          },
          {
            "id": 92,
            "issue_id": "49",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30i/00_30.1fm.pdf"
          },
          {
            "id": 93,
            "issue_id": "49",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30i/08_30.1bm.pdf"
          },
          {
            "id": 94,
            "issue_id": "49",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30i/01_30.1.pdf"
          },
          {
            "id": 95,
            "issue_id": "49",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30i/oraltradition_30_1.zip"
          },
          {
            "id": 96,
            "issue_id": "49",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/30i/30.1complete.pdf"
          },
          {
            "id": 97,
            "issue_id": "49",
            "name": "Volume",
            "value": "30"
          },
          {
            "id": 98,
            "issue_id": "49",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 99,
            "issue_id": "49",
            "name": "Date",
            "value": "2016-03-25"
          }
        ]
      },
      {
        "id": 55,
        "name": "29ii",
        "title": "Volume 29, Issue 2: Transmissions and Transitions in Indian Oral Traditions",
        "articles": [
          {
            "id": 8513,
            "title": "Transmissions and Transitions in Indian Oral Traditions: An Introduction",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Kirin Narayan"
              }
            ],
            "files": []
          },
          {
            "id": 8511,
            "title": "“Who am I . . . what significance do I have?” Shifting Rituals, Receding Narratives, and Potential Change of the Goddess’ Identity in Gangamma Traditions of South India",
            "content": "<p>Burkhalter Flueckiger documents and analyzes numerous changes over the last 20 years in the narratives and rituals of the south Indian village goddess Gangamma in the south Indian pilgrimage town of Tirupati.  There have been radical transformations in ritual, architecture, and personnel serving the goddess at Gangamma’s largest temple, Tatayyaguta; and the local goddess’ narrative repertoire seems to be receding from the public imagination, even silenced, being unknown to many in the burgeoning <em>jatara</em> crowds drawn from beyond the boundaries of Tirupati. These changes raise questions about what each constituent part individually creates, their relationship, and what is lost or gained by narrative and ritual as they change. The essay answers: What is created when Sanskritic rituals (traditionally offered to <em>puranic</em> deities rather than village deities [<em>gramadevatas</em>] such as Gangamma) are added to temple service, when middle-class aesthetics influence architectural changes, and when Gangamma’s narratives are unknown to many ritual participants? How is the goddess’ identity potentially changing with these narrative and ritual shifts? Bringing a performative lens to older issues of the relationships between ritual and narrative, ethnographic and performance analyses of Gangamma ritual and narrative traditions show the finely tuned ways in which both are independent and codependent and reflect and create—with the potential to change—the identity of the goddess.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joyce Burkhalter Flueckiger"
              }
            ],
            "files": []
          },
          {
            "id": 8509,
            "title": "Kabīr: Oral to Manuscript Transitions",
            "content": "<p>Kabīr (ca. 1400-50) is one of the most famous poet saints of Northern India. Circulating in Indian oral traditions for over six centuries, his songs continue to be part of the lived experience of people in India. Beginning in the late-sixteenth century, Kabīr songs were written down in a variety of manuscript traditions associated with different communities. Friedlander examines how manuscript and print traditions offer a window on the contexts in which Kabīr songs flourished as oral traditions. He argues that the various forms the songs took as they transitioned from oral, manuscript, and print media vividly portray not only the voices of the singers, but also the worlds of the audiences who listened to Kabīr songs.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Peter Friedlander"
              }
            ],
            "files": []
          },
          {
            "id": 8502,
            "title": "Waiting for Moonrise: Fasting, Storytelling, and Marriage in Provincial Rajasthan",
            "content": "<p>Two popular Hindu women’s rituals in Rajasthan, North India, involve fasting until visible moonrise: <em>Bari Tij</em> (“Grand Third”) and <em>Karva Chauth</em> (“Pitcher Fourth”). Women vow to undertake these fasts in order to ensure their own auspicious married states by protecting their husbands’ longevity. On these occasions, groups of women, often neighbors, collectively perform rituals that include devotional storytelling. Drawing on 30 years of ethnographic fieldwork in different regions of Rajasthan, Grodzins Gold discusses ritual narratives and performative contexts comparatively, attending to rural/urban as well as generational and social differences. Tentative conclusions suggest that the appeal of fasts and accompanying rituals may lie, in part, in their ability to sustain an illusion of stability and continuity while incorporating processes of change.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ann Grodzins Gold"
              }
            ],
            "files": [
              {
                "id": 8504,
                "title": "gold-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29ii/full/gold-1.jpg",
                "caption": "Women in Jahazpur read the ritual story of Tij from a printed pamphlet."
              },
              {
                "id": 8505,
                "title": "gold-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29ii/full/gold-2.jpg",
                "caption": "Tij goddess shrine with <em>neem</em> leaves in Ghatiyali."
              },
              {
                "id": 8506,
                "title": "gold-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29ii/full/gold-3.jpg",
                "caption": "Tij goddess shrine with neem leaves in Jahazpur."
              },
              {
                "id": 8507,
                "title": "gold-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29ii/full/gold-4.jpg",
                "caption": "Offering tray with clay pitcher for Karva Chauth in Jahazpur."
              },
              {
                "id": 8508,
                "title": "gold-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29ii/full/gold-5.jpg",
                "caption": "Neighborhood women greet the moon on Karva Chauth in Jahazpur."
              }
            ]
          },
          {
            "id": 8500,
            "title": "The Social Life of Transcriptions: Interactions around Women’s Songs in Kangra",
            "content": "<p>Moving oral traditions into the domain of the printed word involves a first step of transcription. Narayan argues for the value of reflecting on the practice of transcribing and the interactions generated around the material artifacts that are made in the course of moving oral tradition into written form. Unlike fieldnotes, transcriptions can represent mutually recognizable fragments of shared cultural knowledge. Invaluable for eliciting oral literary criticism and facilitating interpretive collaborations, transcriptions can also represent material talismans of continued relationships across time. Though each researcher’s experience with transcriptions is likely to be shaped by genre, social context, and intended goals, Narayan draws on examples from her work with women’s songs in Kangra, in the Western Himalayan foothills, to illustrate how transcriptions are a site of sociable interaction and insight.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Kirin Narayan"
              }
            ],
            "files": []
          },
          {
            "id": 8498,
            "title": "Cordelia’s Salt: Interspatial Reading of Indic Filial-Love Stories",
            "content": "<p>Prasad re-tools Hannah Arendt’s concept of “interspace,” the world that emerges from the simultaneous recognition of the distinctiveness of subjects, their shared relations, and purposes. She compares discrepant related texts, acknowledging and bridging their distances. This is illustrated through a comparative and inter- and extra-textual reading of Shakespeare’s <em>King Lear</em> and variants of Indic filial-love folktales drawn from late nineteenth-century folktale collections and contemporary ethnographic studies. Prasad concludes that though Indic filial-love stories share some central predicaments and premises with <em>King Lear</em>, they propose different reconciliations to questions about selfhood, namely, “Who am I?” and “Who makes me?” Read together, Indic filial-love stories and <em>King Lear</em> illustrate that the self, always an intertext, finds itself by being symbiotically sovereign.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Leela Prasad"
              }
            ],
            "files": []
          },
          {
            "id": 8492,
            "title": "Ritual, Performance, and Transmission: The Gaddi Shepherds of Himachal Himalayas",
            "content": "<p><em>Nawala</em>, a sacrificial ritual offering made to Shiva, is central to the identity of the Gaddis, nomadic mountain shepherds of Himachal Pradesh in Northern India. Over a long period of time the Gaddis were encouraged to abandon shepherding and settle on the plains among agricultural communities. Displaced from their ancestral habitat and nomadic ways, the focus of their ritual performances changed. Examining one ritual text as a prism for folklore and social change, Sharma analyzes how dislocated “identity” negotiates and reinvents itself through ritual performance. The transformation of ritual text, disappearing lineages of traditional singers and ritual specialists, new interpretations and transmission agencies, as well as the eventual preservation and transmission of ritual-lore through audio and video technology, chart the trajectory of identity transformation and engagement with modernity.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mahesh Sharma"
              }
            ],
            "files": [
              {
                "id": 8494,
                "title": "sharma-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29ii/full/sharma-1.jpg",
                "caption": "A Gaddi couple in traditional dress, Chunaid village, Gadherana."
              },
              {
                "id": 8495,
                "title": "sharma-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29ii/full/sharma-2.jpg",
                "caption": "The Gaddis moved across altitudinal zones in their quest for pasturage. In this Kangra painting from the Archer collection, stylistically dated to c. 1900 (Archer 1973: II: 234. Pl. 76), the painter depicts one such family on the move with their flock—sheep and goats—that is guarded by the Gaddi shepherd dog. One can see the heavy load that they carried and how they stowed provisions in the loosely tied upper garment."
              },
              {
                "id": 8496,
                "title": "sharma-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29ii/full/sharma-3.jpg",
                "caption": "Kathu, of village Chaleda (Grima <em>panchayat</em>)."
              },
              {
                "id": 8497,
                "title": "sharma-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29ii/full/sharma-4.jpg",
                "caption": "Bhagal Ram of village Thanetar, Gadherana, along with his daughter and son-in-law."
              }
            ]
          }
        ],
        "customFields": [
          {
            "id": 100,
            "issue_id": "55",
            "name": "Subtitle",
            "value": "Transmissions and Transitions in Indian Oral Traditions"
          },
          {
            "id": 101,
            "issue_id": "55",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29ii/00_29.2cover.pdf"
          },
          {
            "id": 102,
            "issue_id": "55",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29ii/29.2cover.jpg"
          },
          {
            "id": 103,
            "issue_id": "55",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29ii/00_29.2fm.pdf"
          },
          {
            "id": 104,
            "issue_id": "55",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29ii/09_29.2bm.pdf"
          },
          {
            "id": 105,
            "issue_id": "55",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29ii/01_29.2.pdf"
          },
          {
            "id": 106,
            "issue_id": "55",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29ii/oraltradition_29_2.zip"
          },
          {
            "id": 107,
            "issue_id": "55",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29ii/29.2complete.pdf"
          },
          {
            "id": 108,
            "issue_id": "55",
            "name": "Volume",
            "value": "29"
          },
          {
            "id": 109,
            "issue_id": "55",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 110,
            "issue_id": "55",
            "name": "Date",
            "value": "2015-10-01"
          }
        ]
      },
      {
        "id": 62,
        "name": "29i",
        "title": "Volume 29, Issue 1",
        "articles": [
          {
            "id": 8533,
            "title": "Why Jews Quote",
            "content": "<p>Quotation is a feature of cultures throughout history and across continents. The purpose of this article is to present a phenomenology of quotation in Jewish culture, where it has had pride of place for millennia. Following a brief historical overview, six functions are discussed: quotation as 1) an engagement with tradition; 2) a means of resuscitating the quoted source; 3) legitimization of the quoter; 4) enrichment and embellishment of a text or oral performance; 5) an act of community-building; and 6) an incantatory act, a means of effecting change. In conclusion, the article suggests that quotation performs a rhapsodic function in the original sense of sewing together fragments in the creation of a thick fabric. Through quotation the Jew is in conversation with past, present, and future.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Michael Marmur"
              }
            ],
            "files": []
          },
          {
            "id": 8531,
            "title": "Voices from Kilbarchan: Two versions of “The Cruel Mother” from South-West Scotland, 1825",
            "content": "<p>This essay seeks to characterize the singing community of a nineteenth century Scottish village and to identify two distinct voices from the village repertoire. By closely analyzing two versions of the same ballad story recorded from two women on the same day in 1825, an attempt is made to demonstrate how the two singers manage to produce two distinct versions of the same ballad by employing the same traditional narrative techniques in slightly differing ways, thus allowing us a glimpse into the mechanisms of stability and variation in the ballad genre.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Flemming G. Andersen"
              }
            ],
            "files": []
          },
          {
            "id": 8527,
            "title": "“It may be verifyit that thy wit is thin”: Interpreting Older Scots Flyting through Hip Hop Aesthetics",
            "content": "<p>This study seeks to demystify the tradition of Older Scots flyting—a form of poetic invective unique to the late medieval Scottish court. Hip Hop battle raps provide a modern venue for exploring the motivations and potential rewards for engaging in this sort of technical poetic contest. The two forms, though culturally and historically distant, both exhibit analogous rhetorical techniques, which make this comparison possible. Each form is concerned with poetic identity—this is evident through each poet’s identification with specific communities or classes; while the concern with demonstrating superior technical skill is also essential to these invectives and is often highlighted through the manipulation of traditional forms and tropes. As an extension of this comparison, we hope to recover something of the tone and purpose of the medieval tradition, namely, that the poets who engaged in these public invectives were actually amicable rivals competing for increased court status and wealth.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Caitlin Flynn"
              },
              {
                "name": "Christy Mitchell"
              }
            ],
            "files": [
              {
                "id": 8529,
                "title": "flynn_mitchell-2.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29i/full/flynn_mitchell-2.mp3",
                "caption": "MC Shan, “Kill That Noise,” from <em>Down by Law (Deluxe Edition)</em> 2007, Cold Chillin’ Records, mp3. We direct your attention especially to 1:11-3:13."
              },
              {
                "id": 8530,
                "title": "flynn_mitchell-1.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29i/full/flynn_mitchell-1.mp3",
                "caption": "Boogie Down Productions, “South Bronx,” from <em>Criminal Minded (Elite Edition)</em> 2010, Traffic Entertainment, mp3. We direct your attention especially to 1:14-2:56."
              }
            ]
          },
          {
            "id": 8525,
            "title": "Seneca Storytelling: Effect of the Kinzua Dam on Interpretations of Supernatural Stories",
            "content": "<p>This paper analyzes supernatural lore of the <em>Hodinöhŝyönih</em> (Iroquois) of the Northeastern United States, specifically the Seneca people living near the Kinzua Dam. After briefly tracing the historical richness of Iroquois and Seneca stories about supernatural beings and occurrences, it offers interpretations on these stories that are pertinent to a new generation. Traditional storytellers such as the various Indigenous peoples of North America use their tales as vehicles for instruction in their communities; an important part of this instruction is maintenance and strengthening of cultural traditions within communities and families. The building of the Kinzua Dam on the Seneca Allegany Territory in the late 1960s and subsequent upheavals in the community have deepened the tradition of stories about supernatural incidents while serving as a means through which old stories have gained strength and guided those removed by the dam’s construction to overcome those tragic events.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Melissa Borgia"
              }
            ],
            "files": []
          },
          {
            "id": 8523,
            "title": "Hesiod and <em>Hávamál</em>: Transitions and the Transmission of Wisdom",
            "content": "<p>This article offers fresh insights into Hesiod’s <em>Works and Days</em> by comparing it with the Eddic <em>Hávamál</em>, a didactic poem far removed in terms of geography and date, but compellingly close in subject matter, construction, and transmission. It finds parallels between the poems in the methods of teaching and in what is being taught, focusing on the shared theme of self-sufficiency (both intellectual and practical). It finds parallels in structure, as <em>Hávamál</em> is, like the <em>Works and Days</em>, made up not only of precepts and maxims but also of elaborate mythological sections, and is associated with catalogic elements which may be original or later accretions, just like Hesiod’s <em>Days</em>, or the <em>Catalogue of Women</em>, or the <em>Ornithomanteia</em>. This article also traces parallel scholarly trajectories, exploring the strikingly similar histories of scholarship on the two poems. Finally, this paper finds parallels in the transmission of the poems, as both are rooted in the oral tradition but poised at a crucial juncture: the advent of writing. Despite the striking similarities between the poems, this paper refrains from any suggestion of a straight channel of reception but rather interprets the parallels as a reflection of comparable societies, or at least societies at comparable points in their developments. Archaic Greece and Viking Scandinavia might not be exactly parallel cultures, but they evidently share certain cultural concerns: as agrarian societies with strong family and household structures, polytheistic religions and honor codes, they offer similar advice in similar formulations through similar didactic strategies. Such similarities may encourage us to think in terms of the shared characteristics of transitional products. If we exclude direct reception we are left with a cultural constant: the transmission of wisdom. And with recurring elements such as gnomic language, myth and catalogue, we are also left with constant expressions of that wisdom.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lilah Grace Canevaro"
              }
            ],
            "files": []
          },
          {
            "id": 8517,
            "title": "The Oral Poetics of Professional Wrestling, or Laying the Smackdown on Homer",
            "content": "<p>While typically used to analyze oral poetry and oral poetic traditions, composition in performance theory can be used to illuminate other types of art. One of these is professional wrestling, whose narrative structure and ways to make meaning remain largely unstudied. Through composition in performance theory, we can see that professional wrestling maneuvers are in fact formulae, serving to provide consistent and repeatable meaning rather than verisimilitude. Wrestling’s use of discrete narratives given meaning by an expansive and unending storyline is best understood with this theory. The study of wrestling can also inform investigations into the traditions typically studied through composition in performance theory, particularly concerning how formulae come into use and disuse.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William Duffy"
              }
            ],
            "files": [
              {
                "id": 8519,
                "title": "duffy-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29i/full/duffy-1.jpg",
                "caption": "Shawn Michaels and Marty Jannetty, wrestling together as The Rockers. Note the identical outfits and move lists, which formulaically links them together as a unit."
              },
              {
                "id": 8520,
                "title": "duffy-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29i/full/duffy-2.jpg",
                "caption": "Shawn Michaels’ “Heel turn” on Marty Jannetty. Note the repeated references to past events, and to the dramatic change in Michaels’ clothing, showing the break with the previous character and its formulae."
              },
              {
                "id": 8521,
                "title": "duffy-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29i/full/duffy-3.jpg",
                "caption": "The Hulk Hogan Leg Drop, known for the past thirty years as a devastating finisher."
              },
              {
                "id": 8522,
                "title": "duffy-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/29i/full/duffy-4.jpg",
                "caption": "This maneuver is a transitional move in a match, <em>not</em> a finisher."
              }
            ]
          },
          {
            "id": 8515,
            "title": "Hades’ Famous Foals and the Prehistory of Homeric Horse Formulas",
            "content": "<p>This article investigates Hades’ enigmatic Homeric epithet κλυτόπωλος (“of famous foals”) in an attempt to identify its origins within the linguistic history of Homeric composition. Through an analysis of the phonetic similarities that κλυτόπωλος shares with other Homeric expressions, it becomes possible to identify a broad network of formulas to which this epithet must be genetically related. Furthermore, analysis of this network as a whole allows insight into the diachronic development of a wide range of Homeric equine formulas, from rather deep Indo-European antiquity to archaic Greece.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ryan Platte"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 111,
            "issue_id": "62",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 112,
            "issue_id": "62",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29i/00_29.1cover.pdf"
          },
          {
            "id": 113,
            "issue_id": "62",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 114,
            "issue_id": "62",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29i/00_29.1fm.pdf"
          },
          {
            "id": 115,
            "issue_id": "62",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29i/09_29.1bm.pdf"
          },
          {
            "id": 116,
            "issue_id": "62",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29i/01_29.1.pdf"
          },
          {
            "id": 117,
            "issue_id": "62",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29i/oraltradition_29_1.zip"
          },
          {
            "id": 118,
            "issue_id": "62",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/29i/29.1complete.pdf"
          },
          {
            "id": 119,
            "issue_id": "62",
            "name": "Volume",
            "value": "29"
          },
          {
            "id": 120,
            "issue_id": "62",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 121,
            "issue_id": "62",
            "name": "Date",
            "value": "2014-03-01"
          }
        ]
      },
      {
        "id": 71,
        "name": "28ii",
        "title": "Volume 28, Issue 2: Archives, Databases, and Special Collections",
        "articles": [
          {
            "id": 8656,
            "title": "Orality and Technology, or the Bit and the Byte: The Work of the World Oral Literature Project",
            "content": "<p>Exploring the role of literacy and technology in the production and documentation of oral literature, this essay focuses on the activities of World Oral Literature Project. Digital archiving, community partnerships, sustainable funding models, and online publishing are addressed citing examples of successful and collaborative interventions. The essay makes the case for open knowledge partnerships that acknowledge community ownership of cultural traditions and proposes equitable and transparent database structures and curatorial practices.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark Turin"
              }
            ],
            "files": []
          },
          {
            "id": 8654,
            "title": "Noting the Tunes of Seventeenth-Century Broadside Ballads: The English Broadside Ballad Archive (EBBA)",
            "content": "<p>This essay focuses on the ways a digital archive of printed, pre-1701 English broadside ballads facilitates and complicates the orality that folklorists have traditionally seen in opposition to print. The English Broadside Ballad Archive (EBBA) recaptures the multi-media nature of early broadside ballads—as text, art, <em>and song</em>—providing recordings of all extant tunes for the printed ballads and, in doing so, capturing the ease and difficulties of matching text to tune.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Patricia Fumerton"
              },
              {
                "name": "Eric Nebeker"
              }
            ],
            "files": []
          },
          {
            "id": 8638,
            "title": "On the Principles of a Digital Text Corpus: New Opportunities in Working on Heroic Epics of the Shors",
            "content": "<p>This essay discusses the main principles of a Digital Text Corpus initiated in 2011 with support from the Department of Northern and Siberian Studies at the Institute of Ethnology and Anthropology of the Russian Academy of Sciences. With special focus on the vast <em>Shor</em> (a Turkic people in the south of Western Siberia) materials, the essay showcases how this Corpus offers unique and varied means for analyzing folklore texts in lesser used, mostly endangered Siberian languages.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Dmitri A. Funk"
              }
            ],
            "files": [
              {
                "id": 8640,
                "title": "funk-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-1.jpg",
                "caption": "Main territories now occupied by the Shors."
              },
              {
                "id": 8641,
                "title": "funk-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-2.jpg",
                "caption": "Proportions of the numbers of the Shor epic texts recorded by scholars between 1861-2006."
              },
              {
                "id": 8642,
                "title": "funk-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-3.jpg",
                "caption": "Volume of the Shor Corpus (with a list of the 20 most frequent word-forms)."
              },
              {
                "id": 8643,
                "title": "funk-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-4.jpg",
                "caption": "The structure of the Shor Corpus."
              },
              {
                "id": 8644,
                "title": "funk-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-5.jpg",
                "caption": "Vladimir Tannagashev in his apartment (in the kitchen) in the town of Myski, Kemerovo region, 2003."
              },
              {
                "id": 8645,
                "title": "funk-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-6.jpg",
                "caption": "List of epic texts from the Tannagashev&rsquo;s repertoire in the Shor Corpus."
              },
              {
                "id": 8646,
                "title": "funk-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-7.jpg",
                "caption": "Taken by an anonymous photographer on June 15, 1969. The picture is accompanied by Torbokov&rsquo;s note in Russian: &ldquo;(I am) reciting (the epos) Ak-Salgyn [&ldquo;White Wind&rdquo;] by accompanying on the kay-komus.&rdquo;"
              },
              {
                "id": 8647,
                "title": "funk-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-8.jpg",
                "caption": "An original version of the epos Ak-Pilek given in parallel with the standardized (normalized) one."
              },
              {
                "id": 8648,
                "title": "funk-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-9.jpg",
                "caption": "A scanned page from Tannagashev&rsquo;s self-recording of the epos Ak-Pilek."
              },
              {
                "id": 8649,
                "title": "funk-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-10.jpg",
                "caption": "A list of word-forms from the epos <em>Ak-Pilek</em> with examples."
              },
              {
                "id": 8650,
                "title": "funk-11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-11.jpg",
                "caption": "Search options."
              },
              {
                "id": 8651,
                "title": "funk-12.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-12.jpg",
                "caption": "Information about the word-form pagda (&ldquo;on a leash&rdquo;) (with graphic representation and contextual examples)."
              },
              {
                "id": 8652,
                "title": "funk-13.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-13.jpg",
                "caption": "Recurring expressions in the eposes Ak-Pilek and Altyn-Torgu (with the level of similarity of 0.900 and higher)."
              },
              {
                "id": 8653,
                "title": "funk-1.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/funk-1.mp3",
                "caption": "A short excerpt of the epos <em>Chylan-Toochii</em>, Vladimir Tannagashev.&rdquo;"
              }
            ]
          },
          {
            "id": 8631,
            "title": "The Working Papers of Iona and Peter Opie",
            "content": "<p>This essay describes the archival collection of Iona and Peter Opie, the English folklorists who published a series of classic books on children’s folklore in the second half of the twentieth century. It focuses particularly on their working papers at the Bodleian Libraries, which contain the responses of schoolchildren in the United Kingdom to the Opies’ questionnaires and the Opies’ research notes for their books.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Julia C. Bishop"
              }
            ],
            "files": [
              {
                "id": 8633,
                "title": "bishop-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/bishop-1.jpg",
                "caption": "Iona and Peter Opie skipping."
              },
              {
                "id": 8634,
                "title": "bishop-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/bishop-2.jpg",
                "caption": "Extract from &ldquo;The Oral Lore of Schoolchildren II&rdquo; questionnaire (box 11)."
              },
              {
                "id": 8635,
                "title": "bishop-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/bishop-3.jpg",
                "caption": "Extract from a response to the Opies&rsquo; second questionnaire, boy aged 12-13, Ecclesfield Grammar School, 1954 (box 7)."
              },
              {
                "id": 8636,
                "title": "bishop-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/bishop-4.jpg",
                "caption": "Extract from an essay on &ldquo;My Favourite Game,&rdquo; boy aged 8, St. Winefride&rsquo;s School, Manor Park, London (1962), Opie Working Papers, Special Collections, Bodleian Libraries (box 30)."
              },
              {
                "id": 8637,
                "title": "bishop-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/bishop-5.jpg",
                "caption": "Transcription of the clapping game &ldquo;Eeny Meeny Dessameeny&rdquo; as filmed at Monteney Primary School, Sheffield in 2009."
              }
            ]
          },
          {
            "id": 8625,
            "title": "The Matti Kuusi International Database of Proverbs",
            "content": "<p>Based on Matti Kuusi’s library of proverb collections, the Matti Kuusi International Database of Proverbs is designed for cultural researchers, translators, and proverb enthusiasts. Kuusi compiled a card-index with tens of thousands of references to synonymic proverbs. The database is a tool for studying global similarities and national specificities of proverbs. This essay offers a practical introduction to the database, its classification system, and describes future plans for improvement of the database.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Outi Lauhakangas"
              }
            ],
            "files": [
              {
                "id": 8627,
                "title": "lauhakangas-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/lauhakangas-1.jpg",
                "caption": "Browsing the Matti Kuusi International Database of Proverbs."
              },
              {
                "id": 8628,
                "title": "lauhakangas-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/lauhakangas-2.jpg",
                "caption": "Search results for &ldquo;one-eyed&rdquo; in the Matti Kuusi International Database of Proverbs."
              },
              {
                "id": 8629,
                "title": "lauhakangas-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/lauhakangas-3.jpg",
                "caption": "Illustration of the classification structure surrounding the English proverb type &ldquo;In the kingdom of the blind the one-eyed man is a king.&rdquo;"
              },
              {
                "id": 8630,
                "title": "lauhakangas-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/lauhakangas-4.jpg",
                "caption": "Extract from an essay on &ldquo;My Favourite Game,&rdquo; boy aged 8, St. Winefride&rsquo;s School, Manor Park, London (1962), Opie Working Papers, Special Collections, Bodleian Libraries (box 30)."
              }
            ]
          },
          {
            "id": 8620,
            "title": "The SKVR Database of Ancient Poems of the Finnish People in Kalevala Meter and the Semantic Kalevala",
            "content": "<p>This essay presents the principal aims of establishing a web-based infrastructure for epic research and demonstrates possibilities for using this infrastructure for textual research on epics. The web infrastructure discussed here has developed out of the Finnish Literature Society’s pioneering open-access digitized edition of Kalevala-meter poetry <em>Suomen Kansan Vanhat Runot</em> (“The Ancient Poems of the Finnish People”; 34 volumes, SKVR) and the Semantic Kalevala, an electronic indexing of the Finnish national epic <em>Kalevala</em> that uses the Culture Sampo web portal.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lauri Harvilahti"
              }
            ],
            "files": [
              {
                "id": 8622,
                "title": "harvilahti-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/harvilahti-1.jpg",
                "caption": "A visualization of the narrative structure of one of the poems of the <em>Kalevala</em> in the Culture Sampo. The cantos (poems) of the <em>Kalevala</em> are listed on the left, the text of the beginning of the fortieth canto is in the middle, and the plot structure of the episodes of that canto on the right, in this case the origin of the <em>kantele</em> (a musical instrument) made of fish bones."
              },
              {
                "id": 8623,
                "title": "harvilahti-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/harvilahti-2.jpg",
                "caption": "A screenshot from the SKVR database: a search of poems containing the formula <em>The kantele got finished</em>. (The list of poems appears on the left, and the text of our example is on the right.)"
              },
              {
                "id": 8624,
                "title": "harvilahti-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/harvilahti-3.jpg",
                "caption": "A search for the formula <em>The Kannel got finished</em> from the Estonian Runic Songs Database."
              }
            ]
          },
          {
            "id": 8613,
            "title": "The Heritage of Australian Children’s Play and Oral Tradition",
            "content": "<p>The <em>Childhood, Tradition, and Change</em> project undertook the largest nation-wide study of children’s games and play culture in Australia in 2007-11 and resulted in a substantial archive of visual, oral, and written materials. Much of it is available on an open-access website, including details of a variety of games. This essay traces the project’s methodology and findings and examines how the online resource can contribute to understanding among scholars and laypersons of the dynamic heritage of Australian children’s play.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Kate Darian-Smith"
              }
            ],
            "files": [
              {
                "id": 8615,
                "title": "darian-smith-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/darian-smith-1.jpg",
                "caption": "Some children still learn the skills attached to the traditional string games."
              },
              {
                "id": 8616,
                "title": "darian-smith-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/darian-smith-2.jpg",
                "caption": "A game called &ldquo;The Greatest&rdquo; that involves a lot of physical challenges."
              },
              {
                "id": 8617,
                "title": "darian-smith-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/darian-smith-3.jpg",
                "caption": "Playing a clapping game &ldquo;Miss Moo.&rdquo;"
              },
              {
                "id": 8618,
                "title": "darian-smith-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/darian-smith-4.jpg",
                "caption": "A Fairy Couch."
              },
              {
                "id": 8619,
                "title": "darian-smith-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/darian-smith-5.jpg",
                "caption": "Playing Baby&rsquo;s Cradle on the monkey bars."
              }
            ]
          },
          {
            "id": 8604,
            "title": "Ukrainian Folklore Audio",
            "content": "<p>Ukrainian Folklore Audio is a website that relies on crowdsourcing and allows the public to hear songs, stories, and beliefs recorded in Ukraine and among the Ukrainian diaspora of Kazakhstan. Crowdsourcing of field recordings engages the public transcribing and translating audio files. Crowdsourcing also benefits the researcher by allowing observation of public reaction to heritage texts that bear intense emotional significance. Thus, crowdsourcing allows observation of real responses and provides information about heritage.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Natalie Kononenko"
              }
            ],
            "files": [
              {
                "id": 8606,
                "title": "kononenko-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/kononenko-1.jpg",
                "caption": "Our original searchable audio website."
              },
              {
                "id": 8607,
                "title": "kononenko-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/kononenko-2.jpg",
                "caption": "An example of a search through &ldquo;Wedding&rdquo; subtopics."
              },
              {
                "id": 8608,
                "title": "kononenko-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/kononenko-3.jpg",
                "caption": "An example of the background information to a sound file."
              },
              {
                "id": 8609,
                "title": "kononenko-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/kononenko-4.jpg",
                "caption": "The opening page of the Ukrainian Folklore Audio project."
              },
              {
                "id": 8610,
                "title": "kononenko-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/kononenko-5.jpg",
                "caption": "An example of a page showing already processed files and files available for processing."
              },
              {
                "id": 8611,
                "title": "kononenko-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/kononenko-6.jpg",
                "caption": "An example of &ldquo;published&rdquo; transcriptions and translations."
              },
              {
                "id": 8612,
                "title": "kononenko-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/kononenko-7.jpg",
                "caption": "An example of the page that a registered visitor sees."
              }
            ]
          },
          {
            "id": 8602,
            "title": "Curation of Oral Tradition from Legacy Recordings: An Australian Example",
            "content": "<p>Oral tradition recorded by linguists in the course of their fieldwork needs to be archived for longterm access and re­use. This article discusses the Pacific and Regional Archive for Digital Sources in Endangered Cultures (PARADISEC) and its importance in preserving endangered records of indigenous cultures. Established in 2003 to digitize and curate the legacy made by Australian academic researchers since the 1960s, PARADISEC archives the contents of various collections, ranging across various language traditions, genres, and performance contexts.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Nick Thieberger"
              }
            ],
            "files": []
          },
          {
            "id": 8600,
            "title": "The Digital Archiving of Endangered Language Oral Traditions: <em>Kaipuleohone</em> at the University of Hawai‘i and <em>C’ek’aedi Hwnax</em> in Alaska",
            "content": "<p>This essay compares and contrasts two small-scale digital endangered language archives with regard to their relevance for oral tradition research. The first is a university-based archive curated at the University of Hawai‘i, which is designed to house endangered language materials arising from the fieldwork of university researchers. The second is an indigenously-administered archive in rural Alaska that serves the language maintenance needs of the Ahtna Athabaskan Alaska Native community.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Andrea L. Berez"
              }
            ],
            "files": []
          },
          {
            "id": 8598,
            "title": "The Sirat Bani Hilal Digital Archive",
            "content": "<p>This essay provides a description of the Sirat Bani Hilal Digital Archives of the University of California, Santa Barbara, including an account of the original fieldwork and collection of the materials in an Egyptian village in the 1980s, the funding history of the archives, historical background about the Arabic oral epic tradition of Sirat Bani Hilal, and an account of the special features of this collection on interest to researchers in a variety of fields.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Dwight F. Reynolds"
              }
            ],
            "files": []
          },
          {
            "id": 8589,
            "title": "Medieval Storytelling and Analogous Oral Traditions Today: Two Digital Databases",
            "content": "<p>This essay presents two open-access digital databases of video clips of modern performances of medieval narratives and analogous living oral storytelling traditions: <em>Performing Medieval Narrative Today: A Video Showcase</em> and <em>Arthurian Legend in Performance</em>. To help people recognize the performability of medieval narratives, these websites offer examples of medieval-type storytelling that are still alive today in various parts of the world, as well as clips from performances of medieval narrative created by a new generation of storytellers. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Evelyn Birge Vitz"
              },
              {
                "name": "Marilyn Lawrence"
              }
            ],
            "files": [
              {
                "id": 8591,
                "title": "vitz-lawrence-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/vitz-lawrence-1.jpg",
                "caption": "The home page of <em>Performing Medieval Narrative Today: A Video Showcase</em>."
              },
              {
                "id": 8592,
                "title": "vitz-lawrence-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/vitz-lawrence-2.jpg",
                "caption": "The <em>jyrau</em> Jumabay Bazarov performs the Turkic <em>Edige</em> in Karakalpakistan, Uzbekistan."
              },
              {
                "id": 8593,
                "title": "vitz-lawrence-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/vitz-lawrence-3.jpg",
                "caption": "Awadallah Abd aj-Jalil Ali performs the Egyptian Hilali epic <em>Abu Zayd</em> in Aswan, Egypt."
              },
              {
                "id": 8594,
                "title": "vitz-lawrence-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/vitz-lawrence-4.jpg",
                "caption": "Benjamin Bagby performs the Anglo-Saxon epic <em>Beowulf</em> in Helsingborg, Sweden."
              },
              {
                "id": 8595,
                "title": "vitz-lawrence-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/vitz-lawrence-5.jpg",
                "caption": "Benjamin Bagby performs the Anglo-Saxon epic <em>Beowulf</em> in New York."
              },
              {
                "id": 8596,
                "title": "vitz-lawrence-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/vitz-lawrence-6.jpg",
                "caption": "The homepage of the website <em>Arthurian Legend in Performance</em>."
              },
              {
                "id": 8597,
                "title": "vitz-lawrence-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/vitz-lawrence-7.jpg",
                "caption": "Matthieu Boyd performs the <em>Welsh Triads</em> in New York."
              }
            ]
          },
          {
            "id": 8587,
            "title": "Estonian Folklore Archives",
            "content": "<p>This essay presents the history and development of the The Estonian Folklore Archives, the central folklore archives of Estonia established in 1927, and examines its function in relation to the development of online databases. The Archives’ primary purpose has been to make the manuscript materials easily available for researchers working with the collection. The collections consist of manuscripts, sound recordings, photographs, and film and video materials, with an experimental multimedia collection as of 2008. The most extensive project related to the collections was the digitization in 2011-12 of Jakob Hurt’s folklore collection, stored in <em>Kivike</em>, a new file repository and archival infosystem of the Estonian Literary Museum.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Risto Järv"
              }
            ],
            "files": []
          },
          {
            "id": 8585,
            "title": "A Jukebox Full of Stories",
            "content": "<p>This essay describes Project Jukebox, an online program that features oral history recordings associated texts and visuals. The program offers thematic comparisons and the ability to search by individual contributor.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William Schneider"
              }
            ],
            "files": []
          },
          {
            "id": 8577,
            "title": "The James Madison Carpenter Collection of Traditional Song and Drama",
            "content": "<p>This essay describes the folklore collection of James Madison Carpenter (1888-1983), who gathered an extensive body of folk songs and mummers’ plays in the United States and particularly in England and Scotland from 1928-35. The essay illustrates the interest of some of these items, including the ballads and the previously little-studied “dreg songs” of Scottish oyster fishers. It also outlines the Carpenter Collection Project to publish the collection in a critical edition.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Julia C. Bishop"
              },
              {
                "name": "David  Atkinson"
              },
              {
                "name": "Robert Young Walser"
              }
            ],
            "files": [
              {
                "id": 8579,
                "title": "bishop-atkinson-walser-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/bishop-atkinson-walser-1.jpg",
                "caption": "James Madison Carpenter, <em>circa</em> 1938."
              },
              {
                "id": 8580,
                "title": "bishop-atkinson-walser-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/bishop-atkinson-walser-2.jpg",
                "caption": "James Madison Carpenter in his Austin roadster, <em>circa</em> 1929."
              },
              {
                "id": 8581,
                "title": "bishop-atkinson-walser-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/bishop-atkinson-walser-3.jpg",
                "caption": "Sam Bennett, Ilmington Morris fiddler, Warwickshire, <em>circa</em> 1933."
              },
              {
                "id": 8582,
                "title": "bishop-atkinson-walser-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/bishop-atkinson-walser-4.jpg",
                "caption": "Bell Duncan of Lambhill, Aberdeenshire, knitting outside her home, <em>circa</em> 1930."
              },
              {
                "id": 8583,
                "title": "bishop-atkinson-walser-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/bishop-atkinson-walser-5.jpg",
                "caption": "Text transcription of &ldquo;Hoodah Day,&rdquo; as sung by Captain Edward B. Trumbull, Salem, Massachusetts."
              },
              {
                "id": 8584,
                "title": "bishop-atkinson-walser-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/bishop-atkinson-walser-6.jpg",
                "caption": "Music transcription of &ldquo;The Wife of Usher&rsquo;s Well&rdquo; (Child 79), as sung by Mrs. Annie Kidd, Glen Ythan, Rothienorman, Aberdeenshire."
              }
            ]
          },
          {
            "id": 8574,
            "title": "<em>Cnuasach Bhéaloideas Éireann</em>: The National Folklore Collection, University College Dublin",
            "content": "<p> <em>Cnuasach Bhéaloideas Éireann</em>, The National Folklore Collection at University College Dublin and successor to the Irish Folklore Commission (1935-1970), consists of manuscript, photographic, and audio/video archives, as well as a specialist library and a music archive. It contains two million manuscript pages, thousands of hours of audio recordings, 80,000 photographs, and a number of paintings. The specialist library holds some 50,000 items relating to Irish and comparative folklore and ethnology. It is tasked with the preservation, dissemination, and augmentation of the collections.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ríonach uí Ógáin"
              }
            ],
            "files": [
              {
                "id": 8576,
                "title": "ogain-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/ogain-1.jpg",
                "caption": "Transcription of &ldquo;Ceaite na gCuach&rdquo; from fieldwork by Séamus Ennis."
              }
            ]
          },
          {
            "id": 8569,
            "title": "The Folk Literature of the Sephardic Jews Digital Library",
            "content": "<p>The essay describes the digital library containing audio files and transcriptions of Hispanic ballads and other oral literary genres sung by Sephardic Jewish informants and collected initially by the late Professors Samuel Armistead and Joseph Silverman, later joined by the ethnomusicologist Israel Katz. Beginning in 1957 their fieldwork was conducted over several decades in North America, North Africa, Israel, Greece, and the Balkans.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bruce Rosenstock"
              },
              {
                "name": "Belén Bistué"
              }
            ],
            "files": [
              {
                "id": 8571,
                "title": "rosenstock-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/rosenstock-1.jpg",
                "caption": "Homepage for The Folk Literature of the Sephardic Jews Digital Library."
              },
              {
                "id": 8572,
                "title": "rosenstock-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/rosenstock-2.jpg",
                "caption": "All transcriptions as listed in the database&rsquo;s &ldquo;browse&rdquo; function."
              },
              {
                "id": 8573,
                "title": "rosenstock-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/rosenstock-3.jpg",
                "caption": "Sample transcription of a ballad, associated metadata, and audio player link."
              }
            ]
          },
          {
            "id": 8567,
            "title": "MacEdward Leach and the Songs of Atlantic Canada",
            "content": "<p>MacEdward Leach made the earliest ethnographic recordings of folksong on both Cape Breton Island and Newfoundland, recordings that remained largely unknown for over 50 years. This essay describes the history of the <em>MacEdward Leach and the Songs of Atlantic Canada</em> website and the efforts to provide context for the songs for two key audiences: the academy in general and the local community who understand the songs as a cultural inheritance.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Beverley Diamond"
              },
              {
                "name": "Ian Brodie"
              }
            ],
            "files": []
          },
          {
            "id": 8554,
            "title": "The Milman Parry Collection of Oral Literature",
            "content": "<p>This essay provides a brief historical sketch of the Milman Parry Collection of Oral Literature and describes recent efforts to provide electronic access to the contents of the archive. It concludes with thoughts about fruitful areas for new research on the materials in the Parry Collection.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "David F. Elmer"
              }
            ],
            "files": [
              {
                "id": 8556,
                "title": "elmer-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/elmer-1.jpg",
                "caption": "Nikola Vujnović, singing for Parry&rsquo;s Parlograph, Dubrovnik, 1934."
              },
              {
                "id": 8557,
                "title": "elmer-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/elmer-2.jpg",
                "caption": "Parry&rsquo;s phonograph recording apparatus in use in the village of Kijevo (Croatia). The amplifier and embossing units can be seen on the left side of the image; a singer, Ante Cicvarić, seated before the microphone, is visible in the doorway on the right."
              },
              {
                "id": 8558,
                "title": "elmer-3-1-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/elmer-3-1-1.jpg",
                "caption": "Screen shot 1 of 4 representing the first of three possible searches for PN 662, Alija Fjuljanin&rsquo;s &ldquo;Halil Hrnjičić i Miloš Keserdžija.&rdquo;"
              },
              {
                "id": 8559,
                "title": "elmer-3-1-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/elmer-3-1-2.jpg",
                "caption": "Screen shot 2 of 4 representing the first of three possible searches for PN 662, Alija Fjuljanin&rsquo;s &ldquo;Halil Hrnjičić i Miloš Keserdžija.&rdquo;"
              },
              {
                "id": 8560,
                "title": "elmer-3-1-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/elmer-3-1-3.jpg",
                "caption": "Screen shot 3 of 4 representing the first of three possible searches for PN 662, Alija Fjuljanin&rsquo;s &ldquo;Halil Hrnjičić i Miloš Keserdžija.&rdquo;"
              },
              {
                "id": 8561,
                "title": "elmer-3-1-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/elmer-3-1-4.jpg",
                "caption": "Screen shot 4 of 4 representing the first of three possible searches for PN 662, Alija Fjuljanin&rsquo;s &ldquo;Halil Hrnjičić i Miloš Keserdžija.&rdquo;"
              },
              {
                "id": 8562,
                "title": "elmer-3-2-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/elmer-3-2-1.jpg",
                "caption": "Screen shot 1 of 2 representing the second of three possible searches for PN 662, Alija Fjuljanin&rsquo;s &ldquo;Halil Hrnjičić i Miloš Keserdžija.&rdquo;"
              },
              {
                "id": 8563,
                "title": "elmer-3-2-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/elmer-3-2-2.jpg",
                "caption": "Screen shot 2 of 2 representing the second of three possible searches for PN 662, Alija Fjuljanin&rsquo;s &ldquo;Halil Hrnjičić i Miloš Keserdžija.&rdquo;"
              },
              {
                "id": 8564,
                "title": "elmer-3-3-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/elmer-3-3-1.jpg",
                "caption": "Screen shot 1 of 2 representing the third of three possible searches for PN 662, Alija Fjuljanin&rsquo;s &ldquo;Halil Hrnjičić i Miloš Keserdžija.&rdquo;"
              },
              {
                "id": 8565,
                "title": "elmer-3-3-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/elmer-3-3-2.jpg",
                "caption": "Screenshot 2 of 2 representing the third of three possible searches for PN 662, Alija Fjuljanin&rsquo;s &ldquo;Halil Hrnjičić i Miloš Keserdžija.&rdquo;"
              },
              {
                "id": 8566,
                "title": "elmer-1.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/elmer-1.mp3",
                "caption": "Alija Fjuljanin in (PN 662, &ldquo;Halil Hrnjičić i Miloš Keserdžija&rdquo; [&ldquo;Halil Hrnjičić and Miloš the Highwayman&rdquo;]), November, 1934."
              }
            ]
          },
          {
            "id": 8549,
            "title": "From Spoken Word to Digital Corpus: The Calum Maclean Project",
            "content": "<p>This essay describes an online, digital corpus of Gaelic folklore collected by the late Calum Maclean (1915-1960), Scotland’s most prolific ethnology fieldworker. The collection consists of over 13,000 transcribed items recorded primarily in Gaelic with English summaries provided. Technical aspects of the resource, along with its uses in ethnological research are summarized. The project was carried out with funding from the Arts and Humanities Research Council (United Kingdom).</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Shaw"
              },
              {
                "name": "Andrew Wiseman"
              }
            ],
            "files": [
              {
                "id": 8551,
                "title": "shaw-wiseman-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/shaw-wiseman-1.jpg",
                "caption": "Angus MacMillan, Moss Cottage, Griminish, Benbecula, recording on the Ediphone for Calum Maclean in 1947."
              },
              {
                "id": 8552,
                "title": "shaw-wiseman-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/shaw-wiseman-2.jpg",
                "caption": "Data Flow Diagram."
              },
              {
                "id": 8553,
                "title": "shaw-wiseman-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/shaw-wiseman-3.jpg",
                "caption": "Screenshot of the Calum Maclean Project website."
              }
            ]
          },
          {
            "id": 8547,
            "title": "<em>Cartlanna Sheosaimh Uí Éanaí</em>: The Joe Heaney Archives",
            "content": "<p>This essay outlines the creation of the Joe Heaney digital archives. A renowned traditional performer from Galway, Ireland, Heaney (1919-1984) became a professional singer and was awarded the National Endowment for the Arts in 1982. The creation of the Archives was supported by the Irish Research Council and in various ways by the National University of Ireland, Galway and the University of Washington, where much of the original material is housed. The Archives seek to redress neglect of Heaney’s memory in the years since his death.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lillis Ó Laoire"
              }
            ],
            "files": []
          },
          {
            "id": 8539,
            "title": "The Philippine Epics and Ballads Multimedia Archive",
            "content": "<p>This essay offers an introduction to the Philippine Epics and Ballads Archive. This collection is a joint endeavor between singers, scholars, knowledgeable local persons, and technical assistants. This archive exemplifies a part of the cultural heritage among 15 national cultural communities and their respective languages. A multi-media eCompanion offers an interactive version of a Palawan epic song.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Nicole Revel"
              }
            ],
            "files": [
              {
                "id": 8541,
                "title": "revel-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/revel-1.jpg",
                "caption": "Chart of the nine categories of verbal arts in the Palawan language and their respective translations in English."
              },
              {
                "id": 8542,
                "title": "revel-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/revel-2.jpg",
                "caption": "Map of the Philippine Archipelago, with locations of epic collection indicated (red: animists groups; green: Islamicized groups; blue: Christianized groups)."
              },
              {
                "id": 8543,
                "title": "revel-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/revel-3.jpg",
                "caption": "The overall arrangement of the Phillipine Epics and Ballads Archive."
              },
              {
                "id": 8544,
                "title": "revel-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/revel-4.jpg",
                "caption": "A sample beginning of an archive listing (in this case for the Tala-andig linguistic group)."
              },
              {
                "id": 8545,
                "title": "revel-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/revel-5.jpg",
                "caption": "A small sample of the Palawan and English versions of <em>Mämiminbin</em>."
              },
              {
                "id": 8546,
                "title": "revel-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/revel-6.jpg",
                "caption": "<em>Le chant d&rsquo;une épopée palawan / The Song of a Palawan Epic: Mämiminbin</em>, Literature of the Voice 1."
              }
            ]
          },
          {
            "id": 8535,
            "title": "The Archive of the Indigenous Languages of Latin America: An Overview",
            "content": "<p>The Archive of the Indigenous Languages of Latin America (AILLA) is a digital repository of audio, video, and textual materials concerning the indigenous languages of Latin America. In this essay we describe the archive, its mission, and its importance; we discuss the precursors that led to its establishment; we illuminate some of the collections in the archive; and we illustrate ways in which people have used the materials they find in AILLA.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Susan Smyth Kung"
              },
              {
                "name": "Joel Sherzer"
              }
            ],
            "files": [
              {
                "id": 8537,
                "title": "sherzer-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/sherzer-1.jpg",
                "caption": "AILLA Resource Information for KPC003R000."
              },
              {
                "id": 8538,
                "title": "sherzer-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28ii/full/sherzer-2.jpg",
                "caption": "AILLA Resource Information for TZO004R003."
              }
            ]
          }
        ],
        "customFields": [
          {
            "id": 122,
            "issue_id": "71",
            "name": "Subtitle",
            "value": "Archives, Databases, and Special Collections"
          },
          {
            "id": 123,
            "issue_id": "71",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28ii/00_28.2cover.pdf"
          },
          {
            "id": 124,
            "issue_id": "71",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 125,
            "issue_id": "71",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28ii/00_28.2fm.pdf"
          },
          {
            "id": 126,
            "issue_id": "71",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28ii/25_28.2bm.pdf"
          },
          {
            "id": 127,
            "issue_id": "71",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28ii/01_28.2.pdf"
          },
          {
            "id": 128,
            "issue_id": "71",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28ii/oraltradition_28_2.zip"
          },
          {
            "id": 129,
            "issue_id": "71",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28ii/28.2complete.pdf"
          },
          {
            "id": 130,
            "issue_id": "71",
            "name": "Volume",
            "value": "28"
          },
          {
            "id": 131,
            "issue_id": "71",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 132,
            "issue_id": "71",
            "name": "Date",
            "value": "2013-10-01"
          }
        ]
      },
      {
        "id": 102,
        "name": "28i",
        "title": "Volume 28, Issue 1",
        "articles": [
          {
            "id": 8716,
            "title": "Text and Memory in the “Oral” Transmission of a Crime and Execution Ballad: “The Suffolk Tragedy” in England and Australia",
            "content": "<p>A news ballad published in connection with a notorious murder trial in England in 1828 (the Maria Marten case) is here juxtaposed with versions of the song recorded from New South Wales in the twentieth century. The aim is to establish exactly the impact of oral tradition on content and form. The textual and contextual information provided by Australian folklorists permits deep insight into the way singers handle their songs and relate to their role as bearers of tradition.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Tom Pettitt"
              }
            ],
            "files": [
              {
                "id": 8718,
                "title": "Sally SloaneSingsMariaMarten.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/Sally SloaneSingsMariaMarten.mp3",
                "caption": "Sally Sloane sings “The Red Barn.”"
              },
              {
                "id": 8719,
                "title": "pettitt-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/pettitt-2.jpg",
                "caption": "Four pages from the “songbook” of Lily Bobbin (“Aunt Lil”)."
              },
              {
                "id": 8720,
                "title": "pettitt-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/pettitt-3.jpg",
                "caption": "Page 1 of 4 from the “songbook” of Lily Bobbin (“Aunt Lil”)."
              },
              {
                "id": 8721,
                "title": "pettitt-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/pettitt-4.jpg",
                "caption": "Page 2 of 4 from the “songbook” of Lily Bobbin (“Aunt Lil”)."
              },
              {
                "id": 8722,
                "title": "pettitt-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/pettitt-5.jpg",
                "caption": "Page 3 of 4 from the “songbook” of Lily Bobbin (“Aunt Lil”)."
              },
              {
                "id": 8723,
                "title": "pettitt-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/pettitt-6.jpg",
                "caption": "Page 4 of 4 from the “songbook” of Lily Bobbin (“Aunt Lil”)."
              },
              {
                "id": 8724,
                "title": "pettitt-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/pettitt-7.jpg",
                "caption": "Carrie Milliner’s Written Reconstruction - two pages."
              },
              {
                "id": 8725,
                "title": "pettitt-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/pettitt-8.jpg",
                "caption": "Carrie Milliner’s Written Reconstruction - page 1 of 2."
              },
              {
                "id": 8726,
                "title": "pettitt-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/pettitt-9.jpg",
                "caption": "Carrie Milliner’s Written Reconstruction - page 2 of 2."
              },
              {
                "id": 8727,
                "title": "CarrieMillinerFirstPerformance.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/CarrieMillinerFirstPerformance.mp3",
                "caption": "Carrie Milliner sings “Maria Marten”: first performance."
              },
              {
                "id": 8728,
                "title": "CarrieMillinerSecondPerformance.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/CarrieMillinerSecondPerformance.mp3",
                "caption": "Carrie Milliner sings “Maria Marten”: second perfor-mance."
              }
            ]
          },
          {
            "id": 8696,
            "title": "In and Out of Culture: Okot p’Bitek’s Work and Social Repair in Post-Conflict Acoliland",
            "content": "<p>Acknowledging the angst of Acoli youths and elders regarding the youths’ disconnection from culture (<em>tekwaro</em>) after decades of war, this essay examines post-conflict processes of social repair in rural Northern Uganda in light of the much overlooked academic work of Acoli scholar and writer Okot p’Bitek. It explores how the recognition of p’Bitek’s holistic approach to Acoli culture, personhood, and oral tradition as social action facilitates a place-based understanding of post-conflict transitions.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lara Rosenoff Gauvin"
              }
            ],
            "files": [
              {
                "id": 8698,
                "title": "gauvin-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-1.jpg",
                "caption": "Picture 1. Padibe Internally Displaced Person’s Camp, Northern Uganda. January 2007."
              },
              {
                "id": 8699,
                "title": "gauvin-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-2.jpg",
                "caption": "Picture 2. Beatrice and Kilama start to clear land for farming after living in Padibe IDP camp for 5 years. July 2008."
              },
              {
                "id": 8700,
                "title": "gauvin-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-3.jpg",
                "caption": "Picture 3. Author washing dishes in front of her hut in Pabwoc-East village. August 2012."
              },
              {
                "id": 8701,
                "title": "gauvin-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-4.jpg",
                "caption": "Picture 4. This boy escaped from the Lord’s Resistance Army (LRA) Rebel group five days before this photo was taken. The LRA is made up mostly of abducted children. October 2004."
              },
              {
                "id": 8702,
                "title": "gauvin-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-5.jpg",
                "caption": "Picture 5. Up to 50,000 children walked each night to the three city centers in Northern Uganda at the height of the war in order to protect themselves against abduction by the Lord’s Resistance Army. October 2004."
              },
              {
                "id": 8703,
                "title": "gauvin-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-6.jpg",
                "caption": "Picture 6. Fires that spread quickly throughout the IDP camps were quite common due to massive overcrowding. Anaka IDP camp, March 2005."
              },
              {
                "id": 8704,
                "title": "gauvin-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-7.jpg",
                "caption": "Picture 7. Beatrice harvests peas (<em>lapena</em>) on her neighbor’s plot just outside Padibe IDP camp. January 2007."
              },
              {
                "id": 8705,
                "title": "gauvin-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-8.jpg",
                "caption": "Picture 8. Throughout the long war, residents were wholly dependent on food aid from the World Food Program for survival. Purongo IDP camp, March 2005."
              },
              {
                "id": 8706,
                "title": "gauvin-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-9.jpg",
                "caption": "Picture 9. Augustine (the author’s research assistant and friend) drew this map to represent his home village of Pabwoc-East. March 2012."
              },
              {
                "id": 8707,
                "title": "gauvin-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-10.jpg",
                "caption": "Picture 10. The author’s family’s homestead in Pabwoc-East village, Lamwo district, Northern Uganda. September 2012."
              },
              {
                "id": 8708,
                "title": "gauvin-11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-11.jpg",
                "caption": "Picture 11. Wang’oo (nightly fireside gathering) in Pabwoc-East village. August 2012."
              },
              {
                "id": 8709,
                "title": "gauvin-12.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-12.jpg",
                "caption": "Picture 12. Okot p’Bitek."
              },
              {
                "id": 8710,
                "title": "gauvin-13.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-13.jpg",
                "caption": "Picture 13. Dorkus leads the author to her garden, explaining who gardens in the other plots along the way, where the next villages begin, and what the histories are between Pabwoc-East and their neighbors. July 2012."
              },
              {
                "id": 8711,
                "title": "gauvin-14.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-14.jpg",
                "caption": "Picture 14. Mama (the author’s adoptive mother) instructs the author and other young girls on how to serve the evening family (often extended) meal. April 2012."
              },
              {
                "id": 8712,
                "title": "gauvin-15.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-15.jpg",
                "caption": "Portrait 1. Almarina and Justo (husband and wife). April 2012."
              },
              {
                "id": 8713,
                "title": "gauvin-16.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-16.jpg",
                "caption": "Portrait 2. Yolanda. September 2012."
              },
              {
                "id": 8714,
                "title": "gauvin-17.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-17.jpg",
                "caption": "Portrait 3. Alice and Grace (co-wives). August 2012."
              },
              {
                "id": 8715,
                "title": "gauvin-18.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/gauvin-18.jpg",
                "caption": "Portrait 4. Okot. September 2012."
              }
            ]
          },
          {
            "id": 8687,
            "title": "The Beast Had to Marry Balinda: Using Story Examples to Explore Socializing Concepts in Ugandan Caregivers’ Oral Stories",
            "content": "<p>Within the context of storytelling as oral tradition, this essay uses a grounded theory approach to explore a single research question about the socializing concepts found in examples of stories told to young children by their mothers and grandmothers in a rural Ugandan village. These story examples were gathered during the implementation of a socio-educational intervention project. This essay provides a descriptive analysis of the emergent themes and constructs in these story examples against the backdrop of a relevant theoretical framework and life in this rural Ugandan village.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Geoff Goodman"
              },
              {
                "name": "Valeda Dent Goodman"
              }
            ],
            "files": [
              {
                "id": 8689,
                "title": "dent-goodman-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/dent-goodman-1.jpg",
                "caption": "The original Kitengesa Community Library, built in 2002."
              },
              {
                "id": 8690,
                "title": "dent-goodman-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/dent-goodman-2.jpg",
                "caption": "The new Kitengesa Community Library, built in 2009."
              },
              {
                "id": 8691,
                "title": "dent-goodman-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/dent-goodman-3.jpg",
                "caption": "A woman taking her cattle to pasture."
              },
              {
                "id": 8692,
                "title": "dent-goodman-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/dent-goodman-4.jpg",
                "caption": "Children carrying kindling for their households."
              },
              {
                "id": 8693,
                "title": "dent-goodman-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/dent-goodman-5.jpg",
                "caption": "Children carrying a chicken home."
              },
              {
                "id": 8694,
                "title": "dent-goodman-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/dent-goodman-6.jpg",
                "caption": "A few of the participants wait in the Library for the interviews to begin."
              },
              {
                "id": 8695,
                "title": "dent-goodman-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/dent-goodman-7.jpg",
                "caption": "The research team: Julius Ssentume, Geoff Goodman, Valeda Dent, Ssewa Baker, and Karen Gubert."
              }
            ]
          },
          {
            "id": 8682,
            "title": "Unity and Difference in Andean Songs",
            "content": "<p>This essay explores the concepts of “unity” and “difference” in Andean songs. The verses pertain to the Masha ritual enacted annually in Mangas, central Peru, and combine Quechua (the indigenous language) with Spanish. Through detailed exegesis of the texts, this essay argues that, far from being irreconcilable, “unity” and “difference” are best understood as mutually informing since the recognition of difference opens up the parameters of potential exchange.  This optic is informed by a worldview that emphasizes “relation” over “entities.”</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Charles Maurice Pigott"
              }
            ],
            "files": [
              {
                "id": 8684,
                "title": "pigott-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/pigott-1.jpg",
                "caption": "The seventeenth-century church in Mangas, focus of the Masha festival."
              },
              {
                "id": 8685,
                "title": "pigott-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/pigott-2.jpg",
                "caption": "The church’s straw roof, which is traditionally replaced every year."
              },
              {
                "id": 8686,
                "title": "pigott-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/pigott-3.jpg",
                "caption": "The main square of Mangas, which separates Kotos from Allawkay, and where the bullfight is enacted."
              }
            ]
          },
          {
            "id": 8666,
            "title": "Cultural Circles and Epic Transmission: The Dai People in China",
            "content": "<p>This essay discusses the dual influences of Buddhist culture and indigenous religion found in Dai communities in terms of “cultural circles” and demonstrates that all Dai traditional poetry—Buddhist and indigenous—employs a key technique that can be termed “waist-feet rhyme,” wherein the last syllable of one line rhymes with an internal syllable in the succeeding line. This feature is embedded in both the oral and written traditions and is an important enabling device within the poetry of the Dai people.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Qu Yongxian"
              }
            ],
            "files": [
              {
                "id": 8668,
                "title": "qu-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-1.jpg",
                "caption": "Map 1. Yunnan Province, China."
              },
              {
                "id": 8669,
                "title": "qu-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-2.jpg",
                "caption": "Map 2. Dehong Prefecture and Xishuangbanna Prefecture, Yunnan Province."
              },
              {
                "id": 8670,
                "title": "qu-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-3.jpg",
                "caption": "Chart 1. The branches of Dai in China."
              },
              {
                "id": 8671,
                "title": "qu-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-4.jpg",
                "caption": "Chart 2. The four dialects of the Dai language."
              },
              {
                "id": 8672,
                "title": "qu-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-5.jpg",
                "caption": "Chart 3. Three kinds of Dai scripts in current use."
              },
              {
                "id": 8673,
                "title": "qu-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-6.jpg",
                "caption": "Photo 1. A Dai-luo elder who was pasturing cattle when I arrived at Lengdun Town, Yuanyang County, Honghe Prefecture.<p><br />Members of this group call themselves Dai-luo or Dai-dam (“Black Dai,” in relation to their traditional black dress), but outsiders usually categorize them as one of the Hua-yao Dai. They wear the drum-type skirt, while the upper outer garment is influenced by the dress of the Yi people. They display a tattoo on the hand and wear a silver bracelet, as do other Dais. She told me she did not think the Dai-luo could communicate with the Dai from Xishuangbanna or Dehong.</p>"
              },
              {
                "id": 8674,
                "title": "qu-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-7.jpg",
                "caption": "Photo 2. Manuscripts employing traditional Dai scripts. Banyan village, Menghai Couty, Xishuangbanna Autonomous Prefecture, Yunnan Province.<p><br />After manuscripts are hand-transcribed and consecrated by villagers, they are then stored in temples. Most of these manuscripts are not well preserved and are often damaged by moisture, bugs, or mice. However, the manuscripts are sacred and therefore cannot be discarded. When villagers decide to build a new temple or pagoda, they usually bury these damaged manuscripts underground. Additionally, hundreds of traditional manuscripts were burned during the Cultural Revolution in China from 1966-1976, a time during which many traditional customs were forbidden. After the Reform and Open Policy implemented in the 1980s, some such customs have been resurrected, and the Dai people have begun transcribing and consecrating these manuscripts once again.</p>"
              },
              {
                "id": 8675,
                "title": "qu-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-8.jpg",
                "caption": "Photo 3. An elder (pictured in the center) selling his manuscripts during a ceremony on October 11, 2009, in the Menghuan pagoda, Dehong Prefecture."
              },
              {
                "id": 8676,
                "title": "qu-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-9.jpg",
                "caption": "Photo 4. The manuscript of <em>Ba Ta Ma Ga Pheng Shang Luo</em> (written in traditional Dai-lue script), preserved in the temple of Banyan Village, Mengzhe County, Xishuangbanna Prefecture.<p><br />One can find handwritten copies of this epic in many temples, for it plays an important role in Dai life and is used within many ceremonies.</p>"
              },
              {
                "id": 8677,
                "title": "qu-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-10.jpg",
                "caption": "Photo 5. A performance of <em>Na Du Xiang</em> (“The Precious Door”) as part of the celebration for a new house in Mangshi Town, Dehong Prefecture, on September 28, 2009.<p><br />People gather to celebrate a new house in Mangshi Town, Dehong Prefecture, on Spetember 28, 2009. These villagers sit in the drawing room and listen to the male elders (who mandatorily worship Buddha before the performance) singing a work entitled <em>Na Du Xiang</em> (“The Precious Door”). People believe that a family will be prosperous and harmonious through the singing of this song.</p>"
              },
              {
                "id": 8678,
                "title": "qu-11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-11.jpg",
                "caption": "Photo 6. The <em>zhangha</em> Yuyan, a professional singer famous amoung the Dai-lue people in Xishuangbanna.<p><br />When villagers celebrate a completed new house, baby, or monk, a host will prepare a bamboo table with delicious fruits and other dishes and invite her and her piccolo accompanist to perform. Sometime these performances are of traditional epics and other songs, but at other times she will improvise a song meant to flatter the host and relatives in the audience for their kindness, prosperity, and so on. The audiences will acclaim her song through the onomatopoeic phrase “shui-shui” and even give money as a reward.</p>"
              },
              {
                "id": 8679,
                "title": "qu-12.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-12.jpg",
                "caption": "Photo 7. A Dai-yat elder performing during an evocation ceremony on July 28, 2010, in Mosha Town, Xinping County, Yuxi City.<p><br />A Dai-yat elder performing during a ceremony on July 28, 2010, in Mosha Town, Xinping County, Yuxi City. This is a kind of evocation ceremony, and the elder—who does a job similar to that of a shaman—is called a <em>yamot</em>. Dai people believe that a person has 30 (or even 90) souls, and these souls can leave, play, or get lost. When one is ill and the disease is not curable with medicine, they consider that some souls must be lost somewhere; a <em>yamot</em> is then invited to the individual’s home to perform this ceremony in order to retrieve the missing souls, some of whom must be bribed with sacrificial food or convinced by means of love songs.</p>"
              },
              {
                "id": 8680,
                "title": "qu-13.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-13.jpg",
                "caption": "Photo 8. The Mengmian-Qingge Festival is held only among the Dai-la group during mid-May every year in Yuanjian County."
              },
              {
                "id": 8681,
                "title": "qu-14.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/qu-14.jpg",
                "caption": "Chart 4. The evolutionary history of Dai literature. Based on Wang Song 1983."
              }
            ]
          },
          {
            "id": 8660,
            "title": "From the <em>Árran</em> to the Internet: Sami Storytelling in Digital Environments",
            "content": "<p>By examining instances of adaptation of Sami tales and legends to digital environments, this essay investigates new premises and challenges for the emergence of traditional narratives. The Internet is approached as a place of creation and negotiation for traditional storytelling, and this study illustrates how the potential of the Internet must be nuanced and interpreted in relation to offline practices regarding such materials and traditions.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Coppélie Cocq"
              }
            ],
            "files": [
              {
                "id": 8662,
                "title": "cocq-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/cocq-1.jpg",
                "caption": "Screenshot from <a href=\"http://www.ur.se/cugu\" target=\"_blank\">www.ur.se/cugu</a> homepage."
              },
              {
                "id": 8663,
                "title": "cocq-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/cocq-2.jpg",
                "caption": "Homepage of Gulahalan (screenshot) <a href=\"http://www.ur.se/gulahalan/\" target=\"_blank\">http://www.ur.se/gulahalan/</a>."
              },
              {
                "id": 8664,
                "title": "cocq-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/cocq-3.jpg",
                "caption": "Screenshot of Cujaju homepage (<a href=\"http://www.cujaju.no\" target=\"_blank\">www.cujaju.no</a>)."
              },
              {
                "id": 8665,
                "title": "cocq-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/28i/full/cocq-4.jpg",
                "caption": "Stállu in the short film <e,>Stállu ja garjá</em>."
              }
            ]
          },
          {
            "id": 8658,
            "title": "Poetry’s Politics in Archaic Greek Epic and Lyric",
            "content": "<p>This essay builds on work in <em>The Poetics of Consent</em> (2013), which argues that the <em>Iliad</em>’s representation of politics reflects the workings of the oral tradition underlying the poem as we have it, a tradition that developed in the context of Panhellenic festivals. Applying a similar perspective to poetry belonging to the very different performative context of the symposium, this essay draws evidence from Theognis and Alcaeus suggesting that the social dynamics of sympotic performance could be expressed in terms of political fragmentation and alienation. In the <em>Odyssey</em>, the contrast between the epic singers Phemios and Demodokos reflects an awareness of the difference between these performative contexts. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "David F. Elmer"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 133,
            "issue_id": "102",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 134,
            "issue_id": "102",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28i/00_28.1cover.pdf"
          },
          {
            "id": 135,
            "issue_id": "102",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 136,
            "issue_id": "102",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28i/00_28.1fm.pdf"
          },
          {
            "id": 137,
            "issue_id": "102",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28i/09_28.1bm.pdf"
          },
          {
            "id": 138,
            "issue_id": "102",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28i/01_28.1.pdf"
          },
          {
            "id": 139,
            "issue_id": "102",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28i/oraltradition_28_1.zip"
          },
          {
            "id": 140,
            "issue_id": "102",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/28i/28.1complete.pdf"
          },
          {
            "id": 141,
            "issue_id": "102",
            "name": "Volume",
            "value": "28"
          },
          {
            "id": 142,
            "issue_id": "102",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 143,
            "issue_id": "102",
            "name": "Date",
            "value": "2013-03-01"
          }
        ]
      },
      {
        "id": 110,
        "name": "27ii",
        "title": "Volume 27, Issue 2",
        "articles": [
          {
            "id": 8768,
            "title": "Sounding Out the Heirs of Abraham (Rom 4:9-12)",
            "content": "<p>This article employs a newly devised analytical tool called a “sound map” to recreate the sound signature within a passage from an ancient Greek text (Romans 4:9-12). It then uses the map as an interpretive tool to uncover the intended meaning of the passage. Beyond seeking to resolve an interpretive problem within the passage itself, the paper further discusses the technique of sound mapping, a method of interpretation that can be employed on any ancient text more generally.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Nina E. Livesey"
              }
            ],
            "files": []
          },
          {
            "id": 8759,
            "title": "Sensing “Place”: Performance, Oral Tradition, and Improvisation in the Hidden Temples of Mountain Altai",
            "content": "<p>This article suggests that during two Ak Jang (“White Way”) Sary Bür (“Yellow Leaves”) rituals in hidden open-air temples in Mountain Altai, kaleidoscopic relations are created through bodily movements, oral poetry, epic, and song. These components stimulate three interrelated senses of “place” for participants: a topographical, indigenous “place of gatherings;” a numinous interactive spiritual place; and a situational “being-in-place” that serve to strengthen personhood and enable personal transitions in the face of difficult contemporary political and natural change.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Carole Pegg"
              },
              {
                "name": "Elizaveta Yamaeva"
              }
            ],
            "files": [
              {
                "id": 8761,
                "title": "Pegg-Yamaeva-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Pegg-Yamaeva-1.jpg",
                "caption": "<em>Küree</em> temple above Lower Talda, Kuroty Valley, Altai Republic, 2010, showing <em>tagy</em>l altars, <em>maany</em> banner, <em>kyira</em> ribbons, and three participants kneeling at the “hearth.”"
              },
              {
                "id": 8762,
                "title": "Pegg-Yamaeva-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Pegg-Yamaeva-2.jpg",
                "caption": "<em>Küree</em> temple above Kulady, Karakol Valley, Altai Republic, 2006, showing Ak Jang officiant Kumdus Borboshev and anthropologist Carole Pegg performing <em>mürgüül</em> prayers."
              },
              {
                "id": 8763,
                "title": "Pegg-Yamaeva-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Pegg-Yamaeva-3.jpg",
                "caption": "Valentina Todoshevna Chechaeva and Elena Tӧlӧsӧvna Mandaeva singing a ritual <em>jangar</em> song, Kulady, 2010."
              },
              {
                "id": 8764,
                "title": "Pegg-Yamaeva-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Pegg-Yamaeva-4.jpg",
                "caption": "Female participants circumambulating between outer and inner <em>tagyl</em> altar crescents after purifying and tying up a <em>kyira</em> ribbon. Lower Talda <em>küree</em>, 2010."
              },
              {
                "id": 8765,
                "title": "Pegg-Yamaeva-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Pegg-Yamaeva-5.jpg",
                "caption": "Arzhan <em>jarlyk</em> reciting blessing-fortune (<em>alkysh-byian</em>) verses while sprinkling milk, his “helper” stroking his head. <em>Shatra</em> offerings are on a <em>tagyl</em> altar, <em>kyira</em> ribbons are tied on lines between hitching posts, and the clan village lies below. Lower Talda <em>küree</em>, 2010."
              },
              {
                "id": 8766,
                "title": "Pegg-Yamaeva-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Pegg-Yamaeva-6.jpg",
                "caption": "Valentina Bachibaeva making food offerings to Üch Kurbustan via the fire in the hearth, Lower Talda <em>küree</em>, 2010."
              },
              {
                "id": 8767,
                "title": "Pegg-Yamaeva-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Pegg-Yamaeva-7.jpg",
                "caption": "Arzhan <em>jarlyk</em> playing <em>topshuur</em> lute and performing blessing-fortune verses using <em>kai</em> throat-singing before the hearth, Lower Talda <em>küree</em>, 2010."
              }
            ]
          },
          {
            "id": 8757,
            "title": "Kumeyaay Oral Tradition, Cultural Identity, and Language Revitalization",
            "content": "<p>This article discusses an example of Kumeyaay oral tradition from Baja California, Mexico, focusing specifically on the relation between stories and song cycles and their importance as vehicles for the intergenerational transmission of cultural values and group identity. Focusing on the nature of variationist language ideology in the Kumeyaay community and the indexicalization of group identity in storytelling, this essay demonstrates the importance of collaborative research in American Indian communities, especially in relation to language revitalization and archiving. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Margaret Field"
              },
              {
                "name": "Jon Meza Cuero"
              }
            ],
            "files": []
          },
          {
            "id": 8743,
            "title": "Patronage, Commodification, and the Dissemination of Performance Art: The Shared Benefits of Web Archiving",
            "content": "<p>Now that the Internet functions as a broadcasting forum, the “commodification” and marketing of indigenous performance art often takes place with no financial benefit to the performers. Therefore, scholars should work to ensure that traditional artists benefit from studies in “documentation” for the perpetuation of their livelihoods and cultural legacy. To help traditional arts survive, scholars need to create income-generating platforms in agreement with performance artists and transform archives into active fora for publicity and digital sales. This essay thus addresses the aesthetic and ethical dimensions of documenting oral performance on film, specifically with reference to performances of the epic of Pabuji in Rajasthan, India.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Elizabeth Wickett"
              }
            ],
            "files": [
              {
                "id": 8745,
                "title": "Wickett-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-1.jpg",
                "caption": "Pabuji’s <em>phad</em>."
              },
              {
                "id": 8746,
                "title": "Wickett-A.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-A.mp4",
                "caption": "Gogaji transformed into a cobra bites Kelam (with Parvati Devi and Hari Ram)."
              },
              {
                "id": 8747,
                "title": "Wickett-B.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-B.mp4",
                "caption": "Harmal Devasi is propelled across the salty sea to Lanka by the blessing of Pabuji (with Patashi Devi and Bhanwar Lal (Pabusar)."
              },
              {
                "id": 8748,
                "title": "Wickett-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-2.jpg",
                "caption": "Patashi Devi Bhopi in Pabusar."
              },
              {
                "id": 8749,
                "title": "Wickett-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-3.jpg",
                "caption": "Man Bhari Devi Bhopi in Jodhpur."
              },
              {
                "id": 8750,
                "title": "Wickett-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-4.jpg",
                "caption": "Parvati Devi Bhopi in Jaisalmer."
              },
              {
                "id": 8751,
                "title": "Wickett-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-5.jpg",
                "caption": "Santosh Devi Bhopi in Jaisalmer."
              },
              {
                "id": 8752,
                "title": "Wickett-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-6.jpg",
                "caption": "Hari Ram Bhopa performing in Jaisalmer on the <em>ravanhatta</em>."
              },
              {
                "id": 8753,
                "title": "Wickett-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-7.jpg",
                "caption": "Sugana Ram Bhopa performing in Jodhpur on the <em>ravanhatta</em> during the performance of the epic."
              },
              {
                "id": 8754,
                "title": "Wickett-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-8.jpg",
                "caption": "Priest at Pabusar performing the rituals before the <em>phad</em>."
              },
              {
                "id": 8755,
                "title": "Wickett-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-9.jpg",
                "caption": "Santosh Devi Bhopi describes healing rituals during performance of epic in Jaisalmer."
              },
              {
                "id": 8756,
                "title": "Wickett-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Wickett-10.jpg",
                "caption": "Bhanwar Lal reads the <em>phad</em> in Jaipur."
              }
            ]
          },
          {
            "id": 8740,
            "title": "“Copy Debts”?—Towards a Cultural Model for Researchers’ Accountability in an Age of Web Democracy",
            "content": "<p>The central concern of this article is the accountability of researchers in their desire to combine the publication of oral traditions with respect for the performers of these traditions. It is argued that the concept of copyrights implicitly imposes a written-text perspective and prioritizes national laws by which a group is a collection of individuals. Basing discussion on a representative case study of an epic text recorded in 2007 in Mali, this essay explores an alternative concept of “copy debts”: a cultural framework based on permanent dialogue, in which the performing group determines dynamically the terms and values of ownership/property.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jan Jansen"
              }
            ],
            "files": [
              {
                "id": 8742,
                "title": "Jansen-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Jansen-1.jpg",
                "caption": "The Kamabolon sanctuary in Kangaba in 1992."
              }
            ]
          },
          {
            "id": 8731,
            "title": "Instrument Teaching in the Context of Oral Tradition: A Field Study from Bolu, Turkey",
            "content": "<p>The primary objective in the present study has been to identify general characteristics of learning and teaching the playing of musical instruments in the context of oral tradition in Bolu, Turkey. The research data have been compiled over a two-year period via observation and narrative interviews conducted in the city center and surrounding villages in the province. The article specifically describes aspects of the learning-teaching process with respect to trainer, learner, setting, frequency of instruction, learning objectives, the music genres that are taught, and the musical instruments themselves.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Nesrin Kalyoncu"
              },
              {
                "name": "Cemal Özata"
              }
            ],
            "files": [
              {
                "id": 8733,
                "title": "Kalyoncu-Ozata-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Kalyoncu-Ozata-1.jpg",
                "caption": "<em>Violin</em> trainer and learner from <em>Ovadüzü</em> village."
              },
              {
                "id": 8734,
                "title": "Kalyoncu-Ozata-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Kalyoncu-Ozata-2.jpg",
                "caption": "<em>Darbuka</em> trainer and learner from Bahçeköy village."
              },
              {
                "id": 8735,
                "title": "Kalyoncu-Ozata-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Kalyoncu-Ozata-3.jpg",
                "caption": "Violin trainer and learner from Bahçeköy village."
              },
              {
                "id": 8736,
                "title": "Kalyoncu-Ozata-3b.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Kalyoncu-Ozata-3b.jpg",
                "caption": "Violin trainer and learner from Bahçeköy village."
              },
              {
                "id": 8737,
                "title": "Kalyoncu-Ozata-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Kalyoncu-Ozata-4.jpg",
                "caption": "<em>Bağlama</em> trainer and learner from Demirciler village."
              },
              {
                "id": 8738,
                "title": "Kalyoncu-Ozata-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Kalyoncu-Ozata-5.jpg",
                "caption": "Clarinet (Bahçeköy) and <em>davul</em> (Bolu) learners at the beginning stage."
              },
              {
                "id": 8739,
                "title": "Kalyoncu-Ozata-5b.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27ii/full/Kalyoncu-Ozata-5b.jpg",
                "caption": "Clarinet (Bahçeköy) and <em>davul</em> (Bolu) learners at the beginning stage."
              }
            ]
          },
          {
            "id": 8729,
            "title": "Challenges in Comparative Oral Epic",
            "content": "<p>Originally written in 2001 and subsequently published in China, this collaborative essay explores five questions central to comparative oral epic with regard to Mongolian, South Slavic, ancient Greek, and Old English traditions: “What is a poem in oral epic tradition?” “What is a typical scene or theme in oral epic tradition?” “What is a poetic line in oral epic tradition?” “What is a formula in an oral epic tradition?” “What is the register in oral epic poetry?” Now available for the first time in English, this essay reflects a foundational stage of what has become a productive and long-term collaboration between the Center for Studies in Oral Tradition and the Institute of Ethnic Literature of the Chinese Academy of Social Sciences.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Miles Foley"
              },
              {
                "name": "Chao Gejin"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 144,
            "issue_id": "110",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 145,
            "issue_id": "110",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27ii/00_27.2cover.pdf"
          },
          {
            "id": 146,
            "issue_id": "110",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 147,
            "issue_id": "110",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27ii/00_27.2fm.pdf"
          },
          {
            "id": 148,
            "issue_id": "110",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27ii/09_27.2bm.pdf"
          },
          {
            "id": 149,
            "issue_id": "110",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27ii/01_27.2.pdf"
          },
          {
            "id": 150,
            "issue_id": "110",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27ii/oraltradition_27_2.zip"
          },
          {
            "id": 151,
            "issue_id": "110",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27ii/27.2complete.pdf"
          },
          {
            "id": 152,
            "issue_id": "110",
            "name": "Volume",
            "value": "27"
          },
          {
            "id": 153,
            "issue_id": "110",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 154,
            "issue_id": "110",
            "name": "Date",
            "value": "2012-10-01"
          }
        ]
      },
      {
        "id": 122,
        "name": "27i",
        "title": "Volume 27, Issue 1",
        "articles": [
          {
            "id": 8835,
            "title": "Managing the “Boss”: Epistemic Violence, Resistance, and Negotiations in Milman Parry’s and Nikola Vujnović’s <em>Pričanja</em> with Salih Ugljanin",
            "content": "<p>Rather than approaching the Parry-Vujnović interviews with Salih Ugljanin, singer of South Slavic and Albanian epics, as primarily contextual and supplementary material, this article explores their neglected performative aspect, the subdued social, ethnic, and religious tensions that underlie them, and the subtle power-struggles, shifting allegiances, clashes, and confluences of interests and intentions between the participants. Special attention is given to the singer’s strategies of coping with, negotiating, and resisting the imposition of alien scholarly terms of engagement.</p>",
            "ecompanionhtml": "<html><head><style type=\"text/css\">\np.hangingindent {\n  margin: 0;\n  padding-left: 3em;\n  text-indent: -3em;\n}\n</style>\n</head><body><a name=\"audio1\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-1.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio1\">Example where the warm “grandson-grandfather” interlude preceding a storytelling</a> <span style=\"font-weight:normal\">(PN 656, IV:40, R 983: 3:53-4:04):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio1\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">N: <em>Koju ćemo dedo?</em></p>\n<p class=\"hangingindent\">S: <em>Ej koju? Pa ćes se nasmijati.</em></p>\n<p class=\"hangingindent\">N: <em>Dobro! Neka bude malo smiješna.</em></p>\n<p class=\"hangingindent\">S: <em>Valahi smiješna će bit, i ono je istinito.</em><br><br></p>\n<p class=\"hangingindent\">N: Which one [story] shall we [pick? hear?], grandpa?</p>\n<p class=\"hangingindent\">S: Eh which? Then you’re going to laugh.</p>\n<p class=\"hangingindent\">N: Good! Let it be a bit funny.</p>\n<p class=\"hangingindent\">S: Well, funny it’ll be, but true as well.</p></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio2\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-2.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio2\">The more the collector forces himself into the conversation, the more he remains on the outside</a> <span style=\"font-weight:normal\">(PN 652, I:67, R 877: 0:13-0:24):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio2\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">MP: <em>Jeli prićao</em> [Sadik?] <em>o Musi i Marku?</em></p>\n<p class=\"hangingindent\">S [in affirmation]: <em>E.</em></p>\n<p class=\"hangingindent\">MP: <em>Đe si čuo?</em></p>\n<p class=\"hangingindent\">S [realizing he needs to be clearer but still trying to rely on the context]: <em>Musu i Marka od onoga sam ćuo.</em></p>\n<p class=\"hangingindent\">N [trying to help out]: <em>U Mitrovici.</em></p>\n<p class=\"hangingindent\">S [realizing he needs to complete the “citation” for Parry’s benefit]: <em>U Mitrovicu od Sadika Bošnjaka.</em><br><br></p>\n<p class=\"hangingindent\">MP: Did he [Sadik?] tell about Musa and Marko?</p>\n<p class=\"hangingindent\">S [in affirmation]: Aye.</p>\n<p class=\"hangingindent\">MP: Where did you hear [it from]?</p>\n<p class=\"hangingindent\">S [realizing he needs to be clearer but still trying to rely on the context]: Musa and Marko I heard from that one.</p>\n<p class=\"hangingindent\">N [trying to help out]: In Mitrovica.</p>\n<p class=\"hangingindent\">S [realizing he needs to complete the “citation” for Parry’s benefit]: In Mitrovica from Sadik Bošnjak.</p></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio3\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-3.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio3\">Instead of the good, obedient son, Nikola plays the rascal</a> <span style=\"font-weight:normal\">(PN 659, VI:57, R 1055: 2:28-3:26):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio3\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">N: <em>Dobro. Ti malo počini i odmori se</em> [i smisli se31] <em>malo, koju pjesmu.</em></p>\n<p class=\"hangingindent\">S [interrupting the end of Nikola’s sentence]: <em>Prati mi jedan ćaj tako ti sveca.</em></p>\n<p class=\"hangingindent\">N [most likely pretending not to hear]: <em>Što, što?</em></p>\n<p class=\"hangingindent\">S: <em>Jedan ćaj mi prati otud.</em></p>\n<p class=\"hangingindent\">N [now definitely pretending]: <em>Reci dobro, ja te nečujem.</em></p>\n<p class=\"hangingindent\">S [louder]: <em>Jedan ćaj mi prati.</em></p>\n<p class=\"hangingindent\">N [playing silly, yet serious]: <em>Zašto?</em></p>\n<p class=\"hangingindent\">S: <em>Da pijem brate.</em></p>\n<p class=\"hangingindent\">N: <em>Hotli popit rakiju?</em></p>\n<p class=\"hangingindent\">S [resolutely]: <em>Jok vala, fala, rakiju neću nikako.</em></p>\n<p class=\"hangingindent\">N [playing ignorant]: <em>Zašto ne?</em></p>\n<p class=\"hangingindent\">S: <em>Ja ne pijem nikad.</em></p>\n<p class=\"hangingindent\">N: <em>A Da ti uspe</em>[m?] <em>malo rakije u čaj, bili ti popijo? Zdravije ti je.</em></p>\n<p class=\"hangingindent\">S [adamantly]: <em>Bogami jok, nikako.</em></p>\n<p class=\"hangingindent\">N: <em>Zašto bolan nebijo?</em></p>\n<p class=\"hangingindent\">S: <em>Ja sam ostavijo rakiju sad.</em></p>\n<p class=\"hangingindent\">N: <em>E, dobro, ti počini malo sad.</em></p>\n<p class=\"hangingindent\">S: <em>A hoću da mislim sad, a ti . . .</em> [Nikola here turns away from the microphone addressing someone else (Parry?). The singer now refers to whatever is happening (Nikola lighting a cigarette, or fire in the room?)]: <em>Ne, ne to ti je pala žiška kad si naložijo . . .</em> [Now returning to his earlier request]: <em>Prati mi jedan ćaj.</em><br><br></p>\n<p class=\"hangingindent\">N: All right. You rest a while and think a little, which song [you would like to sing].</p>\n<p class=\"hangingindent\">S [interrupting the end of Nikola’s sentence]: Send me one tea, by your saint.</p>\n<p class=\"hangingindent\">N [most likely pretending not to hear]: What, what?</p>\n<p class=\"hangingindent\">S: One tea, send it for me from there [the coffee-house kitchen?].</p>\n<p class=\"hangingindent\">N [now definitely pretending]: Say it well; I can’t hear you.</p>\n<p class=\"hangingindent\">S [louder]: Send me one tea.</p>\n<p class=\"hangingindent\">N [playing silly, yet serious]: Why?</p>\n<p class=\"hangingindent\">S: To drink, of course.</p>\n<p class=\"hangingindent\">N: How about some brandy?</p>\n<p class=\"hangingindent\">S [resolutely]: No thanks, I don’t want any brandy.</p>\n<p class=\"hangingindent\">N [playing ignorant]: Why not?</p>\n<p class=\"hangingindent\">S: I never drink.</p>\n<p class=\"hangingindent\">N: And if they [I?] pour a bit of brandy in your tea, would you drink it? It’s healthier for you.</p>\n<p class=\"hangingindent\">S [adamantly]: By God, no, no way.</p>\n<p class=\"hangingindent\">N: Why, might you not be ill?</p>\n<p class=\"hangingindent\">S: I have given up brandy now.</p>\n<p class=\"hangingindent\">N: Eh, all right, now you rest a while.</p>\n<p class=\"hangingindent\">S: And I will think now [about which song to sing next], and you . . . [Nikola here turns away from the microphone addressing someone else (Parry?). Then the singer refers to whatever is happening (Nikola lighting a cigarette, or the fire in the room?)]: No, no, but a spark fell when you lit up . . . [Now returning to his earlier request]: Send me one tea.</p></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio4\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-4.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio4\">Nikola, the Boss’s agent, leads the interviews and manages the purse</a> <span style=\"font-weight:normal\">(PN 659, VI:12, R 1044: 1:52-2:27):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio4\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">N: <em>E hajde sad lijepo Salja, od kraja do konca, ali, ako puštiš koji stih, nećemo ti platit ništa.</em></p>\n<p class=\"hangingindent\">S [anxiously]: <em>A da nemogu dok se neodmorim, bogme, ono</em> [the song] <em>je dugaćko. Teke po jedan mah da stanem da se odmorim.</em></p>\n<p class=\"hangingindent\">N: <em>Dobro, dobro, kada budeš umorit se, a ti odmori</em></p>\n<p class=\"hangingindent\">S [interrupting the end of Nikola’s sentence above]: <em>A onako da brojim hoću.</em></p>\n<p class=\"hangingindent\">N [feigning seriousness in a “schoolteacher” manner]: <em>Jes, jes, jes, samo svako slovo ako nebudeš kazat, tačno, ja je znam cijelu. . . .</em></p>\n<p class=\"hangingindent\">S [interrupts again]: <em>Ja vala. . . .</em></p>\n<p class=\"hangingindent\">N: <em>Ako nebudeš kazat tačno</em> [Salih here interjects, defensively]: <em>Oooh, nećemo ti platit ništa.</em> [Exclaiming, as though to cut the interruptions from the singer and stress his seriousness]: <em>Salja!</em></p>\n<p class=\"hangingindent\">S [continues, slightly dejected and defensive at the start, but gaining confidence]: <em>Oh, ja ne znam kako koji peva, a ja kako je pevam, belji ostavit neću.</em></p>\n<p class=\"hangingindent\">N [interrupting the end of Salih’s sentence above]: <em>E dobro! E hajde, bicmilah!</em></p>\n<p class=\"hangingindent\">S [with a small laugh of approval, amused by Nikola’s “Islamic” exclamation]: <em>Bicmilja i Bože pomozi.</em></p>\n<p class=\"hangingindent\">N [instructively]: <em>Samo čisto, jasno, glasno da pjevaš.</em></p>\n<p class=\"hangingindent\">S [calmed, in affirmation]: <em>A da.</em><br><br></p>\n<p class=\"hangingindent\">N: Eh, come on now, Salja, nicely, from the beginning to the end, but if you miss a verse, we are not going to pay you anything.</p>\n<p class=\"hangingindent\">S [anxiously]: Eh, I can’t until I rest first, by God, it [the song] is too long. But if I stop at one point to rest a little.</p>\n<p class=\"hangingindent\">N: Yes, yes, yes, but if you don’t say every letter, exactly, [Salih here interjects, defensively]: Ooo, I know it [the song] whole. . . .</p>\n<p class=\"hangingindent\">S [interrupting the end of Nikola’s sentence above]: And I’ll recount like that.</p>\n<p class=\"hangingindent\">N [feigning seriousness in a “schoolteacher” manner]: All right, all right, when you get tired, you take a rest.</p>\n<p class=\"hangingindent\">S [interrupts again]: Well, yes. . . .</p>\n<p class=\"hangingindent\">N: If you don’t say [it] exactly, we are not going to pay you anything. [Exclaiming, as though to cut the interruptions from the singer and stress his seriousness]: Salja!</p>\n<p class=\"hangingindent\">S [continues, slightly dejected and defensive at the start, but gaining confidence]: Oh, I don’t know how others sing it, but the way I sing it, I [won’t]51 leave out anything, of course.</p>\n<p class=\"hangingindent\">N [interrupting the end of Salih’s sentence above]: All right, then! Eh, come on, bismillah!</p>\n<p class=\"hangingindent\">S [with a small laugh of approval, amused by Nikola’s “Islamic” exclamation]: Bismillah and with God’s help.</p>\n<p class=\"hangingindent\">N [instructively]: Only sing neatly, clearly, loudly.</p>\n<p class=\"hangingindent\">S [calmed, in affirmation]: Well, yes.</p></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio5\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-5.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio5\">Nikola approaches Salih as would an adult playing a game of knowledge with a child</a> <span style=\"font-weight:normal\">(PN 654, II:43, R 921: 0:55-1:03):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio5\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">N: <em>Ja mislim da je Mostar najviši grad u Bosni.</em></p>\n<p class=\"hangingindent\">S: J<em>ok ima višije. Sarajevo je više.</em></p>\n<p class=\"hangingindent\">N [mildly incredulous]: <em>Više od njega!</em></p>\n<p class=\"hangingindent\">S [in confirmation]: <em>E.</em><br><br></p>\n<p class=\"hangingindent\">N: I think Mostar is the biggest town in Bosnia.</p>\n<p class=\"hangingindent\">S: Nope, there are bigger ones. Sarajevo is bigger.</p>\n<p class=\"hangingindent\">N [mildly incredulous]: Bigger than [Mostar]?</p>\n<p class=\"hangingindent\">S [in confirmation]: Yeah.</p></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio6\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-6.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio6\">Nikola feigns confusion in a ruse intended to provoke hilarity</a> <span style=\"font-weight:normal\">(PN 654, II:90, R 935: 2:20-2:25):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio6\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">N: <em>Đe? U džehenemu, jeli?</em></p>\n<p class=\"hangingindent\">S [pronouncing slowly for emphasis]: <em>U đenet!</em></p>\n<p class=\"hangingindent\">N: <em>A kako je u džehenemu?</em></p>\n<p class=\"hangingindent\">S: <em>A u đehnem sačuva Bože!</em><br><br></p>\n<p class=\"hangingindent\">N: Where [is all that]? In hell, isn’t it?</p>\n<p class=\"hangingindent\">S [pronouncing slowly for emphasis]: In heaven!</p>\n<p class=\"hangingindent\">N: And how is it in hell?</p>\n<p class=\"hangingindent\">S: But in hell, God keep [us from there]!</p></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio7\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-7.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio7\">Parry interjects into Salih’s reverie on the joys of heaven</a> <span style=\"font-weight:normal\">(PN 654, II:92-93, R 936: 0:51-1:09):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio7\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">MP: <em>E kad ti umriješ?</em></p>\n<p class=\"hangingindent\">S [longingly]: <em>Ej ako bogda tu da me povedu!</em></p>\n<p class=\"hangingindent\">N: <em>U džehenem jeli?</em></p>\n<p class=\"hangingindent\">S [emphatically]: <em>Nedaj Bože</em> [Nikola and Parry chuckle].</p>\n<p class=\"hangingindent\">N [through laughter]: <em>Đe bi ti volijo? U dženet ili u džehenem?</em></p>\n<p class=\"hangingindent\">S [laughing along]: <em>Ej, ja bi volijo u đenet . . .</em> [makes a hopeful sound and then coughs]: <em>dekiku no ovamo hiljadu godina.</em><br><br></p>\n<p class=\"hangingindent\">MP: And when you die?</p>\n<p class=\"hangingindent\">S [longingly]: Eh, may God grant that they take me there!</p>\n<p class=\"hangingindent\">N: To hell, you mean?</p>\n<p class=\"hangingindent\">S [emphatically]: God forbid [Nikola and Parry chuckle].</p>\n<p class=\"hangingindent\">N [through laughter]: Where would you like to go, to heaven or to hell?</p>\n<p class=\"hangingindent\">S [laughing along]: Eh, I would like to [go to] heaven . . . [makes a hopeful sound and then coughs]: [and spend] a minute [there] rather than [be] here for a thousand years.</p></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio8\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-8.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio8\">Nikola makes the same “mistake” one more time before finally giving up and causing another bout of laughter in the process</a> <span style=\"font-weight:normal\">(PN 654, II:96, R 937: 2:25-2:45):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"2\"><a href=\"#audio8\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">N: <em>I onda kad se umre, onda se ide u džehenem jeli?</em></p>\n<p class=\"hangingindent\">S: <em>Ne, neko u đenem, neko u đenet. Tu nema sem dva, dva puta.</em></p>\n<p class=\"hangingindent\">N: <em>Dva puta?</em></p>\n<p class=\"hangingindent\">S: <em>Nejma! Treće nejma!</em></p>\n<p class=\"hangingindent\">N [laughing while hinting at a known saying]: <em>A sad ako zna đadu dobro je!</em> [Both chuckle.] <em>A ko nezna đadu, ode u Kaniđu jeli?</em></p>\n<p class=\"hangingindent\">S [laughing in recognition and quoting the full rhyme]: <em>Ooo! A da! “A ko nezna đadu, on ode u Kajniđu gradu.”</em><br><br></p>\n<p class=\"hangingindent\">N: And then when one dies, one goes to hell, doesn’t he?</p>\n<p class=\"hangingindent\">S: No, some [go] to hell, some to heaven. There are but two, two ways there.</p>\n<p class=\"hangingindent\">N: Two ways?</p>\n<p class=\"hangingindent\">S: There isn’t. There isn’t a third.</p>\n<p class=\"hangingindent\">N [laughing while hinting at a known saying]: Well now, whoever knows the road, good [for him]! [Both chuckle.] And who doesn’t know the road, off he goes to Kaniđa!</p>\n<p class=\"hangingindent\">S [laughing in recognition and quoting the full rhyme]: Oooh! But of course! “Who doesn’t know the road [down], he ends up in Kajniđa town!”</p></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio9\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-9.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio9\">Nikola questions Salih about his battlefield courage</a> <span style=\"font-weight:normal\">(PN 655, III:63-64, R 953: 0:17-0:35):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"4\"><a href=\"#audio9\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">S [recites]: <em>Neko viće jao mene majko, Neko kuku prifatime druže!</em></p>\n<p class=\"hangingindent\">N [through laughter]: <em>Jesili ti koji put reko: “Kuku majko!”</em></p>\n<p class=\"hangingindent\">S [emphatically]: <em>Nikad!</em> [Through laughter, but adamant]: <em>Nijesam zakukao tako mi vere!</em></p>\n<p class=\"hangingindent\">N: <em>A kako, kad je jedan stari mene iz Hercegovine meni pričo, da te ćerao kad si ratovao tamo neđe s Crnogorcima? Da te pušijo preko nekoga polja.</em></p>\n<p class=\"hangingindent\">S [through laughter, but firmly denying]: <em>Au, tako mi Boga laže!</em> [Someone interjects with laughter, most likely Parry.] <em>Auh, nije tako mi Boga ni video. . . .</em><br><br></p>\n<p class=\"hangingindent\">S [recites]: Someone cries: “Woe to me, my mother,” / Someone: “Alas, comrade, catch me!”</p>\n<p class=\"hangingindent\">N [through laughter]: Have you ever said: “Alas, mother!”</p>\n<p class=\"hangingindent\">S [emphatically]: Never! [Through laughter, but adamant]: By my faith, I have never wept!</p>\n<p class=\"hangingindent\">N: How is that, since one old man from Herzegovina told me that he chased you when you warred somewhere there with the Montenegrins? That he smoked you [made your feet smoke from running? / blew you off?] over some field?</p>\n<p class=\"hangingindent\">S: [through laughter, but firmly denying]: Huh, by God, he lies! [Someone interjects with laughter, most likely Parry.] Huh, by God, he didn’t even see. . . .</p></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio10\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-10.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio10\">Nikola casts doubt on the carving of a well by  the Muslim Hero Alija</a> <span style=\"font-weight:normal\">(PN 656, IV:74, R 993: 2:29-2:44):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"3\"><a href=\"#audio10\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">N [suspiciously]: <em>Aha! A da nije to Sveti Đurđe udrijo tu sabljom pa otvorijo vodu?</em></p>\n<p class=\"hangingindent\">S [interrupts with dismissive laughter]: <em>Kakav Đurđe i krmak? On je gotovo puka žedan.</em> [Nikola (and Parry?) laughs at the singer’s passionate dismissal.] <em>Oni, oni nije drugome ništa dao. No ažda ih je opkoljila jadom. . . . </em><br><br></p>\n<p class=\"hangingindent\">N [suspiciously]: Ha! And could it be that it was St. George who hit there with his saber and opened a well?</p>\n<p class=\"hangingindent\">S [interrupts with dismissive laughter]: Which George and a swine? He nearly burst of thirst! [Nikola (and Parry?) laughs at the singer’s passionate dismissal.] He, he gave nothing to another. But the dragon besieged them with suffering. . . .</p></td>\n  </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio11\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-11.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio11\">The collectors briefly lose patience with Salih</a> <span style=\"font-weight:normal\">(PN 655, III:78, R 957: 0:31-0:39):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"13\"><a href=\"#audio11\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">MP: <em>Glasnije. Prićaj glasnije.</em></p>\n<p class=\"hangingindent\">N: <em>Glasnije pričaj stari!</em></p>\n<p class=\"hangingindent\">S: <em>Glasnije . . . </em></p>\n<p class=\"hangingindent\">N [now softer, jokingly]: <em>Da se čuje, ja sam malo gluh ja ne čujem.</em></p>\n<p class=\"hangingindent\">MP [justifying the outburst]: <em>Kad ja ne čujem dobro odavlen. . . .</em> [Presumably he is close by.]</p>\n<p class=\"hangingindent\">S: <em>Znam, znam.</em><br><br></p>\n<p class=\"hangingindent\">MP: Louder, speak louder!</p>\n<p class=\"hangingindent\">N: Speak louder, old man!</p>\n<p class=\"hangingindent\">S: Louder . . . </p>\n<p class=\"hangingindent\">N [now softer, jokingly]: So that it can be heard, I am a little deaf, I can’t hear.</p>\n<p class=\"hangingindent\">MP [justifying the outburst]: When I can’t hear well from here. . . . [Presumably he is close by.]</p>\n<p class=\"hangingindent\">S: I know, I know.</p></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio12\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-12.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio12\">Generally kind and considerate to Salih, the collectors press him though he needs to rest</a> <span style=\"font-weight:normal\">(PN 659, VI:92-93, R 1066: 2:03-2:37):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"5\"><a href=\"#audio12\"><img src=\"/images/mp3.jpg\" align=\"left\"></a><br><br></td>\n  <td><p class=\"hangingindent\">N: <em>A zašto si prestao sada?</em></p>\n<p class=\"hangingindent\">S: <em>Bogami ne mogu.</em></p>\n<p class=\"hangingindent\">N: <em>Kako ne moreš?</em></p>\n<p class=\"hangingindent\"><em>. . . </em></p>\n<p class=\"hangingindent\">S: <em>Odavno prićam ođe.</em></p>\n<p class=\"hangingindent\">N: <em>Što ima, dva sahata još nema . . . </em></p>\n<p class=\"hangingindent\">MP [interjects]: <em>Ni dva sata nema!</em></p>\n<p class=\"hangingindent\">N: <em>. . . da si došao. </em></p>\n<p class=\"hangingindent\">S: <em>Bogami . . . </em></p>\n<p class=\"hangingindent\">N: <em>I dva si puta počivao!</em></p>\n<p class=\"hangingindent\">S: <em>Pa jes no hej duša jedna, nemore, nemore da je konj.</em></p>\n<p class=\"hangingindent\">N [complimenting and chiding all at once]: <em>Da ja imam pričat koliko ti ja bi pričao deset dana, ne bi nikada prestao.</em><br><br></p>\n<p class=\"hangingindent\">N: And why have you stopped now?</p>\n<p class=\"hangingindent\">S: By God, I can’t.</p>\n<p class=\"hangingindent\">N: How come you can’t?</p>\n<p class=\"hangingindent\">. . . </p>\n<p class=\"hangingindent\">S: I’ve been talking here for ages.</p>\n<p class=\"hangingindent\">N: What is there, there’s not yet two hours . . . </p>\n<p class=\"hangingindent\">MP [interjects]: Not even two hours!</p>\n<p class=\"hangingindent\">N: . . . since you came.</p>\n<p class=\"hangingindent\">S: By God . . . </p>\n<p class=\"hangingindent\">N: And twice you rested!</p>\n<p class=\"hangingindent\">S: Well, yes, but hey, there’s only one soul [I have], it can’t, it couldn’t if it were a horse.</p>\n<p class=\"hangingindent\">N [complimenting and chiding all at once]: If I had as much to tell as you, I would talk for ten days; I would never stop.</p></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio13\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-13.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio13\">Salih complains of physical pain</a> <span style=\"font-weight:normal\">(PN 654, II:67-68, R 928: 2:19-2:50):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio13\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">S [halting mid-recitation]: <em>Iju!</em></p>\n<p class=\"hangingindent\">MP [barely audible]: <em>Što ti kažeš?</em></p>\n<p class=\"hangingindent\">S [to Parry, through a quick painful laugh]: <em>. . . Zohar mi ovde, nešto me zabolje.</em></p>\n<p class=\"hangingindent\">N: <em>Što ti je bilo?</em></p>\n<p class=\"hangingindent\">S: <em>Đe prićam . . . </em></p>\n<p class=\"hangingindent\">N: <em>Nemoj ti prekinut sad. Pričaj naprijed.</em></p>\n<p class=\"hangingindent\">S: <em>Ne mogu, đe pričam . . . </em></p>\n<p class=\"hangingindent\">MP: <em>Eh mi ćemo poćinut, poćivati malo. Dobro je za kafu.</em></p>\n<p class=\"hangingindent\">S: <em>Da poćinem.</em></p>\n<p class=\"hangingindent\">MP: <em>Da.</em></p>\n<p class=\"hangingindent\">S: <em>Sam da malo se odmorim.</em></p>\n<p class=\"hangingindent\">N: <em>Samo nemoj zaboravit, đe si osto.</em></p>\n<p class=\"hangingindent\">S: <em>Jok.</em><br><br></p>\n<p class=\"hangingindent\">S [halting mid-recitation]: Ouch!</p>\n<p class=\"hangingindent\">MP [barely audible]: What do you say?</p>\n<p class=\"hangingindent\">S [to Parry, through a quick painful laugh]: . . . [I feel pain] here, something started to hurt . . . </p>\n<p class=\"hangingindent\">N: What is it with you?</p>\n<p class=\"hangingindent\">S: Where I speak . . . </p>\n<p class=\"hangingindent\">N: Don’t you stop now. Go on.</p>\n<p class=\"hangingindent\">S: I can’t, where I talk . . . </p>\n<p class=\"hangingindent\">MP: Eh, we’ll rest, rest a little. It’s a good [time] for coffee.</p>\n<p class=\"hangingindent\">S: For me to rest.</p>\n<p class=\"hangingindent\">MP: Yes.</p>\n<p class=\"hangingindent\">S: Just to rest a little.</p>\n<p class=\"hangingindent\">N: Just don’t forget where you’re at.</p>\n<p class=\"hangingindent\">S: I won’t.</p></td>\n </tr>\n </tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio14\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-14.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio14\">Threatened by Nikola’s logical criticisms, Salih appeals to the higher authority of the tradition</a> <span style=\"font-weight:normal\">(PN 674, VII:24-25, R 1234: 3:03-4:10):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"4\"><a href=\"#audio14\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">S: <em>E pa oni tako pjevaju</em></p>\n<p class=\"hangingindent\">MP: <em>Tako pjevaju?</em></p>\n<p class=\"hangingindent\">S: <em>E.</em></p>\n<p class=\"hangingindent\">MP [argumentatively]: <em>Ali je li dobro da tako pjevaju?</em></p>\n<p class=\"hangingindent\">N [clarifying]: <em>Jeli to istina valja čut?</em></p>\n<p class=\"hangingindent\">MP: <em>Jeli bila istina? Ako nije bila istina zašto se pjeva?</em></p>\n<p class=\"hangingindent\">S [adamant]: <em>E pa, on da nije istina, nebi ga on pevao.</em></p>\n<p class=\"hangingindent\"><em>. . . </em></p>\n<p class=\"hangingindent\">S [on Parry’s suggestion to include all this subsequent reasoning in his song]: <em>E oni ne kazuju da je imao koji oruža, da se digao na njega da učini huđum, niko.</em></p>\n<p class=\"hangingindent\">N [teasingly]:<em> Sigurno si ti preskočijo.</em></p>\n<p class=\"hangingindent\">S [emphatically, imitating Nikola’s contesting tone]: <em>Nijesam.</em></p>\n<p class=\"hangingindent\">N: <em>E dobro!</em></p>\n<p class=\"hangingindent\">MP [interjects passionately]: <em>Mislim da loši pjevač kaže samo da je, Haljil, odsjekao pedeset glava, tako, ali da dobar pjevač bi rekao tačno.</em></p>\n<p class=\"hangingindent\">S [not following]: <em>Ha!</em></p>\n<p class=\"hangingindent\">MP [explaining]: <em>Kako je bilo, zašto je mogo da k, o, osjeć</em> [laughs at own stammering], <em>osječe toliko glava.</em><br><br></p>\n<p class=\"hangingindent\">S: Well, they sing it like that.</p>\n<p class=\"hangingindent\">MP: They sing it like that?</p>\n<p class=\"hangingindent\">S: Yes.</p>\n<p class=\"hangingindent\">MP [argumentatively]: But is it good that they sing it like that?</p>\n<p class=\"hangingindent\">N [clarifying]: Is that the truth? It should be heard?</p>\n<p class=\"hangingindent\">MP: Was that the truth? If it wasn’t the truth, why is it sung [like that]?</p>\n<p class=\"hangingindent\">S [adamant]: Well, he . . . if it wasn’t true, he [the singer from whom he learned the song?] wouldn’t have sung it.</p>\n<p class=\"hangingindent\">. . . </p>\n<p class=\"hangingindent\">S [on Parry’s suggestion to include all this subsequent reasoning in his song]: Well, they don’t say if anyone had weapons . . . that he got up to attack him, no one . . . </p>\n<p class=\"hangingindent\">N [teasingly]: You have skipped [something] for sure.</p>\n<p class=\"hangingindent\">S [emphatically, imitating Nikola’s contesting tone]: I haven’t.</p>\n<p class=\"hangingindent\">N: Well, OK.</p>\n<p class=\"hangingindent\">MP [interjects passionately]: I think that a bad singer only says that Halil cut off fifty heads, like that, but a good singer would tell it correctly . . . </p>\n<p class=\"hangingindent\">S [not following]: Ha?!</p>\n<p class=\"hangingindent\">MP [explaining]: . . . as it was, how come he was able to, c-, c-, cut [laughs at own stammering], cut off that many heads.</p></td>\n </tr>\n </tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio15\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-15.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio15\">Example of subversion: Salih deflects Nikola’s insistence on a song in which the Serbs are victorious</a> <span style=\"font-weight:normal\">(PN 655, III:49-50, R 949: 1:17-2: 03):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"3\"><a href=\"#audio15\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">N: <em>Kako to? Ti si rekao da ćeš pjevat pravoslavnu pjesmu, da ćeš pričat a ti si već sada da su turci pobjedili. Kako to Bogati?</em></p>\n<p class=\"hangingindent\">S [through laughter]: <em>Bogami, ja onako mi dade uz riječ, a neznam . . .</em> [All chuckle].</p>\n<p class=\"hangingindent\">MP: <em>Rekao si da će bit pravoslavna pjesma.</em></p>\n<p class=\"hangingindent\">N [repeating Parry’s remark louder]: <em>Rekao si da će bit pravoslavna pjesma.</em></p>\n<p class=\"hangingindent\">S: <em>Vala pravoslavna jes, ama teke, ja zar zanosim na turski, a oni ovo pevaju.</em></p>\n<p class=\"hangingindent\"><em>. . . </em></p>\n<p class=\"hangingindent\">N: <em>A znaš li ti koju drugu srpsku pjesmu?</em></p>\n<p class=\"hangingindent\">S: <em>A pa ima.</em></p>\n<p class=\"hangingindent\">N: <em>Ma đe srbin pobijedio turčina? Znaš li?</em></p>\n<p class=\"hangingindent\">S: <em>Vala, pa znam to nekoliko.</em></p>\n<p class=\"hangingindent\">N: <em>E hajde jednu da mi kažeš, koju? Koju to hoćeš?</em></p>\n<p class=\"hangingindent\">S: <em>A da ope će platit Srbin najzadnje.</em><br><br></p>\n<p class=\"hangingindent\">N: How’s that? You said you were going to sing an Orthodox song, that you are going to tell, but you now [made it so] that the Turks won!<br> How’s that, by God?</p>\n<p class=\"hangingindent\">S [through laughter]: By God, that’s how the words came to me, and I don’t know . . . [All chuckle].</p>\n<p class=\"hangingindent\">MP: You said it was going to be an Orthodox song.</p>\n<p class=\"hangingindent\">N [repeating Parry’s remark louder]: You said it was going to be an Orthodox song.</p>\n<p class=\"hangingindent\">S: Well it is Orthodox, but I lean towards the Turkish [point of view?], and they sing this.</p>\n<p class=\"hangingindent\">. . . </p>\n<p class=\"hangingindent\">N: But do you know some other Serbian song?</p>\n<p class=\"hangingindent\">S: Ah, well there are some.</p>\n<p class=\"hangingindent\">N: But where a Serb won against a Turk? Do you know [any]?</p>\n<p class=\"hangingindent\">S: Well, I know a few.</p>\n<p class=\"hangingindent\">N: Eh, come on, tell me one! Which? Which one do you want?</p>\n<p class=\"hangingindent\">S: Well, yes, but the Serb will pay in the end again.</p></td>\n </tr>\n </tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio16\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-16.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio16\">Reluctantly consenting to compose a poem about the six-days of meetings, Salih improvises in the traditional manner</a> <span style=\"font-weight:normal\">(PN 655, III:106-07, R 966: 0:13-1:12):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"11\"><a href=\"#audio16\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">S: <em>Kako ime . . . gazdi?</em></p>\n<p class=\"hangingindent\">N: <em>Milman.</em></p>\n<p class=\"hangingindent\">S: <em>Milman?</em></p>\n<p class=\"hangingindent\">N: <em>Jes!</em></p>\n<p class=\"hangingindent\">S: <em>Tebe Nikola.</em></p>\n<p class=\"hangingindent\">N: <em>Jes.</em></p>\n<p class=\"hangingindent\">S [referring to Lord operating the phonograph from another room]: <em>Onoga neka.</em></p>\n<p class=\"hangingindent\">N: <em>Što, što si reko.</em></p>\n<p class=\"hangingindent\">S: <em>Onog, onoga nećemo dofatit, znaš, no samo vas dvojicu.</em></p>\n<p class=\"hangingindent\">MP: <em>Dobro! Kako hoćeš.</em> [Laughter, mostly Nikola’s.]</p>\n<p class=\"hangingindent\">N: <em>Ajde Salja da čujemo!</em></p>\n<p class=\"hangingindent\">S [quietly, to himself]: <em>E da vidim u koji dan smo poćelji . . . u ponedeljak. . . .</em></p>\n<p class=\"hangingindent\">N: <em>Ma lijepo ko za gusle znaš.</em></p>\n<p class=\"hangingindent\">S [quietly]: <em>A da vala. . . . </em></p>\n<p class=\"hangingindent\">N: <em>Počeli smo radit ovđe u poneđeljak, a danas je subota.</em></p>\n<p class=\"hangingindent\">S [pensively]: <em>Jes . . . demek radilji smo cijo dan do, do noći.</em></p>\n<p class=\"hangingindent\">N: <em>Jes.</em></p>\n<p class=\"hangingindent\">S [more confidently]: <em>U svaki dan cijo dan do noći, radilji smo . . . </em></p>\n<p class=\"hangingindent\">N: <em>Jes.</em></p>\n<p class=\"hangingindent\">S: <em>I tako,</em> [Parry interrupts] <em>. . . i tako smo pjesmu. . . .</em></p>\n<p class=\"hangingindent\">MP: <em>Glasnije!</em></p>\n<p class=\"hangingindent\">S: <em>A?</em></p>\n<p class=\"hangingindent\">N: <em>Glasnije pričaj!</em></p>\n<p class=\"hangingindent\">S: <em>Glasnije ću pričat, teke sad dok . . .</em> [Recitation follows after a pause of seven seconds].<br><br></p>\n<p class=\"hangingindent\">S: What is the name . . . of the boss?</p>\n<p class=\"hangingindent\">N: Milman.</p>\n<p class=\"hangingindent\">S: Milman?</p>\n<p class=\"hangingindent\">N: Yes.</p>\n<p class=\"hangingindent\">S: Yours is Nikola.</p>\n<p class=\"hangingindent\">N: Yes.</p>\n<p class=\"hangingindent\">S: [referring to Lord operating the phonograph from the next room]: That one, let him be.</p>\n<p class=\"hangingindent\">N: What, what did you say?</p>\n<p class=\"hangingindent\">S: The other, the other one, we won’t put him in [the song], you know, but only you two.</p>\n<p class=\"hangingindent\">MP: All right, as you please. [Laughter, mostly Nikola’s.]</p>\n<p class=\"hangingindent\">N: C’mon, Salja, let’s hear it!</p>\n<p class=\"hangingindent\">S: [quietly, to himself]: Well, let me see, what day did we start . . . on Monday. . . . </p>\n<p class=\"hangingindent\">N: But nicely, as though for the gusle, you know.</p>\n<p class=\"hangingindent\">S [quietly]: Ah, yes, of course. . . . </p>\n<p class=\"hangingindent\">N: We started working here on Monday, and today is Saturday.</p>\n<p class=\"hangingindent\">S [pensively]: Yes . . . indeed, we worked the whole day till night.</p>\n<p class=\"hangingindent\">N. Yes.</p>\n<p class=\"hangingindent\">S [more confidently]: Every day, the whole day until the night we worked. . . . </p>\n<p class=\"hangingindent\">N: Yes.</p>\n<p class=\"hangingindent\">S: And so, [Parry interrupts] . . . and so we did the song. . . . </p>\n<p class=\"hangingindent\">MP: Louder!</p>\n<p class=\"hangingindent\">S: Huh?</p>\n<p class=\"hangingindent\">N: Speak louder!</p>\n<p class=\"hangingindent\">S: I will speak louder, but now while . . . [Recitation follows after a pause of seven seconds]</p></td>\n </tr>\n </tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio17\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/27i/full/Rankovic-17.mp3\" type=\"audio/mpeg\">\n</audio>\n\n<h4><a href=\"#audio17\">Power shifts from the individuals to the story and the performance itself</a> <span style=\"font-weight:normal\">(PN 655, III:99-100, R 963: 2:30 to R 964: 0:37):</span><br><br></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60px\" valign=\"top\" rowspan=\"17\"><a href=\"#audio17\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n  <td><p class=\"hangingindent\">N [fighting his own and general laughter]: <em>Pričaj još Bogati jebem!</em></p>\n<p class=\"hangingindent\">S [himself laughing]: <em>Ma neda mi smijeh.</em></p>\n<p class=\"hangingindent\">[Salih continues, and at times also enacts his story, interrupted only by common laughter. . . .]</p>\n<p class=\"hangingindent\">N [coughing and laughing along with the others]: <em>Jeli to istina bila čiča?</em></p>\n<p class=\"hangingindent\">S: <em>Istina istinska, ovo ti pričam.</em></p>\n<p class=\"hangingindent\">N [laughing and swearing approvingly]: <em>I nije išo po drugu jeli?</em></p>\n<p class=\"hangingindent\">S [fighting his own laughter]: <em>Bože saćuvaj!</em> [Parry here contributes an inaudible but obviously jolly remark] <em>. . . Tako mi Boga . . . ne znam. . . . </em></p>\n<p class=\"hangingindent\">N [interjecting, through laughter]: <em>Ajde Bogavam da čujemo ovu ploču šta je bilo?</em> [All burst out laughing.]</p>\n<p class=\"hangingindent\">S: <em>Haj Bogami . . . Ovo nema niđe ni u Auropu.</em><br><br></p>\n<p class=\"hangingindent\">N [fighting his own and general laughter]: Tell more, for fuck’s sake!</p>\n<p class=\"hangingindent\">S [himself laughing]: The laughter is not letting me.</p>\n<p class=\"hangingindent\">[Salih continues, and at times also enacts his story, interrupted only by common laughter. . . .]</p>\n<p class=\"hangingindent\">N [coughing and laughing along with the others]: Was that a true story, old man?</p>\n<p class=\"hangingindent\">S: The truest truth, this, I tell you.</p>\n<p class=\"hangingindent\">N [laughing and swearing approvingly]: And he didn’t go for a second one [wife], ha?</p>\n<p class=\"hangingindent\">S [fighting his own laughter]: God forbid! [Parry here contributes an inaudible but obviously jolly remark] . . . I swear to God . . . I don’t know. . . . </p>\n<p class=\"hangingindent\">N [interjecting, through laughter]: C’mon, by God, let’s hear this record [again], what happened. [All burst out laughing.]</p>\n<p class=\"hangingindent\">S: Let’s, by God . . . There’s nothing like this anywhere, not even in Europe!</p></td>\n </tr>\n </tbody></table></body></html>",
            "authors": [
              {
                "name": "Slavica Ranković"
              }
            ],
            "files": []
          },
          {
            "id": 8831,
            "title": "Oral/Aural Culture in Late Modern Society? Traditional Singing as Professionalized Genre and Oral-Derived Expression",
            "content": "<p>In late modern societies traditional arts tend to become disembedded from a functioning vernacular milieu and become partly absorbed into institutionalized structures such as education. This article focuses on traditional singing in present-day Sweden as one evident example where oral tradition as a comprehensive concept is being transformed and renegotiated into a selection of oral techniques, style markers, and aesthetic ideals. A related issue also presented here is the balance between verbal and music-related sides of orality.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ingrid Åkesson"
              }
            ],
            "files": [
              {
                "id": 8833,
                "title": "akesson-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Akesson-1.jpg",
                "caption": "Three intermingling trends in late modern folk music."
              },
              {
                "id": 8834,
                "title": "akesson-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Akesson-2.jpg",
                "caption": "Ideals in different but overlapping contemporary folk song milieus."
              }
            ]
          },
          {
            "id": 8818,
            "title": "Masonic Song in Scotland: Folk Tunes and Community",
            "content": "<p>This article explores the place of Masonic songs historically in Scotland, assessing the oral culture surrounding the genre. The article further shows that folk tunes were commonly used and investigates aspects of the group performance that was central to the Lodges. Finally, the study  concludes with an examination of a Masonic procession in Northeast Scotland that survives to the present day, focusing especially on the role of music and song within it.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Katherine Campbell"
              }
            ],
            "files": [
              {
                "id": 8820,
                "title": "campbell-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Campbell-1.jpg",
                "caption": "“The Master’s Song,” (Anderson 1723:85-86)."
              },
              {
                "id": 8821,
                "title": "campbell-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Campbell-2.jpg",
                "caption": "“The Warden’s Song,” (Anderson 1723:87-88)."
              },
              {
                "id": 8822,
                "title": "campbell-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Campbell-3.jpg",
                "caption": "“The Enter’d Prentice’s Song,” (Anderson 1723:84 [lyrics], 90 [music])."
              },
              {
                "id": 8823,
                "title": "campbell-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Campbell-4.jpg",
                "caption": "“Farewell to the Brethren at St. James Lodge, Tarbolton,” (Dick 1903:214-15)."
              },
              {
                "id": 8824,
                "title": "campbell-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Campbell-5.jpg",
                "caption": "“Come All Ye Freemasons,” (Riddell 1906-11:118)."
              },
              {
                "id": 8825,
                "title": "campbell-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Campbell-6.jpg",
                "caption": "“While Yet as a Cowan,” (Greig-Duncan 1374C [tune], St Cecilia 1782:no. 3 [text])."
              },
              {
                "id": 8826,
                "title": "campbell-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Campbell-7.jpg",
                "caption": "“Sons of Levi,” (Karpeles 1974:ii, 489)."
              },
              {
                "id": 8827,
                "title": "campbell-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Campbell-8.jpg",
                "caption": "“Wi the Apron On,” (Greig-Duncan 471D)."
              },
              {
                "id": 8828,
                "title": "campbell-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Campbell-9.jpg",
                "caption": "Fiddlers’ Gallery, Eyemouth."
              },
              {
                "id": 8829,
                "title": "campbell-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Campbell-10.jpg",
                "caption": "Masonic procession (Wilson and Chambers 1840:66)."
              },
              {
                "id": 8830,
                "title": "campbell-11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Campbell-11.jpg",
                "caption": "“Ho Ro The Merry Masons,” (Tune collected in 2007 from Mr. Crawford, senior member of the Lodge)."
              }
            ]
          },
          {
            "id": 8806,
            "title": "The Storyteller, the Scribe, and a Missing Man:  Hidden Influences from Printed Sources in the Gaelic  Tales of Duncan and Neil MacDonald",
            "content": "<p>This article concerns the well-known case of storytelling brothers Neil and Duncan MacDonald from South Uist, Scotland. The impressive verbal consistency of their hero tales has been taken to indicate that some Gaelic storytellers could acquire, recite, and transmit their repertoire in a near verbatim fashion. However, by deploying plagiarism detection techniques across an electronic corpus of texts, the author reveals that previous observations about the brothers’ verbal conservativeness have been skewed by corrupt evidence.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William Lamb"
              }
            ],
            "files": [
              {
                "id": 8808,
                "title": "lamb-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lamb-1.jpg",
                "caption": "Duncan MacDonald."
              },
              {
                "id": 8809,
                "title": "lamb-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lamb-2.jpg",
                "caption": "Accession sheet label submitted by Donald John MacDonald for Neil MacDonald’s recitation of <em>Fear na h-Eabaid</em>."
              },
              {
                "id": 8810,
                "title": "lamb-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lamb-3.jpg",
                "caption": "<em>banais</em> (line 4, word 3)."
              },
              {
                "id": 8811,
                "title": "lamb-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lamb-4.jpg",
                "caption": "DJM:3557, <em>banais</em> (line 4, word 1)."
              },
              {
                "id": 8812,
                "title": "lamb-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lamb-5.jpg",
                "caption": "<em>bainis</em> (line 2, word 3)."
              },
              {
                "id": 8813,
                "title": "lamb-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lamb-6.jpg",
                "caption": "DJM-N:3557, <em>bainis</em> (line 4, word 1) with motif annotation on right."
              },
              {
                "id": 8814,
                "title": "lamb-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lamb-7.jpg",
                "caption": "Donald John MacDonald transcribing the wrong line of text from Craig (from DJM:3564)."
              },
              {
                "id": 8815,
                "title": "lamb-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lamb-8.jpg",
                "caption": "Example showing <em>nan teintean</em> immediately above <em>a’ bhoireannaich</em>."
              },
              {
                "id": 8816,
                "title": "lamb-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lamb-9.jpg",
                "caption": "Dice coefficient values with <em>MWHT</em> over three sections of <em>Iain Òg Mac Rìgh na Frainge</em> from the manuscripts of Donald John MacDonald."
              },
              {
                "id": 8817,
                "title": "lamb-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lamb-10.jpg",
                "caption": "Dice coefficient values over three sections of ATU 1651/506 in DJM-N and <em>MWHT</em>."
              }
            ]
          },
          {
            "id": 8801,
            "title": "Stepping Stones through Time",
            "content": "<p>Indo-European mythology is known only through written records but it needs to be understood in terms of the preliterate oral-cultural context in which it was rooted. It is proposed that this world was conceptually organized through a memory-capsule consisting of the current generation and the three before it, and that there was a system of alternate generations with each generation taking a step into the future under the leadership of a white or red king.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Emily Lyle"
              }
            ],
            "files": [
              {
                "id": 8803,
                "title": "Lyle-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lyle-1.jpg",
                "caption": "Alternate forms of the four-generation capsule with bilateral cross-cousin marriage. The kings (white and red) are shown in relation to their ultimate ancestress (yellow), and the previous king’s daughter—whom the candidate for kingship must marry—is highlighted in purple."
              },
              {
                "id": 8804,
                "title": "Lyle-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lyle-2.jpg",
                "caption": "Orientation in time."
              },
              {
                "id": 8805,
                "title": "Lyle-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Lyle-3.jpg",
                "caption": "The four-generation capsule."
              }
            ]
          },
          {
            "id": 8793,
            "title": "Dipping into the Well: Scottish Oral Tradition",
            "content": "<p>The School of Scottish Studies was set up in 1951 to collect, research, archive, and publish material relating to the folklore, ethnology, and traditional arts of Scotland. <em>Tobar an Dualchais/Kist o Riches</em> is a recent project that has enabled digitization of audio recordings and the creation of an online resource containing thousands of items from the school’s archives. This article describes the project and provides a historical tale from St Kilda as an example of archival material.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Cathlin Macaulay"
              }
            ],
            "files": [
              {
                "id": 8795,
                "title": "Macaulay-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Macaulay-1.jpg",
                "caption": "Lachlan MacNeill, John Francis Campbell, and Hector MacLean, Islay, 1870."
              },
              {
                "id": 8796,
                "title": "Macaulay-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Macaulay-2.jpg",
                "caption": "Angus MacNeil and Calum Maclean, Smearisary, 1959."
              },
              {
                "id": 8797,
                "title": "Macaulay-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Macaulay-3.jpg",
                "caption": "Tobar an Dualchais: Process of digitization."
              },
              {
                "id": 8798,
                "title": "Macaulay-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Macaulay-4.jpg",
                "caption": "St Kilda. Village Bay, Oiseabhal and Conachair, 1938."
              },
              {
                "id": 8799,
                "title": "Macaulay-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Macaulay-5.jpg",
                "caption": "Finlay MacQueen snaring puffins, Carn Mor, St Kilda, 1938."
              },
              {
                "id": 8800,
                "title": "Macaulay-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Macaulay-6.jpg",
                "caption": "St Kilda: Stac an Armainn, 1947."
              }
            ]
          },
          {
            "id": 8780,
            "title": "“Our Grandparents Used to Say That We Are Certainly Ancient People, We Come From the <em>Chullpas</em>”: The Bolivian Chipayas’ Mythistory",
            "content": "<p>The Chipaya people live in the Bolivian Altiplano. Their ecological, economic, and social isolation forms the basis of a strong ethnic consciousness present in their mythistory. This consciousness is closely related to the present, explaining and justifying their way of life and their tense relationship with their Aymara neighbors. In the story, mythic and historical discourse is fused into “mythistory” in order to construct their “ethnic identity,” concepts that provide the article’s theoretical framework.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sabine Dedenbach-Salazar Sáenz"
              }
            ],
            "files": [
              {
                "id": 8782,
                "title": "Dedenbach-Salazar-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Dedenbach-Salazar-1.jpg",
                "caption": "Uru-Chipaya language communities."
              },
              {
                "id": 8783,
                "title": "Dedenbach-Salazar-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Dedenbach-Salazar-2.jpg",
                "caption": "The Central Andes and Chipaya."
              },
              {
                "id": 8784,
                "title": "Dedenbach-Salazar-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Dedenbach-Salazar-3.jpg",
                "caption": "Chipaya and surroundings."
              },
              {
                "id": 8785,
                "title": "Dedenbach-Salazar-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Dedenbach-Salazar-4.jpg",
                "caption": "Santa Ana de Chipaya (DOBES project, 2005)."
              },
              {
                "id": 8786,
                "title": "Dedenbach-Salazar-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Dedenbach-Salazar-5.jpg",
                "caption": "View from outside the village: river, salty soil, houses in pasture-lands, and mountains (DOBES project, 2005)."
              },
              {
                "id": 8787,
                "title": "Dedenbach-Salazar-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Dedenbach-Salazar-6.jpg",
                "caption": "Bird-hunting, (DOBES project, 2005)."
              },
              {
                "id": 8788,
                "title": "Dedenbach-Salazar-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Dedenbach-Salazar-7.jpg",
                "caption": "Attending the pigs’ castration ceremony, in the pasture-lands (DOBES project, 2002)."
              },
              {
                "id": 8789,
                "title": "Dedenbach-Salazar-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Dedenbach-Salazar-8.jpg",
                "caption": "Archaeological <em>Chullpa</em> remains near Chipaya (DOBES project, 2005)."
              },
              {
                "id": 8790,
                "title": "Dedenbach-Salazar-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Dedenbach-Salazar-9.jpg",
                "caption": "<em>Chullpa</em> with human bones, near Chipaya (DOBES project, 2005)."
              },
              {
                "id": 8791,
                "title": "Dedenbach-Salazar-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Dedenbach-Salazar-10.jpg",
                "caption": "The bell-tower of Sabaya church (© Pascale Soubrane, 2009).<br /><a href=\"http://www.flickr.com/photos/twiga_269/3600778465/\" target=\"_blank\">http://www.flickr.com/photos/twiga_269/3600778465/</a>"
              },
              {
                "id": 8792,
                "title": "Dedenbach-Salazar-11.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Dedenbach-Salazar-11.mp3",
                "caption": "Origin of the Chipayas as told by a seventy-two-year-old man."
              }
            ]
          },
          {
            "id": 8772,
            "title": "Butterflies and Dragon-Eagles: Processing Epics from Southwest China",
            "content": "In the mountains of southwest China, epic narratives are part of the traditional performance-scapes of many ethnic minority cultures. In some cases locals participate in the preservation of oral or oral-connected epics from their respective areas. This article discusses the dynamics of acquiring and translating texts from two major ethnic minority groups in cooperation with local tradition-bearers, poets, and scholars.",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark Bender"
              }
            ],
            "files": [
              {
                "id": 8774,
                "title": "bender-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Bender-1.jpg",
                "caption": ""
              },
              {
                "id": 8775,
                "title": "bender-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Bender-2.jpg",
                "caption": ""
              },
              {
                "id": 8776,
                "title": "bender-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Bender-3.jpg",
                "caption": ""
              },
              {
                "id": 8777,
                "title": "bender-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Bender-4.jpg",
                "caption": ""
              },
              {
                "id": 8778,
                "title": "bender-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Bender-5.mp4",
                "caption": "This video provides a glimpse of the cultural and performance contexts of the Miao (Hmong) epics collected in Taijiang County, SE Guizhou province by Jin Dan and his collaborators."
              },
              {
                "id": 8779,
                "title": "bender-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/27i/full/Bender-6.mp4",
                "caption": "This video provides a glimpse of the cultural and performance context of Jjivo Zoqu’s text of the Nuosu <em>Book of Origins</em>.  The footage was taken in Xide county, Liangshan Yi Autonomous Prefecture."
              }
            ]
          },
          {
            "id": 8770,
            "title": "“With This Issue . . .”: A Record of <em>Oral Tradition</em>",
            "content": "<p>In honor and memory of John Miles Foley, this chronicle of <em>Oral Tradition</em> provides abridged versions of selected columns written by Professor Foley between 1986 and 2011, collected with the aim of highlighting the patterns of scholarship within <em>Oral Tradition</em> as well as the many and varied milestones in the journal’s development. Within these columns, we find unwavering dedication toward continuity in the journal’s original mission as well as the innovative spirit that advanced the journal through developments in technology and increasing globalization among <em>Oral Tradition</em>’s contributors and readership. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Editorial Staff"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 155,
            "issue_id": "122",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 156,
            "issue_id": "122",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27i/00_27.1cover.pdf"
          },
          {
            "id": 157,
            "issue_id": "122",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 158,
            "issue_id": "122",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27i/00_27.1fm.pdf"
          },
          {
            "id": 159,
            "issue_id": "122",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27i/11_27.1bm.pdf"
          },
          {
            "id": 160,
            "issue_id": "122",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27i/01_27.1.pdf"
          },
          {
            "id": 161,
            "issue_id": "122",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27i/oraltradition_27_1.zip"
          },
          {
            "id": 162,
            "issue_id": "122",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/27i/27.1complete.pdf"
          },
          {
            "id": 163,
            "issue_id": "122",
            "name": "Volume",
            "value": "27"
          },
          {
            "id": 164,
            "issue_id": "122",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 165,
            "issue_id": "122",
            "name": "Date",
            "value": "2012-03-01"
          }
        ]
      },
      {
        "id": 132,
        "name": "26ii",
        "title": "Volume 26, Issue 2: Festschrift for John Miles Foley",
        "articles": [
          {
            "id": 8933,
            "title": "Foreword",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joseph Falaky Nagy"
              }
            ],
            "files": []
          },
          {
            "id": 8931,
            "title": "Introduction and <em>Tabula Gratulatoria</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "R. Scott Garner"
              },
              {
                "name": "Lori Ann Garner"
              }
            ],
            "files": []
          },
          {
            "id": 8929,
            "title": "Juxtaposing <em>Cogadh Gáedel re Gallaib</em> with <em>Orkneyinga saga</em>",
            "content": "<p>The fields of Scandinavian studies and Celtic studies have reveled in the rich trove of vernacular literature preserved in medieval forms of Icelandic and Irish. The scholarly traditions within the fields, however, have hindered cross-cultural comparison, despite the fact that Irish and Scandinavians had abundant cultural contact and produced texts that at times refer to each other in detail. This paper explores the usefulness of comparing two often-marginalized works—the Irish Munster saga and royal panegyric, <em>Cogadh Gáedel re Gallaib</em>, and the history of the earls of Orkney known as <em>Orkneyinga saga</em>.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas A. DuBois"
              }
            ],
            "files": []
          },
          {
            "id": 8927,
            "title": "Toward an Ethnopoetically Grounded Edition of Homer’s <em>Odyssey</em>",
            "content": "<p>How would a folklorist, if miraculously transported to an eighth-century BCE social gathering in Ionia where Homer was performing a version of the <em>Odyssey</em>, transcribe that oral performance into a textual form? What would such a transcription and textualization look like? Simply imagining this utterly fanciful exercise forces us to raise otherwise seldom asked questions about the social setting of the performance, the demeanor and involvement of the audience, the length of the performance units, the nature of the singing, the contribution of musical instrumentation, and the function of non-verbal cues by the bard.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Steve Reece"
              }
            ],
            "files": []
          },
          {
            "id": 8925,
            "title": "Matija Murko, Wilhelm Radloff, and Oral Epic Studies",
            "content": "<p>Commonly regarded as pioneers in the documentation of oral epic singing, Wilhelm Radloff and Matija Murko were personally acquainted and spent time together in St. Petersburg, Russia, during the 1880s—a fact not widely known until now. This essay excavates Murko’s (untranslated) Slovenian-language memoirs and discloses a number of historical considerations from the autobiographical account, placing this acquaintance into a wider context, always with an eye toward the history of folklore studies and with special attention paid to a history of Central European research that has been at times neglected on account of its linguistic and historical complexity.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Aaron Phillip Tate"
              }
            ],
            "files": []
          },
          {
            "id": 8923,
            "title": "“A Swarm in July”: Beekeeping Perspectives  on the Old English <em>Wið Ymbe</em> Charm",
            "content": "<p>This exploration of an Old English charm against a swarm of bees (<em>wið ymbe</em>) augments and complements prior work on this enigmatic text by bringing knowledgeable and experienced beekeepers <em>directly</em> into the discussion. Based on insights gained through sharing the text with them and inviting their reactions, this essay offers a highly collaborative and genuinely interdisciplinary interpretation of both the charm’s ritual instructions and the poetic incantation.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lori Ann Garner"
              },
              {
                "name": "Kayla M. Miller"
              }
            ],
            "files": []
          },
          {
            "id": 8921,
            "title": "Cicero the Homerist",
            "content": "<p>Cicero clearly knew both the texts of Homer and the Alexandrian scholarship on those texts, but he chose not to exhibit this knowledge frequently in his works. Instead, his expertise in Homer and Homeric scholarship is displayed only in accordance with specific concerns of genre and audience: most of Cicero’s Homeric citations come in his letters to Atticus; a few appear in his philosophical works; almost none are found in his speeches. Such variation shows that Cicero was well aware of the ambiguous status of Greek literature and learning in the Roman world.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Carolyn Higbie"
              }
            ],
            "files": []
          },
          {
            "id": 8919,
            "title": "Toward a Ritual Poetics: <em>Dream of the Rood</em> as a Case Study",
            "content": "<p>The notion of “ritual poetics” explored in this essay weds the findings of John Miles Foley’s immanent art to ritual theories of signification in order to show that some features of early medieval verse may carry a metonymic force linking the spoken or oral-related written word to the vivid, multilayered experience of ritualized situations. The hypothesis that ritual features, when integrated into oral-related poems, preserve their association with lived, emergent ritual processes is examined through close analysis of <em>Dream of the Rood</em>. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Heather Maring"
              }
            ],
            "files": []
          },
          {
            "id": 8917,
            "title": "Oral Tradition and Sappho",
            "content": "<p>Through an exploration of Sappho’s verse-structuring tendencies and repeated phraseology, the current essay demonstrates that Sappho’s stanzaic poetry was enabled primarily by a traditional system of composition that allowed her words to be encoded with extralexical meaning (or to use John Miles Foley’s term, “traditional referentiality”). Through a renewed appreciation of these oral traditional influences on Sappho’s poetry we thus can begin to approach an understanding of this art much closer to that held by its earliest ancient Greek audiences.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "R. Scott Garner"
              }
            ],
            "files": []
          },
          {
            "id": 8904,
            "title": "Variation within Limits: An Evolutionary Approach to the Structure and Dynamics of the Multiform",
            "content": "<p>This essay draws upon research in evolutionary biology and cognitive psychology to explain the evolution and stability of the oral-traditional multiform. The mind tends to categorize variable entities in terms of cognitive <em>prototypes</em>. The dynamics of human mnemonic and communicative processes then generate both variability (in the absence of written texts) and contrasting selection pressure on multiform oral-traditional forms to evolve towards these mental abstractions, thereby producing the variability of the multiform. By visualizing the variation spaces of such cultural entities as <em>adaptive landscapes</em>, we see that variation-within-limits of the multiform, rather than being paradoxical, results from universal processes of replication and selection. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Michael D. C. Drout"
              }
            ],
            "files": [
              {
                "id": 8906,
                "title": "drout-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/drout-1.jpg",
                "caption": "Representation of the relative fitness of morphotypes of Cædmon’s Hymn, line 5."
              },
              {
                "id": 8907,
                "title": "drout-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/drout-2.jpg",
                "caption": "“Skyscraper” landscape represents fitness of multiple discrete morphotypes."
              },
              {
                "id": 8908,
                "title": "drout-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/drout-3.jpg",
                "caption": "Many minor variations of morphotypes create a near-gradient."
              },
              {
                "id": 8909,
                "title": "drout-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/drout-4.jpg",
                "caption": "Adaptive landscape populated with entities."
              },
              {
                "id": 8910,
                "title": "drout-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/drout-5.jpg",
                "caption": "Adaptive landscape populated with entities."
              },
              {
                "id": 8911,
                "title": "drout-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/drout-6.jpg",
                "caption": "Cognitive categorizations on two axes."
              },
              {
                "id": 8912,
                "title": "drout-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/drout-7.jpg",
                "caption": "Influence model – oversimplified."
              },
              {
                "id": 8913,
                "title": "drout-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/drout-8.jpg",
                "caption": "Influence model."
              },
              {
                "id": 8914,
                "title": "drout-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/drout-9.jpg",
                "caption": "A cognitive prototype (represented by the sphere) influences the shape of an adaptive landscape."
              },
              {
                "id": 8915,
                "title": "drout-10a.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/drout-10a.jpg",
                "caption": "At a given time, regions of adaptive morphospace appear discrete."
              },
              {
                "id": 8916,
                "title": "drout-10b.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/drout-10b.jpg",
                "caption": "Areas of morphospace that appear discrete during a certain time interval (represented by the .“sea level”) are historiacally continuous."
              }
            ]
          },
          {
            "id": 8902,
            "title": "Leslie Marmon Silko and Simon J. Ortiz:  Pathways to the Tradition",
            "content": "<p>Both Leslie Marmon Silko and Simon J. Ortiz have retold the story of a 1952 murder by two Pueblo brothers, a story that both writers first heard during their childhood years as it quickly became part of the local Native American traditional corpus. Both Silko and Ortiz are self-consciously indebted to the Native American storytelling tradition, particularly with respect to its malleability in the face of change, a fluidity that operates in tension with the preservation of certain fundamental religious and philosophical constants. Accordingly, Silko and Ortiz see their stories as providing pathways to the tradition, molding, reforming, and contributing to it without departing from it. Content, tone, style, and purpose are analyzed to reveal the variants in their redactions.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Dave Henderson"
              }
            ],
            "files": []
          },
          {
            "id": 8900,
            "title": "“Stricken to Silence”: Authoritative Response, Homeric Irony, and the Peril of a Missed Language Cue",
            "content": "<p>The formula “Thus he spoke, but they all were stricken to silence” (ὣς ἔφαθ’, οἳ δ’ ἄρα πάντες ἀκὴν ἐγένοντο σιωπῇ) has received significant treatment in a number of recent studies, although the overarching significance of the formula for what follows in each narrative moment has not yet been fully recognized. This article offers a reconsideration of the formula’s referential meaning and concludes that it introduces the authoritative response of a group while determining the trajectory of the ensuing narrative. In the two instances where the formula’s cue is not followed (Il. 9.430 and Od. 20.320), Homer is employing irony to highlight Achilles’ and the suitors’ deafness to the pleas and warnings of others.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Andrew E. Porter"
              }
            ],
            "files": []
          },
          {
            "id": 8898,
            "title": "Vernacular Phrasal Display: Towards the Definition of a Form",
            "content": "<p>This essay discusses a genre of folksay, one paradoxically widely collected but little studied, lacking even a satisfactory definition or an agreed-upon name. The term “proverbial comparison”—which properly acknowledges the relation of the form to proverbs as understood more broadly—has sometimes been used to designate this form, but its defining characteristics are more commonly left to be inferred from lists of “rural-” or “old-time expressions,” although the form is quite at home in contemporary urban settings. Following on the regularity with which they are attributed to particular eloquent individuals, the present article conceptualizes these speech-items as a performance genre and examines their social functions. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Adam Brooke Davis"
              }
            ],
            "files": []
          },
          {
            "id": 8896,
            "title": "The Role of Memory in the Tradition Represented by the Deuteronomic History and the Book of Chronicles",
            "content": "<p>Albert Lord and John Miles Foley have discussed the role of memory and multiformity in oral traditions. Their work helps us better understand the interplay of the oral and the written within the context of communal memory as well as the role of multiformity within the broader tradition. This essay argues that the multiformity present in the textual traditions of Samuel-Kings/Chronicles points to the existence of a broader tradition behind these texts that existed in the communal memory (both oral and written) of ancient Israel.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Raymond F. Person, Jr."
              }
            ],
            "files": []
          },
          {
            "id": 8894,
            "title": "Memory on Canvas: <em>Commedia dell’Arte</em> as a Model for Homeric Performance",
            "content": "<p>Although, thanks to many years of research and comparative scholarship, we have a more complete understanding of the making of the Homeric poems, it can still be useful to apply the performance practices of other oral—or oral-derived—arts to increase that understanding. This essay applies the techniques of the Italian <em>commedia dell’arte</em>, which combines the written and the created-in-performance, to augment our knowledge of just how such poems may originally have been performed.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Timothy W. Boyd"
              }
            ],
            "files": []
          },
          {
            "id": 8866,
            "title": "Changing Traditions and Village Development in Kalotaszentkirály",
            "content": "<p>The continuity of village traditions depends on the stability and cohesion of village communities. Since the opening of Transylvania after the fall of Nicolae Ceauşescu, there has been a sort of revival of Hungarian village dance and music, on the one hand, but, on the longer term, the communities themselves are threatened by economic challenges and by consequent demographic changes. This essay is based on field research conducted in Kalotaszentkirály (Sincraiu) from 1995 to 2010.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Wayne Kraft"
              }
            ],
            "files": [
              {
                "id": 8868,
                "title": "kraft-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-1.mp4",
                "caption": "Instruction at Kalotaszeg dance camp in Kalotaszentkirály, 1995: András “Cucus” and Tekla Tötszegi from Méra are the best-known village dance teachers."
              },
              {
                "id": 8869,
                "title": "kraft-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-2.jpg",
                "caption": "Welcome Sign, 2010."
              },
              {
                "id": 8870,
                "title": "kraft-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-3.mp4",
                "caption": "Water buffalo returning from pasture, 2000: As the herd returns to the village, the buffalos and cows enter their own yards/barns for the night. By the time the sizable herd reaches the further end of the village, its numbers are already greatly diminished."
              },
              {
                "id": 8871,
                "title": "kraft-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-4.jpg",
                "caption": "Signage 1, 2007."
              },
              {
                "id": 8872,
                "title": "kraft-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-5.jpg",
                "caption": "Signage 2, 2007."
              },
              {
                "id": 8873,
                "title": "kraft-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-6.jpg",
                "caption": "Path marker, 2010."
              },
              {
                "id": 8874,
                "title": "kraft-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-7.jpg",
                "caption": "Decorative well 1, 2007."
              },
              {
                "id": 8875,
                "title": "kraft-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-8.jpg",
                "caption": "Decorative well 2, 2007."
              },
              {
                "id": 8876,
                "title": "kraft-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-9.jpg",
                "caption": "Decorative well 3, 2007."
              },
              {
                "id": 8877,
                "title": "kraft-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-10.jpg",
                "caption": "Guesthouse kitsch 1, 2010."
              },
              {
                "id": 8878,
                "title": "kraft-11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-11.jpg",
                "caption": "Guesthouse kitsch 2, 2010."
              },
              {
                "id": 8879,
                "title": "kraft-12.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-12.jpg",
                "caption": "Main school building and gate, 2010."
              },
              {
                "id": 8880,
                "title": "kraft-13.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-13.jpg",
                "caption": "Bench in the style of Szeklerland, 2010."
              },
              {
                "id": 8881,
                "title": "kraft-14.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-14.jpg",
                "caption": "Bench and social interaction, 2010."
              },
              {
                "id": 8882,
                "title": "kraft-15.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-15.jpg",
                "caption": "Trouser-belt fields 1, 2010."
              },
              {
                "id": 8883,
                "title": "kraft-16.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-16.jpg",
                "caption": "Trouser-belt fields 2, 2010."
              },
              {
                "id": 8884,
                "title": "kraft-17.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-17.jpg",
                "caption": "Potato harvest work party, 2007."
              },
              {
                "id": 8885,
                "title": "kraft-18.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-18.jpg",
                "caption": "First day of potato harvest, 2007."
              },
              {
                "id": 8886,
                "title": "kraft-19.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-19.jpg",
                "caption": "Herd and combine, 2010."
              },
              {
                "id": 8887,
                "title": "kraft-20.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-20.jpg",
                "caption": "Herds return from pasture, 2007."
              },
              {
                "id": 8888,
                "title": "kraft-21.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-21.jpg",
                "caption": "Herds return from pasture 2, 2007."
              },
              {
                "id": 8889,
                "title": "kraft-22.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-22.jpg",
                "caption": "Part of the herd at dusk, 2007."
              },
              {
                "id": 8890,
                "title": "kraft-23.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-23.mp4",
                "caption": "Walking the milk can to the collection point, 2000: This ritual ended as Romania began to conform to EU guidelines that do not allow for the pooling of milk from individual households."
              },
              {
                "id": 8891,
                "title": "kraft-24.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-24.mp4",
                "caption": "Márton and Anna Bálint, dancing in heritage dance group, 1995; the center couple are Márton and Anna Bálint."
              },
              {
                "id": 8892,
                "title": "kraft-25.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-25.mp4",
                "caption": "Márton and Anna Bálint, demonstrating Kalotaszentkirály’s couples’ dance."
              },
              {
                "id": 8893,
                "title": "kraft-26.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/kraft-26.jpg",
                "caption": "Villager shares his life story, 2010."
              }
            ]
          },
          {
            "id": 8863,
            "title": "Intentionally Adrift: What The Pathways Project Can Teach Us About Teaching and Learning",
            "content": "<p>Recent generations of college students, brought up in a digital world of short bytes of information and nonlinear patterns of reading, often present a particular challenge to professors of text-heavy disciplines such as literature, history, and English. This essay explores recent theories of learning and the scholarship of oral tradition, especially that of John Miles Foley, in an attempt to discover how an understanding of pathways in the oWorld and eWorld can provide us with better ways to teach texts of all types.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bonnie D. Irwin"
              }
            ],
            "files": [
              {
                "id": 8865,
                "title": "irwin-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/irwin-1.jpg",
                "caption": "Bonnie’s Research Map."
              }
            ]
          },
          {
            "id": 8861,
            "title": "<em>Sean-nós i gConamara</em> / <em>Sean-nós in Connemara</em>:  Digital Media and Oral Tradition in the West of Ireland",
            "content": "<p>In the west of Ireland, Irish-speaking regions called <em>gaeltachts</em> are home to a long-standing but understudied form of unaccompanied and highly ornamented singing in Irish known as <em>sean-nós</em>. Supported by fieldwork conducted with sean-nós singers in the South Conamara <em>gaeltacht</em> in 2005, this article seeks to provide an ethnographically-based introduction to <em>sean-nós</em> and an examination of the ways in which digital technologies are being used to continue this fascinating tradition.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Holly Hobbs"
              }
            ],
            "files": []
          },
          {
            "id": 8859,
            "title": "The Metonym: Rhetoric and Oral Tradition at the Crossroads",
            "content": "<p>This article explores the intersection between scholarship of rhetoric and oral tradition through the trope of the metonym. Metonymic referentiality has a persuasive function in contemporary discourse through its ability to immerse speaker and audience within a shared context. While a given rhetorical situation might not be strictly traditional in performance, the invoking of traditional associations through metonyms can be a powerful rhetorical act that, if used effectively, creates a deep sense of commonality between rhetor and audience.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Catherine Quick"
              }
            ],
            "files": []
          },
          {
            "id": 8857,
            "title": "Heroic Register, Oral Tradition, and the <em>Alliterative Morte Arthure</em>",
            "content": "<p>By employing an oral traditional approach to the text, this essay investigates how the use of alliteration and speech-acts in the <em>Alliterative Morte Arthure</em> establishes a heroic register that marks the poem as participating in a tradition hearkening back to Old English heroic models. The text begins with an appeal to the audience to listen and to hear the tale, highlighting the importance of aurality and speech and signaling a way to “read” the poem that distinguishes it from Anglo-Norman literary tradition. By making such distinctions, this approach elucidates passages often deemed confusing, such as the two narrated deaths of the Roman Emperor Lucius.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Rebecca Richardson Mouser"
              }
            ],
            "files": []
          },
          {
            "id": 8855,
            "title": "Prisons, Performance Arena, and Occupational Humor",
            "content": "<p>Correctional officers use occupational humor to communicate complex meanings. These messages are often essential to occupational and institutional well-being, yet are rarely studied. Occupational humor in correctional work takes place within a space that can be more productively understood through what has been described as the performance arena. This short study draws on the author’s own ethnographic research among white, Midwestern correctional officers and concludes with a brief performance-centered analysis of a joke collected by Ted Conover. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Claire Schmidt"
              }
            ],
            "files": []
          },
          {
            "id": 8853,
            "title": "<em>Beowulf</em>’s Singers of Tales as Hyperlinks",
            "content": "<p>The scenes of oral poetic performance that occur throughout <em>Beowulf</em> have received an array of critical responses. This essay builds upon recent work on the correlation between oral tradition and new media to argue that the depicted performances in <em>Beowulf</em> function similarly to hyperlinks in the way that they connect the main narrative of the poem to other traditional songs and tales. As a result of such scenes, <em>Beowulf</em> is structured much more like an open and ongoing performance event than a fixed or finished text.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Peter Ramey"
              }
            ],
            "files": []
          },
          {
            "id": 8851,
            "title": "Rethinking Individual Authorship: Robert Burns, Oral Tradition, and the Twenty-First Century",
            "content": "<p>The songs of late-eighteenth-century Scottish poet Robert Burns provide a rich case study of literature that challenges existing notions of the author as an autonomous entity. Responding to twenty-first-century examples of contested issues of intellectual property and plagiarism in an age of digital media, this project illustrates the ways in which precepts of oral tradition can inform our thinking about cultural production within contexts seemingly permeated by ever-present literacy or text-based thinking in order to provide a new outlook on such situations of artistic borrowing or “plagiarism.”</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ruth Knezevich"
              }
            ],
            "files": []
          },
          {
            "id": 8849,
            "title": "“A Misnomer of Sizeable Proportions”: SMS and Oral Tradition",
            "content": "<p>As a relatively recent communication technology, SMS—more colloquially known as text-messaging—has received a good deal of attention both in popular media and the academy. For linguists in particular, text messaging has emerged as a rich source of study. This essay proposes a merger of current research on text messaging and the study of oral tradition in order to shed light on the relationship between this new mode of communication and the workings of consciousness being transformed by what John Foley has termed the eAgora.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sarah Zurhellen"
              }
            ],
            "files": []
          },
          {
            "id": 8847,
            "title": "The Old English Verse Line in Translation: Steps Toward a New Theory of Page Presentation",
            "content": "<p>This short essay examines the practice of printing translations of Old English poems in the predictable displays of full- and half-line lineation. While the Old English verse line cannot be said to exist as a visual construction, any prosodic system in present-day English into which an Old English poem might be translated must have visual lineation as a feature of its prosody. Accordingly, this essay insists that the interplay between the aural and visual constructions of lines should be of central concern to the verse translator. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Derek Updegraff"
              }
            ],
            "files": []
          },
          {
            "id": 8845,
            "title": "Communication Then and Now",
            "content": "<p>This essay examines the preaching of Jesus in relation to research into communication in primarily oral cultures, and then turns to the consideration of the communication situation of postmodernism. Several parallels are drawn between the approach of Jesus and the demands of the postmodern hearer, which should be helpful to preachers in the twenty-first century.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bruce E. Shields"
              }
            ],
            "files": []
          },
          {
            "id": 8841,
            "title": "Remix: <em>Pathways of the Mind</em>",
            "content": "<p>This brief piece discusses John Foley’s recent work on The Pathways Project, which explores the relationship between oral tradition and Internet technology. “Mashups” serves as a case study and introduction to some principal concepts in this project, with parallels between oral tradition, the ancient <em>cento</em>, and contemporary mashup music illustrating the correspondences between the oral, textual, and electronic worlds.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Morgan E. Grey"
              }
            ],
            "files": [
              {
                "id": 8843,
                "title": "grey-1.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/grey-1.mp3",
                "caption": "Grey Album."
              },
              {
                "id": 8844,
                "title": "grey-2.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26ii/full/grey-2.mp3",
                "caption": "Night Ripper."
              }
            ]
          },
          {
            "id": 8839,
            "title": "A Personal Appreciation: How John Miles Foley Laid the Foundation for My Life in an Ashram",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ward Parks"
              }
            ],
            "files": []
          },
          {
            "id": 8837,
            "title": "Annotated Bibliography of Works by John Miles Foley",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "R. Scott Garner"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 166,
            "issue_id": "132",
            "name": "Subtitle",
            "value": "Festschrift for John Miles Foley"
          },
          {
            "id": 167,
            "issue_id": "132",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26ii/00_26.2cover.pdf"
          },
          {
            "id": 168,
            "issue_id": "132",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 169,
            "issue_id": "132",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26ii/00_26.2fm.pdf"
          },
          {
            "id": 170,
            "issue_id": "132",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26ii/31_26.2fm.pdf"
          },
          {
            "id": 171,
            "issue_id": "132",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26ii/01_26.2.pdf"
          },
          {
            "id": 172,
            "issue_id": "132",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26ii/oraltradition_26_2.zip"
          },
          {
            "id": 173,
            "issue_id": "132",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26ii/26.2complete.pdf"
          },
          {
            "id": 174,
            "issue_id": "132",
            "name": "Volume",
            "value": "26"
          },
          {
            "id": 175,
            "issue_id": "132",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 176,
            "issue_id": "132",
            "name": "Date",
            "value": "2011-10-01"
          }
        ]
      },
      {
        "id": 160,
        "name": "26i",
        "title": "Volume 26, Issue 1",
        "articles": [
          {
            "id": 8990,
            "title": "Revenge of the Spoken Word?: Writing, Performance, and New Media in Urban West Africa",
            "content": "<p>This paper examines the impact of digital media on the relationship between writing, performance, and textuality from the perspective of literate verbal artists in Mali. It considers why some highly educated verbal artists in urban Africa self-identify as writers despite the oralizing properties of new media, and despite the fact that their own works circulate entirely through performance. The motivating factors are identified as a desire to present themselves as composers rather than as performers of texts, and to differentiate their work from that of minimally educated performers of texts associated with traditional orality.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Moradewun Adejunmobi"
              }
            ],
            "files": [
              {
                "id": 8992,
                "title": "Adejunmobi-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Adejunmobi-1.jpg",
                "caption": "Awa Dembélé Macalou reading a story to children at the Marché de Missra in Bamako."
              },
              {
                "id": 8993,
                "title": "Adejunmobi.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Adejunmobi.mp3",
                "caption": "Awa Dembélé Macalou singing a refrain from the story of Nayé as she recited the story in French."
              },
              {
                "id": 8994,
                "title": "Adejunmobi-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Adejunmobi-2.jpg",
                "caption": "Students at Lycée Manssa Maka Diabaté learning how to write slam poetry in March 2009. Their teachers are BoubacHaman and Azizsiten’k."
              }
            ]
          },
          {
            "id": 8967,
            "title": "Singing Dead Tales to Life: Rhetorical Strategies in Shandong Fast Tales",
            "content": "<p>This article provides a brief overview of the Shandong fast tale tradition, a Chinese oral performance genre that began in rural northern China approximately four hundred years ago. Included in this overview are brief descriptions of the origins, audience composition, tale length, repertoire, and major characteristics of the stories and performances. Following these descriptions is a discussion of the expressive and rhetorical devices used by the tale-tellers as they perform live, such as formulaic language, repetition, character roles, shifts in speech register, body language, facial expressions, memory, onomatopoeia, physical humor, and hyperbolic language.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Eric Shepherd"
              }
            ],
            "files": [
              {
                "id": 8969,
                "title": "Shepherd-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-1.jpg",
                "caption": "<em>Yuanyangban</em>, the primary brass rhythm-keeping device used in Shandong fast tales."
              },
              {
                "id": 8970,
                "title": "Shepherd-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-2.mp4",
                "caption": "Master fast tale performer Wu Yanguo performs <em>Cimao</em> (<em>Schlocky</em>) before an audience of 30,000 people in Penglai, Shandong, May 2005.<p>Video: Eric Shepherd."
              },
              {
                "id": 8971,
                "title": "Shepherd-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-3.mp4",
                "caption": "Master fast tale performer Wu Yanguo performs <em>Da puke</em> (<em>Playing Poker</em>) during a holiday variety show in Dongying, Shandong, May 2005."
              },
              {
                "id": 8972,
                "title": "Shepherd-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-4.jpg",
                "caption": "Afternoon crowd at <em>Jiangning huiguan</em>, July 2010."
              },
              {
                "id": 8973,
                "title": "Shepherd-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-5.jpg",
                "caption": "The audience at a weekend evening performance at <em>Jiangning huiguan</em>, July 17, 2010."
              },
              {
                "id": 8974,
                "title": "Shepherd-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-6.jpg",
                "caption": "The audience on a weekday evening at <em>Jiangning huiguan</em>, July 2010."
              },
              {
                "id": 8975,
                "title": "Shepherd-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-7.jpg",
                "caption": "Evening performance, <em>Jiangning huiguan</em>, July 2, 2009."
              },
              {
                "id": 8976,
                "title": "Shepherd-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-8.jpg",
                "caption": "Gao Hongsheng, son of the renowned performer Gao Yuanjun, in character role at <em>Jiangning huiguan</em>, July 2010."
              },
              {
                "id": 8977,
                "title": "Shepherd-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-9.jpg",
                "caption": "Master fast tale performer Wu Yanguo in narrator role, Qingdao Shanfo Hotel, July 2010."
              },
              {
                "id": 8978,
                "title": "Shepherd-10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-10.jpg",
                "caption": "Liu Liwu in narrator role, <em>Jiangning huiguan</em>, July 2010."
              },
              {
                "id": 8979,
                "title": "Shepherd-11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-11.jpg",
                "caption": "Li Hongmin in character role, <em>Jiangning huiguan</em>, July 2010."
              },
              {
                "id": 8980,
                "title": "Shepherd-12.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-12.mp4",
                "caption": "Master fast tale performer Wu Yanguo demonstrates a segment from the famous tale <em>Wu Song Fights the Tiger</em> during a class for five apprentices in his home in Qingdao, January 2005."
              },
              {
                "id": 8981,
                "title": "Shepherd-13.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-13.jpg",
                "caption": "Wu Yanguo performs <em>Ma Erha</em> at Haimengyuan Hotel, July 2002."
              },
              {
                "id": 8982,
                "title": "Shepherd-14.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-14.jpg",
                "caption": "Dong Jiancheng performs <em>Shazi yao wawa</em> (<em>Knucklehead Wants a Tot</em>) at Haimengyuan Hotel, July 2002."
              },
              {
                "id": 8983,
                "title": "Shepherd-15.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-15.jpg",
                "caption": "Gao Hongsheng performs at <em>Jiangning huiguan</em>, July 2010."
              },
              {
                "id": 8984,
                "title": "Shepherd-16.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-16.jpg",
                "caption": "Head of the Shandong Fast Tale Research Association, Sun Zhenye, performs at <em>Jiangning huiguan</em>, July 2010."
              },
              {
                "id": 8985,
                "title": "Shepherd-17.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-17.jpg",
                "caption": "Wu Yanguo performs <em>Dazhen</em> (<em>Getting an Injection</em>), at Haimengyuan Hotel, October 2004."
              },
              {
                "id": 8986,
                "title": "Shepherd-18.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-18.jpg",
                "caption": "Gao Jingzuo performs <em>Wu Song Gan Hui</em> (<em>Wu Song Goes to the Fair</em>) at <em>Jiangning huiguan</em>, July 2010."
              },
              {
                "id": 8987,
                "title": "Shepherd-19.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-19.jpg",
                "caption": "Bamboo clapper fast tale performer Li Dongfeng performs at <em>Jiangning huiguan</em>, July 2010."
              },
              {
                "id": 8988,
                "title": "Shepherd-20.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-20.jpg",
                "caption": "Qin Yongchao performs <em>Wu Song Fights the Tiger</em> at <em>Jiangning huiguan</em>, July 2010."
              },
              {
                "id": 8989,
                "title": "Shepherd-21.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Shepherd-21.mp4",
                "caption": "Wu Yanguo performs <em>Dazhen</em> (<em>Getting an Injection</em>) at Haimengyuan Hotel, July 2002."
              }
            ]
          },
          {
            "id": 8964,
            "title": "Crossing Boundaries, Breaking Rules: Continuity and Social Transformation in Trickster Tales from Central Asia",
            "content": "<p>The article investigates stories from Kyrgyzstan depicting the adventures of a folk hero in tsarist Russia and the Soviet Union. These specimens of oral tradition were published in the Soviet and post-Soviet era and are therefore situated at the interface of the oral and written realms. The authors argue that these tales should not be dismissed as de-contextualized and deprived of their original meaning, but as re-contextualized narratives that have been adjusted according to changing power relations.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ildikó Bellér-Hann"
              },
              {
                "name": "Raushan Sharshenova"
              }
            ],
            "files": [
              {
                "id": 8966,
                "title": "BellerHannMap.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/BellerHannMap.jpg",
                "caption": "Regions in Northern Kyrgyzstan where Kuyruchuk has been popular."
              }
            ]
          },
          {
            "id": 8958,
            "title": "A Case Study in Byzantine Dragon-Slaying: Digenes and the Serpent",
            "content": "<p>The Byzantine epic <em>Digenes Akrites</em> has similarities with ancient and medieval Iranian traditions that, in consideration of the epic’s Eastern settings, suggest Iranian influences.  Digenes resembles dragon-slaying heroes of other Indo-European traditions.  He also resembles the Irish hero <em>Cú Chulainn</em> in that he is not psychologically fit to live in the midst of the community that depends on his protection.  Freudian readings of Digenes’ encounters with the dragon and the Amazon Maximou are proposed.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Christopher Livanos"
              }
            ],
            "files": [
              {
                "id": 8960,
                "title": "Livanos-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Livanos-1.jpg",
                "caption": "“Digenes and the dragon” from the Athenian agora excavations (late twelfth or early thirteenth century)."
              },
              {
                "id": 8961,
                "title": "Livanos-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Livanos-2.jpg",
                "caption": "Digenes and the dragon, reconstructed by J. Travlos based on fragments from Corinth and Athens."
              },
              {
                "id": 8962,
                "title": "Livanos-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Livanos-3.jpg",
                "caption": "Naked dragon slayer (twelfth century), excavated at Thebes."
              },
              {
                "id": 8963,
                "title": "Livanos-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Livanos-4.jpg",
                "caption": "Hero (Herakles?) slaying a serpent, from an Etruscan vase at Perugia."
              }
            ]
          },
          {
            "id": 8956,
            "title": "The Forgotten Text of Nikolai Golovin: New Light on the Igor Tale",
            "content": "<p>Mann argues that a rare text of the <em>Skazanie o Mamaevom poboishche</em> comes from an early, fifteenth-century redaction that scholars could never locate—a redaction that is the prototype for all the redactions that have been studied heretofore. He maintains that unique parallels between this redaction and the <em>Slovo o polka Igoreve</em> support the hypothesis that the Igor Tale was an oral epic song in a tradition that actually continued into the fourteenth and fifteenth centuries, when oral tales about the Kulikovo Battle (1380) were composed. He places the new parallels in the context of other evidence for oral composition in the Igor Tale.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Robert Mann"
              }
            ],
            "files": []
          },
          {
            "id": 8954,
            "title": "Collecting South Slavic Oral Epic in 1864: Luka Marjanović’s Earliest Account",
            "content": "<p>Luka Marjanović (1844-1920) collected more than 250,000 verses from epic singers in Croatian and Bosnian regions in 1886-88, excerpts of which were published in 1898-99 in the seminal two-volume anthology, <em>Hrvatske narodne pjesme</em> [<em>Croatian Folk Songs</em>] <em>III-IV</em>. Marjanović’s first fieldwork report, however, is virtually unknown, dating from an 1864 preface written for a songbook. This article provides a translation of that preface, with accompanying notes and introduction, and thus supplements current scholarship on nineteenth-century oral epic studies.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Aaron Phillip Tate"
              }
            ],
            "files": []
          },
          {
            "id": 8951,
            "title": "Possibilities of Reality, Variety of Versions: The Historical Consciousness of Ainu Folktales",
            "content": "<p>In Ainu oral literature there are ubiquitous motifs or story-patterns shared among stories. These stories are integrated by a certain motif, and collectively compose interrelated corpora. To understand each individual narrative, we should refer to other stories based on traditional referentiality. This article illustrates how Ainu oral literature can be interpreted, focusing on one of its major motifs: the trade between the Ainu and the <em>Wajin</em>, or ethnic Japanese. In the process, the historical consciousness of the Ainu narratives is also considered.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Minako Sakata"
              }
            ],
            "files": [
              {
                "id": 8953,
                "title": "SakataMap.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/SakataMap.jpg",
                "caption": "Traditional territories of the Ainu."
              }
            ]
          },
          {
            "id": 8937,
            "title": "Pir Sultan Abdal: Encounters with Persona in Alevi Lyric Song",
            "content": "<p>The lyric songs of Pir Sultan Abdal provide one of the richest perspectives on the historical construction, communal perceptions, and creative impetus of Alevi culture. While the traditional verse and persona associated with Pir Sultan Abdal have engendered a large body of commentary and texts in Turkey since the early twentieth century, Alevi lyric song (<em>deyiş</em>), and Pir Sultan Abdal in particular, have received little attention in English-language scholarship. This article presents an introductory work in English on Pir Sultan Abdal, providing a presentation of this significant persona by focusing on encounters in text and expressive culture in order to establish the beginnings of a broad interpretive perspective. More specifically, the self-naming convention (<em>mahlas</em>), as exemplified in the case of Pir Sultan Abdal, is identified as a largely overlooked lyric device requiring further scholarly and comparative investigation.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Paul Koerbin"
              }
            ],
            "files": [
              {
                "id": 8939,
                "title": "Koerbin-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-1.jpg",
                "caption": "View of the village of Banaz, Sivas Province, Turkey (June 2007)."
              },
              {
                "id": 8940,
                "title": "Koerbin-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-2.jpg",
                "caption": "View of the Pir Sultan Abdal festival site looking towards Yılız Dağı (Star Mountain). Banaz, Sivas Province, Turkey (June 2007)."
              },
              {
                "id": 8941,
                "title": "Koerbin-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-3.jpg",
                "caption": "View of the Pir Sultan Abdal festival from rear of the amphitheater looking towards Yıldız Dağı (Star Mountain). The Pir Sultan Abdal statue can be seen on the right. Banaz, Sivas Province, Turkey (June 2007)."
              },
              {
                "id": 8942,
                "title": "Koerbin-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-4.jpg",
                "caption": "Banazlı elders performing a semah (ritual dance) at the opening of the Pir Sultan Abdal festival. The singer and bağlama player is İsmail Şimşek. Banaz, Sivas Province, Turkey (22 June 2007)."
              },
              {
                "id": 8943,
                "title": "Koerbin-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-5.jpg",
                "caption": "Tolga Sağ performing with bağlama at the Pir Sultan Abdal festival. Banaz, Sivas Province, Turkey (22 June 2007)."
              },
              {
                "id": 8944,
                "title": "Koerbin-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-6.jpg",
                "caption": "Memorial at Banaz for the victims of the attack on Madımak Otel in Sivas during the 1993 Pir Sultan Abdal festival. Banaz, Sivas Province, Turkey (June 2007)."
              },
              {
                "id": 8945,
                "title": "Koerbin-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-7.jpg",
                "caption": "Memorial at Banaz remembering the names of the victims of the attack on Madımak Otel in Sivas during the 1993 Pir Sultan Abdal festival. Banaz, Sivas Province, Turkey (June 2007)."
              },
              {
                "id": 8946,
                "title": "Koerbin-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-8.jpg",
                "caption": "View of the Pir Sultan Abdal statue constructed and erected at Banaz in 1978-79. Banaz, Sivas Province, Turkey (June 2007)."
              },
              {
                "id": 8947,
                "title": "Koerbin-9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-9.jpg",
                "caption": "The author in the small courtyard of the ‘Pir Sultan Abdal house’ in Banaz village standing beside the mill stone which according to legend was brought to Banaz by the Pir from Khorasan or Yemen. Banaz, Sivas Province, Turkey (June 2002)."
              },
              {
                "id": 8948,
                "title": "Koerbin-10.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-10.mp3",
                "caption": "Tolga Sağ (voice and <em>bağlama</em>) performing ‘<em>Dün gece seyrimde çostuydu dağlar</em>,’ an <em>ağıt</em> (lament) for Pir Sultan Abdal reputed to have been composed by his daughter, Sanem, after his execution. Recorded at the 2007 Pir Sultan Abdal festival, Banaz, Sivas Province, Turkey (22 June 2007)."
              },
              {
                "id": 8949,
                "title": "Koerbin-11.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-11.mp3",
                "caption": "Tolga Sağ (voice and <em>bağlama</em>) performing ‘<em>Her sabah her sabah bir seher yeli</em>’, a <em>semah</em> (ritual dance) <em>deyiş</em> attributed to Pir Sultan Abdal. Recorded at the 2007 Pir Sultan Abdal festival, Banaz, Sivas Province, Turkey (22 June 2007)."
              },
              {
                "id": 8950,
                "title": "Koerbin-12.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/26i/full/Koerbin-12.mp3",
                "caption": "Recording by Feyzullah Çınar (1937-1983) of ‘<em>Şu kanlı zalimin ettiği işler</em>’, one of the most renowned <em>deyiş</em> attributed to Pir Sultan Abdal which concerns the tribulations of his final days leading to his to his execution."
              }
            ]
          },
          {
            "id": 8935,
            "title": "Ritual Scenes in the Iliad: Rote, Hallowed, or Encrypted as Ancient Art?",
            "content": "<p>Based in oral poetic and ritual theory, this article proposes that ritual scenes in Homer’s <em>Iliad</em> reflect unique compositional constraints beyond those found in other kinds of typical scenes. The focus is on oath-sacrifices and commensal sacrifices. Both ritual scene types exhibit strong identifying features, although they differ in their formal particulars and cultural implications. It is argued that both sorts of sacrificial scenes preserve especially ancient ritual patterns that may have parallels in Anatolian texts.</p>",
            "ecompanionhtml": "<html><head></head><body><br>\n<table class=\"chart\" cellspacing=\"0\" border=\"1\" width=\"100%\" style=\"font-size:12px\">\n <caption style=\"font-size:14px;font-weight:bold;\">Chart 1: Commensal sacrifice</caption>\n <tbody><tr>\n  <td width=\"50%\" valign=\"top\">\n  (1) 1.447-8:<br>... They swiftly set in order the sacred hecatomb for the god around the well-built altar,\n  </td>\n  <td width=\"50%\" valign=\"top\">\n  <p>(1) 1.447-8<br>...œÑŒø·Ω∂ Œ¥‚Äô·Ω¶Œ∫Œ± Œ∏Œµ·ø∑ ·º±ŒµœÅ·Ω¥ŒΩ ·ºëŒ∫Œ±œÑœåŒºŒ≤Œ∑ŒΩ<br>\n  ·ºëŒæŒµŒØŒ∑œÇ ·ºîœÉœÑŒ∑œÉŒ±ŒΩ  ·ºêŒ∞Œ¥ŒºŒ∑œÑŒøŒΩ œÄŒµœÅ·Ω∂ Œ≤œâŒºœåŒΩ,</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(2) 1.449<br>\n  They washed their hands and took up barley.</p>\n  </td>\n  <td valign=\"top\">\n  <p>(2) 1.449<br>\n  œáŒµœÅŒΩŒØœàŒ±ŒΩœÑŒø Œ¥‚Äô ·ºîœÄŒµŒπœÑŒ± Œ∫Œ±·Ω∂ Œø·ΩêŒªŒøœáœçœÑŒ±œÇ ·ºÄŒΩŒ≠ŒªŒøŒΩœÑŒø.</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(3) 1.450 [prayer]<br>\n  On their behalf, Chryses held up his hands and prayed;...</p>\n  </td>\n  <td valign=\"top\">\n  <p>(3) 1.450<sup><a style=\"font-size:8pt;\" href=\"#_ftn1\" name=\"_ftnref1\" title=\"\">1</a></sup><br>\n  œÑŒø·øñœÉŒπŒΩ Œ¥·Ω≤ ŒßœÅœçœÉŒ∑œÇ ŒºŒµŒ≥Œ¨Œª‚Äô Œµ·ΩïœáŒµœÑŒø œáŒµ·øñœÅŒ±œÇ ·ºÄŒΩŒ±œÉœáœéŒΩ¬∑ ...</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(4) 1.458 (<b>ditto\n  2.421</b>)<br>\n  But once they prayed and threw barley,</p>\n  </td>\n  <td valign=\"top\">\n  <p>(4) 1.458 (<b>ditto\n  2.421</b>)<br>\n  Œ±·ΩêœÑ·Ω∞œÅ ·ºêœÄŒµŒØ ·ø•‚ÄôŒµ·ΩîŒæŒ±ŒΩœÑŒø Œ∫Œ±·Ω∂ Œø·ΩêŒªŒøœáœçœÑŒ±œÇ œÄœÅŒøŒ≤Œ¨ŒªŒøŒΩœÑŒø,</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(5) 1.459 (<b>ditto\n  at 2.422; cf. 24.622</b>)<br>\n  They held up the [victims‚Äô heads] first, and then cut the throats and flayed them,</p>\n  </td>\n  <td valign=\"top\">\n  <p>(5) 1.459 (<b>ditto\n  at 2.422; cf. 24.622</b>)<sup><a style=\"font-size:8pt;\" href=\"#_ftn2\" name=\"_ftnref2\" title=\"\">2</a></sup><br>\n  Œ±·ΩêŒ≠œÅœÖœÉŒ±ŒΩ Œº·Ω≤ŒΩ œÄœÅ·ø∂œÑŒ± Œ∫Œ±·Ω∂ ·ºîœÉœÜŒ±ŒæŒ±ŒΩ Œ∫Œ±·Ω∂ ·ºîŒ¥ŒµŒπœÅŒ±ŒΩ,</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(6) 1.460-1 (<b>ditto\n  2.423-4</b>)<br>\n  They cut out the thigh pieces and hid them under the fat, making two folds,</p>\n  </td>\n  <td valign=\"top\">\n  <p>(6) 1.460-1 (<b>ditto\n  at 2.423-4</b>)<br>\n  ŒºŒ∑œÅŒøœçœÇ œÑ‚Äô ·ºêŒæŒ≠œÑŒ±ŒºŒøŒΩ Œ∫Œ±œÑŒ¨ œÑŒµ Œ∫ŒΩŒØœÉ·øÉ ·ºêŒ∫Œ¨ŒªœÖœàŒ±ŒΩ<br>\nŒ¥ŒØœÄœÑœÖœáŒ± œÄŒøŒπŒÆœÉŒ±ŒΩœÑŒµœÇ, </p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(7) 1.461 (<b>ditto at 2.434</b>)<br>\n  They placed raw strips of flesh over [the thighs];</p>\n  </td>\n  <td valign=\"top\">\n  <p>(7) 1.461 (<b>ditto at 2.434</b>)<br>\n  ...·ºêœÄ‚Äô Œ±·ΩêœÑ·ø∂ŒΩ Œ¥‚Äô ·Ω†ŒºŒøŒ∏Œ≠œÑŒ∑œÉŒ±ŒΩ¬∑</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(8) 1.462-63<br>\n  The old man burnt them over split wood, and poured shining wine</p>\n  </td>\n  <td valign=\"top\">\n  <p>(8) 1.462-63<sup><a style=\"font-size:8pt;\" href=\"#_ftn3\" name=\"_ftnref3\" title=\"\">3</a></sup><br>\n  Œ∫Œ±·øñŒµ Œ¥‚Äô ·ºêœÄ·Ω∂ œÉœáŒØŒ∂·øÉœÇ ·ΩÅ Œ≥Œ≠œÅœâŒΩ, ·ºêœÄ·Ω∂ Œ¥‚Äô Œ±·º¥Œ∏ŒøœÄŒ± Œø·º∂ŒΩŒøŒΩ<br ?=\"\">\n  ŒªŒµ·øñŒ≤Œµ</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(9) 1.463<br>\n  By him the young men held forks in their hands.</p>\n  </td>\n  <td valign=\"top\">\n  <p>(9) 1.463<br>\n  ...ŒΩŒ≠ŒøŒπ Œ¥·Ω≤ œÄŒ±œÅ‚Äô Œ±·ΩêœÑ·Ω∏ŒΩ ·ºîœáŒøŒΩ œÄŒµŒºœÄœéŒ≤ŒøŒªŒ± œáŒµœÅœÉŒØŒΩ.</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(10) 1.464 (<b>ditto\n  at 2.427</b>)<br>\n  But when they had burned the thighs and tasted the innards</p>\n  </td>\n  <td valign=\"top\">\n  <p>(10) 1.464 (<b>ditto\n  at 2.427</b>)<br>\n  Œ±·ΩêœÑ·Ω∞œÅ ·ºêœÄ·Ω∂ Œ∫Œ±œÑ·Ω∞ Œº·øÜœÅŒµ Œ∫Œ¨Œ∑ Œ∫Œ±ŒØ œÉœÄŒª·Ω∞Œ≥œáŒΩŒ± œÄŒ¨œÉŒ±ŒΩœÑŒø</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(11) 1.465 (<b>ditto\n  2.428; cf. 7.317, 9.210, 24.623</b>)<br>\n  they cut the rest into bits and pierced it with spits,</p>\n  </td>\n  <td valign=\"top\">\n  <p>(11) 1.465 (<b>ditto\n  at 2.428; cf. 7.317, 9.210, 24.623</b>)<br>\n  ŒºŒØœÉœÑœÖŒªŒªœåŒΩ œÑ‚Äô ·ºÑœÅŒ± œÑ·ºÜŒªŒªŒ± Œ∫Œ±·Ω∂ ·ºÄŒºœÜ‚Äô ·ΩÄŒ≤ŒµŒªŒø·øñœÉŒπŒΩ ·ºîœÄŒµŒπœÅŒ±ŒΩ,</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(12) 1.466 (<b>ditto\n  2.429, 24.624; cf. 7.318</b>)<br>\n  They roasted it expertly, and drew it all off [the spits].</p>\n  </td>\n  <td valign=\"top\">\n  <p>(12) 1.466 (<b>ditto 2.429 and 24.624; cf. 7.318</b>)<sup><a style=\"font-size:8pt;\" href=\"#_ftn4\" name=\"_ftnref4\" title=\"\">4</a></sup><br>\n  ·Ω§œÄœÑŒ∑œÉŒ¨ŒΩ œÑŒµ œÄŒµœÅŒπœÜœÅŒ±Œ¥Œ≠œâœÇ, ·ºêœÅœçœÉŒ±ŒΩœÑœå œÑŒµ œÄŒ¨ŒΩœÑŒ±.</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(13) 1.467 (<b>ditto\n  2.430, 7.319</b>)<br>\n  But once they had ceased their labor and prepared the feast,</p>\n  </td>\n  <td valign=\"top\">\n  <p>(13) 1.467 (<b>ditto\n  2.430, 7.319</b>)<br>\n  Œ±·ΩêœÑ·Ω∞œÅ ·ºêœÄŒµ·Ω∂ œÄŒ±œçœÉŒ±ŒΩœÑŒø œÄœåŒΩŒøœÖ œÑŒµœÑœçŒ∫ŒøŒΩœÑœå œÑŒµ Œ¥Œ±·øñœÑŒ±,</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(14) 1.468 (<b>ditto\n  2.431,7.320</b>)<br>\n  they feasted, and no spirit went lacking the equally divided feast.</p>\n  </td>\n  <td valign=\"top\">\n  <p>(14) 1.468 (<b>ditto\n  2.431,7.320</b>)<br>\n  Œ¥Œ±ŒØŒΩœÖŒΩœÑ‚Äô, Œø·ΩêŒ¥Œ≠ œÑŒπ Œ∏œÖŒº·Ω∏œÇ ·ºêŒ¥ŒµœçŒµœÑŒø Œ¥Œ±ŒπœÑ·Ω∏œÇ ·ºêŒêœÉŒ∑œÇ.</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>15)\n  1.469 (<b>ditto 2.432, 7.323, 9.222,\n  24.628</b>)<br>\n  But when they had sated their desire for food and drink,</p>\n  </td>\n  <td valign=\"top\">\n  <p>(15) 1.469 (<b>ditto\n  2.432, 7.323, 9.222, 24.628</b>)<br>\n  a·ΩêœÑ·Ω∞œÅ ·ºêœÄŒµ·Ω∂ œÄœåœÉŒπŒøœÇ Œ∫Œ±·Ω∂ ·ºêŒ¥Œ∑œÑœçŒøœÇ ·ºêŒæ ·ºîœÅŒøŒΩ ·ºïŒΩœÑŒø,</p>\n  </td>\n </tr>\n</tbody></table>\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"font-size:10px;text-align:justify;\">\n <tbody><tr>\n  <td width=\"10%\">&nbsp;</td>\n  <td width=\"90%\">\n<br><hr width=\"33%\" shade=\"noshade\" style=\"float:left\"><br>\n<sup><a style=\"font-size:8px\" href=\"#_ftnref1\" name=\"_ftn1\" title=\"\">1</a></sup> Compare Agamemnon‚Äôs prayer for Priam‚Äôs total destruction, which initiates the commensal sacrifice at 2.410-18. The response of the deity is noted in both prayers (a positive in Book 1, a negative in Book 2).<br>\n\n<sup><a style=\"font-size:8px\" href=\"#_ftnref2\" name=\"_ftn2\" title=\"\">2</a></sup> There is slight variation in Achilles‚Äô sacrifice at 24.622: œÉœÜ·Ω∞Œæ‚Äô¬∑ ·ºïœÑŒ±ŒπœÅŒøŒπ Œ¥·Ω≤ Œ¥ŒµœÅœåŒΩ œÑŒµ Œ∫Œ±·Ω∂ ·ºÑŒºœÜŒµœÑŒøŒΩ Œµ·Ωñ Œ∫Œ±œÑ·Ω∞ Œ∫œåœÉŒºŒøŒΩ.<br>\n\n<sup><a style=\"font-size:8px\" href=\"#_ftnref3\" name=\"_ftn3\" title=\"\">3</a></sup> Compare the slight variation in Book 2: Œ∫Œ±·Ω∂ œÑ·Ω∞ Œº·Ω≤ŒΩ ·ºÇœÅ œÉœáŒØŒ∂·øÉœÉŒπŒΩ ·ºÄœÜœçŒªŒªŒøŒπœÉŒπŒΩ Œ∫Œ±œÑŒ≠Œ∫Œ±ŒπŒøŒΩ (2.425).<br>\n\n<sup><a style=\"font-size:8px\" href=\"#_ftnref4\" name=\"_ftn4\" title=\"\">4</a></sup> Books 9 and 24 have additional steps of bread being laid out in baskets and hands stretched out to the refreshments.<br>\n<b>9. 216-222</b> Œ†Œ¨œÑœÅŒøŒ∫ŒªŒøœÇ Œº·Ω≤ŒΩ œÉ·øñœÑŒøŒΩ ·ºëŒª·ΩºŒΩ ·ºêœÄŒ≠ŒΩŒµŒπŒºŒµ œÑœÅŒ±œÄŒ≠Œ∂·øÉ<br>\nŒ∫Œ±ŒªŒø·øñœÇ ·ºêŒΩ Œ∫Œ±ŒΩŒ≠ŒøŒπœÉŒπŒΩ, ·ºÄœÑ·Ω∞œÅ Œ∫œÅŒ≠Œ± ŒΩŒµ·øñŒºŒµŒΩ ·ºàœáŒπŒªŒªŒµœçœÇ. ‚Ä¶<br>\nŒø·º≥ Œ¥‚Äô ·ºêœÄ‚Äô ·ΩÄŒΩŒµŒØŒ±Œ∏‚Äô ·ºëœÑŒø·øñŒºŒ± œÄœÅŒøŒ∫ŒµŒØŒºŒµŒΩŒ± œáŒµ·øñœÅŒ±œÇ ·º¥Œ±ŒªŒªŒøŒΩ.<br>\nŒ±·ΩêœÑ·Ω∞œÅ ·ºêœÄŒµ·Ω∂ œÄœåœÉŒπŒøœÇ Œ∫Œ±·Ω∂ ·ºêŒ¥Œ∑œÑœçŒøœÇ ·ºêŒæ ·ºîœÅŒøŒΩ ·ºïŒΩœÑŒø ‚Ä¶<br>\n<b>24. 625-27</b>. Œë·ΩêœÑŒøŒºŒ≠Œ¥œâŒΩ Œ¥‚Äô ·ºÑœÅŒ± œÉ·øñœÑŒøŒΩ ·ºëŒª·ΩºŒΩ ·ºêœÄŒ≠ŒΩŒµŒπŒºŒµ œÑœÅŒ±œÄŒ≠Œ∂·øÉ<br>\nŒ∫Œ±ŒªŒø·øñœÇ ·ºêŒΩ Œ∫Œ±ŒΩŒ≠ŒøŒπœÉŒπŒΩ¬∑ ·ºÄœÑ·Ω∞œÅ Œ∫œÅŒ≠Œ± ŒΩŒµ·øñŒºŒµŒΩ ·ºàœáŒπŒªŒªŒµœçœÇ.<br>\nŒø·º≥ Œ¥‚Äô ·ºêœÄ‚Äô ·ΩÄŒΩŒµŒØŒ±Œ∏‚Äô ·ºëœÑŒø·øñŒºŒ± œÄœÅŒøŒ∫ŒµŒØŒºŒµŒΩŒ± œáŒµ·øñœÅŒ±œÇ ·º¥Œ±ŒªŒªŒøŒΩ.<br>\n  </td>\n </tr>\n</tbody></table>\n\n<hr width=\"100%\" shade=\"noshade\">\n<br>\n\n<table class=\"chart\" cellspacing=\"0\" border=\"1\" width=\"100%\" style=\"font-size:12px\">\n <caption style=\"font-size:14px;font-weight:bold;\">Chart 2: Oath sacrifice</caption>\n <tbody><tr>\n  <td width=\"50%\" valign=\"top\">\n  <p>(1) 3.268-70<br>\n  ‚Ä¶ But the high-born heralds led up the trusted oath-sacrifices for the gods, and mixed wine in bowls, then poured water over the hands of the kings.</p>\n  </td>\n  <td width=\"50%\" valign=\"top\">\n  <p>(1) 3.268-70<br>\n  ‚Ä¶   ·ºÄœÑ·Ω∞œÅ Œ∫ŒÆœÅœÖŒ∫ŒµœÇ ·ºÄŒ≥Œ±œÖŒø·Ω∂<br>\n  ·ΩÖœÅŒ∫ŒπŒ± œÄŒπœÉœÑ·Ω∞ Œ∏Œµ·ø∂ŒΩ œÉœçŒΩŒ±Œ≥ŒøŒΩ, Œ∫œÅŒ∑œÑ·øÜœÅŒπ Œ¥·Ω≤ Œø·º∂ŒΩŒøŒΩ<br>\n   ŒºŒØœÉŒ≥ŒøŒΩ, ·ºÄœÑ·Ω∞œÅ Œ≤Œ±œÉŒπŒªŒµ·ø¶œÉŒπŒΩ ·ΩïŒ¥œâœÅ ·ºêœÄ·Ω∂ œáŒµ·øñœÅŒ±œÇ ·ºîœáŒµœÖŒ±ŒΩ.</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(2) 3.271-72 (<b>ditto\n  19.252-53</b>)<br>\n  Atreides, drawing with his hands the <em>machaira</em>, which always hung by the great sheath of his sword,</p>\n  </td>\n  <td valign=\"top\">\n  <p>(2) 3.271-72 (<b>ditto 19.252-53</b>)<br>\n  ·ºàœÑœÅŒµŒêŒ¥Œ∑œÇ Œ¥·Ω≤  ·ºêœÅœÖœÉœÉŒ¨ŒºŒµŒΩŒøœÇ œáŒµŒØœÅŒµœÉœÉŒπ ŒºŒ¨œáŒ±ŒπœÅŒ±ŒΩ,<br>\n  ·º• Œø·º± œÄ·Ω∞œÅ ŒæŒØœÜŒµŒøœÇ ŒºŒ≠Œ≥Œ± Œ∫ŒøœÖŒªŒµ·Ω∏ŒΩ Œ±·º∞·Ω≤ŒΩ ·ºÑœâœÅœÑŒø,</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(3) 3.273<br>\n  he cut hairs from the heads of the lambs,</p>\n  </td>\n  <td valign=\"top\">\n  <p>(3) 3.273<sup><a style=\"font-size:8px;\" href=\"#_ftn5\" name=\"_ftnref5\" title=\"\">5</a></sup><br>\n  ·ºÄœÅŒΩ·ø∂ŒΩ ·ºêŒ∫ Œ∫ŒµœÜŒ±ŒªŒ≠œâŒΩ œÑŒ¨ŒºŒΩŒµ œÑœÅŒØœáŒ±œÇ¬∑</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(4) 3.273-74<br>\n  ‚Ä¶ and then the heralds distributed them to the best of the Trojans and Achaeans.</p>\n  </td>\n  <td valign=\"top\">\n  <p>(4) 3.273-74<br>\n  ‚Ä¶Œ±·ΩêœÑ·Ω∞œÅ ·ºîœÄŒµŒπœÑŒ±<br>\n  Œ∫ŒÆœÅœÖŒ∫ŒµœÇ Œ§œÅœéœâŒΩ Œ∫Œ±·Ω∂ ·ºàœáŒ±Œπ·ø∂ŒΩ ŒΩŒµ·øñŒºŒ±ŒΩ ·ºÄœÅŒØœÉœÑŒøŒπœÇ<sup><a style=\"font-size:8px;\" href=\"#_ftn6\" name=\"_ftnref6\" title=\"\">6</a></sup><br>\n  </p></td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(5) 3.275<br>\n  Before them Atreides prayed, holding up his hands;</p>\n  </td>\n  <td valign=\"top\">\n  <p>(5) 3.275<sup><a style=\"font-size:8px;\" href=\"#_ftn7\" name=\"_ftnref7\" title=\"\">7</a></sup><br>\n  œÑŒø·øñœÉŒπŒΩ Œ¥‚Äô ‚ÄôŒëœÑœÅŒµŒêŒ¥Œ∑œÇ ŒºŒµŒ≥Œ¨Œª‚Äô Œµ·ΩîœáŒµœÑŒø œáŒµ·øñœÅŒ±œÇ ·ºÄŒΩŒ±œÉœáœéŒΩ¬∑</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(6) 3.276-80<br>\n  ‚ÄúZeus Father, counselor from Ida, best and greatest,<br>\n  and Helios, you who see all and hear all,<br>\n  and the rivers and earth, and those who from beneath punish men having toiled, whoever swears a false oath,<br>\n  you be witnesses, and protect the trusted oaths.‚Äù</p>\n  </td>\n  <td valign=\"top\">\n  <p>(6) 3.276-80<sup><a style=\"font-size:8px;\" href=\"#_ftn8\" name=\"_ftnref8\" title=\"\">8</a></sup><br>\n  ‚ÄúŒñŒµ·ø¶ œÄŒ¨œÑŒµœÅ, ·º∫Œ¥Œ∑Œ∏ŒµŒΩ ŒºŒµŒ¥Œ≠œâŒΩ, Œ∫œçŒ¥ŒπœÉœÑŒµ ŒºŒ≠Œ≥ŒπœÉœÑŒµ,<br>\n  ·º®Œ≠ŒªŒπŒøœÇ Œ∏‚Äô, ·ΩÑœÇ œÄŒ¨ŒΩœÑ‚Äô ·ºêœÜŒøœÅ·æ∑œÇ Œ∫Œ±·Ω∂ œÄŒ¨ŒΩœÑ‚Äô ·ºêœÄŒ±Œ∫ŒøœçŒµŒπœÇ,<br>\n  Œ∫Œ±·Ω∂ œÄŒøœÑŒ±ŒºŒø·Ω∂ Œ∫Œ±·Ω∂ Œ≥Œ±·øñŒ±, Œ∫Œ±·Ω∂ Œø·º≥ ·ΩëœÄŒ≠ŒΩŒµœÅŒ∏Œµ Œ∫Œ±ŒºœåŒΩœÑŒ±œÇ<sup><a style=\"font-size:8px;\" href=\"#_ftn9\" name=\"_ftnref9\" title=\"\">9</a></sup><br>\n  ·ºÄŒΩŒ∏œÅœéœÄŒøœÖœÇ œÑŒØŒΩœÖœÉŒ∏ŒøŒΩ, ·ΩÉœÑŒπœÇ Œ∫‚Äô ·ºêœÄŒØŒøœÅŒ∫ŒøŒΩ ·ΩÄŒºœåœÉœÉ·øÉ,<br>\n  ·ΩëŒºŒµ·øñœÇ ŒºŒ¨œÅœÑœÖœÅŒøŒπ ·ºîœÉœÑŒµ, œÜœÖŒªŒ¨œÉœÉŒµœÑŒµ Œ¥‚Äô ·ΩÖœÅŒ∫ŒπŒ± œÄŒπœÉœÑŒ¨¬∑‚Äù</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(7) 3.292 (<b>ditto 19.266</b>)<br>\n  So he said, and he cut the neck of the lambs with the pitiless bronze.</p>\n  </td>\n  <td valign=\"top\">\n  <p>(7) 3.292 (<b>ditto 19.266</b>)<br>\n  ·ºÆ, Œ∫Œ±·Ω∂ ·ºÄœÄ·Ω∏ œÉœÑŒøŒºŒ¨œáŒøœÖœÇ ·ºÄœÅŒΩ·ø∂ŒΩ œÑŒ¨ŒºŒµ ŒΩŒ∑ŒªŒ≠œä œáŒ±ŒªŒ∫·ø∑¬∑</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(8) 3.293-94<br>\n  And he put them on the ground, gasping, depleted of life, for the bronze had\n  taken away their strength.</p>\n  </td>\n  <td valign=\"top\">\n  <p>(8) 3.293-94<sup><a style=\"font-size:8px;\" href=\"#_ftn10\" name=\"_ftnref10\" title=\"\">10</a></sup><br>\n  Œ∫Œ±·Ω∂ œÑŒø·Ω∫œÇ Œº·Ω≤ŒΩ Œ∫Œ±œÑŒ≠Œ∏Œ∑Œ∫ŒµŒΩ ·ºêœÄ·Ω∂ œáŒ∏ŒøŒΩ·Ω∏œÇ ·ºÄœÉœÄŒ±ŒØœÅŒøŒΩœÑŒ±œÇ,<br>\n  Œ∏œÖŒºŒø·ø¶ Œ¥ŒµœÖŒøŒºŒ≠ŒΩŒøœÖœÇ¬∑ ·ºÄœÄ·Ω∏ Œ≥·Ω∞œÅ ŒºŒ≠ŒΩŒøœÇ Œµ·ºµŒªŒµœÑŒø œáŒ±ŒªŒ∫œåœÇ.</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(9) 3. 295-96<br>\n  Drawing wine from bowls with cups, they poured it out, and prayed to the gods who\n  always are,</p>\n  </td>\n  <td valign=\"top\">\n  <p>(9) 3.295-96<br>\n  Œø·º∂ŒΩŒøŒΩ Œ¥‚Äô ·ºêŒ∫ Œ∫œÅŒ∑œÑ·øÜœÅŒøœÖœÇ ·ºÄœÜœÖœÉœÉœåŒºŒµŒΩŒøŒπ Œ¥ŒµœÄŒ¨ŒµœÉœÉŒπŒΩ<br>\n  ·ºîŒ∫œáŒµŒøŒΩ, ·º†Œ¥‚Äô Œµ·ΩîœáŒøŒΩœÑŒø Œ∏ŒµŒø·øñœÇ Œ±·º∞ŒµŒπŒ≥ŒµŒΩŒ≠œÑ·øÉœÉŒπŒΩ¬∑</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(10) 3.297<br>\n  and this is how each one of the Achaeans and Trojans prayed;</p>\n  </td>\n  <td valign=\"top\">\n  <p>(10) 3.297<br>\n  ·ΩßŒ¥Œµ Œ¥Œ≠ œÑŒπœÇ Œµ·º¥œÄŒµœÉŒ∫ŒµŒΩ ‚ÄôŒëœáŒ±Œπ·ø∂ŒΩ œÑŒµ Œ§œÅœéœâŒΩ œÑŒµ¬∑</p>\n  </td>\n </tr>\n <tr>\n  <td valign=\"top\">\n  <p>(11) 3.298-301<br>\n  ‚ÄúZeus best and greatest, and all the other immortal gods, whosoever should first violate the oaths,<br>\n  so let their brains run to the ground like this wine, and those of their children, and let their wives become the spoil of others.‚Äù</p>\n  </td>\n  <td valign=\"top\">\n  <p>(11) 3.298-301<sup><a style=\"font-size:8px;\" href=\"#_ftn11\" name=\"_ftnref11\" title=\"\">11</a></sup><br>\n  ‚ÄúŒñŒµ·ø¶ Œ∫œçŒ¥ŒπœÉœÑŒµ ŒºŒ≠Œ≥ŒπœÉœÑŒµ, Œ∫Œ±·Ω∂ ·ºÄŒ∏Œ¨ŒΩŒ±œÑŒøŒπ Œ∏ŒµŒø·Ω∂ ·ºÑŒªŒªŒøŒπ,<br>\n  ·ΩÅœÄœÄœåœÑŒµœÅŒøŒπ œÄœÅœåœÑŒµœÅŒøŒπ ·ΩëœÄ·Ω≤œÅ ·ΩÑœÅŒ∫ŒπŒ± œÄŒ∑ŒºŒÆŒΩŒµŒπŒ±ŒΩ,<br>\n  ·ΩßŒ¥Œ≠ œÉœÜ‚Äô ·ºêŒ≥Œ∫Œ≠œÜŒ±ŒªŒøœÇ œáŒ±ŒºŒ¨Œ¥ŒπœÇ ·ø§Œ≠ŒøŒπ ·Ω†œÇ ·ΩÖŒ¥Œµ Œø·º∂ŒΩŒøœÇ,<br>\n  Œ±·ΩêœÑ·ø∂ŒΩ Œ∫Œ±·Ω∂ œÑŒµŒ∫Œ≠œâŒΩ, ·ºÑŒªŒøœáŒøŒπ Œ¥‚Äô ·ºÑŒªŒªŒøŒπœÉŒπ Œ¥Œ±ŒºŒµ·øñŒµŒΩ.</p>\n  </td>\n </tr>\n</tbody></table>\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"font-size:10px;text-align:justify;\">\n <tbody><tr>\n  <td width=\"10%\">&nbsp;</td>\n  <td width=\"90%\">\n<br><hr width=\"33%\" shade=\"noshade\" style=\"float:left\"><br>\n<sup><a style=\"font-size:8px\" href=\"#_ftnref5\" name=\"_ftn5\" title=\"\">5</a></sup> Cf. 19.254, which has simply ‚Äúcutting hairs from the boar‚Äù (Œ∫Œ¨œÄœÅŒøœÖ ·ºÄœÄ·Ω∏ œÑœÅŒØœáŒ±œÇ Œ±œÅŒæŒ¨ŒºŒµŒΩŒøœÇ).<br>\n\n<sup><a style=\"font-size:8px\" href=\"#_ftnref6\" name=\"_ftn6\" title=\"\">6</a></sup> This step is missing in Book 19.<br>\n\n<sup><a style=\"font-size:8px\" href=\"#_ftnref7\" name=\"_ftn7\" title=\"\">7</a></sup> Note the close match in book 19: ‚Äúraising his hands to Zeus / he prayed‚Äù (ŒîŒπ·Ω∂ œáŒµ·øñœÅŒ±œÇ ·ºÄŒΩŒ±œÉœá·ΩºŒΩ / Œµ·ΩîœáŒµœÑŒø [19.254-55]). In Book 19 the communal mood is\nemphasized‚ÄîœÑŒø·Ω∂ Œ¥‚Äô ·ºÑœÅŒ± œÄŒ¨ŒΩœÑŒµœÇ ·ºêœÑ‚Äô Œ±·ΩêœÑœåœÜŒπŒΩ ·º•Œ±œÑŒø œÉŒπŒ≥·øá‚Äîas it was just before when all the Achaians rejoiced that Achilles had renounced his isolation and wrath (19.74-75; 19.173-74). This is clearly context driven.<br>\n\n<sup><a style=\"font-size:8px\" href=\"#_ftnref8\" name=\"_ftn8\" title=\"\">8</a></sup> The prayer at 19.258-59 is thematically similar: ‚ÄúLet Zeus see first, who is highest and best of the gods, / and then Ge, and Helios and the Erinyes, who from under earth / punish men, whosoever should swear a false oath‚Äù (‚Äú·º¥œÉœÑœâ  ŒΩ·ø¶ŒΩ ŒñŒµ·Ω∫œÇ œÄœÅ·ø∂œÑŒ±, Œ∏Œµ·ø∂ŒΩ ·ΩïœÄŒ±œÑŒøœÇ Œ∫Œ±·Ω∂ ·ºÑœÅŒπœÉœÑŒøœÇ, / Œì·øÜ œÑŒµ Œ∫Œ±·Ω∂ ·º®Œ≠ŒªŒπŒøœÇ Œ∫Œ±·Ω∂  ·ºòœÅŒπŒΩœçŒµœÇ, Œ±·º¥ Œ∏‚Äô ·ΩêœÄ·Ω∏ Œ≥Œ±·øñŒ±ŒΩ / ·ºÄŒΩŒ∏œÅœéœÄŒøœÖœÇ œÑŒØŒΩœÖŒΩœÑŒ±Œπ, ·ΩÖœÉœÑŒπœÇ Œ∫‚Äô ·ºêœÄŒØŒøœÅŒ∫ŒøŒΩ ·ΩÄŒºœåœÉœÉ·øÉ‚Äù). The wine-pouring is omitted in Book 19; this is not a collective oath, and it is much abbreviated.<br>\n\n<sup><a style=\"font-size:8px\" href=\"#_ftnref9\" name=\"_ftn9\" title=\"\">9</a></sup> Œ∫Œ±ŒºœåŒΩœÑŒµœÇ is given as a variant: toiling Erinyes or, as above, toiling humans? Kirk (1985: loc. cit.) interprets as ‚Äúwhen dead‚Äù (Cf. Leaf 1900: loc. cit.: those who have completed their toil) and sees it as applying to humans.<br>\n\n<sup><a style=\"font-size:8px\" href=\"#_ftnref10\" name=\"_ftn10\" title=\"\">10</a></sup> Cf. 19.267-68: And Talthybios hurled him, whirling, into the great abyss / of the sea, as food for fish (œÑ·Ω∏ŒΩ Œº·Ω≤ŒΩ Œ§Œ±ŒªŒ∏œçŒ≤ŒπŒøœÇ œÄŒøŒªŒπ·øÜœÇ ·ºÄŒª·Ω∏œÇ ·ºêœÇ ŒºŒ≠Œ≥Œ± ŒªŒ±·øñœÑŒºŒ± / ·ø•ŒØœà‚Äô ·ºêœÄŒπŒ¥ŒπŒΩŒÆœÉŒ±œÇ, Œ≤œåœÉŒπŒΩ ·º∞œáŒ∏œçœÉŒπŒΩ).<br>\n\n<sup><a style=\"font-size:8px\" href=\"#_ftnref11\" name=\"_ftn11\" title=\"\">11</a></sup> Cf. 19.264-65: ‚ÄúBut if I have sworn these things falsely, then let the gods give to me pains / very many, as many as they give to anyone who transgresses in swearing‚Äù (Œµ·º∞ Œ¥Œ≠ œÑŒπ œÑ·ø∂ŒΩŒ¥‚Äô ·ºêœÄŒØŒøœÅŒ∫ŒøŒΩ, ·ºêŒºŒø·Ω∂ Œ∏ŒµŒπŒø·Ω∂ ·ºÑŒªŒ≥ŒµŒ± Œ¥Œø·øñŒµŒΩ / œÄŒøŒªŒª·Ω∞ ŒºŒ¨Œª‚Äô, ·ΩÖœÉœÉŒ± Œ¥ŒπŒ¥Œø·ø¶œÉŒπŒΩ ·ΩÖœÑ·Ω∂œÇ œÉœÜ‚Äô ·ºÄŒªŒØœÑŒ∑œÑŒ±Œπ ·ΩÄŒºœåœÉœÉŒ±œÇ).<br>\n  </td>\n </tr>\n</tbody></table>\n\n<hr width=\"100%\" shade=\"noshade\">\n<br>\n\n<table class=\"chart\" cellspacing=\"0\" border=\"1\" width=\"100%\" style=\"font-size:12px\">\n <caption style=\"font-size:14px;font-weight:bold;\">Chart 3: Funeral Feast of Book 23</caption>\n <tbody><tr>\n  <td width=\"50%\" valign=\"top\">\n  <p>. . . Œ±·ΩêœÑ·Ω∞œÅ ·ΩÅ œÑŒø·øñœÉŒπ œÑŒ¨œÜŒøŒΩ ŒºŒµŒΩŒøŒµŒπŒ∫Œ≠Œ± Œ¥Œ±ŒØŒΩœÖ.<br>\n  œÄŒøŒªŒªŒø·Ω∂ Œº·Ω≤ŒΩ Œ≤œåŒµœÇ ·ºÄœÅŒ≥Œø·Ω∂ ·ΩÄœÅŒ≠œáŒ∏ŒµŒøŒΩ ·ºÄŒºœÜ·Ω∂ œÉŒπŒ¥ŒÆœÅ·ø≥ (30)<br>\n  œÉœÜŒ±Œ∂œåŒºŒµŒΩŒøŒπ, œÄŒøŒªŒªŒø·Ω∂ Œ¥‚Äô ·ΩÑœäŒµœÇ Œ∫Œ±·Ω∂ ŒºŒ∑Œ∫Œ¨Œ¥ŒµœÇ Œ±·º∂Œ≥ŒµœÇ¬∑<br>\n  œÄŒøŒªŒªŒø·Ω∂ Œ¥‚Äô ·ºÄœÅŒ≥ŒπœåŒ¥ŒøŒΩœÑŒµœÇ ·ΩïŒµœÇ, Œ∏Œ±ŒªŒ≠Œ∏ŒøŒΩœÑŒµœÇ ·ºÄŒªŒøŒπœÜ·øá,<br>\n  Œµ·ΩëœåŒºŒµŒΩŒøŒπ œÑŒ±ŒΩœçŒøŒΩœÑŒø Œ¥Œπ·Ω∞ œÜŒªŒøŒ≥·Ω∏œÇ ·º©œÜŒ±ŒØœÉœÑŒøŒπŒø¬∑<br>\n  œÄŒ¨ŒΩœÑ·øÉ Œ¥‚Äô ·ºÄŒºœÜ·Ω∂ ŒΩŒ≠Œ∫œÖŒΩ Œ∫ŒøœÑœÖŒªŒÆœÅœÖœÑŒøŒΩ ·ºîœÅœÅŒµŒµŒΩ Œ±·º∑ŒºŒ±.<br>\n  Œë·ΩêœÑ·Ω∞œÅ œÑœåŒΩ Œ≥Œµ ·ºÑŒΩŒ±Œ∫œÑŒ± œÄŒøŒ¥œéŒ∫ŒµŒ± Œ†Œ∑ŒªŒµŒêœâŒΩŒ± (35)<br>\n  Œµ·º∞œÇ ÃìAŒ≥Œ±ŒºŒ≠ŒºŒΩŒøŒΩŒ± Œ¥·øñŒøŒΩ ·ºÑŒ≥ŒøŒΩ Œ≤Œ±œÉŒπŒª·øÜŒµœÇ ÃìAœáŒ±Œπ·ø∂ŒΩ<br>\n  œÉœÄŒøœÖŒ¥·øá œÄŒ±œÅœÄŒµœÄŒπŒ∏œåŒΩœÑŒµœÇ ·ºëœÑŒ±ŒØœÅŒøœÖ œáœâœåŒºŒµŒΩŒøŒΩ Œ∫·øÜœÅ.<br>\n  Œø·º± Œ¥‚Äô ·ΩÖœÑŒµ Œ¥·Ω¥ Œ∫ŒªŒπœÉŒØŒ∑ŒΩ ÃìAŒ≥Œ±ŒºŒ≠ŒºŒΩŒøŒΩŒøœÇ ·º∑ŒæŒøŒΩ ·º∞œåŒΩœÑŒµœÇ,<br>\n  Œ±·ΩêœÑŒØŒ∫Œ± Œ∫Œ∑œÅœçŒ∫ŒµœÉœÉŒπ ŒªŒπŒ≥œÖœÜŒ∏œåŒ≥Œ≥ŒøŒπœÉŒπ Œ∫Œ≠ŒªŒµœÖœÉŒ±ŒΩ<br>\n  ·ºÄŒºœÜ·Ω∂ œÄœÖœÅ·Ω∂ œÉœÑ·øÜœÉŒ±Œπ œÑœÅŒØœÄŒøŒ¥Œ± ŒºŒ≠Œ≥Œ±ŒΩ, Œµ·º∞ œÄŒµœÄŒØŒ∏ŒøŒπŒµŒΩ (40)<br>\n  Œ†Œ∑ŒªŒµŒêŒ¥Œ∑ŒΩ ŒªŒøœçœÉŒ±œÉŒ∏Œ±Œπ ·ºÑœÄŒø Œ≤œÅœåœÑŒøŒΩ Œ±·º±ŒºŒ±œÑœåŒµŒΩœÑŒ±.<br>\n  Œ±·ΩêœÑ·Ω∞œÅ ·ΩÖ Œ≥‚Äô ·º†œÅŒΩŒµ·øñœÑŒø œÉœÑŒµœÅŒµ·ø∂œÇ, ·ºêœÄ·Ω∂ Œ¥‚Äô ·ΩÖœÅŒ∫ŒøŒΩ ·ΩÑŒºŒøœÉœÉŒµŒΩ¬∑<br>\n  ‚ÄúŒø·Ωê Œº·Ω∞ Œñ·øÜŒΩ‚Äô, ·ΩÖœÇ œÑŒØœÇ œÑŒµ Œ∏Œµ·ø∂ŒΩ ·ΩïœÄŒ±œÑŒøœÇ Œ∫Œ±·Ω∂ ·ºÑœÅŒπœÉœÑŒøœÇ,<br>\n  Œø·Ωê Œ∏Œ≠ŒºŒπœÇ ·ºêœÉœÑ·Ω∂ ŒªŒøŒµœÑœÅ·Ω∞ Œ∫Œ±œÅŒÆŒ±œÑŒøœÇ ·ºÜœÉœÉŒøŒΩ ·º±Œ∫Œ≠œÉŒ∏Œ±Œπ,<br>\n  œÄœÅŒØŒΩ Œ≥‚Äô ·ºêŒΩ·Ω∂ Œ†Œ¨œÑœÅŒøŒ∫ŒªŒøŒΩ Œ∏Œ≠ŒºŒµŒΩŒ±Œπ œÄœÖœÅ·Ω∂ œÉ·øÜŒºŒ¨ œÑŒµ œáŒµ·ø¶Œ±Œπ (45)<br>\n  Œ∫ŒµŒØœÅŒ±œÉŒ∏Œ±ŒØ œÑŒµ Œ∫œåŒºŒ∑ŒΩ, ·ºêœÄŒµ·Ω∂ Œø·Ωî Œº‚Äô ·ºîœÑŒπ Œ¥ŒµœçœÑŒµœÅŒøŒΩ ·ΩßŒ¥Œµ<br>\n  ·ºµŒæŒµœÑ‚Äô ·ºÑœáŒøœÇ Œ∫œÅŒ±Œ¥ŒØŒ∑ŒΩ, ·ΩÑœÜœÅŒ± Œ∂œâŒø·øñœÉŒπ ŒºŒµœÑŒµŒØœâ.<br>\n  ·ºÄŒªŒª‚Äô ·º§œÑŒøŒπ ŒΩ·ø¶ŒΩ Œº·Ω≤ŒΩ œÉœÑœÖŒ≥ŒµœÅ·øá œÄŒµŒπŒ∏œéŒºŒµŒ∏Œ± Œ¥Œ±ŒπœÑŒØ¬∑</p>\n  </td>\n  <td width=\"50%\" valign=\"top\">\n  <p>But he prepared a spirit-soothing funeral feast for them.<br>\n  Many white oxen bellowed around the iron as they were\t<br>\nslaughtered, and many sheep and bleating goats;<br>\n  many white-toothed swine teaming with fat<br>\n  being singed were stretched across Hephaestus‚Äô flame;<br>\n  all around the corpse ran blood, cupfuls of it.<br>\n  But the kings of the Achaeans led the swift-footed lord,<br>\nson of Peleus, to godlike Agamemnon<br>\n  With difficulty they persuaded him, still vexed at heart<br>\nfor his companion. When they led him to the hut of<br>\nAgamemnon, at once they ordered shrill-voiced heralds<br>\n  to set around the fire a great tripod, if the son of<br>\nPeleus would be persuaded to wash off the bloody filth.<br>\n  But he refused vigorously, and swore an oath:<br>\n  ‚ÄúNot by Zeus, who is the highest and best of the gods,<br>\n  it is not sanctioned for water to come near my head<br>\n  before I put Patroklos in the fire and pour a <em>s√™ma</em><br>\n  and cut my hair, since not a second time will<br>\nsuch grief come to my head while I go among the living.<br>\nBut come and let us be persuaded to the hateful feast.</p>\n  </td>\n </tr>\n</tbody></table>\n<p style=\"font-size:10px\">(Charts updated October 23, 2013)</p></body></html>",
            "authors": [
              {
                "name": "Margo Kitts"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 177,
            "issue_id": "160",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 178,
            "issue_id": "160",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26i/00_26.1cover.pdf"
          },
          {
            "id": 179,
            "issue_id": "160",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 180,
            "issue_id": "160",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26i/00_26.1fm.pdf"
          },
          {
            "id": 181,
            "issue_id": "160",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26i/11_26.1bm.pdf"
          },
          {
            "id": 182,
            "issue_id": "160",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26i/01_26.1.pdf"
          },
          {
            "id": 183,
            "issue_id": "160",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26i/oraltradition_26_1.zip"
          },
          {
            "id": 184,
            "issue_id": "160",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/26i/26.1complete.pdf"
          },
          {
            "id": 185,
            "issue_id": "160",
            "name": "Volume",
            "value": "26"
          },
          {
            "id": 186,
            "issue_id": "160",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 187,
            "issue_id": "160",
            "name": "Date",
            "value": "2011-03-01"
          }
        ]
      },
      {
        "id": 169,
        "name": "25ii",
        "title": "Volume 25, Issue 2",
        "articles": [
          {
            "id": 9086,
            "title": "“Like Cords Around My Heart”: Sacred Harp Memorial Lessons and the Transmission of Tradition",
            "content": "<p>Sacred Harp singing is a participatory American vernacular hymnody tradition with historical roots in New England and the rural South. This article addresses “memorial lessons,” ritualized lament speeches that play a key role in transmitting community values to new singers. Because of their reliance on oral tradition, their emphasis on memory and personal relationships, and their emotionally charged juxtaposition of speech and song, memorial lessons define, confirm, and perpetuate particular conceptions of “traditional singing.”</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Kiri Miller"
              }
            ],
            "files": [
              {
                "id": 9088,
                "title": "Miller-figure1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Miller-figure1.jpg",
                "caption": "Figure 1: The Hollow Square."
              },
              {
                "id": 9089,
                "title": "Miller-figure2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Miller-figure2.jpg",
                "caption": "Figure 2: Shape-note notation."
              },
              {
                "id": 9090,
                "title": "JB_midwest1_2007.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/JB_midwest1_2007.jpg",
                "caption": "The front bench of the treble section at the 2007 Midwest Sacred Harp Convention in Chicago."
              },
              {
                "id": 9091,
                "title": "JB_midwest2_2007.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/JB_midwest2_2007.jpg",
                "caption": "Anne Heider leads a tune at the 2007 Midwest Sacred Harp Convention in Chicago."
              },
              {
                "id": 9092,
                "title": "JB_midwest3_2007.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/JB_midwest3_2007.jpg",
                "caption": "The hollow square at the 2007 Midwest Sacred Harp Convention in Chicago."
              },
              {
                "id": 9093,
                "title": "JK_WMSHC1_2009.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/JK_WMSHC1_2009.jpg",
                "caption": "Singers lead a tune at the 2009 Western Massachusetts Sacred Harp Convention in Northampton."
              },
              {
                "id": 9094,
                "title": "JK_WMSHC2_2009.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/JK_WMSHC2_2009.jpg",
                "caption": "George and Jean Seiler lead a tune at the 2009 Western Massachusetts Sacred Harp Convention in Northampton."
              },
              {
                "id": 9095,
                "title": "JK_WMSHC3_2009.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/JK_WMSHC3_2009.jpg",
                "caption": "The front bench of the tenor section at the 2009 Western Massachusetts Sacred Harp Convention in Northampton."
              },
              {
                "id": 9096,
                "title": "KGM_bass_bench.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/KGM_bass_bench.jpg",
                "caption": "The front bench of the bass section at the 2007 New York State Sacred Harp Convention in Rochester."
              },
              {
                "id": 9097,
                "title": "KGM_Nathan_leading_444.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/KGM_Nathan_leading_444.jpg",
                "caption": "A singer leads a tune at the 2005 Chattahoochee Musical Convention, held at Wilson&rsquo;s Chapel (near Carrollton, Georgia)."
              },
              {
                "id": 9098,
                "title": "KGM_PA_hollow_square.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/KGM_PA_hollow_square.jpg",
                "caption": "The hollow square at the 2009 Keystone Sacred Harp Convention in Philadelphia."
              },
              {
                "id": 9099,
                "title": "KGM_Zoe_and_Fiona.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/KGM_Zoe_and_Fiona.jpg",
                "caption": "Singers lead a tune at the 2009 Keystone Sacred Harp Convention in Philadelphia."
              },
              {
                "id": 9100,
                "title": "05_Save,_Lord_or_We_Perish_(ISUJ).mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/05_Save,_Lord_or_We_Perish_(ISUJ).mp3",
                "caption": "&ldquo;Save, Lord, Or We Perish&rdquo; (224). Recorded at the 96th session of the United Sacred Harp Musical Association, Liberty Church, Henagar, Alabama, September 11-12, 1999. Released on the album In Sweetest Union Join, available at <a href=\"http://www.cdbaby.com/cd/ushma\" target=\"_blank\">http://www.cdbaby.com/cd/ushma</a>."
              },
              {
                "id": 9101,
                "title": "09_Marcia_Johnson_memorial_(ISUJ).mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/09_Marcia_Johnson_memorial_(ISUJ).mp3",
                "caption": "Memorial lesson by Marcia Johnson. Recorded at the 96th session of the United Sacred Harp Musical Association, Liberty Church, Henagar, Alabama, September 11-12, 1999. Released on the album In Sweetest Union Join, available at <a href=\"http://www.cdbaby.com/cd/ushma\" target=\"_blank\">http://www.cdbaby.com/cd/ushma</a>."
              }
            ]
          },
          {
            "id": 9082,
            "title": "“Writing” and “Reference” in Ifá Divination Chants",
            "content": "<p>The 256 graphic inscriptions (<em>odù</em>), and the infinite number of illustrative narratives (<em>ẹsẹ</em>) that they generate, constitute the two main parts of Ifá divination protocols. However, the narratives do not adhere coherently to durable patterns that attach to specific inscriptions. This paper argues that the storytelling freedom exercised by diviners decouples interpretation from “writing” and creates room for individually authored therapeutic commentaries. “Writing” links the present to the past, but storytelling keeps them apart.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Adélékè Adéẹ̀kọ́"
              }
            ],
            "files": [
              {
                "id": 9084,
                "title": "Ifa-Chart.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Ifa-Chart.jpg",
                "caption": "<p>If&#225; Divination &ldquo;Writing&rdquo;: the 256 combinations</p><ul style=\"list-style-type:disc;margin-left:5%\"><li>Basic signs are on the extreme right column</li><li>Signs are nameable or decodable with narratives only when doubled</li><li>Reading/naming sequence is right to left</li><li>Order of &ldquo;importance&rdquo; is top to bottom</li></ul><br /><a href=\"/files/ecompanions/25ii/full/adeeko.html\" target=\"_blank\">Full-size chart (opens in a new window)</a>"
              },
              {
                "id": 9085,
                "title": "Drewal,-Opon-Ifa-from-Ketu.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Drewal,-Opon-Ifa-from-Ketu.jpg",
                "caption": "Divination tray."
              }
            ]
          },
          {
            "id": 9080,
            "title": "Orality, Literacy, Popular Culture: An Eighteenth-Century Case Study",
            "content": "<p>Taking two antiquarian texts as its central focus—Henry Bourne’s <em>Antiquitates Vulgares: Antiquities of the Common People</em> (1725) and the revised and extended version compiled by John Brand as <em>Observations on Popular Antiquities</em> (1777)—this paper explores how orality, literacy, and popular culture were understood and represented by the literate and educated during the eighteenth century in order to shed light on the heritage of certain key terms that have become common in the modern critical lexicon. It examines the ways in which Anglican hostility towards the Catholic Church, anxiety about public order, and doubts about the state of the national character shaped attitudes toward traditional beliefs and oral practices. This complex scenario did not produce a clear opposition between the oral and the literate or between the popular and the elite, but instead created a series of conceptual uncertainties, at the heart of which was a different kind of question: who or what constitutes “the people,” and what about “the public”? </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Laura Davies"
              }
            ],
            "files": []
          },
          {
            "id": 9046,
            "title": "“Secret Language” in Oral and Graphic Form:  Religious-Magic Discourse in Aztec Speeches and Manuscripts",
            "content": "<p>The purpose of this paper is to analyze a particular linguistic register of the Nahua (Aztec) people that has been classified as “magical-religious” because it was used for communication with the sacred realm. A hypothesis is developed that in the Mesoamerican pictographic calendar-religious manuscripts, which treat matters strictly linked to the supernatural world, a similar register should be evident. Far from considering the information presented in these resources as resulting from the direct transcription of oral language, the idea is that the graphic form represents elements emblematic of orality, although adapted to this particular context and mode for expression.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Katarzyna Mikulska Dąbrowska"
              }
            ],
            "files": [
              {
                "id": 9048,
                "title": "01_Vienna-48.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/01_Vienna-48.jpg",
                "caption": "Fig. 1. Priests in array of eagle and fire serpent. <em>Vindobonensis Codex</em> (lam. 48)."
              },
              {
                "id": 9049,
                "title": "02_huehuetl_Malin_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/02_huehuetl_Malin_Nadia.jpg",
                "caption": "Fig. 2. Drum called <em>Huehuetl de Malinalco</em>."
              },
              {
                "id": 9050,
                "title": "03_guerra-Mendoza_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/03_guerra-Mendoza_Nadia.jpg",
                "caption": "Fig. 3. Symbol of war. <em>Codex Mendoza</em> (fol. 4v)."
              },
              {
                "id": 9051,
                "title": "04-a_yohual-ehec_Borgia-29_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/04-a_yohual-ehec_Borgia-29_Nadia.jpg",
                "caption": "Fig. 4. Graphic representation of the diphrasism <em>in yohualli in ehecatl</em> (a), compared with the image of night sky (b), and the face of the wind god Ehecatl (c).<br />Fig. 4a. <em>Borgia Codex</em> (lam. 29)."
              },
              {
                "id": 9052,
                "title": "04-b_noche_Borgia_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/04-b_noche_Borgia_Nadia.jpg",
                "caption": "Fig. 4. Graphic representation of the diphrasism <em>in yohualli in ehecatl</em> (a), compared with the image of night sky (b), and the face of the wind god Ehecatl (c).<br />Fig. 4b. <em>Borgia Codex</em> (lam. 52)."
              },
              {
                "id": 9053,
                "title": "04-c_Ehecatl_Borgia-72_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/04-c_Ehecatl_Borgia-72_Nadia.jpg",
                "caption": "Fig. 4. Graphic representation of the diphrasism <em>in yohualli in ehecatl</em> (a), compared with the image of night sky (b), and the face of the wind god Ehecatl (c).<br />Fig. 4c. <em>Borgia Codex</em> (lam. 72)."
              },
              {
                "id": 9054,
                "title": "05-a_asiento-PM-51r_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/05-a_asiento-PM-51r_Nadia.jpg",
                "caption": "Fig. 5. Different graphic representations of the diphrasism with the meaning of &ldquo;authority.&rdquo;<br />Fig. 5a. Sahag&#250;n <em>Primeros Memoriales facs</em>. (fol. 51r)."
              },
              {
                "id": 9055,
                "title": "05-b_asient_Vat-B_KMD.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/05-b_asient_Vat-B_KMD.jpg",
                "caption": "Fig. 5. Different graphic representations of the diphrasism with the meaning of &ldquo;authority.&rdquo;<br />Fig. 5b. <em>Vaticanus B Codex</em> (lam. 42)."
              },
              {
                "id": 9056,
                "title": "05-c_Chalchi-Borgia-65_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/05-c_Chalchi-Borgia-65_Nadia.jpg",
                "caption": "Fig. 5. Different graphic representations of the diphrasism with the meaning of &ldquo;authority.&rdquo;<br />Fig. 5c. <em>Borgia Codex</em> (lam. 65)."
              },
              {
                "id": 9057,
                "title": "05-d_Venus--Borgia54_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/05-d_Venus--Borgia54_Nadia.jpg",
                "caption": "Fig. 5. Different graphic representations of the diphrasism with the meaning of &ldquo;authority.&rdquo;<br />Fig. 5d. <em>Borgia Codex</em> (lam. 54)."
              },
              {
                "id": 9058,
                "title": "05-e_Venus-guerr-VatB_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/05-e_Venus-guerr-VatB_Nadia.jpg",
                "caption": "Fig. 5. Different graphic representations of the diphrasism with the meaning of &ldquo;authority.&rdquo;<br />Fig. 5e. <em>Vaticanus B Codex</em> (lam. 83)."
              },
              {
                "id": 9059,
                "title": "06-a_Venus-jaguar-Cospi-11_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/06-a_Venus-jaguar-Cospi-11_Nadia.jpg",
                "caption": "Fig. 6. Representations of warriors.<br />Fig. 6a. As a jaguar. <em>Cospi Codex</em> (lam. 11)."
              },
              {
                "id": 9060,
                "title": "06-b_Venus-guerr-Borgia54_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/06-b_Venus-guerr-Borgia54_Nadia.jpg",
                "caption": "Fig. 6. Representations of warriors.<br />Fig. 6b. As a shield with arrows. <em>Borgia Codex</em> (lam. 54)."
              },
              {
                "id": 9061,
                "title": "07-a_Mayah-amam--FM-28_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/07-a_Mayah-amam--FM-28_Nadia.jpg",
                "caption": "Fig. 7. Graphic representations of the agave goddess Mayahuel.<br />Fig. 7a. <em>Fej&#233;rv&#225;ry-Mayer Codex</em> (lam. 28)."
              },
              {
                "id": 9062,
                "title": "07-b_Mayah---Vat-B-40_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/07-b_Mayah---Vat-B-40_Nadia.jpg",
                "caption": "Fig. 7. Graphic representations of the agave goddess Mayahuel.<br />Fig. 7b. <em>Vaticanus B Codex</em> (lam. 40)."
              },
              {
                "id": 9063,
                "title": "08-a-1,2_Cinteotl_Cospi-2_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/08-a-1,2_Cinteotl_Cospi-2_Nadia.jpg",
                "caption": "Fig. 8. &ldquo;Reduced&rdquo; images of some deities of the <em>tonalpohualli</em> cycle in the <em>Cospi Codex</em>.<br />Fig. 8a. The maize god, Centeotl, in an anthropomorphic form (with corn cobs in his headdress) and as a bird claw with corn cobs and maize flowers. <em>Cospi Codex</em> (lam. 2)."
              },
              {
                "id": 9064,
                "title": "08-b-1,2_Mictlan_Cospi-7_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/08-b-1,2_Mictlan_Cospi-7_Nadia.jpg",
                "caption": "Fig. 8. &ldquo;Reduced&rdquo; images of some deities of the <em>tonalpohualli</em> cycle in the <em>Cospi Codex</em>.<br />Fig. 8b. The god of the Underworld, Mictlantecuhtli, in an anthropomorphic form (as a skull) and as an image of a large bone. <em>Cospi Codex</em> (lam. 7)."
              },
              {
                "id": 9065,
                "title": "08-c-1,2_Tlazol_Cospi-7_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/08-c-1,2_Tlazol_Cospi-7_Nadia.jpg",
                "caption": "Fig. 8. &ldquo;Reduced&rdquo; images of some deities of the <em>tonalpohualli</em> cycle in the <em>Cospi Codex</em>.<br />Fig. 8c. The fertility goddess, Tlazolteotl, in an anthropomorphic form (with un-spun cotton and a crescent moon nose ornament) and as a ball of cotton with the symbol of a crescent moon. <em>Cospi Codex</em> (lam. 7)."
              },
              {
                "id": 9066,
                "title": "09-a_templo-Mictlant-Cospi-13_KMD.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/09-a_templo-Mictlant-Cospi-13_KMD.jpg",
                "caption": "Fig. 9a. The temple of South (related to the Place of the Dead) decorated with <em>tlaquaquallo</em> elements. <em>Cospi Codex</em> (lam. 13)."
              },
              {
                "id": 9067,
                "title": "09-b_muer-cielo-Borgia-57_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/09-b_muer-cielo-Borgia-57_Nadia.jpg",
                "caption": "Fig. 9b. Dead man devoured by a skeletal deity, with a recipient with <em>tlaquaquallo</em> elements on its side. <em>Borgia Codex</em> (lam. 57)."
              },
              {
                "id": 9068,
                "title": "10-a_mantica-Vat-B-7_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/10-a_mantica-Vat-B-7_Nadia.jpg",
                "caption": "Fig. 10. &ldquo;Divination scenes&rdquo; with <em>tlaquaquallo</em> elements.<br />Fig. 10a. <em>Vaticanus B Codex</em> (lam. 7)."
              },
              {
                "id": 9069,
                "title": "10-b_matica-Cospi-7_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/10-b_matica-Cospi-7_Nadia.jpg",
                "caption": "Fig. 10. &ldquo;Divination scenes&rdquo; with <em>tlaquaquallo</em> elements.<br />Fig. 10b. <em>Cospi Codex</em> (lam. 7)."
              },
              {
                "id": 9070,
                "title": "10-c_Borgia-7-mant-mano_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/10-c_Borgia-7-mant-mano_Nadia.jpg",
                "caption": "Fig. 10. &ldquo;Divination scenes&rdquo; with <em>tlaquaquallo</em> elements.<br />Fig. 10c. <em>Borgia Codex</em> (lam. 7)."
              },
              {
                "id": 9071,
                "title": "11-a_sangre-Borgia-48_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/11-a_sangre-Borgia-48_Nadia.jpg",
                "caption": "Fig. 11. Graphic representations of blood (the red color is marked with grey).<br />Fig. 11a. <em>Borgia Codex</em> (lam. 48)."
              },
              {
                "id": 9072,
                "title": "11-b_Borgia-8_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/11-b_Borgia-8_Nadia.jpg",
                "caption": "Fig. 11. Graphic representations of blood (the red color is marked with grey).<br />Fig. 11b. <em>Borgia Codex</em> (lam. 8)."
              },
              {
                "id": 9073,
                "title": "12-a_Borgia-50-inf_KMD.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/12-a_Borgia-50-inf_KMD.jpg",
                "caption": "Fig. 12a. Graphic representation of water as precious liquid. Figs. 12a-b have two different signs meaning &ldquo;gold&rdquo;. <em>Borgia Codex</em> (lam. 50)."
              },
              {
                "id": 9074,
                "title": "12-b_Borgia-50-sup_KMD.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/12-b_Borgia-50-sup_KMD.jpg",
                "caption": "Fig. 12b. Graphic representation of water as precious liquid. Figs. 12a-b have two different signs meaning &ldquo;gold&rdquo;. <em>Borgia Codex</em> (lam. 50)."
              },
              {
                "id": 9075,
                "title": "13_Borgia-29-KMD.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/13_Borgia-29-KMD.jpg",
                "caption": "Fig. 13. The &ldquo;god pot&rdquo; with the face in form of skull, and with legs and arms opened on both sides. <em>Borgia Codex</em> (lam. 29)."
              },
              {
                "id": 9076,
                "title": "14-a_Borgia-31_KMD.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/14-a_Borgia-31_KMD.jpg",
                "caption": "Fig. 14a. Graphic representation of &ldquo;god pot.&rdquo; <em>Borgia Codex</em> (lam. 31)."
              },
              {
                "id": 9077,
                "title": "14-b_Borgia-32_KMD.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/14-b_Borgia-32_KMD.jpg",
                "caption": "Fig. 14b. Graphic representation of &ldquo;god pot.&rdquo; <em>Borgia Codex</em> (lam. 32)."
              },
              {
                "id": 9078,
                "title": "15-a_Hakmack-base_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/15-a_Hakmack-base_Nadia.jpg",
                "caption": "Fig. 15a. Earth in monster form, painted with arms and legs open. Image on the base of so called Hakmack Box."
              },
              {
                "id": 9079,
                "title": "15b_Tlazolteotl_Borb-13_Nadia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/15b_Tlazolteotl_Borb-13_Nadia.jpg",
                "caption": "Fig. 15b. The fertility goddess Tlazolteotl, giving birth. <em>Borbonicus Codex</em> (lam. 13)."
              }
            ]
          },
          {
            "id": 9033,
            "title": "Improvised Song in Schools: Breaking Away from the Perception of Traditional Song as Infantile by Introducing a Traditional Adult Practice",
            "content": "<p>This article revolves around a project aimed at incorporating improvised song into primary school education. Among its objectives, this pilot scheme aimed to solve the problem of infantilization and the lack of functionality affecting the traditional school repertoire of songs in Catalonia by introducing a hitherto untested genre of traditional song into the official curriculum. The findings obtained in five centers suggest that this traditional form of oral expression through singing obtains positive results in the 10-12- year-old age group, and manages to break free of the clichés about traditional song pre-existing in the school environment.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jaume Ayats"
              },
              {
                "name": "Albert Casals"
              },
              {
                "name": "Mercè Vilar"
              }
            ],
            "files": [
              {
                "id": 9035,
                "title": "Catalan_countries_map3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Catalan_countries_map3.jpg",
                "caption": "Map of Pa&#239;sos Catalans Marc Belzunces."
              },
              {
                "id": 9036,
                "title": "Photo1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Photo1.jpg",
                "caption": "<em>Corrandistes</em> singing after a special lunch."
              },
              {
                "id": 9037,
                "title": "Photo2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Photo2.jpg",
                "caption": "Lo Teixid&#243;, one of the last traditional singers of <em>Jotes de l&rsquo;Ebre</em>."
              },
              {
                "id": 9038,
                "title": "Photo3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Photo3.jpg",
                "caption": "Carles Belda, a leading singer from the new generation of <em>corrandistes</em>."
              },
              {
                "id": 9039,
                "title": "Photo4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Photo4.jpg",
                "caption": "Anyone can become the soloist."
              },
              {
                "id": 9040,
                "title": "Video1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Video1.mp4",
                "caption": "A <em>corrandista</em> singing <em>Can&#231;&#243; de pandero</em>."
              },
              {
                "id": 9041,
                "title": "Photo5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Photo5.jpg",
                "caption": "The arrangement of the participants in a circle encourages communication and participation as soloists."
              },
              {
                "id": 9042,
                "title": "Video2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Video2.mp4",
                "caption": "Children improvising verses in a special school event."
              },
              {
                "id": 9043,
                "title": "Photo6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Photo6.jpg",
                "caption": "Pupils improvising <em>corrandes</em> in a final special session."
              },
              {
                "id": 9044,
                "title": "Figura_OT.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Figura_OT.jpg",
                "caption": "Figure 1. The building of group identity."
              },
              {
                "id": 9045,
                "title": "Video3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Video3.mp4",
                "caption": "The atmosphere in a class during an improvised song performance."
              }
            ]
          },
          {
            "id": 9026,
            "title": "The Tuzu <em>Gesar</em> Epic: Performance and Singers",
            "content": "<p>The <em>Gesar</em> epic is a Tibetan heroic epic widely spread across the vast ethnic regions of China. It amounts to an encyclopedia that illustrates the life of ancient Tibetans. It has been in contact with other traditional ethnic cultures, and therefore has multiethnic characteristics. The Tuzu <em>Gesar</em> is a long epic, comprised of alternating prose and verse, that has been greatly influenced by the Tibetan <em>Gesar</em>. Tuzu (the minority group is called Tu) is a spoken language with no written system. The great performers of this epic have passed away, leaving only Wang Yongfu, 79 years old and now ill. Thus it is extremely urgent to study and to protect the Tuzu <em>Gesar</em> as performed by him. This paper offers a brief introduction to the content, distribution areas, and historical context of the epic.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Wang Guoming"
              }
            ],
            "files": [
              {
                "id": 9028,
                "title": "Tuzu-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Tuzu-1.jpg",
                "caption": "Wang Yongfu performing the Tuzu <em>Gesar</em>."
              },
              {
                "id": 9029,
                "title": "TuzuEpicMap.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/TuzuEpicMap.jpg",
                "caption": "Map of Northwest China."
              },
              {
                "id": 9030,
                "title": "Tuzu-Gesar.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Tuzu-Gesar.mp4",
                "caption": "Discovery and Protection of Tuzu <em>Gesar</em>."
              },
              {
                "id": 9031,
                "title": "Tuzu-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Tuzu-2.jpg",
                "caption": "Wang Yongfu praying before his performance."
              },
              {
                "id": 9032,
                "title": "Tuzu-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Tuzu-3.jpg",
                "caption": "Wang Yongfu speaking with his son, Wang Guoming."
              }
            ]
          },
          {
            "id": 9008,
            "title": "Interperformative Relationships in Ingrian Oral Poetry",
            "content": "<p>Kalevala-metric oral poetry comprises various genres, from epic and lyric to ritual poetry, dancing songs, and lullabies. These poems were performed in diverse ways and applied to various social situations. The present article highlights the complexity of performance, referentiality, and local genres in Ingria. This complexity makes it crucial to take into account various analytical levels of the poem, from the content and meter to the melodies, refrains, and vocal style.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Kati Kallio"
              }
            ],
            "files": [
              {
                "id": 9010,
                "title": "MAP_1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/MAP_1.jpg",
                "caption": "Map 1: Ingria, Estonia, Finland, Karelia, and Russia."
              },
              {
                "id": 9011,
                "title": "MAP_2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/MAP_2.jpg",
                "caption": "Map 2: The villages of the West-Ingria."
              },
              {
                "id": 9012,
                "title": "ah533_004-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/ah533_004-2.jpg",
                "caption": "Photo 1: Swinging in the Kallivieri village during the Second World War in 1943."
              },
              {
                "id": 9013,
                "title": "Museovirasto_128_11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Museovirasto_128_11.jpg",
                "caption": "Photo 2: The girls of Risum&#228;ki village posing still by the swing for the collector A. O. V&#228;is&#228;nen in 1914."
              },
              {
                "id": 9014,
                "title": "krak2009_0001_f.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/krak2009_0001_f.jpg",
                "caption": "Photo 3: West-Ingrian singers returning from the recording trip to Tallinn in 1937. Valpuri Vohta, the collector Aili Laiho, Mari Vahter, Tatjana Jegorov, Anna Kivisoo, and Darja Lehti."
              },
              {
                "id": 9015,
                "title": "eC1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/eC1.jpg",
                "caption": "1. Mit&#228; noisen laulamahan (What Shall I Rise to Sing)."
              },
              {
                "id": 9016,
                "title": "eC2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/eC2.mp4",
                "caption": "2. La ka katson leikkavani (Let Me See My Swing)."
              },
              {
                "id": 9017,
                "title": "eC3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/eC3.jpg",
                "caption": "3. Leeluttaja keekuttaja (The Swinger, the Swayer)."
              },
              {
                "id": 9018,
                "title": "eC4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/eC4.jpg",
                "caption": "4. Lavansaaren liekkulaulu (The Swinging Song from Lavansaari)."
              },
              {
                "id": 9019,
                "title": "eC5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/eC5.jpg",
                "caption": "5. Mit&#228; noisen laulamahan (What Shall I Rise to Sing)."
              },
              {
                "id": 9020,
                "title": "eC6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/eC6.mp4",
                "caption": "6. Nyt ol lusti nuoren noissa (Now It Is Fun for a Young One to Rise)."
              },
              {
                "id": 9021,
                "title": "eC7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/eC7.mp4",
                "caption": "7. Ei miun laulella pitt&#228;isi (I Should not Be Singing)."
              },
              {
                "id": 9022,
                "title": "eC8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/eC8.jpg",
                "caption": "8. Oli t&#228;ss&#228; ennen kyl&#228; (It Used to Be a Village Here)."
              },
              {
                "id": 9023,
                "title": "eC9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/eC9.mp4",
                "caption": "9. Nuku nuku yski silm&#228; (Sleep Sleep the One Eye)."
              },
              {
                "id": 9024,
                "title": "eC10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/eC10.mp4",
                "caption": "10. Leino leski (The Sad Widow)."
              },
              {
                "id": 9025,
                "title": "eC11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/eC11.mp4",
                "caption": "11. L&#228;hemm&#228; l&#228;pi kyl&#228;sen (We Shall Get through the Village)."
              }
            ]
          },
          {
            "id": 9006,
            "title": "Rethinking the Orality-Literacy Paradigm in Musicology",
            "content": "<p>This paper poses questions regarding the implications of mainstream orality-literacy research on musicological perspectives and the relevance of musicological research for orality-literacy studies. First, why has musical scholarship been largely ignored in orality-literacy research? Second, what have been the casualties of lost connections with the academic mainstream? Finally, what are the possible unique contributions of musicological research to the overarching questions of the orality-literacy problematic, particularly in the electronic world? </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Francesca R. Sborgi Lawson"
              }
            ],
            "files": []
          },
          {
            "id": 8997,
            "title": "Asian Origins of Cinderella: The Zhuang Storyteller of Guangxi",
            "content": "<p>The acceptance and understanding of the Asian origins of the “Cinderella” story should replace the widely held belief that the story is fundamentally Western or universal. The Zhuang, an ethnic group at the intersection of China and Vietnam, combined ideas from their own traditions and experiences with motifs from Hindu and Buddhist narratives circulating in their area during the Tang Dynasty, and should be credited with creating this subversive, virginal, talented, and compassionate heroine. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Fay Beauchamp"
              }
            ],
            "files": [
              {
                "id": 8999,
                "title": "Figure-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Figure-1.jpg",
                "caption": "Figure 1. Fishponds outside Guilin, Guangxi Province, identified as created by and belonging to the Zhuang."
              },
              {
                "id": 9000,
                "title": "Figure-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Figure-2.jpg",
                "caption": "Figure 2. A niche with a statue identified as a Tang Dynasty Guanyin. The niche is at the base of Elephant Hill which has the remains of a Buddhist pagoda on the top. The statue was said to be moved to this location after the original Tang Dynasty statue was destroyed during the Cultural Revolution (1966-1976)."
              },
              {
                "id": 9001,
                "title": "Figure-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Figure-3.jpg",
                "caption": "Figure 3. Directly below the Guanyin statue in Figure 2 is a &ldquo;Pond of Mercy&rdquo; where small red fish are currently released to acquire good luck."
              },
              {
                "id": 9002,
                "title": "Figure-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Figure-4.jpg",
                "caption": "Figure 4. Amulets bought on the steps leading up to the Yongshuo terraced rice paddies. The seller explained that one carving inside shows a Bodhisattva Guanyin figure; the other side shows a child. The current amulets are made of inexpensive plastic, but a magnifying glass  shows a feminized Guanyin figure."
              },
              {
                "id": 9003,
                "title": "Figure-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Figure-5.jpg",
                "caption": "Figure 5. (not referenced in the text)  Terraced rice paddies in Yangshuo, up the Li River from Guilin, created by the Zhuang beginning in the ninth century. These terraces were made when the Zhuang were displaced from riverside farms. The picture  was taken in November when the terraces were dry; in the summer they are flooded to grow rice and stocked with red fish."
              },
              {
                "id": 9004,
                "title": "Figure-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Figure-6.jpg",
                "caption": "Figure 6. (not referenced in the text). There are two historic lakes within Nanning. The larger, South Lake, was created in the Tang Dynasty after a flood when an official Lu Ren, in charge of  military, created a water storage project to guard against further damage (according to a sign near the lake). In the middle of a park there is a second smaller &ldquo;White Dragon Lake&rdquo; with a sign saying &ldquo;Enjoying Fish at Dragon Pond. General Di Qing in the Song Dynasty named the lake.&rdquo; The sign further says that the fish died due to pollution and the lake was stocked with 10,000 Koi in 1998 (The word &ldquo;Koi&rdquo; was used on the sign in English).</p><p>In the photo’s background is a young man holding a baby; nearer to the photographer is a young couple feeding the fish. Their child, not in this picture, came up to the author to give her bread to give to the fish."
              },
              {
                "id": 9005,
                "title": "Figure-7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25ii/full/Figure-7.jpg",
                "caption": "Figure 7. (not referenced in the text).  The size of the fish that came to be fed at Dragon Pond, if not five nor ten feet long as in the Yexian story, was impressive."
              }
            ]
          },
          {
            "id": 8995,
            "title": "A Bibliography of Publications by Albert Bates Lord",
            "content": "<p>This bibliography was compiled by Morgan E. Grey, using the prior bibliography attached to J.M. Foley, “Albert Bates Lord (1912-1991): An Obituary,” <em>Journal of American Folklore</em>, 105 (1992): 57-65, together with subsequent annotations made by Mary Louise Lord.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Miles Foley"
              },
              {
                "name": "Morgan E. Grey"
              },
              {
                "name": "Mary Louise Lord"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 188,
            "issue_id": "169",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 189,
            "issue_id": "169",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25ii/00_25.2cover.pdf"
          },
          {
            "id": 190,
            "issue_id": "169",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 191,
            "issue_id": "169",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25ii/00_25.2fm.pdf"
          },
          {
            "id": 192,
            "issue_id": "169",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25ii/12_25.2bm.pdf"
          },
          {
            "id": 193,
            "issue_id": "169",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25ii/01_25.2.pdf"
          },
          {
            "id": 194,
            "issue_id": "169",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25ii/oraltradition_25_2.zip"
          },
          {
            "id": 195,
            "issue_id": "169",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25ii/25.2complete.pdf"
          },
          {
            "id": 196,
            "issue_id": "169",
            "name": "Volume",
            "value": "25"
          },
          {
            "id": 197,
            "issue_id": "169",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 198,
            "issue_id": "169",
            "name": "Date",
            "value": "2010-10-01"
          }
        ]
      },
      {
        "id": 181,
        "name": "25i",
        "title": "Volume 25, Issue 1: Oral Tradition in Judaism, Christianity, and Islam",
        "articles": [
          {
            "id": 9137,
            "title": "Oral Tradition in Judaism, Christianity, and Islam:  Introduction",
            "content": "<p>The present issue of <em>Oral Tradition</em> stands as a tribute to a conference initiated and convened by professors Werner Kelber and Paula Sanders on the topic of Oral-Scribal Dimensions of Scripture, Piety, and Practice in Judaism, Christianity, and Islam, April 12-14, 2008. Sixteen active participants (a keynote speaker, four specialists in each of three world religions, and three respondents) met to examine the aesthetic, compositional, memorial, and performative aspects of three faiths (Judaism, Christianity, and Islam) in their appropriate media contexts. In many ways, this approach differs from, and indeed challenges, historical scholarship. Beginning with the pre-modern period and reaching into our postmodern world, the strictly philological, textual paradigm has served as the intellectual premise for classical and biblical scholarship, for medieval studies, and for the study of world religions as well. The Rice conference and the papers that emanated from it are designed to provide the philological, textual study of the monotheistic faiths with fresh insights and to suggest significant modifications. The largely Western paradigm of the three monotheistic faiths as quintessential religions of the book is, thereby, called into question in the present issue of <em>Oral Tradition</em>. If the flourishing discipline of orality-scribality-memory studies has shown anything conclusively, it is that prior to the invention of print technology the verbal arts were an intricate interplay of oral and scribal verbalization, with manuscripts often serving as mere reference points for recitation and memorization. The papers that follow show that this scenario applies with special relevance to Judaism, Christianity, and Islam.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Werner H. Kelber"
              },
              {
                "name": "Paula Sanders"
              }
            ],
            "files": [
              {
                "id": 9139,
                "title": "KelberIntro.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/KelberIntro.mp4",
                "caption": "Orality &amp; Literacy VII<br />Werner Kelber: Introduction."
              },
              {
                "id": 9140,
                "title": "SandersIntro.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/SandersIntro.mp4",
                "caption": "Orality &amp; Literacy VII<br />Paula Sanders: Introduction."
              },
              {
                "id": 9141,
                "title": "Foley.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Foley.mp4",
                "caption": "Orality &amp; Literacy VII<br />John Miles Foley: Keynote Address."
              }
            ]
          },
          {
            "id": 9134,
            "title": "Response from an Africanist Scholar",
            "content": "<p>Coming from a background of comparative work on orality and literacy but a non-specialist on the scriptures of Judaism, Christianity, and Islam, I was struck by how the conference themes paralleled developments in oral literary studies in Africa. These included the move away from generalized assertion to more focused insights into multiple historical and culturally specific diversities; a more nuanced, culturally aware, and critical approach to the concept of “the oral”; the fading influence of speculative teleological models; the historically specific epistemologies of oral and written as part of the subject matter; and the concepts of multi-literacies and multi-oralities.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ruth Finnegan"
              }
            ],
            "files": [
              {
                "id": 9136,
                "title": "Finnegan.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Finnegan.mp4",
                "caption": "Orality &amp; Literacy VII<br />Ruth Finnegan."
              }
            ]
          },
          {
            "id": 9131,
            "title": "Torah on the Heart: Literary Jewish Textuality Within Its Ancient Near Eastern Context",
            "content": "<p>This essay examines evidence for the interplay of memory recall and written technology in ancient Israel and surrounding cultures. The essay begins with a summary of the comparative evidence for writing-supported textual transmission in Mesopotamia, Egypt, Greece, and Israel (based on the ’s <em>Writing on the Tablet of the Heart: Origins of Scripture and Literature</em> [2005]). This is followed by a survey of studies in the humanities and social sciences on how memory shifts affect textual transmission. The article concludes with a preliminary summary of the author’s research on memory shifts and other trends in documented cases of transmission history.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "David M. Carr"
              }
            ],
            "files": [
              {
                "id": 9133,
                "title": "Carr.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Carr.mp4",
                "caption": "Orality &amp; Literacy VII<br />David M. Carr."
              }
            ]
          },
          {
            "id": 9128,
            "title": "Guarding Oral Transmission: Within and Between Cultures",
            "content": "<p>A 1997 claim that “Muslim hostility to the writing of tradition” was of “Jewish origin” stimulated this article's attempt to de-essentialize the notion of “Jewish oralism.” It reconstructs the vastly disparate concerns and historical circumstances that prompted Jews of third-century Palestine on the one hand, and of eighth- and ninth-century Baghdad on the other, to champion and guard the oral transmission of certain corpora of rabbinic tradition. The article concludes by considering a Babylonian-Palestinian Jewish dispute of the geonic era against the backdrop of Abbasid-Umayyad tensions.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Talya Fishman"
              }
            ],
            "files": [
              {
                "id": 9130,
                "title": "Fishman.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Fishman.mp4",
                "caption": "Orality &amp; Literacy VII<br />Talya Fishman."
              }
            ]
          },
          {
            "id": 9125,
            "title": "The Interplay Between Written and Spoken Word in the Second Testament as Background to the Emergence of Written Gospels",
            "content": "<p>Although comprised of self-consciously written texts, the New Testament reflects an intersection between oral and written words and worlds. In this paper, I undertake a survey of the New Testament with respect to the language of written and spoken word in an effort to identify where they overlap, ways in which they shape one another, places where they describe distinct worlds, and the social dynamics associated with each kind of word. In the conclusion I suggest ways in which the interplay between written and spoken word led to the emergence of the Gospels.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Holly Hearon"
              }
            ],
            "files": [
              {
                "id": 9127,
                "title": "Hearon.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Hearon.mp4",
                "caption": "Orality &amp; Literacy VII<br />Holly Hearon."
              }
            ]
          },
          {
            "id": 9122,
            "title": "Oral and Written Communication and Transmission of Knowledge in Ancient Judaism and Christianity",
            "content": "<p>This paper examines the contexts of oral communication and the use of written messages in Josephus’ writings, the New Testament, and rabbinic literature, and discusses the possible reasons for using orality or writing in the respective Jewish and Christian contexts in antiquity. It is argued that an individual’s social power depended on his position within the communication network and his ability to control and manipulate the dissemination of knowledge among his co-religionists. Mobility was an important means of creating these networks and the most mobile rabbis would have been the most well-connected.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Catherine Hezser"
              }
            ],
            "files": [
              {
                "id": 9124,
                "title": "Hezser.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Hezser.mp4",
                "caption": "Orality &amp; Literacy VII<br />Catherine Hezser."
              }
            ]
          },
          {
            "id": 9119,
            "title": "Oral and Written Aspects of the Emergence of the Gospel of Mark as Scripture",
            "content": "<p>How the Gospel of Mark, which would not have qualified as a “respectable” text in the Hellenistic-Roman world, became included in the Scripture of established Christianity is explored in the following steps: an examination of Mark’s relation to the Judean Scriptures in comparison with scribal cultivation of those scriptures; an exploration of the importance of oral communication in the origin of the Gospel; a review of the predominance of oral communication, oral performance, and oral-written texts in the context in which Mark was cultivated; an examination of the ways in which the Gospel of Mark was memorable and performable; and an analysis of Mark’s resonance with hearers in its historical performance context.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Richard A. Horsley"
              }
            ],
            "files": [
              {
                "id": 9121,
                "title": "Horsley.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Horsley.mp4",
                "caption": "Orality &amp; Literacy VII<br />Richard A. Horsley."
              }
            ]
          },
          {
            "id": 9116,
            "title": "The History of the Closure of Biblical Texts",
            "content": "<p>This paper examines the history of the biblical texts from their oral and papyrological beginnings to their triumphant apotheosis in print culture. Focusing on the oral-scribal-memorial-typographic dynamics, it demonstrates ways in which the biblical texts were communicated, transformed, interiorized, and lived by. The central thesis states that the media transformations of the Bible can be viewed by and large as reductive processes commencing with multiformity and polyvalency and moving away from oral, memorial sensibilities in the direction toward the autosemantic print authority.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Werner H. Kelber"
              }
            ],
            "files": [
              {
                "id": 9118,
                "title": "Kelber.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Kelber.mp4",
                "caption": "Orality &amp; Literacy VII<br />Werner H. Kelber."
              }
            ]
          },
          {
            "id": 9113,
            "title": "Two Faces of the Qur’ān: <em>Qur’ān</em> and <em>Muṣḥaf</em>",
            "content": "<p>The Qur’ān is usually read as a canonical text, constituting the foundational document of the Islamic religion. This paper proposes a new approach: reading the Qur’ān as the communication process between the Prophet and his community, which allows for the recovery of its dramatic character. It also enables us to reclaim its cultural context by tracing out the processes of negotiations, appropriations, or rejections of earlier Jewish and Christian traditions. The Qur’ān thus emerges as the testimony of a revolutionary late-antique religious paradigm.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Angelika Neuwirth"
              }
            ],
            "files": [
              {
                "id": 9115,
                "title": "Neuwirth.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Neuwirth.mp4",
                "caption": "Orality &amp; Literacy VII<br />Angelika Neuwirth."
              }
            ]
          },
          {
            "id": 9111,
            "title": "Biblical Performance Criticism: Performance as Research",
            "content": "<p>The purpose of this paper is to explore the New Testament as performance literature. Recent scholarship has brought to the fore the importance of interpreting the New Testament in the context of the oral cultures of the first century. This leads us to ask about the oral/performance dynamics of the “writings” now in the New Testament canon. The paper is comprised of three parts. Part 1 offers some reasons why the New Testament writings, if they are to be appropriately interpreted in their original oral context, must be treated as performance literature. Part 2 outlines performance criticism as a methodology for this task—constructing first-century scenarios of performance as a context for interpretation, reorienting and expanding methodological approaches to the New Testament as oral literature, and employing contemporary performance as a research tool. Part 3 represents the bulk of the paper, laying out in detail the idea of performance as research, arguing that what we can learn from contemporary performances may help us interpret the New Testament writings in the context of the oral cultures of the first century.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "David Rhoads"
              }
            ],
            "files": []
          },
          {
            "id": 9108,
            "title": "The Constitution of the Koran as a Codified Work: Paradigm for Codifying <em>Hadîth</em> and the Islamic Sciences?",
            "content": "<p>It was on practical grounds that the initial inscription of both Koran and <em>hadîth</em> (as well as most of the other genuine Islamic sciences) was undertaken: in both cases the use of script served to bolster the memory. There were no ideological reasons opposing this undertaking. There were also very practical grounds for the deliberate collections of Koran and <em>hadîth</em>: individual persons, above all rulers and court figures, wanted to have copies of the Koran and collections of <em>hadîth</em> at their disposal for private use. While there were no reasons to oppose a (non-official) codification of copies of the Koran for private use, strong ideological reservations arose against the codification of the <em>hadîth</em>. It was precisely the existence of the now-codified Koran that for a long time hindered the development of a second prospectively codified body of religious texts.  In the case of the Koran it was the professional interests of the Koran readers who appeared to lose their monopoly as the sole custodians of the Holy Book; in the case of the <em>hadîth</em> there were continued misgivings about placing similar text corpora alongside the Koran. While the codification of the Koran can be regarded as paradigmatic for the codification of <em>hadîth</em> only with considerable restrictions, the <em>hadîth</em> codification, on the other hand, was paradigmatic for the codification process of a great number of other Arabic-Islamic sciences.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Gregor Schoeler"
              }
            ],
            "files": [
              {
                "id": 9110,
                "title": "Schoeler.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Schoeler.mp4",
                "caption": "Orality &amp; Literacy VII<br />Gregor Schoeler."
              }
            ]
          },
          {
            "id": 9105,
            "title": "From <em>Jāhiliyyah</em> to <em>Badī<span style=\"font-size:0.65em; vertical-align:top;\">c</span>iyyah</em>: Orality, Literacy, and the Transformations of Rhetoric in Arabic Poetry",
            "content": "<p>Starting with the mnemonic imperative governing the use of rhetoric in pre- and early Islamic Arabic oral poetry, this essay proposes that in the later literary periods, rhetorical devices, now free of their mnemonic obligation, took on further communicative or expressive functions. In the High <span style=\"font-size:0.08em; vertical-align:18%;\">C</span>Abbāsid age, rhetorical devices are “retooled” to serve as the “linguistic correlative” of Islamic hegemony as witnessed in caliphal court panegyrics of the rhetorically complex <em>badī<span style=\"font-size:0.65em; vertical-align:top;\">c</span></em> style. Finally, the “rhetorical excess” of the post-classical <em>badī<span style=\"font-size:0.65em; vertical-align:top;\">c</span>iyyah</em> (a poem to the Prophet Muḥammad in which each line must exhibit a particular rhetorical device) is interpreted as a memorial structure typical of the medieval manuscript (as opposed to modern print) tradition.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Suzanne Pinckney Stetkevych"
              }
            ],
            "files": [
              {
                "id": 9107,
                "title": "Stetkevych.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Stetkevych.mp4",
                "caption": "Orality &amp; Literacy VII<br />Suzanne Pinckney Stetkevych."
              }
            ]
          },
          {
            "id": 9102,
            "title": "Summation",
            "content": "<p>My response to the conference focuses on five issues: (1) the importance of the reciprocity and interdependence of orality and writtenness; (2) the importance of socio-political-economic facets of textual uses; (3) the recitative, performative dimensions of the texts treated; (4) the largely Mediterranean provenance of the texts treated; and (5) the authority of texts, both written and oral. Finally, I make a tentative suggestion about the importance of the perceived authority rather than writtenness of major religious texts as the locus of sacrality, since the papers demonstrate how much both orality and writtenness are involved in the life of these texts.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William Graham"
              }
            ],
            "files": [
              {
                "id": 9104,
                "title": "Graham.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/25i/full/Graham.mp4",
                "caption": "Orality &amp; Literacy VII<br />William Graham."
              }
            ]
          }
        ],
        "customFields": [
          {
            "id": 199,
            "issue_id": "181",
            "name": "Subtitle",
            "value": "Oral Tradition in Judaism, Christianity, and Islam"
          },
          {
            "id": 200,
            "issue_id": "181",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25i/00_25.1cover.pdf"
          },
          {
            "id": 201,
            "issue_id": "181",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 202,
            "issue_id": "181",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25i/00_25.1fm.pdf"
          },
          {
            "id": 203,
            "issue_id": "181",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25i/15_25.1bm.pdf"
          },
          {
            "id": 204,
            "issue_id": "181",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25i/01_25.1.pdf"
          },
          {
            "id": 205,
            "issue_id": "181",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25i/oraltradition_25_1.zip"
          },
          {
            "id": 206,
            "issue_id": "181",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/25i/25.1complete.pdf"
          },
          {
            "id": 207,
            "issue_id": "181",
            "name": "Volume",
            "value": "25"
          },
          {
            "id": 208,
            "issue_id": "181",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 209,
            "issue_id": "181",
            "name": "Date",
            "value": "2010-03-02"
          }
        ]
      },
      {
        "id": 201,
        "name": "24ii",
        "title": "Volume 24, Issue 2: Sound Effects",
        "articles": [
          {
            "id": 9180,
            "title": "Sound Effects: The Oral/Aural Dimensions of Literature in English <em>Introduction</em>",
            "content": "<p><em>Sound Effects</em> traces the history of the relationship between oral conditions and aural effect in English literature from its beginnings in the Anglo-Saxon period through to the twenty-first century. Few collections nowadays, other than textbook histories, would attempt a survey of their field from the early middle ages to the present day, and it is not our intention here to offer a continuous narrative. But despite the many centuries covered by this collection, the reader will find that certain themes recur in different contexts and that the individual essays speak to each other, often over long distances of time. It ends where it might have begun, with Homer, though in modern English form. The effect of this pattern is to create an “envelope” structure in which the ancient oral forms of Greek and Anglo-Saxon verse reappear as contexts for understanding how these forms survive and how sound works in the poetry of the modern world. The scope of the volume is also determined by its subject, since we are concerned with tradition as well as with the oral and aural. In particular, we are concerned with how literary production and reception respond to the different waves of media evolution from oral to written, manuscript to print (and the theater), and the later development of machine technology. We are not specifically concerned with the computer and the Internet, though they are an unstated presence behind the project as a whole. A subsidiary theme is the way in which sound, understood in both oral and aural terms, provides the agency through which high and low, elite and popular cultures are brought into conjunction throughout English literature.</p>",
            "ecompanionhtml": "<html><head></head><body><a name=\"audio1\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/1._Dream_2.1.249.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio1\"><em>A Midsummer Night’s Dream</em></a> <span style=\"font-weight:normal\">courtesy of David Crystal (act 2 scene 1 line 249):</span></h4>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n<a name=\"audio2\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/2._Dream_3.2.102.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio2\"><em>A Midsummer Night’s Dream</em> </a> <span style=\"font-weight:normal\">courtesy of David Crystal (act 3 scene 2 line 102):</span></h4>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n<a name=\"audio3\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/3._Dream_5.1.413.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio3\"><em>A Midsummer Night’s Dream</em> </a> <span style=\"font-weight:normal\">courtesy of David Crystal (act 5 scene 1 line 413):</span></h4>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\"></body></html>",
            "authors": [
              {
                "name": "Chris Jones"
              },
              {
                "name": "Neil Rhodes"
              }
            ],
            "files": []
          },
          {
            "id": 9178,
            "title": "The Word Made Flesh: Christianity and Oral Culture in Anglo-Saxon Verse",
            "content": "<p>This paper considers the interface between the native, inherited, secular, vernacular, and oral legacy in Anglo-Saxon poetry and that of the foreign, imported, Christian, Latin, and written tradition that subsumed and largely supplanted it, at least in the extant record. A variety of Anglo-Saxon poets and poems are considered, including Aldhelm, Bede, Alcuin, and Boniface in Latin, and Cædmon and Cynewulf in Old English, as well as a range of anonymous poems including <em>Beowulf</em>, <em>Andreas</em>, <em>Guthlac B</em>, <em>The Seafarer</em>, and <em>The Paris Psalter</em>. The shared roles of memory, imitation, and self-conscious coinage in both Latin and Old English are considered, and it is suggested that traces of a once-thriving oral tradition that was partly shared by literate and illiterate poets alike can still be detected in the surviving written record.</p>",
            "ecompanionhtml": "<html><head></head><body><a name=\"audio1\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/01_Proverb_from_Winfriths_time.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio1\">Proverb from Winfrith’s time</a> <span style=\"font-weight:normal\">(Tangl 1955:283 [no. 146]; Dobbie 1942:57; Stanley 1987:121-23):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio1\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>Oft daedlata&nbsp;&nbsp;&nbsp;&nbsp;domę foręldit,<br>\n   sigisitha gahuem,&nbsp;&nbsp;&nbsp;&nbsp;suuyltit thi ana.<br><br>\n   Often a deed-slack man puts off glory, every chance of winning: for that, he dies alone.</td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n<a name=\"audio2\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/02_Beowulf_953b-955a.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio2\"><em>Beowulf</em></a> <span style=\"font-weight:normal\">(lines 953b-55a; Fulk, Bjork, and Niles 2008:34):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio2\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;“Þu þe self hafast<br>\n   dædum gefremed&nbsp;&nbsp;&nbsp;&nbsp;þæt þin [dom] lyfað<br>\n   awa to aldre.”<br><br>\n   “You yourself have brought about by your deeds that your glory will live forever.”</td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio3\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/03_Beowulf_1386-89.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio3\"><em>Beowulf</em></a> <span style=\"font-weight:normal\">(lines 1386-89; Fulk, Bjork, and Niles 2008:48):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio3\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>“Ure æghwylc sceal&nbsp;&nbsp;&nbsp;&nbsp;ende gebidan<br>\n   worolde lifes;&nbsp;&nbsp;&nbsp;&nbsp;wyrce se þe mote<br>\n   domes ær deaþe;&nbsp;&nbsp;&nbsp;&nbsp;þæt bið drihtguman<br>\n   unlifgendum&nbsp;&nbsp;&nbsp;&nbsp;æfter selest.”<br><br>\n   “Each of us shall experience an end of life in the world: let him who can gain glory before death: that is the best thing afterwards for the noble warrior once he is gone.”</td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio4\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/04_Beowulf_884b-887a.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio4\"><em>Beowulf</em></a> <span style=\"font-weight:normal\">(lines 884b-87a; Fulk, Bjork, and Niles 2008:31-32):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio4\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sigemunde gesprong<br>\n   æfter deaðdæge&nbsp;&nbsp;&nbsp;&nbsp;dom unlytel,<br>\n   syþðan wiges heard&nbsp;&nbsp;&nbsp;&nbsp;wyrm acwealde,<br>\n   hordes hyrde.<br><br>\n   On Sigemund there fell after his death-day no small judgment (or “glory”), after the man keen in battle killed a serpent, the guardian of a hoard.</td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio5\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/05_Beowulf_2855-59.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio5\"><em>Beowulf</em></a> <span style=\"font-weight:normal\">(lines 2855-59; cf. Fulk, Bjork, and Niles 2008:97 and 258):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio5\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>Ne meahte he on eorðan,&nbsp;&nbsp;&nbsp;&nbsp;ðeah he uðe wel,<br>\n   on ðam frumgare&nbsp;&nbsp;&nbsp;&nbsp;feorh gehealdan,<br>\n   ne ðæs wealdendes&nbsp;&nbsp;&nbsp;&nbsp;wiht oncirran;<br>\n   wolde dom godes&nbsp;&nbsp;&nbsp;&nbsp;dædum rædan<br>\n   gumena gehwylcum,&nbsp;&nbsp;&nbsp;&nbsp;swa he nu gen deð.<br><br>\n   He could not, much as he wanted to, keep life on earth in that chieftain, nor change anything of He who Rules, but the judgment (<em>dom</em>) of God would govern every man’s deeds, just as it does now.</td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio6\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/06_Bedes_Death_Song.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio6\"><em>Bede’s Death Song</em></a> <span style=\"font-size:10px;font-weight:normal\">(Dobbie 1942:107; Smith 1968:42; Stanley 1987:131-33):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio6\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>Fore thaem neidfaerae&nbsp;&nbsp;&nbsp;&nbsp;naenig uuiurthit<br>\n   thoncsnotturra,&nbsp;&nbsp;&nbsp;&nbsp;than him tharf sie<br>\n   to ymbhycggannae&nbsp;&nbsp;&nbsp;&nbsp;aer his hiniongae<br>\n   huaet his gastae&nbsp;&nbsp;&nbsp;&nbsp;godaes aeththa yflaes<br>\n   aefter deothdaege&nbsp;&nbsp;&nbsp;&nbsp;doemid uueorthae.<br><br>\n   In the face of that needful journey no one turns out to be wiser in thought than that it is necessary for him to ponder before his journey hence as to what may turn out to be the doom on his soul of good or evil after the day of death.</td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio7\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/07_Cuthbert_on_Bede.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio7\">Cuthberts account of the death of Bede</a> <span style=\"font-weight:normal\">(Plummer 1896:I, clxi):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio7\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>O uere quam beatus uir! Canebat autem sententiam sancti Pauli apostolici dicentis: Horrendum est incidere in manus Dei uiuentis, et multa alia de sancta scriptura, in quibus nos a somno animae exsurgere, praecogitando ultimam horam, admonebat. In nostra quoque lingua, ut erat doctus in nostris carminibus, nonnulla dixit quod ita latine sonat: “ante necessarium exitum prudentior quam opus fuerit nemo existit, ad cogitandum uidelicet antequam hinc proficiscatur anima, quid boni uel mali egerit, qualiter post exitum judicanda fuerit.” Cantebat etiam antiphonas ob nostram consolationem et suam, quarum una est: “O rex gloriae, Domine uirtutum, qui triumphator hodie super omnes celos ascendisti, ne derelinquas nos orphanos, sed mitte promissum Patris in nos, Spiritum ueritatis. Alleluia.”<br><br>\n  O truly what a blessed man! He used to sing the thought of the blessed Apostle Paul saying: “It is a fearful thing to fall into the hands of the living God,” and many other things from Holy Scripture, in which by drawing attention to our final hour he used to urge us to rouse ourselves from the sleep of the soul. Likewise in our own language, since he was learned in our poems, he spoke some words, and it sounds like this in Latin: “Before the necessary exit no one exists who is wiser than that he needs to ponder, before his soul departs hence, what good or evil it has done, how it will be judged after death.” He also used to sing antiphons to console both us and himself, of which one is “O King of Glory, Lord of Might, Who didst this day triumphantly ascend far above all heavens, we beseech Thee leave us not comfortless, but send to us the promise of the Father, even the Spirit of Truth; Hallelujah.”</td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio8\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/08_Solomon_and_Saturn_362-63.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio8\"><em>Solomon and Saturn</em></a> <span style=\"font-weight:normal\">(lines 362-63; Dobbie 1942:44):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"2\"><a href=\"#audio8\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>“Ne mæg mon forildan&nbsp;&nbsp;&nbsp;&nbsp;ænige hwile<br></td>\n  <td align=\"right\" valign=\"top\" rowspan=\"2\">MS <i>forildo</i></td>\n </tr>\n <tr>\n  <td>\n   ðone deoran sið,&nbsp;&nbsp;&nbsp;&nbsp;ac he hine adreogan sceall.”<br><br>\n   “No one can delay for any time the precious journey, but he has to endure it.”<br></td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio9\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/09_Guthlac_B_1325b-1335a.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio9\"><em>Guthlac B</em></a> <span style=\"font-weight:normal\">(lines 1325b-35a; Roberts 1979:122-23 and 179-80):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"4\"><a href=\"#audio9\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Beofode þæt ealond,<br>\n   foldwong onþrong.&nbsp;&nbsp;&nbsp;&nbsp;Ða afyrhted wearð<br>\n   ar, elnes biloren,&nbsp;&nbsp;&nbsp;&nbsp;gewat þa ofestlice<br>\n   beorn unhyðig,&nbsp;&nbsp;&nbsp;&nbsp;þæt he bat gestag,<br>\n   wæghengest wræc,&nbsp;&nbsp;&nbsp;&nbsp;wæterþisa for,<br></td>\n  <td align=\"right\" valign=\"top\">1325</td>\n </tr>\n <tr>\n  <td>snel under sorgum.&nbsp;&nbsp;&nbsp;&nbsp;Swegl hate scan,<br>\n   blac ofer burgsalo.&nbsp;&nbsp;&nbsp;&nbsp;Brimwudu scynde,<br>\n   leoht, lade fus.&nbsp;&nbsp;&nbsp;&nbsp;Lagumearg snyrede,<br>\n   gehlæsted to hyðe,&nbsp;&nbsp;&nbsp;&nbsp;þæt se hærnflota<br>\n   æfter sundplegan&nbsp;&nbsp;&nbsp;&nbsp;sondlond gespearn,<br></td>\n  <td align=\"right\" valign=\"top\">1330</td>\n </tr>\n <tr>\n  <td>grond wið greote.<br><br>\n  That island trembled, the earthly plain burst up. Then the messenger, deprived of courage, became afraid, went in haste, the hapless warrior, so that he embarked on the boat. The wave-stallion stirred, the water-speeder went, swift under sorrows, the hot sky shone, bright over the dwelling-places. The timbered ocean-vessel hastened, light, keen on its course. The flood-horse scudded, loaded to the harbor, so that the wave-floater, after the water-play, trod on the sandy shore, ground against the gravel.</td>\n  <td align=\"right\" valign=\"top\">1335</td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio10\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/10_Andreas_422b-425a.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio10\"><em>Andreas</em></a> <span style=\"font-weight:normal\">(lines 422b-425a; Krapp 1905:17; Brooks 1961:14; parallels are highlighted in <b><i>bold italics</i></b>):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"3\"><a href=\"#audio10\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td colspan=\"2\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mycel is nu gena<br>\n   <b><i>lad</i></b> ofer lagustream,&nbsp;&nbsp;&nbsp;&nbsp;<b><i>land</i></b> swiðe feorr<br>\n   to gesecanne.&nbsp;&nbsp;&nbsp;&nbsp;<b><i>Sund</i></b> is geblonden,<br></td>\n  </tr>\n  <tr>\n   <td><b><i>grund wið greote</i></b>.<br><br></td>\n  <td align=\"right\" valign=\"top\">425</td>\n </tr>\n <tr>\n  <td colspan=\"2\">There is still a great journey over the ocean-stream, land very far to seek. The sea is stirred up, the deep with gravel.</td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio11\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/11_Elene_225-55.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio11\">Cynewulf’s <em>Elene</em></a> <span style=\"font-weight:normal\">(lines 225-55; Krapp 1932:72-73; Cook 1919:10-11; Gradon 1958:36-37; parallels with other Old English poems extant are given in <b><i>bold italics</i></b>):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"13\"><a href=\"#audio11\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>Ongan þa ofstlice&nbsp;&nbsp;&nbsp;&nbsp;eorla mengu<br>\n   <b><i>to flote fysan</i></b>.&nbsp;&nbsp;&nbsp;&nbsp;Fearoðhengestas<br>\n   ymb <b><i>geofenes stæð&nbsp;&nbsp;&nbsp;&nbsp;gearwe stodon</i></b>,<br>\n   <b><i>sælde sæ</i></b>mearas,&nbsp;&nbsp;&nbsp;&nbsp;sunde getenge.<br>\n   Ða wæs <b><i>orcnæwe</i></b>&nbsp;&nbsp;&nbsp;&nbsp;idese siðfæt,</td>\n  <td align=\"right\" valign=\"top\">225</td>\n </tr>\n <tr>\n  <td>siððan wæges helm&nbsp;&nbsp;&nbsp;&nbsp;<b><i>werode gesohte</i></b>.<br>\n   Þær <b><i>wlanc manig</i></b>&nbsp;&nbsp;&nbsp;&nbsp;æt <b><i>Wendelsæ</i></b><br>\n   on stæðe stodon.&nbsp;&nbsp;&nbsp;&nbsp;<b><i>Stundum wræcon</i></b><br>\n   <b><i>ofer mearcpaðu</i></b>,&nbsp;&nbsp;&nbsp;&nbsp;mægen æfter oðrum,<br>\n   ond þa gehlodon&nbsp;&nbsp;&nbsp;&nbsp;hildesercum,<br></td>\n  <td align=\"right\" valign=\"top\">230</td>\n </tr>\n <tr>\n  <td><b><i>bordum ond ordum,&nbsp;&nbsp;&nbsp;&nbsp;byrnwigendum</i></b>,<br>\n   <b><i>werum ond wifum,&nbsp;&nbsp;&nbsp;&nbsp;wæghengestas</i></b>.<br></td>\n  <td align=\"right\" valign=\"top\">235</td>\n </tr>\n <tr>\n  <td>Leton þa ofer fifelwæg&nbsp;&nbsp;&nbsp;&nbsp;famige scriðan<br>\n   <b><i>bronte brim</i></b>þisan.&nbsp;&nbsp;&nbsp;&nbsp;<b><i>Bord</i></b> oft <b><i>onfeng</i></b><br>\n   <b><i>ofer earhgeblond</i></b>&nbsp;&nbsp;&nbsp;&nbsp;yða swengas;<br></td>\n  <td align=\"right\" valign=\"top\"><i>MS altered from</i> fæmige</td>\n </tr>\n <tr>\n  <td>sæ swinsade.&nbsp;&nbsp;&nbsp;&nbsp;<b><i>Ne hyrde ic sið ne ær</i></b><br>\n   <b><i>on egstreame&nbsp;&nbsp;&nbsp;&nbsp;idese lædan</i></b>,<br></td>\n  <td align=\"right\" valign=\"top\">240</td>\n </tr>\n <tr>\n  <td><b><i>on merestræte</i></b>,&nbsp;&nbsp;&nbsp;&nbsp;mægen fægerre.<br>\n   Þær meahte gesion,&nbsp;&nbsp;&nbsp;&nbsp;se ðone <b><i>sið beheold</i></b>,<br></td>\n  <td align=\"right\" valign=\"top\"><i>MS</i> fægrre</td>\n </tr>\n <tr>\n  <td><b><i>brecan ofer bæðweg,&nbsp;&nbsp;&nbsp;&nbsp;brimwudu</i></b> snyrgan<br></td>\n  <td align=\"right\" valign=\"top\"><i>MS</i> spellingum</td>\n </tr>\n <tr>\n  <td>under swellingum,&nbsp;&nbsp;&nbsp;&nbsp;<b><i>sæmearh</i></b> plegean,<br>\n  wadan <b><i>wægflotan</i></b>.&nbsp;&nbsp;&nbsp;&nbsp;Wigan wæron bliðe,<br>\n   <b><i>collenferhðe</i></b>,&nbsp;&nbsp;&nbsp;&nbsp;<b><i>cwen siðes gefeah</i></b>,<br>\n   syþþan to <b><i>hyðe&nbsp;&nbsp;&nbsp;&nbsp;hringedstefnan</i></b><br>\n   <b><i>ofer lagofæsten&nbsp;&nbsp;&nbsp;&nbsp;geliden hæfdon</i></b><br></td>\n  <td align=\"right\" valign=\"top\">245</td>\n </tr>\n <tr>\n  <td><b><i>on Creca land</i></b>.&nbsp;&nbsp;&nbsp;&nbsp;Ceolas leton<br>\n   æt <b><i>sæfearoðe</i></b>,&nbsp;&nbsp;&nbsp;&nbsp;<b><i>sande</i></b> bewrecene,<br></td>\n  <td align=\"right\" valign=\"top\">250</td>\n </tr>\n <tr>\n  <td><b><i>ald yð</i></b>hofu,&nbsp;&nbsp;&nbsp;&nbsp;<b><i>oncrum fæste</i></b><br>\n   on brime <b><i>bidan</i></b>&nbsp;&nbsp;&nbsp;&nbsp;beorna <b><i>geþinges</i></b>,<br></td>\n  <td align=\"right\" valign=\"top\"><i>MS</i> yð liofu</td>\n </tr>\n <tr>\n  <td>hwonne heo sio <b><i>guðcwen&nbsp;&nbsp;&nbsp;&nbsp;gumena þreate</i></b><br></td>\n  <td align=\"right\" valign=\"top\"><i>MS</i> hwone</td>\n </tr>\n <tr>\n  <td><b><i>ofer eastwegas&nbsp;&nbsp;&nbsp;&nbsp;eft gesohte</i></b>.<br><br></td>\n  <td align=\"right\" valign=\"top\">255</td>\n </tr>\n <tr>\n  <td colspan=\"2\">Then a multitude of men quickly began to hasten towards the ocean. Sea-stallions stood poised at the edge of the deep, surge-steeds tethered alongside the sound. The lady’s expedition was widely known, once she sought the wave’s protection with her war-band. There many a proud man stood at the edge, by the Mediterranean. From time to time there traveled over the coast-paths one force after another, and loaded the wave-stallions with battle-shirts, shields and spears, mail-coated fighters, men and women. Then they let the steep ocean-speeders slip, foam-flecked, over the monstrous waves. The ship’s side often caught the billows’ blows across the surge of the deep; the sea resounded. I never heard before or since that a lady led on the streaming ocean, the watery way, a fairer force. There, one who watched that journey, would be able to see forging through the streaming path the timbered ocean-vessels scudding under the swelling sails, the surge-steeds racing, the wave-floaters wading on. The warriors were happy, bold-hearted, the queen delighted in the journey, after the ring-prowed vessels had crossed over the watery fastness to the harbor in the land of the Greeks. They left the keeled boats at the sea’s edge, driven onto the sand, ancient wave-vessels, fast at anchor, to await on the water the outcome for the warriors, when the warlike queen, with her company of men, should seek them out again along roads from the east.</td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio12\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/12_Christ_B_850-66.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio12\">Cynewulf’s <em>Christ B</em></a> <span style=\"font-weight:normal\">(lines 850-66; Krapp and Dobbie 1936:26-27; Cook 1900:33; parallels with other Old English poems extant are given in <b><i>bold italics</i></b>):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"5\"><a href=\"#audio12\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>Nu is þon gelicost&nbsp;&nbsp;&nbsp;&nbsp;swa we on laguflode<br>\n   <b><i>ofer cald wæter&nbsp;&nbsp;&nbsp;&nbsp;ceolum liðan</i></b><br>\n   geond <b><i>sidne sæ</i></b>,&nbsp;&nbsp;&nbsp;&nbsp;<b><i>sundhengestum</i></b>,<br>\n   flodwudu fergen.&nbsp;&nbsp;&nbsp;&nbsp;Is þæt frecne stream<br>\n   yða ofermæta&nbsp;&nbsp;&nbsp;&nbsp;þe we her on lacað<br></td>\n  <td align=\"right\" valign=\"top\">850</td>\n </tr>\n <tr>\n  <td>geond þas wacan woruld,&nbsp;&nbsp;&nbsp;&nbsp;windge holmas<br>\n   <b><i>ofer deop gelad</i></b>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wæs <b><i>se drohtað strong</i></b><br>\n   <b><i>ærþon</i></b> we <b><i>to londe&nbsp;&nbsp;&nbsp;&nbsp;geliden hæfdon</i></b><br>\n   ofer hreone hrycg.&nbsp;&nbsp;&nbsp;&nbsp;Þa us help bicwom,<br>\n   þæt us to <b><i>hælo&nbsp;&nbsp;&nbsp;&nbsp;hyþe gelædde</i></b>,<br></td>\n  <td align=\"right\" valign=\"top\">855</td>\n </tr>\n <tr>\n  <td><b><i>godes gæstsunu</i></b>,&nbsp;&nbsp;&nbsp;&nbsp;<b><i>ond us giefe sealde</i></b><br>\n   þæt we oncnawan magun&nbsp;&nbsp;&nbsp;&nbsp;ofer ceoles bord<br>\n   hwær we <b><i>sælan</i></b> sceolon&nbsp;&nbsp;&nbsp;&nbsp;<b><i>sundhengestas</i></b>,<br>\n   <b><i>ealde yð</i></b>mearas,&nbsp;&nbsp;&nbsp;&nbsp;<b><i>ancrum fæste</i></b>.<br>\n   Utan us to þære hyðe&nbsp;&nbsp;&nbsp;&nbsp;<b><i>hyht staþelian</i></b>,<br></td>\n  <td align=\"right\" valign=\"top\">860</td>\n </tr>\n <tr>\n  <td>ða us gerymde&nbsp;&nbsp;&nbsp;&nbsp;<b><i>rodera waldend</i></b>,<br>\n   <b><i>halge on heahþu</i></b>,&nbsp;&nbsp;&nbsp;&nbsp;þa he <b><i>heofonum astag</i></b>.<br><br></td>\n  <td align=\"right\" valign=\"top\">865</td>\n </tr>\n <tr>\n  <td colspan=\"2\">Now it is most like when on the liquid-flood, over the cold water, throughout the wide sea, we journey in ships, ocean-horses, travel flood-wood. The surge is perilous, waves beyond measure, that we ride on here, throughout this frail world, windy swells over the deep water-way. That plight was severe, before we had crossed to land over the rough ridge. Then help came to us, so that there led us to the safety of harbor God’s spiritual son, and gave us the grace that we might know beyond the ship’s planking where we ought to tether ocean-horses, ancient wave-steeds, secured with anchors. Let us fix our hope on that harbor, holy on high, that the ruler of the firmament opened up for us, when he ascended into the heavens.</td>\n </tr>\n</tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio13\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/13_Gregory_Homily.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio13\">Homily on the Ascension by Gregory the Great</a> <span style=\"font-weight:normal\">(<em>Homeliae in euangelia</em> XXIX, lines 248-52; Étaix 1999:254):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\"><a href=\"#audio13\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>Quamuis adhuc rerum perturbationibus animus fluctuet, iam tamen spei uestrae anchoram in aeternam patriam figite, intentionem mentis in uera luce solidate. Ecce ad caelum ascendisse Dominum audiuimus. Hoc ergo seruemus in meditatione quod credimus.<br><br>\n  Although the soul may still waver from the disturbances of things, nonetheless fasten the anchor of your hope on the eternal homeland, and make firm the aspiration of your heart on the true light. Behold, we have heard that the Lord ascended into heaven; let us keep this in contemplation, as we believe it.</td>\n </tr>\n </tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio14\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/14_Alcuin_1649-58.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio14\">Alcuin</a> <span style=\"font-weight:normal\">(Godman 1982:134):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"4\"><a href=\"#audio14\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td colspan=\"2\">Haec ego nauta rudis teneris congesta carinis,<br></td>\n </tr>\n <tr>\n  <td>Per pelagi fluctus et per vada caeca gubernans,<br>\n   Euboricae ad portum commercia iure reduxi;<br>\n   Utpote quae proprium sibi me nutrivit alumnum,<br>\n   Imbuit et primis utcumque verenter ab annis.<br>\n   Haec idcirco cui propriis de patribus atque<br></td>\n   <td align=\"right\" valign=\"top\">1650</td>\n </tr>\n <tr>\n  <td>Regibus et sanctis ruralia carmina scripsi.<br>\n   Hos pariter sanctos, tetigi quos versibus istis,<br>\n   Deprecor ut nostram mundi de gurgite cymbam<br>\n   Ad portum vitae meritis precibusque gubernent.<br><br></td>\n   <td align=\"right\" valign=\"top\">1655</td>\n </tr>\n <tr>\n  <td colspan=\"2\">I, an inexperienced sailor, steering through the ocean’s waves and dark channels, have rightly brought cargo packed in a vulnerable ship back to the harbor at York, who fostered me as her own product, and reverently raised me from my earliest years, and therefore it is for her that I have written these crude verses concerning her own bishops, kings, and saints. Likewise it is to those saints, whom I have touched on in these verses, that I pray to steer our vessel by their merits and prayers from the whirlpool of the world to the harbor of life.</td>\n </tr>\n </tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio15\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/15_Seafarer_117-24.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio15\"><em>The Seafarer</em></a> <span style=\"font-weight:normal\">(lines 117-24; Krapp and Dobbie 1936:146-47; Gordon 1960:48):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"3\"><a href=\"#audio15\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>Uton we hycgan&nbsp;&nbsp;&nbsp;&nbsp;hwær we ham agen,<br>\n   ond þonne geþencan&nbsp;&nbsp;&nbsp;&nbsp;hu we þider cumen,<br>\n   ond we þonne eac tilien,&nbsp;&nbsp;&nbsp;&nbsp;þæt we to moten<br></td>\n  <td align=\"right\" valign=\"top\">[MS se <em>for second</em> we]</td>\n </tr>\n <tr>\n  <td>in þa ecan&nbsp;&nbsp;&nbsp;&nbsp;eadignesse,<br>\n   þær is lif gelong&nbsp;&nbsp;&nbsp;&nbsp;in lufan dryhtnes,<br>\n   hyht in heofonum.&nbsp;&nbsp;&nbsp;&nbsp;Þæs sy þam halgan þonc,<br>\n   þæt he usic geweorþade,&nbsp;&nbsp;&nbsp;&nbsp;wuldres ealdor,<br>\n   ece dryhten,&nbsp;&nbsp;&nbsp;&nbsp;in ealle tid.&nbsp;&nbsp;Amen.<br><br></td>\n  <td align=\"right\" valign=\"top\">120</td>\n </tr>\n <tr>\n  <td colspan=\"2\">Let us consider where we have a home and then think how we may arrive there, and then we may strive that we are allowed to enter the eternal blessedness, where there is life derived from the love of the Lord, hope in heaven. For that let there be thanks to the holy one, because he has honored us, the prince of glory, eternal lord, forevermore. Amen.</td>\n </tr>\n </tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio16\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/17_PPs_106_22-29.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio16\"><em>Paris Psalter</em></a> <span style=\"font-weight:normal\">(<em>Psalm 106</em> verses 22-29; Krapp 1932:88-89; the parallels with Christ B are given in <b><i>bold italics</i></b>):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60\" valign=\"top\" rowspan=\"11\"><a href=\"#audio16\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td width=\"40\" valign=\"top\">22</td>\n  <td colspan=\"2\">Þa þe sæ seceað,&nbsp;&nbsp;&nbsp;&nbsp;mid scipe liðað,<br>\n   wyrceað weorc mænig&nbsp;&nbsp;&nbsp;&nbsp;on wæterðyrþum.<br></td>\n </tr>\n <tr>\n  <td width=\"40\" valign=\"top\">23</td>\n  <td colspan=\"2\">Hi drihtnes weorc&nbsp;&nbsp;&nbsp;&nbsp;digul gesawon<br>\n   and his wundra wearn&nbsp;&nbsp;&nbsp;&nbsp;on wætergrundum.<br></td>\n </tr>\n <tr>\n  <td width=\"40\" valign=\"top\">24</td>\n  <td>Gif he sylfa cwyð,&nbsp;&nbsp;&nbsp;&nbsp;sona ætstandað<br>\n   ystige gastas&nbsp;&nbsp;&nbsp;&nbsp;ofer egewylmum,<br>\n   beoð heora yþa&nbsp;&nbsp;&nbsp;&nbsp;up astigene.<br></td>\n  <td width=\"120\" align=\"right\" valign=\"top\">[<em>MS</em> æt standeð]</td>\n </tr>\n <tr>\n  <td width=\"40\" valign=\"top\">25</td>\n  <td colspan=\"2\">Þa to <b><i>heofenum</i></b> up&nbsp;&nbsp;&nbsp;&nbsp;<b><i>heah astigað</i></b>,<br>\n   nyþer gefeallað&nbsp;&nbsp;&nbsp;&nbsp;under neowulne grund;<br>\n   oft þa on yfele&nbsp;&nbsp;&nbsp;&nbsp;eft aþindað.<br></td>\n </tr>\n <tr>\n  <td width=\"40\" valign=\"top\" rowspan=\"2\">26</td>\n  <td colspan=\"2\">Gedrefede þa&nbsp;&nbsp;&nbsp;&nbsp;deope syndan,<br>\n   hearde onhrerede&nbsp;&nbsp;&nbsp;&nbsp;her anlicast,<br>\n   hu druncen hwylc&nbsp;&nbsp;&nbsp;&nbsp;gedwæs spyrige;<br></td>\n </tr>\n <tr>\n  <td>ealle heora snytru beoð&nbsp;&nbsp;&nbsp;&nbsp;yfele forglendred.<br></td>\n  <td align=\"right\" valign=\"top\">[<em>MS</em> for gledred]</td>\n </tr>\n <tr>\n  <td width=\"40\" valign=\"top\">27</td>\n  <td colspan=\"2\">Hi on costunge&nbsp;&nbsp;&nbsp;&nbsp;cleopedan to drihtne,<br>\n   and he hi of earfeðum&nbsp;&nbsp;&nbsp;&nbsp;eallum alysde.<br></td>\n </tr>\n <tr>\n  <td width=\"40\" valign=\"top\" rowspan=\"2\">28</td>\n  <td colspan=\"2\">He yste mæg&nbsp;&nbsp;&nbsp;&nbsp;eaðe oncyrran,<br></td>\n </tr>\n <tr>\n  <td>þæt him windes hweoðu&nbsp;&nbsp;&nbsp;&nbsp;weorðeð smylte,<br>\n   and þa yðe&nbsp;&nbsp;&nbsp;&nbsp;eft swygiað,<br>\n   bliþe weorðað,&nbsp;&nbsp;&nbsp;&nbsp;þa þe brimu weþað.<br></td>\n  <td align=\"right\" valign=\"top\">[<em>MS</em> hi]</td>\n </tr>\n <tr>\n  <td width=\"40\" valign=\"top\" rowspan=\"2\">29</td>\n  <td colspan=\"2\">And he hi on <b><i>hælo&nbsp;&nbsp;&nbsp;&nbsp;hyþe gelædde</i></b>,<br>\n   swa he hira willan&nbsp;&nbsp;&nbsp;&nbsp;wyste fyrmest,<br>\n   and he hig of earfoðum&nbsp;&nbsp;&nbsp;&nbsp;eallum alysde.<br><br></td>\n </tr>\n <tr>\n  <td colspan=\"2\">Those who seek the sea, travel on ships, they work many works in the rush of waters.<br>\n   They have seen the secret works of the Lord, and the multitude of his wonders in the watery depths.<br>\n   If he himself speaks, straightaway there stand up stormy spirits over terrifying surges, the waves of which are raised up.<br>\n   Then they rise up high to the heavens, fall back down to the hidden depths; often they fall away into evil.<br>\n   Then they are deeply disturbed, sorely stirred up, here just as any drunken fool would weave his way; all their sense has been evilly swallowed up.<br>\n   In their trials they called out to the Lord, and he set them free from all their hardships.<br>\n   He can easily turn the storm, so that for him the wind’s gusts grow calm, and the waves are silent again; they grow benign, that settle the waters.<br>\n   And he led them to the safety of harbor, just as he knew was their most fervent wish, and he set them free from all their hardships.</td>\n  <td align=\"right\" valign=\"top\"></td>\n </tr>\n </tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n<a name=\"audio17\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/18_PPs_142.9.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio17\"><em>Paris Psalter</em></a> <span style=\"font-weight:normal\">(at Psalm 142.9; O’Neill 1988; the text here follows the Paris Psalter, as in Krapp 1932:140):</span></h4>\n<table width=\"100%\">\n <tbody><tr>\n  <td width=\"60px\" valign=\"top\" rowspan=\"17\"><a href=\"#audio17\"><img src=\"/images/mp3.jpg\" align=\"left\" border=\"0\"></a></td>\n  <td>Do me wegas wise,&nbsp;&nbsp;&nbsp;&nbsp;þæt ic wite gearwe<br>\n   on hwylcne ic gange&nbsp;&nbsp;&nbsp;&nbsp;gleawe mode;<br>\n   nu ic to drihtnes&nbsp;&nbsp;&nbsp;&nbsp;dome wille<br>\n   mine sawle&nbsp;&nbsp;&nbsp;&nbsp;settan geornast.<br><br>\n   Make the paths known to me, so that I know clearly on which I walk with a knowing mind; now I will most eagerly set my soul to the glory of the Lord.</td>\n  <td align=\"right\" valign=\"top\"></td>\n </tr>\n <tr>\n  <td><br>\n   <br>\n   <br>\n   <br>\n   <br></td>\n  <td align=\"right\" valign=\"top\"></td>\n </tr>\n </tbody></table>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\"></body></html>",
            "authors": [
              {
                "name": "Andy Orchard"
              }
            ],
            "files": []
          },
          {
            "id": 9176,
            "title": "The Trumpet and the Wolf: Noises of Battle in Old English Poetry",
            "content": "<p>Descriptions of battle in Old English poetry frequently refer to noise: clattering weapons, howling beasts, and general clamor. Noises are particularly prominent in the type-scenes of the approach to battle and the beasts of battle, and they help evoke the psychological dimension of fighting, especially the mounting excitement and terror before the clash of armies. Further, beast-cries are often depicted as song; this is heavily ironic, but it also refers self-reflexively to the role of the poet. In <em>Exodus</em> a contrast between trumpets and wolves articulates the drama of the Israelites’ struggle of faith. Trumpets are associated with courage, initiative, and communication, wolf-song with terror, paralysis, and loss of speech. The noisiness of the poem also helps to highlight, however, the poet’s mastery of a complex allegory. An exploration of battle noises enables us to relate Old English poetry to Elaine Scarry’s comments on language and war. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Alice Jorgensen"
              }
            ],
            "files": []
          },
          {
            "id": 9174,
            "title": "Mulcaster’s Tyrant Sound",
            "content": "<p>The privileging of writing, often not simply metaphorically, over the “fantasies” of a pristine orality has been the impetus of much recent scholarship built on the foundations laid in Derrida’s <em>Of Grammatology</em> (1967). But such explicit privileging is not new. Richard Mulcaster (1531/32-1611), an Elizabethan schoolmaster and educational reformer, declared in his <em>Positions</em> (1581) that “though writing in order to traine do succeed reading, yet in nature and time it must needes be elder,” a stance Jonathan Goldberg (1990) has perceptively discovered at work even when Mulcaster claims, one year later in the <em>Elementarie</em> (1582), to tell an allegory of sound’s originary position in the history of writing. While it does not seek to restore the primacy of orality in his works, this essay argues first that Mulcaster’s displacement of sound is not as tidy as both he and Goldberg would suggest, and therefore, second, that the consequent perception of “nature”—here that of children—is not as determinative. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Wesley"
              }
            ],
            "files": []
          },
          {
            "id": 9172,
            "title": "Shakespeare’s Sound Government: Sound Defects, Polyglot Sounds, and Sounding Out",
            "content": "<p>The ungovernability of sound in Shakespeare is reflected in the multiple meanings of the word itself, which include the senses of whole or undiseased, and of fathoming or sounding out. This instability is also the source of many aurally generated meanings that have been lost to us through the standardizing editorial traditions of print. This essay sounds out some of these suppressed meanings and locates them within the broader social context of the polyglot communities of early modern London, which were responsible for the macaronic character of Shakespeare’s language. In doing so, Parker shows that puns should not be dismissed as mere verbal quibbles but rather help to reveal the broader cultural associations of the plays. Sound effects are, therefore, integral to meaning, and the aural dimension of Shakespeare’s language is a vital resource both for the editor and for the cultural historian.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Patricia Parker"
              }
            ],
            "files": []
          },
          {
            "id": 9165,
            "title": "On Speech, Print, and New Media: Thomas Nashe and Marshall McLuhan",
            "content": "<p>Marshall McLuhan, pioneer of modern media studies, wrote his Ph.D. thesis on the Elizabethan writer Thomas Nashe and the history of the classical trivium. This essay shows how McLuhan’s early exposure to Nashe influenced his later work on speech, print, and modern media. It argues that Nashe’s use of print to re-create oral conditions and his invention of personae drawn from fairground and marketplace helped shape McLuhan’s response to media and popular culture. Rhodes goes on to argue that it was Nashe’s attack on Ramus, to which McLuhan gave particular emphasis, that was the source of the idea that print promotes linear thinking and closure at the expense of the very different qualities associated with oral culture. He ends by countering some of the charges made against McLuhan that he sentimentalizes the oral by using it to represent an ideal of human wholeness.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Neil Rhodes"
              }
            ],
            "files": [
              {
                "id": 9167,
                "title": "Rhodes-image-1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24ii/full/Rhodes-image-1.jpg",
                "caption": "Thomas Nashe, Strange Newes (1592), sigs. I2v-I3v."
              },
              {
                "id": 9168,
                "title": "Rhodes-image-2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24ii/full/Rhodes-image-2.jpg",
                "caption": "Thomas Nashe, Have With You to Saffron-Walden (1596), sigs. H1v-H3v."
              },
              {
                "id": 9169,
                "title": "Rhodes-image-3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24ii/full/Rhodes-image-3.jpg",
                "caption": "Thomas Nashe, Have With You to Saffron-Walden (1596), sigs. B3v-B4v."
              },
              {
                "id": 9170,
                "title": "Rhodes-image-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24ii/full/Rhodes-image-4.jpg",
                "caption": "Petrus Ramus, Dialecticae libri duo (Cambridge, 1584), p. 54."
              },
              {
                "id": 9171,
                "title": "Rhodes-image-5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24ii/full/Rhodes-image-5.jpg",
                "caption": "Leavis and Thompson, Culture and Environment, pp. 24-25."
              }
            ]
          },
          {
            "id": 9158,
            "title": "James Macpherson’s Ossian Poems, Oral Traditions, and the Invention of Voice",
            "content": "<p>This essay investigates oral culture’s role in the creation of voice in James Macpherson’s Ossian poems. Macpherson insisted that these poems, first published in 1760, were translations of Ossian, an ancient Scottish bard; since their appearance, readers have fiercely debated the authenticity of his claim. This essay seeks to shift the debate by focusing on his poems as a printed object. It argues that Macpherson uses innovative literary strategies and typographical techniques to “invent” oral voices on the page. Through these techniques, Macpherson approximates the sense of connection between singer and listener, and transfers to his text the passion and wildness associated with bardic performance. His poetics addresses readers as participants, and reanimates their experience of the silent reading. This essay ultimately shows that traditions once considered marginal—geographically and politically—to English culture were in fact instrumental to the cohesion of the period’s notion of what poetic voice is and what it can do.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "James Mulholland"
              }
            ],
            "files": [
              {
                "id": 9160,
                "title": "07_Latha_Dhan_Fhinn_Am_Beinn_Iongnaidh_By_Archie_MacDonald.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24ii/full/07_Latha_Dhan_Fhinn_Am_Beinn_Iongnaidh_By_Archie_MacDonald.mp3",
                "caption": "&ldquo;Latha dha&rsquo;n Fhinn am Beinn Iongnaidh.&rdquo; In <em>Music from the Western Isles</em>."
              },
              {
                "id": 9161,
                "title": "James-Macpherson,-Fragment-I.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24ii/full/James-Macpherson,-Fragment-I.jpg",
                "caption": "James Macpherson, Fragment I."
              },
              {
                "id": 9162,
                "title": "1762-Fingal-Frontispiece.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24ii/full/1762-Fingal-Frontispiece.jpg",
                "caption": "1762, Fingal Frontispiece."
              },
              {
                "id": 9163,
                "title": "1787,-Ossian-Singing,-Clemens-Engraving.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24ii/full/1787,-Ossian-Singing,-Clemens-Engraving.jpg",
                "caption": "1787, Ossian Singing."
              },
              {
                "id": 9164,
                "title": "Fingals-Cave,-Library-of-Congress,-JPEG-Version.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24ii/full/Fingals-Cave,-Library-of-Congress,-JPEG-Version.jpg",
                "caption": "Fingal&rsquo;s Cave."
              }
            ]
          },
          {
            "id": 9156,
            "title": "Theorizing Orality and Performance in Literary Anecdote and History: Boswell’s Diaries",
            "content": "<p>This essay analyzes orality and song performance in the eighteenth-century diaries of James Boswell, gentleman Scot and literary figure. Boswell’s engagement of song culture in the course of his activities—literary, political, amorous, familial, domestic, traveling, business, and leisure—demonstrates the eighteenth-century mixing of oral and written and of popular culture and belles lettres, and shows the significance of oral forms and expression among even the most literate and literary people. Theorizing song performance as social interaction shaped by power relations, this essay calls for a widening of the study of orality to include greater consideration of the past, of the informal and quotidian realm, and of the oral and performative dimensions of literate cultures. Boswell’s diaries depict his everyday life from the 1760s to the 1790s in London and Scotland and on various European sojourns. In them he represents himself and others singing and invoking popular songs in complex ways that disclose dynamics of identity formation and relational power.</p>",
            "ecompanionhtml": "<html><head></head><body><a name=\"audio1\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/Youths_the_Season_2.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio1\">“Youth’s the Season Made for Joys,”</a> <span style=\"font-weight:normal\"><em>The Beggar’s Opera</em>:</span></h4>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\"></body></html>",
            "authors": [
              {
                "name": "Dianne Dugaw"
              }
            ],
            "files": []
          },
          {
            "id": 9154,
            "title": "Written Composition and (Mem)oral Decomposition: The Case of “The Suffolk Tragedy”",
            "content": "<p>In seeking to understand the processes and identify the products of oral transmission in early English verbal culture, it can be useful to seek enlightenment in the study of later traditions that are better documented. This paper pursues an ongoing line of research that focuses on English crime ballads, originally published as broadsides, and recovered from folk tradition decades or centuries later. The known relative provenance of the texts allows the impact of oral transmission to be identified exactly and with confidence. In this instance a ballad published in 1828 on the trial of William Corder for the murder of Maria Marten is juxtaposed both with a version recorded from an Oxfordshire singer in 1972, and with a journalistic prose account that was evidently a direct source, enabling an analysis of both the song’s written composition and its “decomposition” in what it is suggested might properly be called “memoral” tradition.</p>",
            "ecompanionhtml": "<html><head></head><body><a name=\"audio1\"></a>\n<audio controls=\"\" preload=\"none\">\n <source src=\"undefined/files/ecompanions/24ii/full/Freda_Palmer_sings_Suffolk_Tragedy.mp3\" type=\"audio/mpeg\">\n</audio>\n<h4><a href=\"#audio1\">Performance of the “The Suffolk Tragedy”</a> <span style=\"font-weight:normal\">by Freda Palmer as recorded by Mike Yates (Hall 1998, item 12):</span></h4>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\"></body></html>",
            "authors": [
              {
                "name": "Tom Pettitt"
              }
            ],
            "files": []
          },
          {
            "id": 9152,
            "title": "Sites of Sound",
            "content": "<p>The unprecedented expansion of cities in nineteenth-century England was not merely a quantitative transformation, but also generated profound changes in the national imaginary and modes of representing the urban order. These changes were evident in a range of discourses, including those of class, ethnicity, gender, place and space, and national identity. A deeper shift underpinning all of these involved the “urban epistemology,” the balance between the city known and represented as something seen and something heard. Increasingly, the city became meaningful as a “sound effect” rather than as a spectacle. The increasing size of cities made it difficult to engage with them literally and conceptually as panoramas, and their sprawling, segmented precincts and infrastructures produced a visual dis-integration. At the same time, the level and character of the acoustic environment emphasized the sonic distinctiveness of the city through the proliferation of technologized sonorities and sonic technologies. This paper exemplifies the literary manifestations of a nineteenth-century shift in the urban information economy from the visual to the auditory.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bruce Johnson"
              }
            ],
            "files": []
          },
          {
            "id": 9150,
            "title": "Joyce’s Noises",
            "content": "<p>James Joyce uses both lexical and nonlexical onomatopoeia extensively in <em>Ulysses</em>; this essay examines some of the ways in which he employs the latter in order to convey noises of many kinds. Nonlexical onomatopoeia is particularly suited to the evocation of noise, though it can only do so in conjunction with shared literary and linguistic conventions. Several of the characters in <em>Ulysses</em> show an interest in the representation of noise in language, but there are many more examples where there is no evidence of mental processes at work. The reader’s pleasure in Joyce’s nonlexical onomatopoeia is very seldom the result of vivid imitation; it is, as these examples testify, Joyce’s play with the workings of the device (and frequently its failure to imitate the nonlinguistic world) that provides enjoyment and some insight into the relation between language and sound.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Derek Attridge"
              }
            ],
            "files": []
          },
          {
            "id": 9148,
            "title": "Where Now the Harp? Listening for the Sounds of Old English Verse, from Beowulf to the Twentieth Century",
            "content": "<p>This essay examines the representation or staging of oral performance and poetic composition within <em>Beowulf</em>, in order to argue that poem thematizes and mythologizes its own origins, and is as much interested in recovering the sounds of oral performances that pre-date its own manuscript inscription as modern Anglo-Saxon scholarship has been. The second half of the essay considers the recovery and reimagining of an Anglo-Saxon “soundscape” in the work of two twentieth-century poets, W. S. Graham and Edwin Morgan. The invocation of this “Saxonesque” patterning of sound invokes or triggers a historically constituted set of associations with the whole body of Old English poetry; that is, an allusion to a corpus, rather than to a specific text, is made through sound patterning.</p>",
            "ecompanionhtml": "<html><head></head><body><a name=\"audio1\"></a>\n  <audio controls=\"\" preload=\"none\">\n    <source src=\"undefined/files/ecompanions/24ii/full/Jones_W_S_Graham_Clip.mp3\" type=\"audio/mpeg\">\n  </audio>\n  <h4><a href=\"#audio1\">“The Voyages of Alfred Wallis”</a> <span style=\"font-weight:normal\">according to the soundscape of Old English verse (87):</span></h4>\n  <table width=\"100%\">\n   <tbody><tr>\n    <td width=\"60\" valign=\"top\"><a href=\"#audio1\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n    <td>Worldhauled, he’s grounded on God’s great bank,<br>\n  Keelheaved to Heaven, waved into boatfilled arms,<br>\n  Falls his homecoming leaving that old sea testament,<br>\n  Watching the restless land sail rigged alongside<br>\n  Townfulls of shallows, gulls on sailing roofs.</td>\n   </tr>\n  </tbody></table>\n  <hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n  <a name=\"audio2\"></a>\n  <audio controls=\"\" preload=\"none\">\n    <source src=\"undefined/files/ecompanions/24ii/full/Jones_Morgan_Clip_1.mp3\" type=\"audio/mpeg\">\n  </audio>\n  <h4><a href=\"#audio2\">“Spacepoem 3: Off Course”</a> <span style=\"font-weight:normal\">(Morgan 1990:268):</span></h4>\n  <table width=\"100%\">\n   <tbody><tr>\n    <td width=\"60\" valign=\"top\"><a href=\"#audio1\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n    <td>the golden flood&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the weightless seat<br>\n  the cabin song&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the pitch black<br>\n  the growing beard&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the floating crumb<br>\n  the shining rendezvous&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the orbit wisecrack<br>\n  the hot spacesuit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the smuggled mouth-organ</td>\n   </tr>\n  </tbody></table>\n  <hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n  <a name=\"audio3\"></a>\n  <audio controls=\"\" preload=\"none\">\n    <source src=\"undefined/files/ecompanions/24ii/full/Jones_Morgan_Clip_2.mp3\" type=\"audio/mpeg\">\n  </audio>\n  <h4><a href=\"#audio3\">“Spacepoem 3: Off Course”</a> <span style=\"font-weight:normal\">(Morgan 1990:269):</span></h4>\n  <table width=\"100%\">\n   <tbody><tr>\n    <td width=\"60\" valign=\"top\"><a href=\"#audio1\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n    <td>the floating lifeline&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the pitch sleep<br>\n  the crawling camera&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the turning silence<br>\n  the space crumb&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the crackling beard<br>\n  the orbit mouth-organ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the floating song.</td>\n   </tr>\n  </tbody></table>\n  <hr width=\"90%\" align=\"center\" shade=\"noshade\">\n\n  <a name=\"audio4\"></a>\n  <audio controls=\"\" preload=\"none\">\n    <source src=\"undefined/files/ecompanions/24ii/full/Jones_Morgan_Clip_3.mp3\" type=\"audio/mpeg\">\n  </audio>\n  <h4><a href=\"#audio4\">“Spacepoem 3: Off Course”</a> <span style=\"font-weight:normal\">(Morgan 1990:269):</span></h4>\n  <table width=\"100%\">\n   <tbody><tr>\n    <td width=\"60\" valign=\"top\"><a href=\"#audio1\" onclick=\"javaScript:showExample(song4,/files/ecompanions/24ii/full/Jones_Morgan_Clip_3.mp3)\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n    <td>the cabin sunrise&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the hot flood<br>\n  the shining spacesuit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the growing moon<br>\n     the crackling somersault&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the smuggled orbit<br>\n     the rough moon&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the visionary rendezvous.</td>\n   </tr>\n  </tbody></table>\n  <hr width=\"90%\" align=\"center\" shade=\"noshade\"></body></html>",
            "authors": [
              {
                "name": "Chris Jones"
              }
            ],
            "files": []
          },
          {
            "id": 9146,
            "title": "Sounding Out Homer: Christopher Logue’s Acoustic Homer",
            "content": "<p>Christopher Logue’s adaptations of Homer’s <em>Iliad</em> go by the collective title of <em>War Music</em>, hinting at the importance of sound for Logue’s conception of the project. This article examines Logue’s Homer in the context of other contemporary translators of Homer who have all sought, in various ways, to produce translations that bring Homer to life. In Logue’s case, performance is a vital part of this enlivening, resulting in a poem with an intrinsic oral dimension, which is reproduced on the page via various typographical cues and reinforced by the poem’s performance history on radio and stage. In this essay the soundscape of Logue’s Homer is illustrated by a detailed case study of a single scene from Book 16 of the <em>Iliad</em>, in which the sound effects present in the Homeric simile are amplified. It considers the paradox of attributing aural fidelity to a free adaptation of Homer, before concluding that Logue’s adaptation can make us more attuned to the acoustic potential of Homer. Conversely, the tension between writing and oral genres inherent in Homeric epic can lead us to a better understanding of the relationship between the written and spoken word in Logue’s Homer.</p>",
            "ecompanionhtml": "<html><head></head><body><a name=\"audio1\"></a>\n  <audio controls=\"\" preload=\"none\">\n    <source src=\"undefined/files/ecompanions/24ii/full/Logueclip.mp3\" type=\"audio/mpeg\">\n  </audio>\n  <h4><a href=\"#audio1\"><em>Iliad</em>, Book 16</a> <span style=\"font-weight:normal\">1981/2001a edition of “Patrocleia” (Logue 2001b, CD 5, track 13):</span></h4>\n  <table width=\"100%\">\n   <tbody><tr>\n    <td width=\"60\" valign=\"top\"><a href=\"#audio1\"><img src=\"/images/mp3.jpg\" align=\"left\"></a></td>\n    <td>Try to recall the pause, thock, pause,<br>\nMade by axe blades as they pace<br>\nEach other through a valuable wood.<br>\nThough the work takes place on the far<br>\nSide of a valley, and the axe strokes are<br>\nMuted by depths of warm, still standing, air,<br>\nThey throb, throb, closely in your ear;<br>\nAnd now and then you catch a phrase<br>\nExchanged between the men who work<br>\nMore than a mile away, with perfect clarity.<br><br>\nLikewise the sound of spear on spear,<br>\nShield against shield, shield against spear<br>\nAround Sarpedon’s body.</td>\n   </tr>\n  </tbody></table>\n  <hr width=\"90%\" align=\"center\" shade=\"noshade\"></body></html>",
            "authors": [
              {
                "name": "Emily Greenwood"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 232,
            "issue_id": "201",
            "name": "Subtitle",
            "value": "Sound Effects"
          },
          {
            "id": 233,
            "issue_id": "201",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24ii/00_24.2cover.pdf"
          },
          {
            "id": 234,
            "issue_id": "201",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 235,
            "issue_id": "201",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24ii/00_24.2fm.pdf"
          },
          {
            "id": 236,
            "issue_id": "201",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24ii/15_24.2bm.pdf"
          },
          {
            "id": 237,
            "issue_id": "201",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24ii/01_24.2.pdf"
          },
          {
            "id": 238,
            "issue_id": "201",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24ii/oraltradition_24_2.zip"
          },
          {
            "id": 239,
            "issue_id": "201",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24ii/24.2complete.pdf"
          },
          {
            "id": 240,
            "issue_id": "201",
            "name": "Volume",
            "value": "24"
          },
          {
            "id": 241,
            "issue_id": "201",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 242,
            "issue_id": "201",
            "name": "Date",
            "value": "2009-10-05"
          }
        ]
      },
      {
        "id": 213,
        "name": "24i",
        "title": "Volume 24, Issue 1",
        "articles": [
          {
            "id": 9225,
            "title": "The Southern Sardinian Tradition of the <em>Mutetu Longu</em>: A Functional Analysis ",
            "content": "<p>The <em>mutetu longu</em> is a traditional genre of Sardinian oral poetry that is still performed in the southern part of Sardinia. According to this tradition, three or more improvisers challenge one another on stage before an audience, singing stanzas accompanied by a guttural male chorus or by guitar. The first part of this article provides a description of the whole phenomenon, including some historical background and a brief explanation of the social context, followed by a discussion of the complexity of the metrical structure that strongly characterizes it. The latter section analyzes the way the inner mechanisms work, evaluating the functional reasons behind the <em>mutetu</em>’s particular metrical structure (which reaches high levels of formal complexity and redundancy), the relevance of memory in the elaboration of the poetic text, and the flow of time and its perception.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Paulu Zedda"
              }
            ],
            "files": [
              {
                "id": 9227,
                "title": "Slide01.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide01.jpg",
                "caption": "Paulu Zedda in a public performance."
              },
              {
                "id": 9228,
                "title": "Slide02.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide02.jpg",
                "caption": "Map of southern Europe delineating Sardinia."
              },
              {
                "id": 9229,
                "title": "Slide03.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide03.jpg",
                "caption": "Thematic map of Sardinia indicating the territories of main diffusion in reference to the four traditional systems."
              },
              {
                "id": 9230,
                "title": "Slide04.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide04.mp4",
                "caption": "The events preceeding the performance at a typical Campidanese gathering (Festival of Santa Barbara in Sinnia)."
              },
              {
                "id": 9231,
                "title": "Slide5-6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide5-6.jpg",
                "caption": "The front and inside pages of a booklet with the manual transcription of a <em>cantada</em> dated 1898."
              },
              {
                "id": 9232,
                "title": "Slide7-8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide7-8.jpg",
                "caption": "Front and inside pages of a printed booklet reporting the transcription of a <em>cantada</em> held in 1947."
              },
              {
                "id": 9233,
                "title": "Slide09.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide09.jpg",
                "caption": "The audience of a <em>cantada</em>. In the front row are some devotees with their tape recorders."
              },
              {
                "id": 9234,
                "title": "Slide10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide10.jpg",
                "caption": "Five <em>cantadoris</em> in 1926."
              },
              {
                "id": 9235,
                "title": "Slide11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide11.jpg",
                "caption": "The audience of a <em>cantada</em>. <em>Basciu e contra</em>, the two-man chorus, is visible on the right."
              },
              {
                "id": 9236,
                "title": "Slide12.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide12.jpg",
                "caption": "Three devotees with their tape recorders in hand."
              },
              {
                "id": 9237,
                "title": "Slide13.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide13.jpg",
                "caption": "Some booklets arranged on a chair for sale during a performance."
              },
              {
                "id": 9238,
                "title": "Slide14.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide14.jpg",
                "caption": "A group of friends sing verses accompanied by a guitar at a familiar get-together."
              },
              {
                "id": 9239,
                "title": "Slide15.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide15.jpg",
                "caption": "Pepuciu Loni, accompanied by a guitarist, improvises his <em>versu</em> on the stage."
              },
              {
                "id": 9240,
                "title": "Slide16.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide16.jpg",
                "caption": "Paulu Zedda, Robertu Zuncheddu, Marcu Melis, and Sarbadori Marras wait for the beginning of the <em>cantada</em>."
              },
              {
                "id": 9241,
                "title": "Slide17.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide17.jpg",
                "caption": "<em>Basciu e contra</em>, Giuanni Cogoni and Austinu Valdes."
              },
              {
                "id": 9242,
                "title": "Slide18.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide18.jpg",
                "caption": "A set table is placed on the stage between the chairs and the microphone."
              },
              {
                "id": 9243,
                "title": "Slide19.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide19.jpg",
                "caption": "A chair is set at the front of the stage behind the microphone. Manueli Saba stands behind it, resting his hands on the back of it as he performs."
              },
              {
                "id": 9244,
                "title": "sabaacordat.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/sabaacordat.mp3",
                "caption": "Manueli Saba tunes the <em>basciu e contra</em>, the throat-singing male chorus."
              },
              {
                "id": 9245,
                "title": "Slide20.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide20.jpg",
                "caption": "Robertu Zuncheddu."
              },
              {
                "id": 9246,
                "title": "Slide21.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide21.jpg",
                "caption": "Sabadori Marras."
              },
              {
                "id": 9247,
                "title": "Slide22.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide22.jpg",
                "caption": "Eliseu Vargiu."
              },
              {
                "id": 9248,
                "title": "Slide23.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide23.jpg",
                "caption": "Antoni Pani."
              },
              {
                "id": 9249,
                "title": "Slide24.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide24.jpg",
                "caption": "Pepuciu Loni."
              },
              {
                "id": 9250,
                "title": "Slide25.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide25.jpg",
                "caption": "Manueli Saba."
              },
              {
                "id": 9251,
                "title": "Slide26.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide26.jpg",
                "caption": "<em>Mutetu</em> in the shape it is performed."
              },
              {
                "id": 9252,
                "title": "Slide27.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide27.jpg",
                "caption": "<em>Antoni Pani."
              },
              {
                "id": 9253,
                "title": "Slide28.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide28.jpg",
                "caption": "<em>Pierpaulu Falqui."
              },
              {
                "id": 9254,
                "title": "Slide29.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide29.jpg",
                "caption": "Omeru Atza."
              },
              {
                "id": 9255,
                "title": "Slide30.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide30.jpg",
                "caption": "Paulu Zedda."
              },
              {
                "id": 9256,
                "title": "Slide31.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide31.jpg",
                "caption": "Pascuali Sanna accompanied on guitar by Antoneddu Pau."
              },
              {
                "id": 9257,
                "title": "Slide32.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide32.jpg",
                "caption": "Paulu Zedda."
              },
              {
                "id": 9258,
                "title": "Slide33.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide33.jpg",
                "caption": "Elaboration of the <em>mutetu</em> phase I."
              },
              {
                "id": 9259,
                "title": "Slide34.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide34.jpg",
                "caption": "Elaboration of the <em>mutetu</em> phase II."
              },
              {
                "id": 9260,
                "title": "Slide35.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide35.jpg",
                "caption": "Elaboration of the <em>mutetu</em> phase III."
              },
              {
                "id": 9261,
                "title": "Slide36.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide36.jpg",
                "caption": "Marcu Melis."
              },
              {
                "id": 9262,
                "title": "Slide37.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide37.jpg",
                "caption": "Marcu Melis."
              },
              {
                "id": 9263,
                "title": "Slide38.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide38.jpg",
                "caption": "Marcu Melis."
              },
              {
                "id": 9264,
                "title": "Slide39.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide39.jpg",
                "caption": "Antoni Pani, Pascuali Sanna, and Manueli Saba."
              },
              {
                "id": 9265,
                "title": "Slide40.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide40.jpg",
                "caption": "Antoneddu Orr&#249;."
              },
              {
                "id": 9266,
                "title": "Slide41.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide41.jpg",
                "caption": "The <em>mutetu</em> as it is performed."
              },
              {
                "id": 9267,
                "title": "Slide42.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide42.jpg",
                "caption": "Dichotomy between the opening section and the couplet."
              },
              {
                "id": 9268,
                "title": "Slide44.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide44.jpg",
                "caption": "Marcu Melis."
              },
              {
                "id": 9269,
                "title": "Slide45.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide45.jpg",
                "caption": "<em>Mutetu</em> in the shape it is performed."
              },
              {
                "id": 9270,
                "title": "Slide43.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide43.jpg",
                "caption": "<em>Cantada</em> for the feast of Santu Bartzolu, in Sinnia."
              },
              {
                "id": 9271,
                "title": "Slide46.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide46.jpg",
                "caption": "Omeru Atza."
              },
              {
                "id": 9272,
                "title": "Slide47.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide47.jpg",
                "caption": "Omeru Atza."
              },
              {
                "id": 9273,
                "title": "Slide48.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Slide48.jpg",
                "caption": "Omeru Atza."
              }
            ]
          },
          {
            "id": 9219,
            "title": "Presentation Formulas in South Slavic Epic Song",
            "content": "<p>South Slavic epic singers make frequent use of direct address to the listener in order to draw attention to important characters or events. These “presentation formulas” fulfill a variety of important discursive functions. Analogously to certain cinematographic techniques (the cuts of montage, panning, or zooming-in of the camera), they shift the listener’s mental vision from one point of reference to another. They can also articulate the structure of a song and create sophisticated aesthetic effects of absence and presence.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "David F. Elmer"
              }
            ],
            "files": [
              {
                "id": 9221,
                "title": "Elmer_Figure_1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Elmer_Figure_1.jpg",
                "caption": "Frequency of presentation formulas."
              },
              {
                "id": 9222,
                "title": "Elmer_Figure_2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Elmer_Figure_2.jpg",
                "caption": "Mi&#263;o Savi&#263; dictating a song to Nikola Vujnovi&#263;."
              },
              {
                "id": 9223,
                "title": "Elmer_Figure_3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Elmer_Figure_3.jpg",
                "caption": "Hajdar Habul dictating a song to Nikola Vujnovi&#263;."
              },
              {
                "id": 9224,
                "title": "Elmer_Figure_4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Elmer_Figure_4.jpg",
                "caption": "&#352;&#263;epan Prka&#269;in dictating a song to Ilija Kutuzov."
              }
            ]
          },
          {
            "id": 9217,
            "title": "The Art of Dueling with Words: Toward a New Understanding of Verbal Duels across the World",
            "content": "<p>A common view of verbal duels is that they are exchanges of insults between young males, and thus a cathartic expression of aggression. Through an examination of verbal duels worldwide, this article demonstrates that this view is overly restrictive. The heterogeneity of forms of verbal duels includes genres performed by both men and women and by children, adults, or the elderly, as well as duels that are staged or improvised, more or less structured, and so on. At the same time, a closer analysis of insults is necessary to understand why, when insults are exchanged, they cannot be immediately connected to aggression. In particular, a distinction must be made between insults and “outrageous speech,” between the target and the recipient of insults, and between verbal duels and ritual insults.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Valentina Pagliai"
              }
            ],
            "files": []
          },
          {
            "id": 9200,
            "title": "Dialogues in Rhyme: The Performative Contexts of Cretan <em>Mantinádes</em>",
            "content": "<p>In Crete, the strong local identity has helped a communicative form of oral poetry, the <em>mantináde</em>, survive to the present day. Emblematic of the performance and composition of these short rhyming couplets is a multilayered dialogism—performative, referential, and textual—that also pervades modern arenas (poems are very popular in the media and even exchanged as text messages). In order to understand how dialogism is embedded in the tradition, this article presents <em>mantinádes</em> as traditionally sung and recited in a wide range of performative discourses.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Venla Sykäri"
              }
            ],
            "files": [
              {
                "id": 9202,
                "title": "Sykari_Photo_1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_1.jpg",
                "caption": "A view of the Milop&#243;tamos valley and the Psilor&#237;tis mountain range."
              },
              {
                "id": 9203,
                "title": "Sykari_Photo_2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_2.jpg",
                "caption": "The workshop of Ant&#243;nis Stefan&#225;kis in Zar&#243;s. Stefan&#225;kis constructs all kinds of stringed instruments. Visible here are two <em>lao&#250;ta</em>, two <em>bouzoukis</em>, and a lao&#250;to (hanging on the wall); situated on the table are half-made <em>lyras</em>."
              },
              {
                "id": 9204,
                "title": "Sykari_Photo_3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_3.jpg",
                "caption": "Ant&#243;nis Stefan&#225;kis still makes reed pipes (called the <em>habi&#243;li</em> or <em>thiamb&#243;li</em>, depending on the region) and he is most likely the last one in Crete to play this pastoral instrument."
              },
              {
                "id": 9205,
                "title": "Sykari_Photo_4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_4.jpg",
                "caption": "A <em>par&#233;a</em> in Ier&#225;petra, with Egglezonikol&#237;s on the violin. The photograph was taken during the first half of the 1960s."
              },
              {
                "id": 9206,
                "title": "Sykari_Photo_5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_5.jpg",
                "caption": "Antonis Papadomanol&#225;kis on the <em>lao&#250;to</em> and Kostas Kirits&#225;kis on the <em>lyra</em> during a performance at the <em>mezedopol&#237;o</em> Mesostrati, Rethymno. May, 2007."
              },
              {
                "id": 9207,
                "title": "Sykari_Photo_6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_6.jpg",
                "caption": "The <em>lyra</em>-player alternates between singing and playing."
              },
              {
                "id": 9208,
                "title": "Sykari_Photo_7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_7.jpg",
                "caption": "The <em>panig&#237;ri</em> of Saint George (the 23rd of April) in As&#237; Goni&#225;, 1997."
              },
              {
                "id": 9209,
                "title": "Sykari_Photo_8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_8.jpg",
                "caption": "A marriage <em>gl&#233;nti</em> in an open-air <em>k&#233;ntro</em> in An&#243;geia, September, 2006."
              },
              {
                "id": 9210,
                "title": "Sykari_Photo_9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_9.jpg",
                "caption": "A local ensemble in a baptismal <em>gl&#233;nti</em> held at the village society&#8217;s festival hall. Milop&#243;tamos, October, 2004."
              },
              {
                "id": 9211,
                "title": "Sykari_Photo_10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_10.jpg",
                "caption": "Parents, godparents, and close relatives begin the first dance to the <em>sirt&#243;s</em> tune."
              },
              {
                "id": 9212,
                "title": "Sykari_Photo_11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_11.jpg",
                "caption": "The <em>par&#233;a</em> of the father and godfather singing <em>mantin&#225;des</em> at the end of the evening."
              },
              {
                "id": 9213,
                "title": "Sykari_Photo_12.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_12.jpg",
                "caption": "A <em>par&#233;a</em> in Ier&#225;petra during the early 1960s. Man&#243;lis Egglez&#225;kis plays the violin."
              },
              {
                "id": 9214,
                "title": "Sykari_Photo_13.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_13.jpg",
                "caption": "A contemporary <em>par&#233;a</em> in Rethymnon on the 3rd of November, 2007. The <em>lyra</em> is played by Ant&#243;nis Pavl&#225;kis, the <em>lao&#250;ta</em> by Gi&#225;nnis Apostol&#225;kis (left) and Gi&#225;nnis Markogi&#225;nnis."
              },
              {
                "id": 9215,
                "title": "Sykari_Photo_14.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Sykari_Photo_14.jpg",
                "caption": "After midnight, a <em>par&#233;a</em> has gathered around the two musicians who began the performance, and new performers now take turns. Mesostr&#225;ti, R&#233;thymnon. May, 2007."
              },
              {
                "id": 9216,
                "title": "Mantinades.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/Mantinades.mp3",
                "caption": "<em>Mantin&#225;des</em> sung to the <em>kontili&#233;s</em> with one <em>lyra</em> and two <em>lao&#250;ta</em>.<br />Performance in Ar&#243;lithos, Ir&#225;klio, 2000.<br />Singers: Nikif&#243;ros Aer&#225;kis, Gial&#225;ftis (Ariste&#237;dis Hair&#233;tis), Pologi&#225;nnis (Gi&#225;nnis Aer&#225;kis)."
              }
            ]
          },
          {
            "id": 9198,
            "title": "Orality and Agency: Reading an Irish Autobiography from the Great Blasket Island",
            "content": "<p><em>The Islandman</em> (1934) by Tomás Ó Criomhthain is the first autobiography to be published by a member of the Irish-speaking community on the Great Blasket Island. This book, whose author was a member of a largely oral community and a participant in many communal oral traditions, has often been read as the work of a passive informant rather than that of an active author. By examining the critical attitudes towards Ó Criomhthain and his work, particularly those that associate orality with passivity and communalism and deny textual authority to members of largely oral communities, this article identifies a crucial tension between opposing readings of this text: reading Ó Criomhthain as a representative type and reading Ó Criomhthain as an author. By developing the latter reading of the text, the reader may recognize the agency of the author-subject of a collaborative autobiography that has its roots in a life lived largely through orality.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Eastlake"
              }
            ],
            "files": []
          },
          {
            "id": 9196,
            "title": "Paradigms of Social Aesthetics in Themne Oral Performance",
            "content": "<p>Based on a long-term study from 1987 to the present and incorporating storytelling apprenticeship, ethnographic fieldwork, and (largely informal) interviews, this paper discusses the dynamic nature of oral art as manifested through Themne storytellers’ efforts to vary the oral performance. It explores the relationship between multimedia resources, both intrinsic and external to the performance environment, as well as artistic variation and social aesthetics, along with the audiences’ appreciation and interpretation of oral performances. It argues that the impulse toward social aesthetics is responsible for the oral artists’ deployment of multimedia resources and their varying of oral narratives during storytelling. Specifically, it examines how sociability, the physical setting of performance, and belief systems or worldview function as paradigms of social aesthetics, focusing on their influence on artistic variation and creativity among the Themne of Sierra Leone.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Amadu Wurie Khan"
              }
            ],
            "files": []
          },
          {
            "id": 9188,
            "title": "The Creation of Basque Oral Poetry by Four American <em>Bertsolaris</em>",
            "content": "<p>This study analyzes the improvisational styles of four Basque <em>bertsolaris</em> who live and perform in the United States, examining a broad range of elements, from the formal aspects of the stanzas to the rhetoric and speech employed by American <em>bertsolaritza</em>. Bearing in mind several rhetorical criteria, I evaluate the quality of this Basque oral genre as performed by these oral poets in a place and context so far removed from the place and context where this phenomenon was born.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Asier Barandiaran"
              }
            ],
            "files": [
              {
                "id": 9190,
                "title": "goni_arriada_kurutxet_goikoetxea.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/goni_arriada_kurutxet_goikoetxea.jpg",
                "caption": "Jesus &#8220;Jess&#8221; Go&#241;i, Martin Goikoetxea, Jesus &#8220;Jess&#8221; Arriada, and Johnny Kurutxet."
              },
              {
                "id": 9191,
                "title": "goikoetxea_goni.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/goikoetxea_goni.jpg",
                "caption": "Martin Goikoetxea (on left) and Jesus Go&#241;i performing in Boise, Idaho."
              },
              {
                "id": 9192,
                "title": "goni_kurutxet_goikoetxea.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/goni_kurutxet_goikoetxea.jpg",
                "caption": "Jesus Go&#241;i (left), Johnny Kurutxet (middle), and Martin Goikoetxea (right)."
              },
              {
                "id": 9193,
                "title": "2003NHFA.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/2003NHFA.mp4",
                "caption": "2003 National Heritage Fellowship Awards, Washington D.C.<br />Bertsolaris: Jesus Arriada, Martin Goikoetxea, Jesus Go&#241;i, Johny Kurutxet<br />Translator: Joxe Mallea-Olaetxe."
              },
              {
                "id": 9194,
                "title": "2003NHFAperformance.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/2003NHFAperformance.mp4",
                "caption": "Performance - 2003 National Heritage Fellowship Awards, Washington D.C.<br />Bertsolaris: Jesus Arriada, Martin Goikoetxea, Jesus Go&#241;i, Johny Kurutxet<br />Translator: Joxe Mallea-Olaetxe."
              },
              {
                "id": 9195,
                "title": "martin_goikoetxea.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/24i/full/martin_goikoetxea.mp3",
                "caption": "Martin Goikoetxea sings about his impressions when he arrived in America.<div style=\"width:100%;height:140px;margin:5px 0px 5px 0px;padding:5px;border:1px solid black;overflow:auto;\">Bertso hauetan esaten dut Amerikara etorri nintzanean zer senti nuen<br />(In these <em>bertsos</em> I tell how I felt when I arrived in America)</p><p>Gazterik atera ni<br />jaiotako herritik<br />ordun partitu nintzan<br />fameli haunditik<br />bihotza penarekin<br />ia lehertu erditik<br />famelia uztean<br />malkoak begitik<br />hainbat pena ez dit eman<br />sekulan Gorritik.<br />(When I was young I went away/ from the town where I was born/ I left <br />a big family/ my heart was sad,/ almost bursting from the middle,/ <br />while I left my family,/ tears were falling from my eyes;/ I have never <br />felt so sad/ about Gorriti.)</p><p>Etxean gurasoak<br />ta hamar senide<br />osaba eta izoa<br />lehengusu ta ahaide<br />herriyan ezagunak<br />hainbeste adiskide<br />utzita Amerikara<br />etorria laide<br />ogia irabaztera<br />horrenbeste bide.<br />(At home we lived,/ ten brothers and sisters/ and our parents/ our <br />uncles,/ our cousins and relatives,/ in town so many acquaintances/ and <br />friends./ I left all of them/ and came to America/ to earn my daily <br />bread,/ traveling so many paths.)</p><p>Pena ateratzea<br />leku xamurretik<br />apartera banua<br />holako lurretik<br />ezin ahaztua daukat<br />halako agurretik<br />ez zaitut ikusiko<br />ama beldurreti<br />gaizkiturik jarria<br />bi astez aurretik.<br />(It is a pity to leave/ from such a sweet place./ I go far away / from <br />this land (I said)/ I cannot forget/ in that farewell/ &ldquo;I won&rsquo;t see you <br />again,&rdquo;/ said my worried mother./ She became sick/ two weeks before I left).</p><p>Han utzia ote nun<br />nik paradisua<br />penak ezin gordea<br />gurasoan sua<br />nunahi zeralakurik<br />eman abisua<br />utzitzen zaitugu<br />semetxo gaixua<br />itsatsia daukat nik<br />orduko musua.<br />(I left there,/ the paradise;/ I could not hide my sorrow/ saying <br />goodbye to my parents&rsquo; fireplace./ &ldquo;Wherever you are,/ let us know how you <br />are./ We let you go,/ our beloved son.&rdquo;/ I have felt/ the kiss they gave <br />me at that time).</p><p>Han utzia ditut nik<br />oroimen biguna<br />jaiotako herria<br />eta ezaguna<br />baitare famelia<br />baitare laguna<br />azken orduak ziran<br />penaz ta iluna<br />ez zait neri ahaztutzen<br />hango azken eguna.<br />(I kept there/ a very warm memory./ It was my birth-town/ the one I knew,/ <br />also my family/ and my friends./ The last hours were sorrowful and dark,/ <br />and I cannot forget/ the last day I spent there).</p><p>Aitak eman azkena<br />nei laguntasuna<br />esanez badizugu<br />guk maitetasuna<br />izan zazu bakea<br />eta osasuna<br />azkenik hau da zuri<br />eskatzen zaizuna<br />ez zazu ahaztu herria<br />ta kristautasuna.<br />(My Daddy gave me/ good advice./ He said to me,/ &ldquo;we love you son,/ I wish <br />you peace and health./ And finally this is what I hope for from you:/ do <br />not forget your people,/ do not leave your Christian life.&rdquo;)</p><p>Etorri ta gauza hau<br />pentsan egon nintzan<br />desertura eraman<br />basapiztin gisan<br />gazte batentzat bizi<br />modua hoi al zan<br />ez gendun denboik pasa<br />herrian ta elizan<br />ordu tristeagorik<br />ez det inoiz izan.<br />(When I arrived (in America)/ I was thinking of this:/ they brought us to <br />a desert/ as if we were beasts./ For a young man / that was the way of <br />living./ We could not spend time in the town/ nor in the church./ I have <br />not had such a sad time/ in my whole life).</p><p>Honera etorrita<br />hasera zorrotza<br />hizketan ere ez jakin<br />hau leku arrotza<br />leku handiak eta<br />ibilera motza<br />Amerikan bizita<br />Euskadin bihotza<br />jaiotako lekuan<br />nahi det heriotza.<br />(After coming here/ the beginning was tough./ We did not know how to speak <br />(English)./ This was a strange land for us;/ the distances were big/ and <br />our foot-travel short./ We lived in America,/ but our hearts were in the <br />Basque Country./ I wish I could die/ in the place where I was born).</div>"
              }
            ]
          },
          {
            "id": 9186,
            "title": "Are We “Misreading” Paul? Oral Phenomena and their Implication for the Exegesis of Paul’s Letters",
            "content": "<p>For a substantial period, orality studies were primarily restricted to non-biblical literature. Ever since the application of orality studies to Gospel criticism, however, this approach has moved from a peripheral to a central place in Gospel studies. However, its application to the letters of Paul has remained quite limited. This article investigates some of the interpretive possibilities that can enhance current methods of reading Paul, taking into account the implications of orality studies in order to hear Paul’s message from the perspective of the original audience.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sam Tsang"
              }
            ],
            "files": []
          },
          {
            "id": 9184,
            "title": "Storytellers of Children’s Literature and their Ideological Construction of the Audience",
            "content": "<p>This paper examines the beliefs and informal theories about their audiences held by a group of twelve Spanish storytellers who perform for children. The interviews were collected as part of a larger study focusing on children’s literature and socialization in urban informal learning contexts. Four aspects or dimensions of storytellers’ ideologies are examined: the role of age as a structuring dimension, the role of children’s upbringing and background in their construction as audiences, their definition of an ideal storytelling context, and the role of audience participation. The study focuses specifically on the effect that contact with the formal educational system can have on the formation of storytellers’ beliefs and finds an especially visible relationship in the first and second dimensions.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bruno Alonso"
              },
              {
                "name": "Marta Morgade"
              },
              {
                "name": "David Poveda"
              }
            ],
            "files": []
          },
          {
            "id": 9182,
            "title": "Performative Loci of the Imperial Edicts in Nara Japan, 749-70",
            "content": "<p>The Japanese Empress Kōken/Shōtoku (r. 749-70) governed not merely from a static setting, a throne in the palace at Nara, but by delivering her edicts in a wide variety of performative loci: in Buddhist temples, mansions of the nobility, and temporary palaces during royal progresses around the realm. This paper analyzes the texts, settings, and audiences of edicts, arguing that eighth-century Japan is an important venue for the study of transitions from orality to literacy.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ross Bender"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 243,
            "issue_id": "213",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 244,
            "issue_id": "213",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24i/00_24.1cover.pdf"
          },
          {
            "id": 245,
            "issue_id": "213",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 246,
            "issue_id": "213",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24i/00_24.1fm.pdf"
          },
          {
            "id": 247,
            "issue_id": "213",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24i/12_24.1bm.pdf"
          },
          {
            "id": 248,
            "issue_id": "213",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24i/01_24.1.pdf"
          },
          {
            "id": 249,
            "issue_id": "213",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24i/oraltradition_24_1.zip"
          },
          {
            "id": 250,
            "issue_id": "213",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/24i/24.1complete.pdf"
          },
          {
            "id": 251,
            "issue_id": "213",
            "name": "Volume",
            "value": "24"
          },
          {
            "id": 252,
            "issue_id": "213",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 253,
            "issue_id": "213",
            "name": "Date",
            "value": "2009-03-26"
          }
        ]
      },
      {
        "id": 224,
        "name": "23ii",
        "title": "Volume 23, Issue 2",
        "articles": [
          {
            "id": 9286,
            "title": "Reading Aloud in Dickens’ Novels",
            "content": "<p>This paper explores the ways in which Dickens’ writing style was influenced by the Victorian practice of reading aloud. Because of this practice he portrayed his characters’ speeches in a notably oral-, aural- and performance-oriented style. To assist readers in reproducing the unique voices of many of his characters, Dickens employs explicit markers such as phonetic spelling, narrative comments, and unusual punctuation. These markers encourage the creation of a vivid oral performance for an audience, and are clear signs that the speeches should be dramatically performed.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Tammy Ho Lai-ming"
              }
            ],
            "files": []
          },
          {
            "id": 9284,
            "title": "(Re)presenting Ourselves: Art, Identity, and Status in U. K. Poetry Slam",
            "content": "<p>The introduction of poetry slam into the U. K. in the mid-1990’s offers an intriguing window into the development of the art worlds and identities of slam participants, as these poets strive to reconcile the introduction of this new, potentially lucrative, foreign art with their own identities as authentic British artists. This paper explores how slam prompts participants in the U. K. performance poetry scene to reassess definitions of performance art, as well as their own identities as artists.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Helen Gregory"
              }
            ],
            "files": []
          },
          {
            "id": 9282,
            "title": "<em>Defendiendo la (Agri)Cultura</em>: Reterritorializing Culture in the Puerto Rican <em>Décima</em>",
            "content": "<p>Through the improvisation of songs based on a sixteenth-century Spanish poetic form in contemporary Puerto Rico, singers symbolically reterritorialize Puerto Rican culture, returning it to its agrarian roots. Cultural reflexivity, born of a series of cultural displacements, has led to both the rigidification of the <em>décima</em> form and an emphasis on the Puerto Rican countryside and rural lifestyle in its lyrics. The article focuses on 58 verses that were improvised for a contest outside of Comerío.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joan Gross"
              }
            ],
            "files": []
          },
          {
            "id": 9280,
            "title": "Karelia: A Place of Memories and Utopias",
            "content": "<p>Karelia is a vast inhabited area in northern Europe. The Winter War of 1939-40 and the so-called Continuation War (1941-44) were both followed by the loss of large areas of Karelia to the Soviet Union in 1944 and the resettlement of 407,000 Karelians in different parts of Finland. This article focuses on how a lost Karelia has been constructed as a utopian place in Finland after the wars of 1939?44. I present an interpretation that is defined on the one hand by the experience of place in Karelian exiles’ reminiscences, and on the other hand by an ideological dream of the restoration of Karelia that persists in Finland.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Outi Fingerroos"
              }
            ],
            "files": []
          },
          {
            "id": 9278,
            "title": "The Anxiety of Writing: A Reading of the Old English <em>Journey Charm</em>",
            "content": "<p>The performative and practical character of the Old English <em>Journey Charm</em> clearly places it within an oral tradition. When the charm is written down, however, the performative power of its speaking subject loses strength. By extension, the entire poem loses its original spell because its performative gestures are replaced by semiotic representations on the page. Arguably aware of this process, the scribe incorporated fears associated with his disempowering activity.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Katrin Rupp"
              }
            ],
            "files": []
          },
          {
            "id": 9276,
            "title": "<em>Thrênoi to Moirológia</em>: Female Voices of Solitude, Resistance, and Solidarity",
            "content": "<p>This essay examines the relationship among gender, lamentation, and death in the Greek lament tradition by comparing ancient Greek literary representations of women in mourning from Euripides’ <em>Suppliants</em> to documented examples of women’s ritual laments for the dead from modern-day rural Greece—specifically Inner Mani and Epiros. The author explores the aesthetics of pain, lament as social protest, and the function of lament for creating solidarity among women mourners.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Andrea Fishman"
              }
            ],
            "files": []
          },
          {
            "id": 9274,
            "title": "The Metamorphosis of an Oral Tradition: Dissonance in the Digital Stories of Aboriginal Peoples in Canada",
            "content": "<p>Digital storytelling, a form of short narrative told in the first person and enhanced by visual text and symbolic imagery, is considered as an extension of the oral tradition of storytelling and represents a continuation of what Aboriginal peoples have been doing from time immemorial. By applying a reflexive ethnographic framework to selected digital stories from the Omushkegowuk area in Ontario, Canada, a critical interpretation emerged—namely, the sense of profound dissonance inherent in Aboriginal peoples’ cultural, civil, symbolic, and spiritual paradigms resulting from the exploitation of Western colonial influence. I also discuss how the elders’ references throughout the stories shape altruistic truths and intrinsic value statements, requiring an imaginative interpretation from those on the cultural periphery of these oral traditions.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lorenzo Cherubini"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 254,
            "issue_id": "224",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 255,
            "issue_id": "224",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23ii/00_23.2cover.pdf"
          },
          {
            "id": 256,
            "issue_id": "224",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 257,
            "issue_id": "224",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23ii/00_23.2fm.pdf"
          },
          {
            "id": 258,
            "issue_id": "224",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23ii/09_23.2bm.pdf"
          },
          {
            "id": 259,
            "issue_id": "224",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23ii/01_23.2.pdf"
          },
          {
            "id": 260,
            "issue_id": "224",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23ii/oraltradition_23_2.zip"
          },
          {
            "id": 261,
            "issue_id": "224",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23ii/23.2complete.pdf"
          },
          {
            "id": 262,
            "issue_id": "224",
            "name": "Volume",
            "value": "23"
          },
          {
            "id": 263,
            "issue_id": "224",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 264,
            "issue_id": "224",
            "name": "Date",
            "value": "2008-10-06"
          }
        ]
      },
      {
        "id": 232,
        "name": "23i",
        "title": "Volume 23, Issue 1",
        "articles": [
          {
            "id": 9304,
            "title": "Of Time, Honor, and Memory: Oral Law in Albania",
            "content": "<p>Former Albanian ambassador to the United States, Tarifa provides a historical account of the role of oral tradition in the transmission of an ancient code of customary law that has shaped and dominated the lives of northern Albanians until well into the mid-twentieth century. This traditional body of law, known as the <em>Kode</em> of Lekë Dukagjini, represents a series of norms, mores, and injunctions that were passed down by word of mouth for generations. The article ultimately seeks to illuminate the role of oral tradition in the formulation and maintenance of law, as well as the specific ways in which Albanian society has been affected—within the greater context of Turkish imperialism—by this ancient and powerful body of knowledge.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Fatos Tarifa"
              }
            ],
            "files": []
          },
          {
            "id": 9302,
            "title": "Narrative Structure and Political Construction: The Epic at Work",
            "content": "<p>This article explores the idea that the construction of meaning lies at the very foundation of oral or “oral-derived” texts, which rely on the totality of tradition to create precise meaning. Through a careful analysis of the epic genre, Goyet asserts that its core function is precisely to allow society as a whole to understand—first dimly and then in more detail—a new political order. The major works treated are the Old French <em>Song of Roland</em>, the ancient Greek <em>Iliad</em>, and the Japanese <em>Hôgen</em> and <em>Heiji monogatari</em>.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Florence Goyet"
              }
            ],
            "files": []
          },
          {
            "id": 9300,
            "title": "The Authority of the Spoken Word: Speech Acts in Mark Twain’s <i>A Connecticut Yankee in King Arthur’s Court</i>",
            "content": "<p>This article begins by noting Mark Twain’s decision to invest in the Paige typesetting machine rather than Alexander Graham Bell’s telephone, and then goes on to examine the main protagonist Hank Morgan’s successful use of both technologies as he faces life-threatening challenges after being transported to King Arthur’s sixth-century England. Morgan also proves a masterful performer of “speech acts,” strategies that effect changes in the people and circumstances that surround him. His “illocutionary” and “perlocutionary” acts enable him time and again to survive to tell his story.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Marie Nelson"
              }
            ],
            "files": []
          },
          {
            "id": 9298,
            "title": "A Spanish Bishop Remembers the Future: Oral Traditions and Purgatory in Julian of Toledo",
            "content": "<p>Stork considers Bishop Julian of Toledo and his seventh-century creation of one of the most influential works on Purgatory, the <em>Prognosticon Futuri Saeculi</em>. Enormously popular in Western Europe, it provided a convenient compendium of source material used by preachers and theologians for centuries. Though the work itself consists of citations from Patristic authors, Julian’s preface and the titles to each section reveal how, in a sort of spiritual “convivium” with his friend Idalius, he initially composed the book from memory. Surviving manuscripts show how the text was studied and transmitted in early monastic school settings.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Nancy P. Stork"
              }
            ],
            "files": []
          },
          {
            "id": 9296,
            "title": "When the Text Becomes the Teller: Apuleius and the <i>Metamorphoses</i>",
            "content": "<p>This article analyzes Apuleius’ <em>Metamorphoses</em> and the ways in which it enacts storytelling on both a contextual and a formal level. Gorman argues that Apuleius creates an alternative countercultural audience for his text, one that resists the Romanization process on the margins of the empire. By questioning the historical moment of production and exploring the political dynamics incorporated into the <em>Metamorphoses</em>, she emphasizes the power of the intermediary genre of storytelling, situated between the highly formal epic and the less rule-bound novel.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Susan Gorman"
              }
            ],
            "files": []
          },
          {
            "id": 9294,
            "title": "From Journalism to Gypsy Folk Song: The Road to Orality of an English Ballad",
            "content": "<p>This essay provides an ingenious analysis of indigenous and enduring folksongs within the Gypsy oral tradition in England. It traces a brief history of scholarship on Gypsy folksong, as well as treats the inherently tricky issue of what a ballad is, before entering into a discussion of the interaction between orally transmitted folksongs and written broadsides. Ultimately, Pettitt illustrates how discernible trends may provide valuable insights into the ways in which oral tradition interacts with and influences verbal performance culture.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Tom Pettitt"
              }
            ],
            "files": []
          },
          {
            "id": 9292,
            "title": "De-composition in Popular Elizabethan Playtexts: A Revalidation of the Multiple Versions of <i>Romeo and Juliet</i> and <i>Hamlet</i>",
            "content": "<p>Petersen addresses the ways in which Shakespeare’s early play-texts have been transmitted from the sixteenth century forward. With specific reference to multiple versions of <em>Hamlet</em> and <em>Romeo and Juliet</em>, she illustrates how the so-called “bad” quartos of those plays show distinct similarities with multiple versions of orally and memorially transmitted folk tales and ballads—in particular, the so-called broadside ballads. Tables and lists of repetitive patterns, formulas, and transpositions throughout the short quarto versions exemplify how the plays may be understood as “de-composing” very similarly to collections of performances from oral tradition.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lene Petersen"
              }
            ],
            "files": []
          },
          {
            "id": 9290,
            "title": "Welsh Saints’ Lives as Legendary Propaganda",
            "content": "<p>Although some medieval legends may have naturally evolved from folklore, others, including the legend of St. David of Wales, are known to have been deliberately constructed as propaganda. This article describes the political situation in Wales in the late eleventh century, and presents the composition of the <em>Life of St David</em> by Rhigyfarch in the light of the western Church’s view of penance and almsgiving. Glimpses are afforded of details from the legend to illustrate its style.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Owain Edwards"
              }
            ],
            "files": []
          },
          {
            "id": 9288,
            "title": "Context and the Emerging Story: Improvised Performance in Oral and Literate Societies",
            "content": "<p>This article derives from recent fieldwork in Bali and offers an alternative methodology that may shed new light on the origins of the Homeric poems. In Bali written texts have co-existed and influenced—and have been influenced by—oral performances for at least a millennium. To discover why and how oral improvised performance persists beyond a few decades or a century, this article draws from current theoretical work in anthropology, archaeology, and performance studies.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thérèse de Vet"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 265,
            "issue_id": "232",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 266,
            "issue_id": "232",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23i/00_23.1cover.pdf"
          },
          {
            "id": 267,
            "issue_id": "232",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 268,
            "issue_id": "232",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23i/00_23.1fm.pdf"
          },
          {
            "id": 269,
            "issue_id": "232",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23i/11_23.1bm.pdf"
          },
          {
            "id": 270,
            "issue_id": "232",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23i/01_23.1editor.pdf"
          },
          {
            "id": 271,
            "issue_id": "232",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23i/oraltradition_23.1.zip"
          },
          {
            "id": 272,
            "issue_id": "232",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/23i/23.1complete.pdf"
          },
          {
            "id": 273,
            "issue_id": "232",
            "name": "Volume",
            "value": "23"
          },
          {
            "id": 274,
            "issue_id": "232",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 275,
            "issue_id": "232",
            "name": "Date",
            "value": "2008-03-28"
          }
        ]
      },
      {
        "id": 241,
        "name": "22ii",
        "title": "Volume 22, Issue 2: Basque Special Issue",
        "articles": [
          {
            "id": 9423,
            "title": "eCompanions",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Miles Foley"
              },
              {
                "name": "Mark Jarvis"
              }
            ],
            "files": []
          },
          {
            "id": 9412,
            "title": "Basque Oral Poetry Championship",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Miles Foley"
              }
            ],
            "files": [
              {
                "id": 9414,
                "title": "UnaivsAndoni2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/UnaivsAndoni2.jpg",
                "caption": "Unai Iturriaga versus Andoni Ega&ntilde;a"
              },
              {
                "id": 9415,
                "title": "ChampionshipLogo.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/ChampionshipLogo.jpg",
                "caption": "The 2005 Championship Logo"
              },
              {
                "id": 9416,
                "title": "8competitors.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/8competitors.jpg",
                "caption": "The 2005 Championship Finalists (left to right):<br />1. Igor Elortza Aranoa<br />2. Andoni Ega&ntilde;a Makazaga (the current and three-time champion)<br />3. Jon Maia Soria<br />4. Amets Arzallus Antia<br />5. Sustrai Colina Acordarrementeria<br />6. Aitor Mendiluze Gonzalez<br />7. Maialen Lujanbio Zugasti (the 2001 runner-up)<br />8. Unai Iturriaga Zugaza-Artaza"
              },
              {
                "id": 9417,
                "title": "MaialenandUnai.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/MaialenandUnai.jpg",
                "caption": "Maialen Lujanbio and Unai Iturriaga"
              },
              {
                "id": 9418,
                "title": "AndonivsJon.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/AndonivsJon.jpg",
                "caption": "Andoni Ega&ntilde;a and Jon Maia"
              },
              {
                "id": 9419,
                "title": "capandtrophy.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/capandtrophy.jpg",
                "caption": "The championship trophy and cap"
              },
              {
                "id": 9420,
                "title": "UnaivsAndoni1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/UnaivsAndoni1.jpg",
                "caption": "Unai Iturriaga versus Andoni Ega&ntilde;a"
              },
              {
                "id": 9421,
                "title": "AndoniwithCap.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/AndoniwithCap.jpg",
                "caption": "The 2005 Champion, Andoni Ega&ntilde;a"
              },
              {
                "id": 9422,
                "title": "AndoniandImolo.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/AndoniandImolo.jpg",
                "caption": "Andoni Ega&ntilde;a with Imanol Lazkano"
              }
            ]
          },
          {
            "id": 9410,
            "title": "A Sociological Study of Sung, Extempore Verse-Making",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Alfredo Retortillo"
              },
              {
                "name": "Xabier Aierdi"
              }
            ],
            "files": []
          },
          {
            "id": 9386,
            "title": "Social Features Of Bertsolaritza",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jon Sarasua"
              }
            ],
            "files": [
              {
                "id": 9388,
                "title": "o233_Ander_Fernandez_de_Betono.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/o233_Ander_Fernandez_de_Betono.jpg",
                "caption": "Audience at a performance"
              },
              {
                "id": 9389,
                "title": "DSC_3332_Alberto_Elosegi.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/DSC_3332_Alberto_Elosegi.jpg",
                "caption": "Audience at Championship of Navarre, 2005"
              },
              {
                "id": 9390,
                "title": "P1060611_Nere_Erkiaga.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/P1060611_Nere_Erkiaga.jpg",
                "caption": "Bertsolaris performing at Mendexa"
              },
              {
                "id": 9391,
                "title": "DSCN0437_EHBE_Bizkaia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/DSCN0437_EHBE_Bizkaia.jpg",
                "caption": "(Working with) Bertsolarism in the classroom"
              },
              {
                "id": 9392,
                "title": "DSCN0429_EHBE_Bizkaia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/DSCN0429_EHBE_Bizkaia.jpg",
                "caption": "(Working with) Bertsolarism in the classroom"
              },
              {
                "id": 9393,
                "title": "DSC_3330_Alberto_Elosegi.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/DSC_3330_Alberto_Elosegi.jpg",
                "caption": "Audience, Championship of Bertsolaris of Navarre"
              },
              {
                "id": 9394,
                "title": "DSC_3336_Alberto_Elosegi.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/DSC_3336_Alberto_Elosegi.jpg",
                "caption": "Audience, Championship of Bertsolaris of Navarre"
              },
              {
                "id": 9395,
                "title": "o306_Edorta_Saenz.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/o306_Edorta_Saenz.jpg",
                "caption": "Audience, Championship of Bertsolaris of Araba"
              },
              {
                "id": 9396,
                "title": "3718_Karlos_Aizpurua.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/3718_Karlos_Aizpurua.jpg",
                "caption": "Audience of children, Aiherra"
              },
              {
                "id": 9397,
                "title": "DSC_0090_Alberto_Elosegi.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/DSC_0090_Alberto_Elosegi.jpg",
                "caption": "N. Gabilondo and A. Sarriegi, Championship of Guipuzcoa"
              },
              {
                "id": 9398,
                "title": "k5_Edurne_Koch.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/k5_Edurne_Koch.jpg",
                "caption": "Audience at a performance"
              },
              {
                "id": 9399,
                "title": "EHBE_helbide_barik.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/EHBE_helbide_barik.jpg",
                "caption": "Logo of the Association Bertsozale Elkartea"
              },
              {
                "id": 9400,
                "title": "IMG_7176_Yudania_Rei.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/IMG_7176_Yudania_Rei.jpg",
                "caption": "Meeting of the General Assembly of the Association"
              },
              {
                "id": 9401,
                "title": "P1000717_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/P1000717_XDZ.jpg",
                "caption": "A day of reflection by the associates"
              },
              {
                "id": 9402,
                "title": "Imagen_054_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/Imagen_054_XDZ.jpg",
                "caption": "A day of reflection by the associates"
              },
              {
                "id": 9403,
                "title": "P1060710_Ihintza_Ostolaza.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/P1060710_Ihintza_Ostolaza.jpg",
                "caption": "Passing the tradition along to the next generation"
              },
              {
                "id": 9404,
                "title": "P1060703_Ihintza_Ostolaza.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/P1060703_Ihintza_Ostolaza.jpg",
                "caption": "Passing the tradition along to the next generation"
              },
              {
                "id": 9405,
                "title": "P1060661_Ihintza_Ostolaza.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/P1060661_Ihintza_Ostolaza.jpg",
                "caption": "Students at a berto-school"
              },
              {
                "id": 9406,
                "title": "082_Alberto_Elosegi.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/082_Alberto_Elosegi.jpg",
                "caption": "Audience at the performance on the Day of the Bertso"
              },
              {
                "id": 9407,
                "title": "o780_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/o780_XDZ.jpg",
                "caption": "Documentary Archive"
              },
              {
                "id": 9408,
                "title": "o781_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/o781_XDZ.jpg",
                "caption": "Documentary Archive"
              },
              {
                "id": 9409,
                "title": "o782_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/o782_XDZ.jpg",
                "caption": "Documentary Archive"
              }
            ]
          },
          {
            "id": 9371,
            "title": "Basque Oral Ecology",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joxerra Garzia"
              }
            ],
            "files": [
              {
                "id": 9373,
                "title": "169_Iturria_Arantzazu_Aldizkaria.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/169_Iturria_Arantzazu_Aldizkaria.jpg",
                "caption": "Basque Language Scholars, 1968"
              },
              {
                "id": 9374,
                "title": "Bertso_papera-2_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/Bertso_papera-2_XDZ.jpg",
                "caption": "A broadside of written bertsos"
              },
              {
                "id": 9375,
                "title": "bertsolariya.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/bertsolariya.jpg",
                "caption": "Written bertsos (bertso jarriak)"
              },
              {
                "id": 9376,
                "title": "p555_Bertsolari_Aldizkaria.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/p555_Bertsolari_Aldizkaria.jpg",
                "caption": "Antonio Zabala, collector and scholar"
              },
              {
                "id": 9377,
                "title": "aia-091_Foto_Cecilio.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/aia-091_Foto_Cecilio.jpg",
                "caption": "Mummery in Zuberoa"
              },
              {
                "id": 9378,
                "title": "maskaradak_h_2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/maskaradak_h_2.jpg",
                "caption": "Mummery in Zuberoa"
              },
              {
                "id": 9379,
                "title": "pastorala.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/pastorala.jpg",
                "caption": "Pastoral in Zuberoa"
              },
              {
                "id": 9380,
                "title": "laboa2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/laboa2.jpg",
                "caption": "Mikel Labo, singer-songwriter"
              },
              {
                "id": 9381,
                "title": "KON_9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/KON_9.jpg",
                "caption": "Mikel Labo, singer-songwriter"
              },
              {
                "id": 9382,
                "title": "c1_santa_eskea_azpillaga_okelar.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/c1_santa_eskea_azpillaga_okelar.mp4",
                "caption": "Serenading Song"
              },
              {
                "id": 9383,
                "title": "c1_santa_eskea_azpeitia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/c1_santa_eskea_azpeitia.mp4",
                "caption": "Serenading song"
              },
              {
                "id": 9384,
                "title": "c2_pastorala.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/c2_pastorala.mp4",
                "caption": "The Pastoral &ldquo;Ramontxu&rdquo;"
              },
              {
                "id": 9385,
                "title": "B1_Baga-biga-higa-Lekeitio2.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/B1_Baga-biga-higa-Lekeitio2.mp3",
                "caption": "Mikel Laboa sings the piece titled: Baga Biga Higa"
              }
            ]
          },
          {
            "id": 9365,
            "title": "Verse Schools",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ainhoa Agirreazaldegi"
              },
              {
                "name": "Arkaitz Goikoetxea"
              }
            ],
            "files": [
              {
                "id": 9367,
                "title": "ag-0072_Argia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/ag-0072_Argia.jpg",
                "caption": "Bersto-school for adults"
              },
              {
                "id": 9368,
                "title": "P1060643_Ihintza_Ostolaza.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/P1060643_Ihintza_Ostolaza.jpg",
                "caption": "Meeting of Bertso-schools"
              },
              {
                "id": 9369,
                "title": "3493_EHBE.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/3493_EHBE.jpg",
                "caption": "Bertso-schools at Summer Camp"
              },
              {
                "id": 9370,
                "title": "o136_Ainhoa_Agirreazaldegi.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/o136_Ainhoa_Agirreazaldegi.jpg",
                "caption": "Bersto-school for adults"
              }
            ]
          },
          {
            "id": 9361,
            "title": "In the School Curriculum",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joxerra Garzia"
              }
            ],
            "files": [
              {
                "id": 9363,
                "title": "Juan_Delmas6.D_EHBE_Bizkaia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/Juan_Delmas6.D_EHBE_Bizkaia.jpg",
                "caption": "Students in guided learning"
              },
              {
                "id": 9364,
                "title": "Gabriel_Aresti_5._EHBE_Bizkaia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/Gabriel_Aresti_5._EHBE_Bizkaia.jpg",
                "caption": "Bertso-school Students"
              }
            ]
          },
          {
            "id": 9332,
            "title": "History of Improvised Bertsolaritza: A Proposal",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joxerra Garzia"
              }
            ],
            "files": [
              {
                "id": 9334,
                "title": "s270_Sendoa.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/s270_Sendoa.jpg",
                "caption": "Etxahun Iruri"
              },
              {
                "id": 9335,
                "title": "bilintx_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/bilintx_XDZ.jpg",
                "caption": "Indalencio Bizkarrondo &ldquo;Bilintx&rdquo;"
              },
              {
                "id": 9336,
                "title": "p508_Sendoa.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/p508_Sendoa.jpg",
                "caption": "Joxe Manuel Lujanbio &ldquo;Txirrita&rdquo;"
              },
              {
                "id": 9337,
                "title": "Bilintx_4_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/Bilintx_4_XDZ.jpg",
                "caption": "Joan Francisco Petriarena &ldquo;Xenpelar&rdquo;"
              },
              {
                "id": 9338,
                "title": "xenpelar.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/xenpelar.jpg",
                "caption": "Joan Francisco Petriarena &ldquo;Xenpelar&rdquo;"
              },
              {
                "id": 9339,
                "title": "p334.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/p334.jpg",
                "caption": "Manuel Lekuona"
              },
              {
                "id": 9340,
                "title": "80.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/80.jpg",
                "caption": "Manuel Lekuona"
              },
              {
                "id": 9341,
                "title": "p501_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/p501_XDZ.jpg",
                "caption": "Homage to Txirrita, Errenteria, 1936"
              },
              {
                "id": 9342,
                "title": "078-Lekuona.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/078-Lekuona.jpg",
                "caption": "Joan Mari Lekuona"
              },
              {
                "id": 9343,
                "title": "p505_Sendoa.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/p505_Sendoa.jpg",
                "caption": "Jose Manuel Lujanbio &ldquo;Txirrita&rdquo;"
              },
              {
                "id": 9344,
                "title": "p534_Sendoa.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/p534_Sendoa.jpg",
                "caption": "Manuel Olaizola &ldquo;Uztapide&rdquo; and Ignacio Eizmendi &ldquo;Basarri&rdquo;"
              },
              {
                "id": 9345,
                "title": "s139_Sendoa.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/s139_Sendoa.jpg",
                "caption": "1960s: Mitxelena, Mattin, Uztapide, Lasarte, Basarri, Xalbador, Lopategi, and Arozamena"
              },
              {
                "id": 9346,
                "title": "li-019_Josu_Argazkina.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/li-019_Josu_Argazkina.jpg",
                "caption": "Prize organized by Radio Loyola, the judges in the first row, 1970"
              },
              {
                "id": 9347,
                "title": "o588_Sendoa.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/o588_Sendoa.jpg",
                "caption": "1960s: &hellip;Mattin, Xalbador, Mugartegi, Azpillaga, Mitxelena, and Uztapide"
              },
              {
                "id": 9348,
                "title": "s392.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/s392.jpg",
                "caption": "1980s: Joxe Miel Iztueta &ldquo;Lazkao txiki&rdquo;"
              },
              {
                "id": 9349,
                "title": "s269_Sendoa.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/s269_Sendoa.jpg",
                "caption": "Mattin Treku &ldquo;Mattin&rdquo; and Mixel Aire &ldquo;Xalbador&rdquo;"
              },
              {
                "id": 9350,
                "title": "o592.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/o592.jpg",
                "caption": "Jon Lopategi and Jon Azpillaga, 1965"
              },
              {
                "id": 9351,
                "title": "be92-4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/be92-4.jpg",
                "caption": "Xabier Amuriza"
              },
              {
                "id": 9352,
                "title": "DSCF1652_Alberto_Elosegi.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/DSCF1652_Alberto_Elosegi.jpg",
                "caption": "Andoni Ega&ntilde;a, champion, 2005"
              },
              {
                "id": 9353,
                "title": "8_Juantxo_Egana.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/8_Juantxo_Egana.jpg",
                "caption": "Txapelketa Nagusia, 2001"
              },
              {
                "id": 9354,
                "title": "ag-0013_Argia.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/ag-0013_Argia.jpg",
                "caption": "Final round of the championship, 1980: Larra&tilde;aga and Etxeberria singing; Azpillaga, Amuriza, Garmendia, Gorrotxategi and Xanpun seated"
              },
              {
                "id": 9355,
                "title": "c3_amuriza_iturriaga_lujanbio.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/c3_amuriza_iturriaga_lujanbio.mp4",
                "caption": "X. Amuriza, U. Iturriaga, and M. Lujanbio, improvised after dinner performance"
              },
              {
                "id": 9356,
                "title": "c6-1_basarri_uztapide.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/c6-1_basarri_uztapide.mp4",
                "caption": "Uztapide/Basarri, a performance in Segura"
              },
              {
                "id": 9357,
                "title": "c6_egana_enbeita.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/c6_egana_enbeita.mp4",
                "caption": "Ega&ntilde;a and J. Enbeita, National championshipi of berstolaris"
              },
              {
                "id": 9358,
                "title": "c6-2_azpillaga_lopategi.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/c6-2_azpillaga_lopategi.mp4",
                "caption": "Lopategi/Azpillaga, a performance in Mutriku"
              },
              {
                "id": 9359,
                "title": "c6-3_egana_lizaso.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/c6-3_egana_lizaso.mp4",
                "caption": "Lizaso/Ega&ntilde;a, after dinner performance in Aizarnazabal"
              },
              {
                "id": 9360,
                "title": "c6-4_elortza_iturriaga.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/c6-4_elortza_iturriaga.mp4",
                "caption": "Iturriaga/Elortza, after dinner performance in Bilbao"
              }
            ]
          },
          {
            "id": 9314,
            "title": "The Process of Creating Improvised Bertsos",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Andoni Egaña"
              }
            ],
            "files": [
              {
                "id": 9316,
                "title": "o411_Gari_Garaialde.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/o411_Gari_Garaialde.jpg",
                "caption": "Final round of the championship of Navarre, 2003"
              },
              {
                "id": 9317,
                "title": "4038_Gorka_Salmeron.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/4038_Gorka_Salmeron.jpg",
                "caption": "Audience at the final round of the national championship, 2001"
              },
              {
                "id": 9318,
                "title": "P1040024_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/P1040024_XDZ.jpg",
                "caption": "After dinner performance, 2005"
              },
              {
                "id": 9319,
                "title": "F270_Iturria_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/F270_Iturria_XDZ.jpg",
                "caption": "After dinner performance, Hernani, 1999"
              },
              {
                "id": 9320,
                "title": "3756.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/3756.jpg",
                "caption": "After dinner performance"
              },
              {
                "id": 9321,
                "title": "P1070768_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/P1070768_XDZ.jpg",
                "caption": "Performance in the public square, Lesaka 2006"
              },
              {
                "id": 9322,
                "title": "Plaza_bete-bete_XDZ.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/Plaza_bete-bete_XDZ.jpg",
                "caption": "Performance in the public square"
              },
              {
                "id": 9323,
                "title": "Korrika_Xabier_Azkue.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/Korrika_Xabier_Azkue.jpg",
                "caption": "Young bertsolaris singing at a cultural event, Zumaia"
              },
              {
                "id": 9324,
                "title": "H33_Edurne_Koch.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/H33_Edurne_Koch.jpg",
                "caption": "Andoni Ega&ntilde;a at a wedding, 1998"
              },
              {
                "id": 9325,
                "title": "s31_Sendoa.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/s31_Sendoa.jpg",
                "caption": "The bertsolari Lasarte singing petitioning verses in Asteasu, 1967"
              },
              {
                "id": 9326,
                "title": "20051218-mendiluze--DSCF1091.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/20051218-mendiluze--DSCF1091.jpg",
                "caption": "Aitor Mendiluze"
              },
              {
                "id": 9327,
                "title": "3917.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/3917.jpg",
                "caption": "Maialen Lujanbio, After dinner performance"
              },
              {
                "id": 9328,
                "title": "p452.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/p452.jpg",
                "caption": "Anjel Mari Pe&ntilde;agarikano, Andoni Ega&ntilde;a at a wedding, 1998"
              },
              {
                "id": 9329,
                "title": "3295b.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/3295b.jpg",
                "caption": "Joxe Agirre, Singing at the homage to Basari, 1999"
              },
              {
                "id": 9330,
                "title": "c5_mendiluze.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/c5_mendiluze.mp4",
                "caption": "A. Mendiluze singing about designer drugs, 1997"
              },
              {
                "id": 9331,
                "title": "B2_19981222arrasate.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/22ii/full/B2_19981222arrasate.mp3",
                "caption": "A debate about wind power between M. Lujanbio and A. M. Pe&ntilde;agarikano"
              }
            ]
          },
          {
            "id": 9312,
            "title": "Toward True Diversity in Frame of Reference",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joxerra Garzia"
              }
            ],
            "files": []
          },
          {
            "id": 9310,
            "title": "Interview with Joxe Agirre Esnal",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Laxaro Azkune"
              }
            ],
            "files": []
          },
          {
            "id": 9308,
            "title": "Interview with Andoni Egaña Makazaga",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Josu Goikoetxea"
              }
            ],
            "files": []
          },
          {
            "id": 9306,
            "title": "Interview with Maialen Lujanbio",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Estitxu Eizagirre"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 276,
            "issue_id": "241",
            "name": "Subtitle",
            "value": "Basque Special Issue"
          },
          {
            "id": 277,
            "issue_id": "241",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22ii/0_cover_22_2.pdf"
          },
          {
            "id": 278,
            "issue_id": "241",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 279,
            "issue_id": "241",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22ii/1a_front_matter_22_2.pdf"
          },
          {
            "id": 280,
            "issue_id": "241",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22ii/14_back_matter_22_2.pdf"
          },
          {
            "id": 281,
            "issue_id": "241",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22ii/1e_editors_column_22_2.pdf"
          },
          {
            "id": 282,
            "issue_id": "241",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22ii/oraltradition_22_2.zip"
          },
          {
            "id": 283,
            "issue_id": "241",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22ii/22_2complete.pdf"
          },
          {
            "id": 284,
            "issue_id": "241",
            "name": "Volume",
            "value": "22"
          },
          {
            "id": 285,
            "issue_id": "241",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 286,
            "issue_id": "241",
            "name": "Date",
            "value": "2007-10-27"
          }
        ]
      },
      {
        "id": 270,
        "name": "22i",
        "title": "Volume 22, Issue 1: Bob Dylan’s Performance Artistry",
        "articles": [
          {
            "id": 9475,
            "title": "Introduction",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Catharine Mason"
              },
              {
                "name": "Richard Thomas"
              }
            ],
            "files": []
          },
          {
            "id": 9473,
            "title": "Dylan and the Nobel",
            "content": "<p>This article argues for Bob Dylan’s nomination for the Nobel Prize in Literature. Traditional criteria for the award include outstanding idealism and work that benefits mankind, criteria that are easily met in Dylan’s case, given his activism in early 1960s civil rights, antiwar compositions, and beyond. Yet questions have been raised concerning Dylan’s eligibility for such an award. Can a literary prize go to a writer of song? Past Nobels in Literature display a breadth that admits such a lineage, however, and the connections between music and poetry have been noted by Laureates Rabindranath Tagore and W. B. Yeats. The Literature Prize has gone to historians and philosophers as well. Moreover, a close examination of selections from Dylan’s lyrics shows that as texts on the page, they compare favorably with literary masters such as Chekhov, Faulkner, and Rimbaud; that they resist many scholarly attempts at schematization testifies to their power as poetry. In terms of global appreciation, Dylan’s work has not merely survived but triumphed. From whatever standpoint Dylan’s work is viewed, this article argues that it deserves consideration for literature’s highest prize.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Gordon Ball"
              }
            ],
            "files": []
          },
          {
            "id": 9471,
            "title": "The Streets of Rome: The Classical Dylan",
            "content": "<p>One of the preoccupations of Dylan scholarship has had to do with his intertexts, where his songs come from, and what meanings they derive from their places of origin, be they textual or musical, secular or religious, ancient or modern. In this article, Thomas explores Dylan’s contact with the ancient worlds of Greece and Rome, evident in particular in the Dylan of the last decade—that is, on the last three albums and in his “autobiography,” <em>Chronicles: Volume One</em>. This article counters the attacks of those who cannot distinguish plagiarism—a charge also leveled against the poet Virgil in antiquity—from creative reuse. Thomas discusses Dylan’s reperformance and lyrical renovation and variation from the perspective of the Homeric rhapsode, who like Dylan himself varies his initial text in performance, so creating constant shifts in meaning and emphasis.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Richard Thomas"
              }
            ],
            "files": []
          },
          {
            "id": 9469,
            "title": "A Face like a Mask and a Voice that Croaks: An Integrated Poetics of Bob Dylan’s Voice, Personae, and Lyrics",
            "content": "<p>This article seeks to examine the literary pleasures derived from Bob Dylan’s songs, paying special attention to how Dylan’s poetical texts are performed and rhythmically rewritten by his voice, as well as the ways in which Dylan uses the songs to “write himself” through the creation of numerous and competing <em>personae</em>. Close reading of the lyrics, this article argues, must therefore be supplemented by a “poetics of the voice” and a detailed analysis of the theatricality of his “games of masks.” While a stylistic approach to the lyrics reveals a thrust towards <em>writerly</em> openness and new poetical idioms that fuse oral traditions with high poetry, the aesthetic and semantic uses Dylan makes of his voice are equally sophisticated. A study of Dylan’s “masks” will show that the artist uses archetypal poetic identities (prophet, trickster, man of sorrow, and so on) as fictional figurations of himself offered to the audience.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Christophe Lebold"
              }
            ],
            "files": []
          },
          {
            "id": 9467,
            "title": "Living, Breathing Songs: Singing Along with Bob Dylan",
            "content": "<p>Taking issue with approaches to Bob Dylan’s art that are preoccupied with his lyrics, this article suggests a route into thinking about his music by focusing on how Dylan’s vocal melodies work at the intersection of speech and singing. Drawing on Gino Stefani’s work on popular melodies, this article explores this issue through a discussion of how people sing along with Dylan’s songs at concerts. The discussion focuses on the song “It Ain’t Me Babe,” and examines more general points about the ways in which Dylan’s melodies connect with the everyday lives of his listeners.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Keith Negus"
              }
            ],
            "files": []
          },
          {
            "id": 9465,
            "title": "Vocal Performance and Speech Intonation: Bob Dylan’s “Like a Rolling Stone”",
            "content": "<p>This article proposes a linguistic analysis of a recorded performance of a single verse of one of Dylan’s most popular songs—the originally released studio recording of “Like A Rolling Stone”—and describes more specifically the ways in which intonation relates to lyrics and performance. This analysis is used as source material for a close reading of the semantic, affective, and “playful” meanings of the performance, and is compared with some published accounts of the song’s reception. The author has drawn on the linguistic methodology formulated by Michael Halliday, who has found speech intonation (which includes pitch movement, timbre, syllabic rhythm, and loudness) to be an integral part of English grammar and crucial to the transmission of certain kinds of meaning. Speech intonation is a deeply-rooted and powerfully meaningful aspect of human communication. This article argues that is plausible that a system so powerful in speech might have some bearing on the communication of meaning in sung performance.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Michael Daley"
              }
            ],
            "files": []
          },
          {
            "id": 9463,
            "title": "Never Quite Sung in this Fashion Before: Bob Dylan’s “Man of Constant Sorrow”",
            "content": "<p>Many of Bob Dylan’s early compositions were drawn from pre-existing musical sources. This article traces the song “I Am a Man of Constant Sorrow,” recorded by Dylan on his first LP, from its composition in the 1910s to the point at which the song entered Dylan’s repertoire in 1961. This article proposes detailed transcription and comparison as a way for scholars to address issues of song transmission and dissemination, as well as intellectual property rights.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Todd Harvey"
              }
            ],
            "files": []
          },
          {
            "id": 9461,
            "title": "“‘Sólo Soy Un Guitarrista’: Bob Dylan in the Spanish-Speaking World—Influences, Parallels, Reception, and Translation”",
            "content": "<p>This article examines key aspects of the relationship between the work of Bob Dylan and the cultures of Spain and Spanish-speaking Latin America, including the Spanish/Latin American presence in Dylan’s songs and prose texts; the reception of Dylan’s work by Spanish-speaking critics and intellectuals; influences and parallels between Dylan and Spanish/Latin American musicians and writers, notably Federico García Lorca; and the translation of Dylan into Spanish. Dylan’s work is seen as a hybrid cultural phenomenon, generating connections between high-cultural and popular elements. Its two-way relationship with Hispanophone culture is seen as an interesting case of bridge-building between cultural systems.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Christopher Rollason"
              }
            ],
            "files": []
          },
          {
            "id": 9459,
            "title": "Amerindian Roots of Bob Dylan’s Poetry",
            "content": "<p>In an application of both the findings and the methods of structural anthropology as laid out in the works of Claude Lévi-Strauss and pursued in the works of Désveaux, this article seeks to account for traces of Amerindian folklore as source material in the writings of Bob Dylan. These influences are discussed in terms of some thematic and poetic images specific to Amerindian traditions, a conception of relationships between the sexes, and an eschatological design in which paradise is not situated in a differentiated time but in a parallel space—an outlook similar to many Amerindian worldviews. These influences are also interpreted with respect to style, borrowing the notion of cognitive style as defined by Elaine Jahner. As a conclusion, the author poses the question of transmission, considering emanations from learned culture as well as those from popular culture as possible channels of influences on Bob Dylan’s writings.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Emmanuel Désveaux"
              }
            ],
            "files": []
          },
          {
            "id": 9457,
            "title": "Bob Dylan, the Ordinary Star",
            "content": "<p>This article provides a study of Bob Dylan’s public image as a “star” performer and examines what Dylan represented for his audiences with respect to the challenges of 1960s counterculture. This study focuses primarily on the image of Dylan in D. A. Pennebaker’s documentary film Don’t Look Back, which portrays Dylan when the star is only 23. A study of Pennebaker’s film shows how the filmmaker captures the paradox of Dylan’s star popularity in his refusal to portray the star, not only as a personal struggle, but as a cultural contradiction. The author further identifies a formal link between Dylan’s portrayal of the ordinary star and the minimalist aesthetic of cinéma vérité.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Laure Bouquerel"
              }
            ],
            "files": []
          },
          {
            "id": 9455,
            "title": "A Semantic and Syntactic Journey Through the Dylan Corpus",
            "content": "<p>This article is a corpus-based exploration of Bob Dylan’s lexicon and syntactic structures based on an examination of the 400-plus songs published to date. By means of a simple concordance program, this article analyzes vocabulary frequencies for each main word class (nouns, verbs, and adjectives). A look at modal auxiliaries illustrate a definite mismatch between large corpora and our corpus, which the author interprets as a deliberate will of Dylan’s to avoid being viewed as a “prophet.” A short exploration of complex noun phrases (the 3 patterns N Ø N, N’s N, and N of N) sheds new light on the specific vehicles used by Dylan to convey his flamboyant imagery. Last but not least, a look at some syntactic idiosyncrasies (especially the use of “do” and the “a-Ving” form) shows a peculiar use of archaic syntax.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jean-Charles Khalifa"
              }
            ],
            "files": []
          },
          {
            "id": 9453,
            "title": "Nothing’s Been Changed, Except the Words: Some Faithful Attempts at Covering Bob Dylan Songs in French",
            "content": "<p>This article deals with the French translation and performed covers of Bob Dylan songs, with a view to setting forth the general rules of adapting songs into another language. Using a large number of examples, this article first explains the difference between covering and translating, which is first and foremost a matter of meter and scansion. The article then explores two approaches to “faithfulness”: one can either be faithful to the sound of the initial <em>words</em> or to the <em>meaning</em>. What is at stake here is the concept of distance: we need intercessors, but still want them removed from the picture. Rather than creators, the singers covering foreign songs have to be considered as transmitters. That is why most of those efforts, whatever their commercial success, often fail to impress as genuine works of art.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Nicolas Froeliger"
              }
            ],
            "files": []
          },
          {
            "id": 9451,
            "title": "“The Low Hum in Syllables and Meters”: Blues Poetics in Bob Dylan’s Verbal Art",
            "content": "<p>Applying the linguistic category of style as put forth by Dell Hymes, this article seeks to identify the poetic devices borrowed by Bob Dylan from lyrics of traditional blues masters. The author highlights rhetorical form as it is connected to personal and cultural meaning in Blind Willie McTell’s “Broke Down Engine,” as recorded both by McTell and later by Dylan. Among the stylistic operations examined, we find a description of the phenomenon of songfulness as defined by Lawrence Kramer, metaphoric designs of Southern American English, expressive grammar deviations, and the syntactic formulation the author defines as “binary blues clauses,” commonly used in the AAB blues structure. The study is illustrated with a close analysis of language and genre use in Dylan’s “10,000 Men.”</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Catharine Mason"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 375,
            "issue_id": "270",
            "name": "Subtitle",
            "value": "Bob Dylan’s Performance Artistry"
          },
          {
            "id": 376,
            "issue_id": "270",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22i/22_1_cover.pdf"
          },
          {
            "id": 377,
            "issue_id": "270",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22i/22_1_cover.jpg"
          },
          {
            "id": 378,
            "issue_id": "270",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22i/22_1_front_matter.pdf"
          },
          {
            "id": 379,
            "issue_id": "270",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22i/22_1_back_matter.pdf"
          },
          {
            "id": 380,
            "issue_id": "270",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22i/editors_column_22_1.pdf"
          },
          {
            "id": 381,
            "issue_id": "270",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22i/oraltradition_22_1.zip"
          },
          {
            "id": 382,
            "issue_id": "270",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/22i/22_1_complete.pdf"
          },
          {
            "id": 383,
            "issue_id": "270",
            "name": "Volume",
            "value": "22"
          },
          {
            "id": 384,
            "issue_id": "270",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 385,
            "issue_id": "270",
            "name": "Date",
            "value": "2007-03-08"
          }
        ]
      },
      {
        "id": 283,
        "name": "21ii",
        "title": "Volume 21, Issue 2",
        "articles": [
          {
            "id": 9485,
            "title": "Throwing Stones in Jest: Kasena Women’s “Proverbial” Revolt",
            "content": "<p>This study is an attempt to document and critically examine what I have called the “proverbial revolt” of Kasena women from northern Ghana, a phenomenon that constitutes, and is constituted by, a subversive and resistive use of a misogynistic oral tradition by traditional women. Kasena women take advantage of a socially sanctioned medium––the joking relationship between a Kasena woman and her husband’s siblings or kin of the same generation––to subvert and contradict Kasem proverbs and the conservative, sexualized local ideology of power therein. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Helen Yitah"
              }
            ],
            "files": []
          },
          {
            "id": 9483,
            "title": "The Devil’s Colors: A Comparative Study of French and Nigerian Folktales",
            "content": "<p>This study, largely based on five separate published collections, compares French and Nigerian folktales––focusing mainly on French Dauphiné and Nigerian Igboland––to consider the role color plays in encounters with supernatural characters from diverse color backgrounds. A study in black, white/red, and green, the paper compares the naming of colors in the two languages and illustrates their usage as a tool to communicate color-coded values. Nigeria’s history, religious beliefs, and language development offer additional clues to what at first appears to be fundamental differences in cultural approach. Attempting to trace the roots of this color-coding, the study also considers the impact of colonization on oral literature and traditional art forms.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Françoise Ugochukwu"
              }
            ],
            "files": []
          },
          {
            "id": 9481,
            "title": "A Meme-Based Approach to Oral Traditional Theory",
            "content": "<p>A meme is the simplest unit of cultural replication. This paper adapts meme theory to explain the workings of several aspects of oral traditions––traditional referentiality, anaphora, and the use of repeated metrical patterns.  All three of these phenomena can be explained by operations of repetition and pattern-recognition. This paper ultimately illustrates that the development of meme theory is an important first step towards a wholly materialist cultural poetics.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Michael D. C. Drout"
              }
            ],
            "files": []
          },
          {
            "id": 9479,
            "title": "Keeping the Word: On Orality and Literacy (With a Sideways Glance at Navajo)",
            "content": "<p>Taking Walter Ong’s work as a starting point, this paper begins with a brief survey of the literature on orality and literacy and goes on to analyze a number of Ong’s assertions within the framework of Navajo interactions with orality and literacy, thus illustrating that certain foundational concepts––such as that of “kept language”––need to be reconsidered. The paper pays special attention to the emergence of Navajo poetry; rather than “orality” and “literacy” being singular concepts, this analysis argues that these concepts must be understood within the cultural practices and linguistic ideologies from which they emerge. </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Anthony K. Webster"
              }
            ],
            "files": []
          },
          {
            "id": 9477,
            "title": "“Culture Education” and the Challenge of Globalization in Modern Nigeria",
            "content": "<p>This paper has to do with the challenges of globalization in modern Nigeria and the process of “culture education,” a terminology used to emphasize the peculiar means and methods of instruction by which a society imparts its body of values and mores in the pursuance and attainment of the society’s collective vision, aspirations, and goals. Within this framework, this paper examines the legacies of imperialism and colonization within the Nigerian educational system––particularly in reference to the teaching of folklore and oral tradition––including the destruction of indigenous knowledge systems and the continuing lack of adequate resources in African universities. The paper concludes by offering suggestions for a more fully synthesized indigenous and formal Nigerian educational system as a method of addressing postcolonial rupture.  </p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ademola Omobewaji Dasylva"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 386,
            "issue_id": "283",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 387,
            "issue_id": "283",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21ii/21_2_cover.pdf"
          },
          {
            "id": 388,
            "issue_id": "283",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21ii/21_2_cover.jpg"
          },
          {
            "id": 389,
            "issue_id": "283",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21ii/21_2_front_matter.pdf"
          },
          {
            "id": 390,
            "issue_id": "283",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21ii/21_2_back_matter.pdf"
          },
          {
            "id": 391,
            "issue_id": "283",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21ii/21_2_editors_column.pdf"
          },
          {
            "id": 392,
            "issue_id": "283",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21ii/oraltradition_21_2.zip"
          },
          {
            "id": 393,
            "issue_id": "283",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21ii/21_2_complete.pdf"
          },
          {
            "id": 394,
            "issue_id": "283",
            "name": "Volume",
            "value": "21"
          },
          {
            "id": 395,
            "issue_id": "283",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 396,
            "issue_id": "283",
            "name": "Date",
            "value": "2006-10-26"
          }
        ]
      },
      {
        "id": 620,
        "name": "21i",
        "title": "Volume 21, Issue 1",
        "articles": [
          {
            "id": 10363,
            "title": "Neoanalysis, Orality, and Intertextuality: An Examination of Homeric Motif Transference",
            "content": "<p>In Homeric studies scholars have speculated on the influence of (non-surviving) preHomeric material on the Iliad. This article expands this line of argument from an oralist perspective, with reference to modern intertextual theory. It concludes that preHomeric and nonHomeric motifs from oral traditions were transferred into the epic poem, creating an intertextually allusive poetics that would have been recognizable to an early Greek audience informed of mythological traditions.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jonathan Burgess"
              }
            ],
            "files": []
          },
          {
            "id": 10361,
            "title": "“Blasts of Language”: Changes in Oral Poetics in Britain Since 1965",
            "content": "<p>This article examines how oral performances of poetry have proliferated over the past forty years to become an essential part of the writing and distribution of poetry in the UK. Our analysis of this phenomenon involves historical research and suggests new ways of looking at the construction of poetic meaning. We draw on interviews with practitioners from diverse poetry communities in considering how performance challenges the exclusive emphasis on the silent, printed text in existing histories of English language poetry.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Peter Middleton"
              },
              {
                "name": "Nicky Marsh"
              },
              {
                "name": "Victoria Sheppard"
              }
            ],
            "files": []
          },
          {
            "id": 10358,
            "title": "Daimokutate: Ritual Placatory Performance of the Genpei War",
            "content": "<p>This article explores the connections between a coming-of-age ritual in rural Japan and one of the important narratives underwriting the rise of Japan’s warrior class. the Genpei War that brought it to power in 1185. Through an explication of the narrative and performative elements of the ritual, Oyler demonstrates how one problematic story about Japan’s history is reworked into a palatable narrative and incorporated into the ritual life of geographically and socially diverse populations.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Elizabeth Oyler"
              }
            ],
            "files": [
              {
                "id": 10360,
                "title": "olyer.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/21i/full/olyer.mp4",
                "caption": "Excerpts from Itsukushima, performed at Kamifukawa, Nara, Japan on October 12, 2002. The clip opens with the michihiki, in which the actors are led by an elder, followed immediately by the actor portraying the deity Benzaiten (denoted by a gold headpiece).  The members of the Taira party carry bows; the scion Kiyomori is distinguished by his robe, which is patterned in contrast with the striped robes of the rest of the actors. He is fifth in the procession, immediately following the Shrine Priest, who carries the ritual heihaku (a baton with white paper strips attached to it at one end). The remaining elders follow the actors. The clip consists of excerpts from the first 2/3 of the piece, concluding with  Benzaitens emergence from the enclosure signifying her shrine."
              }
            ]
          },
          {
            "id": 10356,
            "title": "Performance and Text in the Italian Carolingian Tradition",
            "content": "The Italian chivalric-epic tradition is based primarily on medieval Carolingian lore from France. Oral and written manifestations of this tradition influenced and enriched each other across the centuries. This essay explores the dialectic between oral and written Carolingian epic in Italy. It focuses on the medieval cantari poems and on the Sicilian cunto, which was a type of oral performance that survived into the twentieth century.",
            "ecompanionhtml": "<html><head></head><body><a name=\"audio1\"></a>\r\n  <audio controls=\"\" preload=\"none\">\r\n <source src=\"undefined/files/ecompanions/21i/full/Celano.mp3\" type=\"audio/mpeg\">\r\n</audio>\r\n<h4>Peppino Celano Audio Clip</h4>\r\n<p><a href=\"#audio1\">This recording from 1962 is of Peppino Celano</a>, one of the last <em>cuntastorie</em> (recorded in Palermo by Roberto Leydi, in <em>La canzone narrativa e lo spettacolo popolare</em>. Vol 2.  Albetros, n.d.). The climaxes of the <em>cuntu</em> performance were the combat scenes (duels or battles) when the <em>cuntastorie</em> would narrate in an animated and syncopated rhythm. This technique demonstrates a beautiful confluence of narration and parlalinguistic elements in performance: The storytellers words narrate the sequence of events, while the rhythm of his voice - accompanied by the occasional whack of his wooden sword - conveys the action. The <em>cuntastorie</em>s manner of recitation also suggests panting from physical exertion. This excerpt represents a common motif wherein the two superhero cousins, Orlando and Rinaldo, fight over the beautiful Angelica, but neither can (or is willing to) ultimately subdue the other. Extending slightly the excerpt quoted in the article:</p>\r\n<p><em>Orlandu era forti e valurusu, ma per da scherma vinceva sempre ddu ladruni di Montalbanu che cummannava a settecennu ladruni.</em> (My transcription of Celanos performance language, which is a Sicilian-Italian hybrid.)</p>\r\n<p>Orlando was strong and valorous, but when it came to swordplay that thief of Montalbano who commanded seven-hundred thieves always won.</p>\r\n<p>(Parallel syntax: Orlando was strong and valorous, but for swordplay always won that thief of Montalbano who commanded seven-hundred thieves.)</p></body></html>",
            "authors": [
              {
                "name": "Antonio Scuderi"
              }
            ],
            "files": [
              {
                "id": 10685,
                "title": "Celano",
                "fileURI": "http://journal.oraltradition.archimedes.digital/wp-content/uploads/Celano.mp3",
                "caption": "&lt;h4&gt;Peppino Celano Audio Clip&lt;/h4&gt;&lt;div id=&quot;song&quot;&gt;&nbsp;&lt;/div&gt;&lt;p&gt;&lt;a href=&quot;https://web.archive.org/web/20160404201357/http://journal.oraltradition.org/issues/21i/scuderi#audio&quot;&gt;This recording from 1962 is of Peppino Celano&lt;/a&gt;, one of the last&nbsp;&lt;em&gt;cuntastorie&lt;/em&gt;&nbsp;(recorded in Palermo by Roberto Leydi, in&nbsp;&lt;em&gt;La canzone narrativa e lo spettacolo popolare&lt;/em&gt;. Vol 2. Albetros, n.d.). The climaxes of the&nbsp;&lt;em&gt;cuntu&lt;/em&gt;performance were the combat scenes (duels or battles) when the&nbsp;&lt;em&gt;cuntastorie&lt;/em&gt;&nbsp;would narrate in an animated and syncopated rhythm. This technique demonstrates a beautiful confluence of narration and parlalinguistic elements in performance: The storyteller&#039;s words narrate the sequence of events, while the rhythm of his voice - accompanied by the occasional whack of his wooden sword - conveys the action. The&nbsp;&lt;em&gt;cuntastorie&lt;/em&gt;&#039;s manner of recitation also suggests panting from physical exertion. This excerpt represents a common motif wherein the two superhero cousins, Orlando and Rinaldo, fight over the beautiful Angelica, but neither can (or is willing to) ultimately subdue the other. Extending slightly the excerpt quoted in the article:&lt;/p&gt;&lt;p&gt;&lt;em&gt;Orlandu era forti e valurusu, ma per da scherma vinceva sempre &#039;ddu ladruni di Montalbanu che cummannava a settecennu ladruni.&lt;/em&gt;&nbsp;(My transcription of Celano&#039;s performance language, which is a Sicilian-Italian hybrid.)&lt;/p&gt;&lt;p&gt;Orlando was strong and valorous, but when it came to swordplay that thief of Montalbano who commanded seven-hundred thieves always won.&lt;/p&gt;&lt;p&gt;(Parallel syntax: Orlando was strong and valorous, but for swordplay always won that thief of Montalbano who commanded seven-hundred thieves.)&lt;/p&gt;&lt;div&gt;&lt;div class=&quot;content-footer&quot;&gt;URLs for websites, bibliographic references, and other online resources are reviewed, current, and valid at the time of publication.&nbsp;&lt;em&gt;Oral Tradition&lt;/em&gt;&nbsp;cannot accept responsibility for the future availability of these online materials.&lt;/div&gt;&lt;/div&gt;"
              }
            ]
          },
          {
            "id": 10354,
            "title": "Elaborate Versionings: Characteristics of Emergent Performance in Three Print/Oral/ Aural Poets",
            "content": "Literary studies regards the \"poetry reading\" as a marginal phenomenon. By resituating the published poems of Amiri Baraka, Kamau Brathwaite, and Cecilia Vicuña in performance contexts, this essay proposes that each reading has the dimensions of an emergent performance, with distinguishable oral dynamics. The poet-performers break through into performativity by means of elaboration and versioning. This dynamic activity practice presents a challenge to the dominant practices of literary criticism and the scholarship of print texts.",
            "ecompanionhtml": "<html><head></head><body><a name=\"audio1\"></a>\r\n<audio controls=\"\" preload=\"none\">\r\n <source src=\"undefined/files/ecompanions/21i/full/01-Baraka-IntheFunk.mp3\" type=\"audio/mpeg\">\r\n</audio>\r\n<h4><a href=\"#audio1\">Amiri Baraka. “In the Funk World.”</a> Recorded 23 October 1996. Analog audiotape. Robert Creeley Birthday Celebration, Hallwalls Contemporary Art Center, Buffalo, NY. Printed in <em>Funk Lore</em>. Los Angeles: Littoral Books, 1996.</h4>\r\n<p>Amiri Baraka (formerly Leroi Jones) began to earn renown as a writer within the context of the Beat and then the Black Arts movements, working with other Black Nationalists to produce plays and poetry performances that were both political and populist. Importantly, this reading scene meant that for many writers, oral performance became a significant (usually the initial and sometimes the sole) means of publication. Lorenzo Thomas observes that in the Black Arts period, “the poetry reading as a characteristic mode of publication reinforced poets’ tendency to employ ‘dramatic’ structures and direct first-person address.” The speeches and sermons become like traditional models, so that, in the poetry, “what you hear is the speaking voice that trespasses into song; and an antiphonal interaction with the congregation that reveals the same structures that inform the early ‘collective improvisation’ of New Orleans jazz, bebop, and the avant-garde jazz of the 1960s.”</p>\r\n<p>This short clip demonstrates a skillful play with paralinguistic dimensions of speech as well as demonstrating a cluster of generative or improvisational moves that distinguish an emergent performance from a poetry recitation--indicated by the term “elaboration.” </p>\r\n<hr width=\"90% align=\" center\"=\"\" shade=\"noshade\">\r\n\r\n<a name=\"audio2\"></a>\r\n<audio controls=\"\" preload=\"none\">\r\n <source src=\"undefined/files/ecompanions/21i/full/02-Vicuna-Adiano.mp3\" type=\"audio/mpeg\">\r\n</audio>\r\n<h4><a href=\"#audio2\">Cecilia Vicuña. “Adiano y azumbar.”</a> Recorded 11 April 2002.  Digital minidisc.  University of Texas of the Permian Basin, Odessa, TX. Printed in <em>El Templo</em>. NY: Situations, 2001.</h4>\r\n<p>Cecilia Vicuña, a Chilean-born poet and artist living in New York, explores the themes of sound, voice, writing, and weaving in fourteen volumes of English and bilingual poetry (including: <em>Unravelling Words, The Precarious, El Templo, InStan</em>).  Her cultural resources range from indigenous poetry and song—to postmodern art; she has edited <em>UL: Four Mapuche Poets</em> and participated in the Whitney Biennial and the Museum of Modern Art.  Vicuña frequently prepares the site for a poetry performance in advance by weaving threads throughout a space. Her Texas performance began with the silent screening of a her video featuring dancers weaving on a Hudson River pier at twilight. As the video closed, Vicuña began singing from her seat at the rear of the audience. Rising, she slowly moved to the podium, still singing and using a hand-held light to cast thread-like lines upon the walls, ceiling and audience. (Full audio of performance at <a href=\"http://audibleword.org\" target=\"_blank\">audibleword.org</a>).</p>\r\n<p>This clip demonstrates another way a minimal text might be elaborated, through a repetition and variation of patterns already implicit in the source text; the sung performance of the poem might easily be transcribed at twice the length of the print version or 26 lines with 14 repetitions.</p>\r\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\r\n\r\n<a name=\"audio3\"></a>\r\n<audio controls=\"\" preload=\"none\">\r\n <source src=\"undefined/files/ecompanions/21i/full/03-Vicuna-Tentenelaire.mp3\" type=\"audio/mpeg\">\r\n</audio>\r\n<h4><a href=\"#audio3\">Cecilia Vicuña. “Tentenelaire Zun Zun.”</a> Recorded 11 April 2002.  Digital minidisc.  University of Texas of the Permian Basin, Odessa, TX. Printed in <em>Unravelling Words and the Weaving of Water</em>.  Eliot Weinberger and Suzanne Jill Levine, trans.  Saint Paul, MN: Greywolf Press, 1992.</h4>\r\n<p>This clip documents an innovative bilingual performance of a text published years prior in the familiar, facing-page format of the bilingual edition. Rather than voice the piece as published, beginning on the left in Spanish and following with the right-hand English, the poet/performer dances deliberately back and forth between Spanish and English, creating a new arrangement—a poem in two languages that does not fully correspond to either of the two published versions. Vicuña’s performance cannot be called oral-composition in the usual sense: it begins with a text, and little new material is added. Yet the virtuosic oscillation between Spanish and English along with selective omissions and repetitions present a poem that is quite unlike the print-text. In the active arrangement of the poem’s elements a new work has been constructed—performative versioning.</p>\r\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\r\n\r\n<a name=\"audio4\"></a>\r\n<audio controls=\"\" preload=\"none\">\r\n <source src=\"undefined/files/ecompanions/21i/full/04-Brathwaite-Angel.mp3\" type=\"audio/mpeg\">\r\n</audio>\r\n<h4><a href=\"#audio4\">Kamau Brathwaite. “Angle Engine.”</a> Recorded 19 October 1997.  Lecture/Poetry Reading. XCP: Cross-Cultural Poetics Conference, University of Minnesota, Minneapolis, MN. Printed in Ancestors (<em>A Reinvention of Mother Poem, Sun Poem, and X/Self</em>. NY: New Directions, 2001.</h4>\r\n<p>Kamau Brathwaite self-identifies as a Nation-language poet, moving in the course of a forty-year career towards the use of an English reflective of the socio-historical richness of Afro-Caribbean vernacular speech. A Barbadian scholarship student at Cambridge, he spent several years in the education ministry of Ghana and earned a doctorate in history from Sussex, publishing an authoritative text in the field—<em>The Development of Creole Society in Jamaica 1770-1820</em>—before making a full commitment to poetry.  His life-long poetic project naturally extends, however, from this background: opening up poetry to history, to excluded registers of language and, in particular, to forms of language that sustain diasporic memory or the sounds and physical rhythms of island life.</p>\r\n<p>This clip reflects how aspects of traditional orality serve an emergent function in the work of a contemporary print-poet.  A theme of this poem is the spiritual force of sound and rhythm, which Brathwaite conveys through a verbal underscoring of parallelism and the oral vocables, which are also present on the page, are themselves performance keys.  The two sustaining motifs of the poem— “praaaze be to / praaaze be to / paaaze be to gg” and “bub-a-dups / bub-a-dups / bub-a-dups / /hah” —establish a rhythm that opens the poem into a spatial dimension, articulate the presence of a speaking body, and even imply an associated dance. The rhythms set in play and the viscerally physical articulation of paralinguistic vocables and grunts do not simply ornament or enrich the text; they mark it as a temporal experience.</p></body></html>",
            "authors": [
              {
                "name": "Kenneth Sherwood"
              }
            ],
            "files": [
              {
                "id": 10686,
                "title": "01-Baraka-IntheFunk",
                "fileURI": "http://journal.oraltradition.archimedes.digital/wp-content/uploads/01-Baraka-IntheFunk.mp3",
                "caption": "&lt;h4&gt;&lt;a href=&quot;https://web.archive.org/web/20170628141355/http://journal.oraltradition.org/issues/21i/sherwood#audio1&quot;&gt;Amiri Baraka. &ldquo;In the Funk World.&rdquo;&lt;/a&gt;&nbsp;Recorded 23 October 1996. Analog audiotape. Robert Creeley Birthday Celebration, Hallwalls Contemporary Art Center, Buffalo, NY. Printed in&nbsp;&lt;em&gt;Funk Lore&lt;/em&gt;. Los Angeles: Littoral Books, 1996.&lt;/h4&gt;&lt;p&gt;Amiri Baraka (formerly Leroi Jones) began to earn renown as a writer within the context of the Beat and then the Black Arts movements, working with other Black Nationalists to produce plays and poetry performances that were both political and populist. Importantly, this reading scene meant that for many writers, oral performance became a significant (usually the initial and sometimes the sole) means of publication. Lorenzo Thomas observes that in the Black Arts period, &ldquo;the poetry reading as a characteristic mode of publication reinforced poets&rsquo; tendency to employ &lsquo;dramatic&rsquo; structures and direct first-person address.&rdquo; The speeches and sermons become like traditional models, so that, in the poetry, &ldquo;what you hear is the speaking voice that trespasses into song; and an antiphonal interaction with the congregation that reveals the same structures that inform the early &lsquo;collective improvisation&rsquo; of New Orleans jazz, bebop, and the avant-garde jazz of the 1960s.&rdquo;&lt;/p&gt;&lt;p&gt;This short clip demonstrates a skillful play with paralinguistic dimensions of speech as well as demonstrating a cluster of generative or improvisational moves that distinguish an emergent performance from a poetry recitation--indicated by the term &ldquo;elaboration.&rdquo;&lt;/p&gt;"
              },
              {
                "id": 10687,
                "title": "02-Vicuna-Adiano",
                "fileURI": "http://journal.oraltradition.archimedes.digital/wp-content/uploads/02-Vicuna-Adiano.mp3",
                "caption": "&lt;h4&gt;&lt;a href=&quot;https://web.archive.org/web/20170628141355/http://journal.oraltradition.org/issues/21i/sherwood#audio2&quot;&gt;Cecilia Vicu&ntilde;a. &ldquo;Adiano y azumbar.&rdquo;&lt;/a&gt;&nbsp;Recorded 11 April 2002. Digital minidisc. University of Texas of the Permian Basin, Odessa, TX. Printed in&nbsp;&lt;em&gt;El Templo&lt;/em&gt;. NY: Situations, 2001.&lt;/h4&gt;&lt;p&gt;Cecilia Vicu&ntilde;a, a Chilean-born poet and artist living in New York, explores the themes of sound, voice, writing, and weaving in fourteen volumes of English and bilingual poetry (including:&nbsp;&lt;em&gt;Unravelling Words, The Precarious, El Templo, InStan&lt;/em&gt;). Her cultural resources range from indigenous poetry and song&mdash;to postmodern art; she has edited&nbsp;&lt;em&gt;UL: Four Mapuche Poets&lt;/em&gt;&nbsp;and participated in the Whitney Biennial and the Museum of Modern Art. Vicu&ntilde;a frequently prepares the site for a poetry performance in advance by weaving threads throughout a space. Her Texas performance began with the silent screening of a her video featuring dancers weaving on a Hudson River pier at twilight. As the video closed, Vicu&ntilde;a began singing from her seat at the rear of the audience. Rising, she slowly moved to the podium, still singing and using a hand-held light to cast thread-like lines upon the walls, ceiling and audience. (Full audio of performance at&nbsp;&lt;a href=&quot;https://web.archive.org/web/20170628141355/http://audibleword.org/&quot; target=&quot;_blank&quot; rel=&quot;noopener&quot;&gt;audibleword.org&lt;/a&gt;).&lt;/p&gt;&lt;p&gt;This clip demonstrates another way a minimal text might be elaborated, through a repetition and variation of patterns already implicit in the source text; the sung performance of the poem might easily be transcribed at twice the length of the print version or 26 lines with 14 repetitions.&lt;/p&gt;"
              },
              {
                "id": 10688,
                "title": "03-Vicuna-Tentenelaire",
                "fileURI": "http://journal.oraltradition.archimedes.digital/wp-content/uploads/03-Vicuna-Tentenelaire.mp3",
                "caption": "&lt;h4&gt;&lt;a href=&quot;https://web.archive.org/web/20170628141355/http://journal.oraltradition.org/issues/21i/sherwood#audio3&quot;&gt;Cecilia Vicu&ntilde;a. &ldquo;Tentenelaire Zun Zun.&rdquo;&lt;/a&gt;&nbsp;Recorded 11 April 2002. Digital minidisc. University of Texas of the Permian Basin, Odessa, TX. Printed in&nbsp;&lt;em&gt;Unravelling Words and the Weaving of Water&lt;/em&gt;. Eliot Weinberger and Suzanne Jill Levine, trans. Saint Paul, MN: Greywolf Press, 1992.&lt;/h4&gt;&lt;p&gt;This clip documents an innovative bilingual performance of a text published years prior in the familiar, facing-page format of the bilingual edition. Rather than voice the piece as published, beginning on the left in Spanish and following with the right-hand English, the poet/performer dances deliberately back and forth between Spanish and English, creating a new arrangement&mdash;a poem in two languages that does not fully correspond to either of the two published versions. Vicu&ntilde;a&rsquo;s performance cannot be called oral-composition in the usual sense: it begins with a text, and little new material is added. Yet the virtuosic oscillation between Spanish and English along with selective omissions and repetitions present a poem that is quite unlike the print-text. In the active arrangement of the poem&rsquo;s elements a new work has been constructed&mdash;performative versioning.&lt;/p&gt;"
              },
              {
                "id": 10689,
                "title": "04-Brathwaite-Angel",
                "fileURI": "http://journal.oraltradition.archimedes.digital/wp-content/uploads/04-Brathwaite-Angel.mp3",
                "caption": "&lt;h4&gt;&lt;a href=&quot;https://web.archive.org/web/20170628141355/http://journal.oraltradition.org/issues/21i/sherwood#audio4&quot;&gt;Kamau Brathwaite. &ldquo;Angle Engine.&rdquo;&lt;/a&gt;&nbsp;Recorded 19 October 1997. Lecture/Poetry Reading. XCP: Cross-Cultural Poetics Conference, University of Minnesota, Minneapolis, MN. Printed in Ancestors (&lt;em&gt;A Reinvention of Mother Poem, Sun Poem, and X/Self&lt;/em&gt;. NY: New Directions, 2001.&lt;/h4&gt;&lt;p&gt;Kamau Brathwaite self-identifies as a Nation-language poet, moving in the course of a forty-year career towards the use of an English reflective of the socio-historical richness of Afro-Caribbean vernacular speech. A Barbadian scholarship student at Cambridge, he spent several years in the education ministry of Ghana and earned a doctorate in history from Sussex, publishing an authoritative text in the field&mdash;&lt;em&gt;The Development of Creole Society in Jamaica 1770-1820&lt;/em&gt;&mdash;before making a full commitment to poetry. His life-long poetic project naturally extends, however, from this background: opening up poetry to history, to excluded registers of language and, in particular, to forms of language that sustain diasporic memory or the sounds and physical rhythms of island life.&lt;/p&gt;&lt;p&gt;This clip reflects how aspects of traditional orality serve an emergent function in the work of a contemporary print-poet. A theme of this poem is the spiritual force of sound and rhythm, which Brathwaite conveys through a verbal underscoring of parallelism and the oral vocables, which are also present on the page, are themselves performance keys. The two sustaining motifs of the poem&mdash; &ldquo;praaaze be to / praaaze be to / paaaze be to gg&rdquo; and &ldquo;bub-a-dups / bub-a-dups / bub-a-dups / /hah&rdquo; &mdash;establish a rhythm that opens the poem into a spatial dimension, articulate the presence of a speaking body, and even imply an associated dance. The rhythms set in play and the viscerally physical articulation of paralinguistic vocables and grunts do not simply ornament or enrich the text; they mark it as a temporal experience.&lt;/p&gt;"
              }
            ]
          },
          {
            "id": 10352,
            "title": "Calliope, a Muse apart. Some Remarks on the Tradition of Memory as a Vehicle of Oral Justice",
            "content": "<p>Our analysis of the special role of Calliope, the Muse of “the beautiful voice”, is based on four ancient Greek poetic texts: an extract from Hesiod’s <em>Theogony</em>, an elegy of Solon, a Pindar’s ode and a fragment of Empedocles. Indeed, Calliope is connected with a particular kind of speech. This speech embraces the notions of eloquence and persuasion, and therefore contributes to the application of justice and the peaceful settlement of social conflicts.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Penelope Skarsouli"
              }
            ],
            "files": []
          },
          {
            "id": 10350,
            "title": "“Whistlin’ Towards the Devil’s House”: Poetic Transformations and Natural Metaphysics in an Appalachian Folktale Performance",
            "content": "The late Ray Hicks of Beech Mountain, North Carolina was an acclaimed master of the traditional storytelling art. Yet little has been written that conveys the poetic dimensions of his tellings, nor their striking liberties within traditional molds. This study centers on a performance of one of Hicks’s signature tales, “Wicked John and the Devil.” His masterful play with markers of truth and belief are explored in order to question traditional folkloristic classifications of folktale genres.",
            "ecompanionhtml": "<html><head></head><body><a name=\"audio1\"></a>\r\n<audio controls=\"\" preload=\"none\">\r\n <source src=\"undefined/files/ecompanions/21i/full/storytelling.mp3\" type=\"audio/mpeg\">\r\n</audio>\r\n<p>The <a href=\"#audio1\">twenty-five minutes transcribed here</a> were recorded at Ray Hickss home on Beech Mountain, North Carolina, on June 6, 1985. They are a cutting from a long afternoon and evening of storytelling and conversation in the Hicks front room, with an audience consisting of my friend Kathleen Zundell (a storyteller from Los Angeles), one Beech Mountain neighbor (a man in his fifties), Rays son Ted, and myself. Rays wife Rosa was in the kitchen preparing food.</p></body></html>",
            "authors": [
              {
                "name": "Joseph Sobol"
              }
            ],
            "files": [
              {
                "id": 10681,
                "title": "storytelling",
                "fileURI": "http://journal.oraltradition.archimedes.digital/wp-content/uploads/storytelling.mp3",
                "caption": "&lt;p&gt;The&nbsp;&lt;a href=&quot;https://web.archive.org/web/20170706155420/http://journal.oraltradition.org/issues/21i/sobol#audio1&quot;&gt;twenty-five minutes transcribed here&lt;/a&gt;&nbsp;were recorded at Ray Hicks&#039;s home on Beech Mountain, North Carolina, on June 6, 1985. They are a cutting from a long afternoon and evening of storytelling and conversation in the Hicks front room, with an audience consisting of my friend Kathleen Zundell (a storyteller from Los Angeles), one Beech Mountain neighbor (a man in his fifties), Ray&#039;s son Ted, and myself. Ray&#039;s wife Rosa was in the kitchen preparing food.&lt;/p&gt;"
              }
            ]
          },
          {
            "id": 10348,
            "title": "Carneades’ Quip: Orality, Philosophy, Wit, and the Poetics of Impromptu Quotation",
            "content": "<p>This paper explores the reworking of orally-derived poetry and myth amongst philosophers in the Hellenistic age. The specific topic is a series of poetic quotations that were exchanged between Carneades of Cyrene and one of his pupils. An analysis of this exchange suggests that the aesthetics and communicative power of oral poetics continued to be operative even in the most learned circles of the Hellenistic period and could be invoked to score humorous and sophisticated philosophical points.</p>",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "M. D. Usher"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 694,
            "issue_id": "620",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 695,
            "issue_id": "620",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21i/21_1_cover.pdf"
          },
          {
            "id": 696,
            "issue_id": "620",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21i/21_1_cover.jpg"
          },
          {
            "id": 697,
            "issue_id": "620",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21i/21_1_front_matter.pdf"
          },
          {
            "id": 698,
            "issue_id": "620",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21i/21_1_back_matter.pdf"
          },
          {
            "id": 699,
            "issue_id": "620",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21i/oraltradtion_21_1_editors_column.pdf"
          },
          {
            "id": 700,
            "issue_id": "620",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21i/oraltradition_21_1.zip"
          },
          {
            "id": 701,
            "issue_id": "620",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/21i/21_1_complete.pdf"
          },
          {
            "id": 702,
            "issue_id": "620",
            "name": "Volume",
            "value": "21"
          },
          {
            "id": 703,
            "issue_id": "620",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 704,
            "issue_id": "620",
            "name": "Date",
            "value": "2006-03-14"
          }
        ]
      },
      {
        "id": 293,
        "name": "20ii",
        "title": "Volume 20, Issue 2: Performance Literature (2)",
        "articles": [
          {
            "id": 9552,
            "title": "The How of Literature",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ruth Finnegan"
              }
            ],
            "files": []
          },
          {
            "id": 9535,
            "title": "The Culture of Play: Kabuki and the Production of Texts",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Andrew Gerstle"
              }
            ],
            "files": [
              {
                "id": 9537,
                "title": "gerstle1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle1.jpg",
                "caption": "Ihara Saikaku, <em>Kōshoku gonin onna</em> (“Five Women Who Loved Love,” 1686). Note that the text, though commercially printed, has no punctuation."
              },
              {
                "id": 9538,
                "title": "gerstle2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle2.jpg",
                "caption": "Chikamatsu Monzaemon, <em>Shinshū kawa-nakajima kassen</em> (“Battles at Kawa-nakajima,” 1721). The large triangle-like punctuation marks are breath stops for the Jōruri (Bunraku) chanter."
              },
              {
                "id": 9539,
                "title": "gerstle3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle3.jpg",
                "caption": "Ryūkōsai Jokei, <em>Yakusha mono iwai</em> (“A Celebration of Actors,” 1784). The actor is listed only by his haiku pen name (<em>haimyō</em>) and his clan stage name (<em>yago</em>). The actor is Nakamura Tomijūrō I."
              },
              {
                "id": 9540,
                "title": "gerstle4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle4.jpg",
                "caption": "Shōkōsai Hanbei, <em>Shibai gakuya zue</em> (“Theater Behind the Scenes,” 1800-02). Poems composed and written by actors each in his own calligraphy."
              },
              {
                "id": 9541,
                "title": "gerstle5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle5.jpg",
                "caption": "Shōkōsai Hanbei, <em>Shibai gakuya zue</em> (“Theater Behind the Scenes,” 1800-02). Portraits of two actors both listed by their pen names (<em>haimyō</em>) only."
              },
              {
                "id": 9542,
                "title": "gerstle6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle6.jpg",
                "caption": "Shōkōsai Hanbei, <em>Masukagami</em> (<em>Mirror of Actors</em>, 1806). Ichikawa Danzō IV; <em>kyōka</em> poem by Shōkōsai himself."
              },
              {
                "id": 9543,
                "title": "gerstle7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle7.jpg",
                "caption": "Shōkōsai Hanbei, <em>Masukagami</em> (<em>Mirror of Actors</em>, 1806). Arashi Kichisaburō II, poem by a contemporary poet."
              },
              {
                "id": 9544,
                "title": "gerstle8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle8.jpg",
                "caption": "Shōkōsai Hanbei, <em>Masukagami</em> (<em>Mirror of Actors</em>, 1806). Nakamura Utaemon III (right), poem by a contemporary poet."
              },
              {
                "id": 9545,
                "title": "gerstle9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle9.jpg",
                "caption": "Print privately produced in 1805 (<em>surimono</em>), in <em>Amata kyakushokujō</em> (a series of albums of theater sources)."
              },
              {
                "id": 9546,
                "title": "gerstle10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle10.jpg",
                "caption": "Print privately produced in 1805 (<em>surimono</em>), in <em>Amata kyakushokujō</em> (a series of albums of theater sources)."
              },
              {
                "id": 9547,
                "title": "gerstle11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle11.jpg",
                "caption": "Print privately produced in 1805 (<em>surimono</em>), in <em>Amata kyakushokujō</em> (a series of albums of theater sources)."
              },
              {
                "id": 9548,
                "title": "gerstle12.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle12.jpg",
                "caption": "A print by Ashifune privately produced in the fifth month of 1813 (<em>surimono</em>), in <em>Nishizawa Ippō harikomichō</em> (a three-volume album of theater resources). Arashi Kichisaburō II is in the role of the courtier calligrapher, Ono no Tōfū."
              },
              {
                "id": 9549,
                "title": "gerstle13.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle13.jpg",
                "caption": "Single-sheet actor print issued in the first month of 1821. Arashi Kitsusaburō I (Kichisaburō II). A reissue, with changes, of the 1813 print (Figure 12)."
              },
              {
                "id": 9550,
                "title": "gerstle14.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle14.jpg",
                "caption": "An anonymous single-sheet actor print produced in the eleventh month of 1815. Ichikawa Ebijūrō I in the role of the fisherman Fukashichi. The poem is by the actor who is listed only by his haiku pen name Shinshō. The frontal portrait is rare and reflects his recent taking of a new name as a disciple of the famous Edo actor Ichikawa Danjūrō VII. Ebijūrō had returned to Osaka after many years away."
              },
              {
                "id": 9551,
                "title": "gerstle15.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/gerstle15.jpg",
                "caption": "Kunihiro print of the actor Arashi Kichisaburō II in the role of Akizuki, for a performance in the second month of 1816. This unusual frontal portrait was most likely in response to the print in Figure 14. Ebijūrō was in a rival troupe to Kichisaburō."
              }
            ]
          },
          {
            "id": 9533,
            "title": "Performance, Visuality, and Textuality: The Case of Japanese Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Haruo Shirane"
              }
            ],
            "files": []
          },
          {
            "id": 9531,
            "title": "From Oral Performance to Paper-Text to Cyber-Edition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Miles Foley"
              }
            ],
            "files": []
          },
          {
            "id": 9529,
            "title": "Text and Performance in Africa",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Karen Barber"
              }
            ],
            "files": []
          },
          {
            "id": 9527,
            "title": "On the Concept of “Definitive Text” in Somali Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Martin Orwin"
              }
            ],
            "files": []
          },
          {
            "id": 9511,
            "title": "My Mother Has a Television, Does Yours? Transformation and Secularization in an Ewe Funeral Drum Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "James Burns"
              }
            ],
            "files": [
              {
                "id": 9513,
                "title": "mp3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/Mp3.jpg",
                "caption": "<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Music Transcription 5: Kaya</p><audio controls preload=\"none\" style=\"width:100%\">\r <source src=\"/files/ecompanions/20ii/full/Kaya.mp3\" type=\"audio/mpeg\">\r</audio><br />Drum Language example: “Kaya.”<br />Apeyeme Agbadza group led by Kodzo Tagborlo."
              },
              {
                "id": 9514,
                "title": "mp3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/Mp3.jpg",
                "caption": "<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Music Transcription 5: Kaya</p><audio controls preload=\"none\" style=\"width:100%\">\r <source src=\"/files/ecompanions/20ii/full/Kaya.mp3\" type=\"audio/mpeg\">\r</audio><br />Drum Language example: “Kaya.”<br />Apeyeme Agbadza group led by Kodzo Tagborlo."
              },
              {
                "id": 9515,
                "title": "mp3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/Mp3.jpg",
                "caption": "<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Music Transcription 5: Kaya</p><audio controls preload=\"none\" style=\"width:100%\">\r <source src=\"/files/ecompanions/20ii/full/Kaya.mp3\" type=\"audio/mpeg\">\r</audio><br />Drum Language example: “Kaya.”<br />Apeyeme Agbadza group led by Kodzo Tagborlo."
              },
              {
                "id": 9516,
                "title": "mp3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/Mp3.jpg",
                "caption": "<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Music Transcription 5: Kaya</p><audio controls preload=\"none\" style=\"width:100%\">\r <source src=\"/files/ecompanions/20ii/full/Kaya.mp3\" type=\"audio/mpeg\">\r</audio><br />Drum Language example: “Kaya.”<br />Apeyeme Agbadza group led by Kodzo Tagborlo."
              },
              {
                "id": 9517,
                "title": "mp3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/Mp3.jpg",
                "caption": "<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Music Transcription 5: Kaya</p><audio controls preload=\"none\" style=\"width:100%\">\r <source src=\"/files/ecompanions/20ii/full/Kaya.mp3\" type=\"audio/mpeg\">\r</audio><br />Drum Language example: “Kaya.”<br />Apeyeme Agbadza group led by Kodzo Tagborlo."
              },
              {
                "id": 9518,
                "title": "mp3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/Mp3.jpg",
                "caption": "<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Music Transcription 5: Kaya</p><audio controls preload=\"none\" style=\"width:100%\">\r <source src=\"/files/ecompanions/20ii/full/Kaya.mp3\" type=\"audio/mpeg\">\r</audio><br />Drum Language example: “Kaya.”<br />Apeyeme Agbadza group led by Kodzo Tagborlo."
              },
              {
                "id": 9519,
                "title": "burns1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/burns1.jpg",
                "caption": "Map: The town of Dzodze within Eweland. Also shows surrounding neighbors of Ewe."
              },
              {
                "id": 9520,
                "title": "burns2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/burns2.jpg",
                "caption": "Roman Catholic Mission Primary and Junior Secondary School. Dzodze, Ghana."
              },
              {
                "id": 9521,
                "title": "burns3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/burns3.jpg",
                "caption": "Initiates to the Amegashi shrine, a traditional religious group that specializes in communication with the dead. Dzodze, Ghana."
              },
              {
                "id": 9522,
                "title": "burns4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/burns4.jpg",
                "caption": "Ceremony for the Afa shrine. Dzodze, Ghana."
              },
              {
                "id": 9523,
                "title": "burns5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/burns5.jpg",
                "caption": "The late Mishiso Tagborlo. Dzodze, Ghana."
              },
              {
                "id": 9524,
                "title": "burns6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/burns6.jpg",
                "caption": "King Tagborlo playing a tin can drum- the latest drum prodigy."
              },
              {
                "id": 9525,
                "title": "burns7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/burns7.jpg",
                "caption": "Ageshe dance-drumming at a funeral. Kodzo Tagborlo (left wearing red shirt) is playing the master drum. Dzodze, Ghana."
              },
              {
                "id": 9526,
                "title": "burns8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/burns8.jpg",
                "caption": "Participant/Spectators dancing Ageshe. Dzodze, Ghana."
              }
            ]
          },
          {
            "id": 9509,
            "title": "The Many Shapes of Medieval Chinese Plays: How Texts Are Transformed to Meet the Needs of Actors, Spectators, Censors, and Readers",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Wilt Idema"
              }
            ],
            "files": []
          },
          {
            "id": 9495,
            "title": "Textual Representations of the Sixteenth-Century Chinese Drama <em>Yuzan ji (The Jade Hairpin)</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Andrew Lo"
              }
            ],
            "files": [
              {
                "id": 9497,
                "title": "lo1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo1.jpg",
                "caption": "Pan has the love poem in his hand. The Buddhist sutra, symbolizing the illusory nature of the world, lies on the table to the left."
              },
              {
                "id": 9498,
                "title": "lo2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo2.jpg",
                "caption": "In the illustration, she is actually facing the reader, and it is Pan who unties her gown."
              },
              {
                "id": 9499,
                "title": "lo3.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo3.jpg",
                "caption": "The chess scene, Act 10. The official Zhang Xiaoxiang is trying to seduce Miaochang verbally while playing the elegant game of weiqi. This is a rare depiction of a man and woman who are not from the same family playing weiqi together."
              },
              {
                "id": 9500,
                "title": "lo4.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo4.jpg",
                "caption": "The first column of characters on the top right gives pronunciations of characters. The next two columns on the top right give the pronunciation and meaning of a character. The illustration is of Act 19."
              },
              {
                "id": 9501,
                "title": "lo5.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo5.jpg",
                "caption": "Gao Lian’s <em>Yuzan ji</em> (<em>The Jade Hairpin</em>; see Gao 1954:no. 14, <em>juan xia</em>, 12b). This is a Qing period edition of the <em>Jigu ge</em> edition in the Library of the School of Oriental and African Studies, University of London. The text is from Act 19."
              },
              {
                "id": 9502,
                "title": "lo6.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo6.jpg",
                "caption": "This is the title page, and the scene of the couple with their eyes closed invites the reader to enter a gentle and soft realm (wenrou xiang)."
              },
              {
                "id": 9503,
                "title": "lo7.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo7.jpg",
                "caption": "A scene from Act 19 of <em>The Jade Hairpin</em>."
              },
              {
                "id": 9504,
                "title": "lo8.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo8.jpg",
                "caption": "Pan is kneeling down to apologize for his late arrival (Act 21). Note the similarity with Figure 4, where Pan is kneeling down after the sexual union to say that he will never forget Miaochang (Act 19)."
              },
              {
                "id": 9505,
                "title": "lo9.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo9.jpg",
                "caption": "This is the title page. “The eight accomplished musicians” refers originally to those from the court of the Eastern Han period (25-220; see Fan 1973:3125-27), but here they have been transformed into eight beautiful female musicians."
              },
              {
                "id": 9506,
                "title": "lo10.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo10.jpg",
                "caption": "This is the title page. Note that a price of 0.12 <em>taels</em> of silver has been stamped on top."
              },
              {
                "id": 9507,
                "title": "lo11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo11.jpg",
                "caption": "See Zheng 1988:vol. 4, p. 17."
              },
              {
                "id": 9508,
                "title": "lo11.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20ii/full/lo11.jpg",
                "caption": "See Zheng 1988:vol. 4, p. 17."
              }
            ]
          }
        ],
        "customFields": [
          {
            "id": 419,
            "issue_id": "293",
            "name": "Subtitle",
            "value": "Performance Literature (2)"
          },
          {
            "id": 420,
            "issue_id": "293",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20ii/20_2_cover.pdf"
          },
          {
            "id": 421,
            "issue_id": "293",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20ii/20_2_cover.jpg"
          },
          {
            "id": 422,
            "issue_id": "293",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20ii/20_2_front_matter.pdf"
          },
          {
            "id": 423,
            "issue_id": "293",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20ii/20_2_back_matter.pdf"
          },
          {
            "id": 424,
            "issue_id": "293",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20ii/ot_editors_column_20_ii.pdf"
          },
          {
            "id": 425,
            "issue_id": "293",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20ii/oraltradition_20_ii.zip"
          },
          {
            "id": 426,
            "issue_id": "293",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20ii/20_2_complete.pdf"
          },
          {
            "id": 427,
            "issue_id": "293",
            "name": "Volume",
            "value": "20"
          },
          {
            "id": 428,
            "issue_id": "293",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 429,
            "issue_id": "293",
            "name": "Date",
            "value": "2005-10-01"
          }
        ]
      },
      {
        "id": 321,
        "name": "20i",
        "title": "Volume 20, Issue 1: Performance Literature (1)",
        "articles": [
          {
            "id": 9618,
            "title": "“Fellow Townsmen and My Noble Constituents!”: Representations of Oratory on Early Commercial Recordings",
            "content": "",
            "ecompanionhtml": "<html><head></head><body><h4>Supplementary Sound Files</h4>\n<a name=\"audio1\"></a>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Lincoln’s Speech at Gettysburg, Leonard G. Spencer</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/leonardspencer.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio1\">Leonard G. Spencer. \"Lincoln’s Speech at Gettysburg,\"</a> Victor 16106-A (formerly issued as single-faced Victor 2113), take not shown. Probably recorded Oct. 26, 1906.</p>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n<a name=\"audio2\"></a>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Address by the Late President McKinley at the Pan American Exposition, Harry Spencer</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/harryspencer.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio2\">[Harry Spencer]. \"Address by the Late President McKinley at the Pan American Exposition,\"</a> Columbia disc A278 (formerly issued as single-faced Columbia disc 833), mx. 833-10.  Recorded c. 1904.</p>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n<a name=\"audio3\"></a>\n<iframe src=\"/files/ecompanions/20i/3.html\" style=\"width:100%;margin:0\"></iframe>\n<audio controls=\"\" preload=\"none\">\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">A Meeting of the Lime Kiln Club, American Quartet</p>\n  <source src=\"undefined/files/ecompanions/20i/full/americanquartet.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio3\">American Quartet. \"A Meeting of the Lime Kiln Club,\"</a> Lambert Indestructible Cylinder 590.  Recorded c. 1902.</p>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n<a name=\"audio4\"></a>\n<iframe src=\"/files/ecompanions/20i/4.html\" style=\"width:100%;margin:0\"></iframe>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">McGuire’s Fourth-of-July Celebration, Steve Porter</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/porter.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio4\">Steve Porter. \"McGuire’s Fourth-of-July Celebration,\"</a> Columbia disc A585, mx. 3909-2.  Recorded c. 1907.</p>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n<a name=\"audio5\"></a>\n<iframe src=\"/files/ecompanions/20i/5.html\" style=\"width:100%;margin:0\"></iframe>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Congressman Filkin’s Homecoming, Byron G. Harlan</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/harlan.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio5\">Byron G. Harlan. \"Congressman Filkin’s Homecoming,\"</a> United [=Columbia] disc A1036, mx. 19236-[3?].  Recorded Feb. 10, 1911.</p></body></html>",
            "authors": [
              {
                "name": "Richard Bauman"
              },
              {
                "name": "Patrick Feaster"
              }
            ],
            "files": []
          },
          {
            "id": 9616,
            "title": "Shellac, Bakelite, Vinyl, and Paper: Artifacts and Representations of North Indian Art Music",
            "content": "",
            "ecompanionhtml": "<html><head></head><body><h4>[duPerron and Magrial] have identified three prominent contingencies for the interaction between poetic text and tāl.</h4>\n<p>From pages 137-138:</p>\n<ol>\n  <li><a name=\"audio1\"></a>Songs in which the <em>sam</em> (the first beat of the <em>tāl</em>) falls on a poetically meaningful word, for instance:\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Rāg Yaman Kalyān</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/01YamanBhimsenJoshiSthayi.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio1\">“Rāg Yaman Kalyān,”</a> performed by Bhimsen Joshi:<br><em>e rī ālī piyā bina</em> (“hey my friend, without my lover”)<br>The <em>sam</em> is on <em>pi</em> of <em>piyā</em> (“lover”). Consequently the vowel becomes lengthened in performance.</p>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Rāg Gauḍ Sāraṅg</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/02GaudSarangDVPaluskarSthayi.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio1\">“Rāg Gauḍ Sāraṅg,”</a> performed by D. V. Paluskar:<br><em>piyu pala na lāgī morī ãkhiyā</em>  (“love, I don’t get a moment’s rest”)<br>The <em>sam</em> is on <em>lā</em> of <em>lāgī</em> (“get”).</p>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Rāg Dhanāśrī</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/03DhanashriVHKSthayi.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio1\">“Rāg Dhanāśrī,”</a> performed by Vilayat Hussain Khan:<br><em>tero dhyāna dharata hū dina raina</em> (“I focus my attention on you night and day”)<br>The <em>sam</em> is on <em>dhyā</em> of <em>dhyāna</em> (“attention”).</p>\n</li>\n<li><a name=\"audio2\"></a>Songs in which the <em>sam</em> falls on a syllable that would not have stress in written poetry:\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Rāg Durgā</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/04DurgaMallikarjunMansurSthayi.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio2\">“Rāg Durgā,”</a> performed by Mallikarjun Mansur:<br><em>catura sughara āvo re</em> (“clever beautiful one, please come”)<br>The <em>sam</em> is on <em>ra</em> of <em>sughara</em> (“beautiful”).</p>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Rāg Mārū Bihāg</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/05MaruBihagBhimsenJoshiSthayi.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio2\">“Rāg Mārū Bihāg,”</a> performed by Bhimsen Joshi:<br><em>tarapata raina dina</em> (“I toss and turn day and night”)<br>The <em>sam</em> is on <em>na</em> of <em>dina</em> (“day”). The vowel lengthens in performance.</p>\n</li>\n<li><a name=\"audio3\"></a>Songs in which the <em>sam</em> falls in the middle of a verb:\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Rāg Darbārī Kānaḍa</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/06DarbariAmirKhanSthayi.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio3\">“Rāg Darbārī Kānaḍa,”</a> performed by Amir Khan:<br><em>kina bairana kāna bhare</em> (“what enemy of mine is telling you things?” [literally “filling your ears”])<br>The <em>sam</em> is on <em>re</em> of <em>bhare</em> (“filling”).</p>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Rāg Chāyanaṭ</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/07ChayanatOmkarnathThakurSthayi.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio3\">“Rāg Chāyanaṭ,”</a> performed by Omkarnath Thakur:<br><em>bharī gagarī morī ḍhurakāī chaila</em> (“that rascal threw down my full waterpot”)<br>The <em>sam</em> is on <em>kā</em> of <em>ḍhurakāī</em> (“threw down”).</p>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Rāg Hamīr</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/08HamirDVPaluskarSthayi.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio3\">“Rāg Hamīr,”</a> performed by D. V. Paluskar:<br><em>surajhā rahī hū</em> (“I am getting tangled up”)<br>The <em>sam</em> is on <em>jhā</em>  of <em>surajhā</em> (“tangled”), in this case part of a verbal phrase. (We consider this composition in its entirety later).</p>\n</li></ol>\n<h4><a name=\"audio4\"></a>Rāg Hamīr Tīntāl - D. V. Paluskar</h4>\n<p>From page 149:</p>\n<blockquote><audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/09HamirDVPaluskarAntara.mp3\" type=\"audio/mpeg\">\n</audio>\n<a href=\"#audio4\">This example</a> is from the academic wing of the <em>Gwalior gharānā</em> (stylistic school) of vocal music. The singer D. V. Paluskar was the extraordinarily gifted son of the reformist pedagogue Vishnu Digambar Paluskar. Although replete with an airy buoyant feeling, this rendition is devoid of rhythmic or melodic ambiguities, typical of performances by the pedagogical class of singers. This is to say that the notes are very clear, not obscured by <em>gamak</em> (a vigorous broad shaking of notes), and they tend to fall squarely on the beats. The enunciation is also very clear.  This sort of song is conducive to pain-free transcription.</blockquote>\n<h4><a name=\"audio5\"></a>Rāg Sampūrn Mālkauns Vilambit Tīntāl - Kishori Amonkar</h4>\n<p>From page 149:</p>\n<blockquote>Here, <a href=\"#audio5\">Kishori Amonkar</a>, considered by many to be the greatest living <em>˚yāl</em> singer, sings a rare <em>rāg</em> that is a speciality of the <em>Jaipur-Atrauli gharānā</em>, a seven-note version of the popular pentatonic “Rāg Mālkauns.”<br>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Sthāyī</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/10SampurnMalkaunsKishoriSthayi.mp3\" type=\"audio/mpeg\">\n</audio>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Antarā</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/11SampurnMalkaunsKishoriAntara.mp3\" type=\"audio/mpeg\">\n</audio>\n</blockquote>\n<h4><a name=\"audio6\"></a>Rāg Toḍī Madhya-lay Tīntāl - Faiyaz Khan</h4>\n<p>From pages 151-152:</p>\n<blockquote>Our <a href=\"#audio6\">final example</a>, Figure 13, is of the type of composition with which transcribers wrestle and sweat and then wrestle and sweat again at periodic intervals. Faiyaz Khan, one of the towering figures of Indian music in the twentieth century, appears to have transcended concrete relationships with songs: he pushed them and pulled them, molding them around his musical inspiration. It is not easy, sometimes not possible, to deduce what is actually “song” and what is improvisation; the two blend extremely fluidly.  Although Faiyaz Khan’s rendition of “Rāg Toḍī” is in a medium tempo, the song connects with the <em>tāl</em> in a very abstract manner.<br>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Sthāyī</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/12TodiFaiyazKhanSthayi.mp3\" type=\"audio/mpeg\">\n</audio>\n<br>\n<p style=\"font-weight:bold;text-align:center;margin:6px 0;\">Antarā</p>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/20i/full/13TodiFaiyazKhanAntara.mp3\" type=\"audio/mpeg\">\n</audio>\n</blockquote>\n<p>See Lalita du Perron and Nicolas Magriels article in <em>Oral Tradition</em> 20, i for their new style of transcribing music and voice.</p></body></html>",
            "authors": [
              {
                "name": "Lalita du Perron"
              },
              {
                "name": "Nicolas Magriel"
              }
            ],
            "files": []
          },
          {
            "id": 9602,
            "title": "Visual Takes on Dance in Java",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Felicia Hughes-Freeland"
              }
            ],
            "files": [
              {
                "id": 9604,
                "title": "Freeland.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/Freeland.mp4",
                "caption": "A Bedhaya practice with musicians and singers."
              },
              {
                "id": 9605,
                "title": "swansea.ac.uk.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/swansea.ac.uk.jpg",
                "caption": "Website: Performance in Indonesia: Traditions of Court and Country. (right-click or control-click for navigation such as “back”)."
              },
              {
                "id": 9606,
                "title": "Yudanegara.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/Yudanegara.jpg",
                "caption": "B.R.M Yudanegara, choreographer and dance teacher, with the filmmaker at the sultan’s palace."
              },
              {
                "id": 9607,
                "title": "tayuban.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/tayuban.jpg",
                "caption": "Filming the <em>tayuban</em> again in 1999."
              },
              {
                "id": 9608,
                "title": "formaldanceteachers.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/formaldanceteachers.jpg",
                "caption": "Formal dance teachers and filmmaker at the palace: Bu Yudhanegara and Felicia Hughes-Freeland are to the right hand side."
              },
              {
                "id": 9609,
                "title": "jokingdanceteachers.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/jokingdanceteachers.jpg",
                "caption": "Joking dance teachers and filmmaker at the palace."
              },
              {
                "id": 9610,
                "title": "ledhek.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/ledhek.jpg",
                "caption": "Ledheks and the medium at the <em>tayuban</em>."
              },
              {
                "id": 9611,
                "title": "ledheks.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/ledheks.jpg",
                "caption": "Ledheks sing at a <em>tayuban</em>."
              },
              {
                "id": 9612,
                "title": "makeup.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/makeup.jpg",
                "caption": "Make up for wedding dance."
              },
              {
                "id": 9613,
                "title": "danceturn.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/danceturn.jpg",
                "caption": "Man has a dance turn at the <em>tayuban</em>."
              },
              {
                "id": 9614,
                "title": "paksena.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/paksena.jpg",
                "caption": "Pak Seno at home."
              },
              {
                "id": 9615,
                "title": "Susindahati.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/20i/full/Susindahati.jpg",
                "caption": "Susindahati and Mas Pongti perform the love dance."
              }
            ]
          },
          {
            "id": 9600,
            "title": "How to Read a Reading of a Written Poem",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Peter Middleton"
              }
            ],
            "files": []
          },
          {
            "id": 9598,
            "title": "Moving Performance to Text: Can Performance be Transcribed?",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Edward Schieffelin"
              }
            ],
            "files": []
          },
          {
            "id": 9596,
            "title": "Mediators of Modernity: “Photo-interpreters” in Japanese Silent Cinema",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Isolde Standish"
              }
            ],
            "files": []
          },
          {
            "id": 9594,
            "title": "Plato, Memory, and Performance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Naoko Yamagata"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 474,
            "issue_id": "321",
            "name": "Subtitle",
            "value": "Performance Literature (1)"
          },
          {
            "id": 475,
            "issue_id": "321",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20i/20_1_cover.pdf"
          },
          {
            "id": 476,
            "issue_id": "321",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20i/20_1_cover.jpg"
          },
          {
            "id": 477,
            "issue_id": "321",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20i/20_1_front_matter.pdf"
          },
          {
            "id": 478,
            "issue_id": "321",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20i/20_1_back_matter.pdf"
          },
          {
            "id": 479,
            "issue_id": "321",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20i/editors_column_20_1.pdf"
          },
          {
            "id": 480,
            "issue_id": "321",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20i/oraltradition_20_1.zip"
          },
          {
            "id": 481,
            "issue_id": "321",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/20i/20_1_complete.pdf"
          },
          {
            "id": 482,
            "issue_id": "321",
            "name": "Volume",
            "value": "20"
          },
          {
            "id": 483,
            "issue_id": "321",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 484,
            "issue_id": "321",
            "name": "Date",
            "value": "2005-03-01"
          }
        ]
      },
      {
        "id": 331,
        "name": "19ii",
        "title": "Volume 19, Issue 2",
        "articles": [
          {
            "id": 9634,
            "title": "“Lord of the Iron Bow”: The Return Pattern Motif in the Fifteenth-century Baloch Epic <em>Hero Šey Murīd</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sabir Badalkhan"
              }
            ],
            "files": []
          },
          {
            "id": 9632,
            "title": "Oblique Performance: Snapshots of Oral Tradition in Action",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Robert Cochran"
              }
            ],
            "files": []
          },
          {
            "id": 9630,
            "title": "Oral Theory and the Limits of Formulaic Diction",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Margalit Finkelberg"
              }
            ],
            "files": []
          },
          {
            "id": 9628,
            "title": "Jazz Musicians and South Slavic Oral Epic Bards",
            "content": "",
            "ecompanionhtml": "<html><head></head><body><h4>Martin Langford</h4>\n<p><strong>WF</strong> Can you explain what you do when you improvise?</p>\n<p><strong>ML</strong> —starts with tempering choices to audience composition in order to achieve the most consonance with listeners: styles; </p>\n<p>Improv means quick decision-making:\n</p><ol>\n  <li>chord changes</li>\n  <li>rhythms</li>\n  <li>tempo</li>\n  <li>kinds of notes—scale leading tones want to go up; dominants want to go down.</li>\n</ol><p></p>\n<p><strong>WF</strong> What rhythmic/melodic patterns do you rely on, if any, during improvisation?</p>\n<p><strong>ML</strong> Yes. Licks. Incorporate a beat or two of a lick when I have to. You know, when—mistakes happen in the process.</p>\n<p><strong>WF</strong> A beat or two? Not much, is it? How much material in a lick?</p>\n<p><strong>ML</strong> —Licks are usually 2 to 4 bars. I rely on “licks” in a pinch. From twelve 2-bar licks, one can incorporate a beat or two from other people. Lick = sentence; bar = word. —Length of lick depends on frequency of chord changes. —Longer licks = more time to think. —Mirror interval distances and rhythms at beginning or end of melody.</p>\n<p><strong>WF</strong> So, is a particular lick not made up of same material every time you play it?</p>\n<p><strong>ML</strong> No. ...unless used under same circumstances. —like signature’s shape/ on flags or curves of a letter, S/F. Never has to be exactly like original. Improv not yours; there’s just this shape out there—you grab it when you need it and make it over how you like, how you need to.</p>\n<p><strong>WF</strong> You said before “improv means making several choices, quickly. Chord progressions, leading tones . . . .” Other choices?</p>\n<p><strong>ML</strong> Melody and chord progressions under it. Not conscious decision; done at the moment based on what I hear. —mirroring top/bottom of tune —mirror rhythms —double- and half-time</p>\n<a name=\"audio1\"></a>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/19ii/full/mirroring_inversion.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio1\">Mirroring and Inversion</a></p>\n<p>“Rhythms themselves are characteristic of different jazz style. For example, you could characterize all of 1920s jazz with the rhythm”: [Martin sings 1920s rhythm pattern]</p>\n<a name=\"audio2\"></a>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/19ii/full/cole_porter.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio2\">1920’s Jazz Rhythm - Cole Porter</a></p>\n<h4>David Salge</h4>\n<p><strong>WF</strong> How much of a number is improvisation? Does it happen mostly in cadenza-like sections, or can a whole piece consist of improv?</p>\n<p><strong>DS</strong> First, there’s melody; say, in violin, trumpet, or clarinet. Next, as vocalist sings, instrumentalist adds figuration. I don’t know that I’d call what I do “improvisation”; I think of it as “figuration.” Finally, figuration becomes looser as song goes on.</p>\n<p><strong>WF</strong> Do you use variation of the melody as much as variation of harmonic progressions?</p>\n<p><strong>DS</strong> Yes—in cadenza moments.</p>\n<p><strong>WF</strong> Do you pick up new material often? —really new stuff, or music that reflects someone else’s style? </p>\n<p><strong>DS</strong> Mostly in cadenzas, yes; but I frame them in the preexisting harmonic structure.</p>\n<p><strong>WF</strong> How long is a standard lick for you? Do licks have a certain structure—\narpeggios, for example, or modulation, or rhythmic variation?</p>\n<p><strong>DS</strong> Maybe I’m not aware of playing licks. I can recognize the chords of other players in their licks. My figurations are always hung on the harmonic structure, especially the basis. Then I usually ornament with scalar melodies.</p>\n<p><strong>WF</strong> Do you string several licks together in a series to make a solo?</p>\n<p><strong>DS</strong> Yes. But it’s spontaneous; no time to think. It’s ingrained, internalized. Charlie Parker: “First you master the instrument, then the repertory. Then go play.” (Later edited by David Salge to read: “First you master the instrument, then you master the repertory, then you forget all that shit and just play.”)</p>\n<p><strong>WF</strong> How much of jazz improv is “traditional,” that is, handed down from one player to another over the years?</p>\n<p><strong>DS</strong> All of it is traditional. To go outside the tradition is to no longer be Klezmer.</p>\n<p><strong>WF</strong> Are there any good jazz musicians who learned to improvise without being able to read music?</p>\n<p><strong>DS</strong> Benny Goodman was a Klezmer player first. It’s kind of a myth, you know, that the first jazz players were musically illiterate. There was this golden era, supposedly, when these new rhythms and harmonic progressions started happening. But, you know, it’s not real—I don’t think reading music or being able to read music or not had any real bearing on early jazz.</p>\n<p><strong>WF</strong> During improv, you have to “fit” licks into the section of music, right? Not just tempos and rhythms, but harmonic progressions, too; right? Do you know ahead of time how long your lick of solos will last? What tells you the duration you’ll need for a solo or lick?</p>\n<p><strong>DS</strong> No. Just take the lead. The lead sheet will show chord progressions generally – others – tremolo or –</p>\n<p><strong>WF</strong> How do you gauge the audience’s likely preferences?</p>\n<p><strong>DS</strong> Some Klezmer audiences ask for specific dances. Otherwise, just start with what you think will be good then change. Don’t have the infamous sign: Requests $5, Feelings $50, Proud Mary $500.</p>\n<p><strong>WF</strong> Are there specific rhythms associated with styles? harmonic progressions? lengths of licks?</p>\n<p><strong>DS</strong> Klezmer is sort of like Eastern Europe meets the Andrews Sisters in Yiddish. Dance rhythms dictate traditional Klezmer. Mickey Katz.</p>\n<p><strong>WF</strong> What does “improvise” mean to you? Do you think “improvise” – look ahead – appropriately describes the process?</p>\n<p><strong>DS</strong> —Start with a backbone, i.e., a tune or “a head” – 16-bar chord progressions – create new melodic material over that framework.</p>\n<h4>Larry Slezak</h4>\n<p><strong>WF</strong> What is a “lick”?</p>\n<p><strong>LS</strong> What you mean by “lick” is a word YOU like to use. There is interpretive association. Like a Brooklyn accent: “Hey, what’s happening?” [or the Bronx: “Get outta heah.”] Lick = “up the stairs,” “around the corner.” You like to use these words. You have a lick you like, but you might have to change something to make it fit. Like language, there are general concepts.</p>\n<p><strong>WF</strong> Is improvisation governed strictly by rules; that is, rules that apply to all improvisation and all styles?</p>\n<p><strong>LS</strong> General concepts that can be changed: “to the corner” or “around the corner.” Both mean same thing; use the one you need when you say it. Melody is something worthwhile you gotta say—you put it in your own words, licks, but stay within established framework of chord progressions. Harmony = foundation which DOESN’T change. Improvisation is a different melody to the same harmony. Take A Train. An improviser is first a melody player. Simple variation is not improvisation.</p>\n<p><strong>WF</strong> What abilities does a good player have that a not-so-good one lacks; that is, assuming both players have good technical command of the instrument?</p>\n<p><strong>LS</strong> I know what you mean. In some measure, there’s a connection between hand and ear. A good player has to have a concept of melody and has to have a vocation for melodies. A player must have a sense of derring-do linked to a personal confidence. It’s like bungee jumpers: the exhilaration is in not knowing what will happen.</p>\n<p><strong>WF</strong> Are you ever required to abruptly change the direction the music’s going? For example, if you notice the audience getting restless, or for any other reason?</p>\n<p><strong>LS</strong> Sure, that’s why they hire players like us—like you and me. That’s where good players make the difference.</p>\n<p><strong>WF</strong> Are there any differences between jazz dialects (local traditions) as in, say, New Orleans jazz, Kansas City jazz, Chicago jazz, or Harlem jazz?</p>\n<p><strong>LS</strong> Yes. New York drummers = aggressive. West Coast jazz is mellow and works by insinuation. “Texas tenors” is a way of playing, a kind of tone. High hat rhythm/ragtime rhythm.</p>\n<a name=\"audio3\"></a>\n<audio controls=\"\" preload=\"none\">\n  <source src=\"undefined/files/ecompanions/19ii/full/high_hat_ragtime.mp3\" type=\"audio/mpeg\">\n</audio>\n<p><a href=\"#audio3\" \"=\"\">High Hat and Ragtime Rhythm</a></p>\n<p>—environment not “essential,” but makes it harder to learn the style. Dotson: Dennis Dotson, from Rusk, Texas, learned jazz only from listening to records. He got caught up, though, once he got to Houston and started hearing live music. The environment in which music is created cannot be obtained from recordings. As a white guy growing up in New York City, I put myself in the environment—Harlem. </p>\n<p>—Music is the result of people in an environment—audiences, neighborhoods, bar owners. These guys were pretty tough. If they didn’t like your music, they would tell you to get off the stage. Lifestyle nurtures music.</p>\n<p>—My learning was more complete because of these experiences, but with effort, one can always catch up later. Music is only one aspect. People used to invite me to their homes for dinner because—One guy even offered to buy me a new set of clothes so I would look the part.</p>\n<p>—After World War II, jazz became more of an “art form.” People like Charlie Parker and Dizzie Gillespie began to give concerts. There was no more dancing.</p>\n<p>—You remember, Billy Taylor’s remark—jazz pianist in the 60’s—that jazz is America’s classical music. </p>\n<hr width=\"90%\" align=\"center\" shade=\"noshade\">\n<h4>David Winn</h4>\n<p>(email text from a fourth interviewee [not used for this paper] who addresses related issues)</p>\n<p><strong>DW</strong>: Here is a copy of part of an answer to some questions from Wake Foster in Columbia, Missouri, which I thought I’d pass on to y’all.</p>\n<p>What is improv to me?</p>\n<p>Well, alternate melody, I guess. Given a frame of a chord or chord structure, what melodies can you think of to fit in that frame? Can you do it so well that each idea blends with the next so that together they form a large melodic idea or, carried through a lengthy solo, make each framed solo relate to the previous solo? Good improvisers can do that, guys like Dennis Dotson or Larry Slezak or Tom Cummings. Like a series of paintings by Corot showing a sunrise to mid-day to sunset, hung on a wall in identical frames, each of the same scene but with different lighting as the day progresses, a good soloist can tell a melodic story that relates the first part to a middle and an end. Like a good book, except you don’t get to take anything back – no editing.</p>\n<p>Now, there is a school that wants to drop those frame restrictions, somewhat like some in classical music wanted to drop the restrictions of the period before. Sometimes it works – like Ravel and Debussy, and especially Stravinsky – and sometimes it doesn’t, as in Berg or Schoenberg (this is very subjective here). But I think the desire to want to do something different leads to new ways of thinking and is good, especially in jazz, which should be an ongoing revolution in musical thought.</p>\n<p>However, insofar as jazz is concerned, I have long felt that it is mired in old ways. They are nice ways, and I like them. But it seems like I hear a lot of play-by-the-numbers from a lot of talented players. Most of the great jazzers I know seem to be more concerned with the music and less, or even not al all, concerned with themselves. This is a difficult state to reach.\nHow would I go about learning to improvise?</p>\n<p>I would say that the first step in improvisation is the desire to do it. Hearing someone else do it and wanting to join in, hearing someone doing it badly and wanting to do it better, or maybe just hearing a song or piece of music that you hear an alternate melody to. It could be a classical piece. There are no real restrictions to what can be improvised. After all, many classic and romantic pieces asked the soloists to improvise a cadenza, and I’ll bet that Mozart never played any of his piano pieces the same way twice in a row, any more than Oscar Peterson would. you still would know it is Mozart or Peterson, of course. The music IS them. The style of playing is them, like their voice pattern/thought pattern.</p>\n<p>I’m reminded of another classical story, one about Beethoven. As I remember the story, Haydn gave Beethoven a melody and asked him to write 12 or more variations on it in one hour. When he came back to the lesson an hour later, Beethoven had written over 200 variations. That’s using a given melody as a starting point and improvising, essentially, a bunch more melodies. The difference in most jazz playing is that it is played right now, even while practicing. And if it is written down, it is written down after the fact, usually from a recording.</p>\n<p>This is not always the case, however, Paul Desmond was said to write out his solos and memorize them. I remember reading that somewhere. If this is true, he wrote them really well. I should add that I have a difficult time believing that story. But a lot of the best and most thoughtful jazz players do write out riffs and exercises off of chord changes and practice them to use in solos.</p>\n<p>So, listening to others who can improvise is good instruction and inspiring; copying what they do is more usual than not.</p>\n<p>So, how does one get a start in improvisation?</p>\n<p>First off, I might suggest that they just put on a record – any record – but one that they like and just sing along with it. Make up words or sounds or just hum but get grounded with the tune and then play around with notes here and there. You’ll hear the ones that don’t sound good to you and you’ll learn. Also, you won’t wear out a good reed.</p>\n<p>Or go to a piano and play some common chord progressions. Again, it can be classical or chords from a popular song. Hell, it can be one chord, done in a rhythmic style of one sort of another, a repeating bass, maybe. And sing something, whatever comes to mind, while keeping the sound of the chord and rhythm going. And when you can do this somewhat by singing, get your oboe and do the same thing. Betcha can’t do it the first time, but betcha can if you keep at it. Sing first, play later. Good ear training, at the least.</p>\n<p>Or play or sing a melody. Then play it again, only do it like Frank Sinatra might sing it or Janis Joplin or Miles Davis. Of course, you need to know how each of these people performed. The way Miles Davis plays Surry With the Fringe On Top is a lot different than the way Curley sings it in Oklahoma. And I can’t even imagine what Janis Joplin would have done with it. Wish I could.</p>\n<p>Also, sing a couple of bars of a melody you make up, and then try to play it on the oboe or whatever instrument you have – filled Coke bottles, chimes, window panes, whatever – just so you start to get the connection between what your brain thinks and your fingers respond to on your instrument. For oboe players in particular, listen to Juseff Latiff. You really can’t listen to too many good players. Copy what you hear, try to play along with them, sing along with them. Be loose but structured. If it embarrasses you, you have too much ego involved. Drop it and get on with the music.</p>\n<p><em>- From an e-mail message dated June 8, 2002.</em></p></body></html>",
            "authors": [
              {
                "name": "Wakefield Foster"
              }
            ],
            "files": []
          },
          {
            "id": 9626,
            "title": "From <em>The Book of Margery Kempe</em>: The Trials and Triumphs of a Homeward Journey",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Marie Nelson"
              }
            ],
            "files": []
          },
          {
            "id": 9620,
            "title": "The Right Words: Conflict and Resolution in an Oral Gaelic Song Text",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lillis Ó Laoire"
              }
            ],
            "files": [
              {
                "id": 9622,
                "title": "track7.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/19ii/full/track7.mp3",
                "caption": "Track 7"
              },
              {
                "id": 9623,
                "title": "track10.mp3",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/19ii/full/track10.mp3",
                "caption": "Track 10"
              },
              {
                "id": 9624,
                "title": "pic1.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/19ii/full/pic1.jpg",
                "caption": "Séamus and Gráinne Duggan together with Hannah (sister of Gráinne) and Antoin Rodgers at a Traditional Singing Festival, Dublin 1995."
              },
              {
                "id": 9625,
                "title": "pic2.jpg",
                "fileURI": "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/19ii/full/pic2.jpg",
                "caption": "Hannah Shéamais Bháin, Mrs. Hannah Duggan in the 1980s."
              }
            ]
          }
        ],
        "customFields": [
          {
            "id": 485,
            "issue_id": "331",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 486,
            "issue_id": "331",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19ii/19_2_cover.pdf"
          },
          {
            "id": 487,
            "issue_id": "331",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19ii/19_2_cover.jpg"
          },
          {
            "id": 488,
            "issue_id": "331",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19ii/19_2_front_matter.pdf"
          },
          {
            "id": 489,
            "issue_id": "331",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19ii/19_2_back_matter.pdf"
          },
          {
            "id": 490,
            "issue_id": "331",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19ii/editors_column_19_2.pdf"
          },
          {
            "id": 491,
            "issue_id": "331",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19ii/oraltradition_19_2.zip"
          },
          {
            "id": 492,
            "issue_id": "331",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19ii/19_2_complete.pdf"
          },
          {
            "id": 493,
            "issue_id": "331",
            "name": "Volume",
            "value": "19"
          },
          {
            "id": 494,
            "issue_id": "331",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 495,
            "issue_id": "331",
            "name": "Date",
            "value": "2004-10-01"
          }
        ]
      },
      {
        "id": 336,
        "name": "19i",
        "title": "Volume 19, Issue 1",
        "articles": [
          {
            "id": 9646,
            "title": "Anglo-Saxon Charms in Performance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lori Ann Garner"
              }
            ],
            "files": []
          },
          {
            "id": 9644,
            "title": "Myth and Literary History: Two Germanic Examples",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joseph Harris"
              }
            ],
            "files": []
          },
          {
            "id": 9642,
            "title": "The Germanic <em>Heldenlied</em> and the Poetic <em>Edda</em>: Speculations on Preliterary History",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Edward R. Haymes"
              }
            ],
            "files": []
          },
          {
            "id": 9640,
            "title": "The Implications of “Orality” for Studies of the Biblical Text",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Holly Hearon"
              }
            ],
            "files": []
          },
          {
            "id": 9638,
            "title": "Creating a Seto Epic",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Kristin Kuutma"
              }
            ],
            "files": []
          },
          {
            "id": 9636,
            "title": "Performance and Plot in <em>The Ozidi Saga</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Isidore Okpewho"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 496,
            "issue_id": "336",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 497,
            "issue_id": "336",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19i/19_1_cover.pdf"
          },
          {
            "id": 498,
            "issue_id": "336",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19i/19_1_cover.jpg"
          },
          {
            "id": 499,
            "issue_id": "336",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19i/19_1_front_matter.pdf"
          },
          {
            "id": 500,
            "issue_id": "336",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19i/19_1_back_matter.pdf"
          },
          {
            "id": 501,
            "issue_id": "336",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19i/editors_column_19_1.pdf"
          },
          {
            "id": 502,
            "issue_id": "336",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19i/oraltradition_19_1.zip"
          },
          {
            "id": 503,
            "issue_id": "336",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/19i/19_1_complete.pdf"
          },
          {
            "id": 504,
            "issue_id": "336",
            "name": "Volume",
            "value": "19"
          },
          {
            "id": 505,
            "issue_id": "336",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 506,
            "issue_id": "336",
            "name": "Date",
            "value": "2004-03-01"
          }
        ]
      },
      {
        "id": 290,
        "name": "18ii",
        "title": "Volume 18, Issue 2: Synopses of Oral Traditions (2)",
        "articles": [
          {
            "id": 10032,
            "title": "Pan-Hispanic Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Samuel G. Armistead "
              }
            ],
            "files": []
          },
          {
            "id": 10030,
            "title": "Oral Traditions in Greater Mexico",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Marcia Farr"
              }
            ],
            "files": []
          },
          {
            "id": 10028,
            "title": "Folktales",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Isabel Cardigos"
              }
            ],
            "files": []
          },
          {
            "id": 10026,
            "title": "Portuguese Narrative Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "J. J. Dias Marques"
              }
            ],
            "files": []
          },
          {
            "id": 10024,
            "title": "Oral Tradition: A Definition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Carlos Nogueira"
              }
            ],
            "files": []
          },
          {
            "id": 10022,
            "title": "Oral Tradition as a Worldwide Phenomenon",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "J. M. Pedrosa"
              }
            ],
            "files": []
          },
          {
            "id": 10020,
            "title": "Towards Greater Collaboration in Oral Tradition Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Suzanne H. Petersen"
              }
            ],
            "files": []
          },
          {
            "id": 10018,
            "title": "Medieval Spanish and Judeo-Spanish",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Zemke"
              }
            ],
            "files": []
          },
          {
            "id": 10016,
            "title": "Thoughts on Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mary-Ann Constantine"
              }
            ],
            "files": []
          },
          {
            "id": 10014,
            "title": "From Storytelling to Sermons: The Oral Narrative Tradition of Wales",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sioned Davies"
              }
            ],
            "files": []
          },
          {
            "id": 10012,
            "title": "Oral Tradition in Medieval Welsh Poetry: 1100-1600",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Dafydd Johnston"
              }
            ],
            "files": []
          },
          {
            "id": 10010,
            "title": "Fighting Words",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joseph Falaky Nagy"
              }
            ],
            "files": []
          },
          {
            "id": 10008,
            "title": "Orality in a Norse-Icelandic Perspective",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Michael Chesnutt"
              }
            ],
            "files": []
          },
          {
            "id": 10006,
            "title": "Folklore and Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lauri Harvilahti"
              }
            ],
            "files": []
          },
          {
            "id": 10004,
            "title": "Reconstructing Old Norse Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Stephen Mitchell"
              }
            ],
            "files": []
          },
          {
            "id": 10002,
            "title": "Medieval Icelandic Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Gísli Sigurðsson"
              }
            ],
            "files": []
          },
          {
            "id": 10000,
            "title": "Medieval English Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark C. Amodio "
              }
            ],
            "files": []
          },
          {
            "id": 9998,
            "title": "How the <em>Beowulf</em> Poet Composed His Poem",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Robert Payson Creed"
              }
            ],
            "files": []
          },
          {
            "id": 9996,
            "title": "Medieval Voices",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lori Ann Garner"
              }
            ],
            "files": []
          },
          {
            "id": 9994,
            "title": "Oral Traditional Approaches to Old English Verse",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Heather Maring"
              }
            ],
            "files": []
          },
          {
            "id": 9992,
            "title": "Prizes From the Borderlands",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John D. Niles"
              }
            ],
            "files": []
          },
          {
            "id": 9990,
            "title": "Looking for an Echo: The Oral Tradition in Anglo-Saxon Literature",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Andy Orchard"
              }
            ],
            "files": []
          },
          {
            "id": 9988,
            "title": "Balochi Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sabir Badalkhan"
              }
            ],
            "files": []
          },
          {
            "id": 9986,
            "title": "Oral Narrative Studies in China",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark Bender"
              }
            ],
            "files": []
          },
          {
            "id": 9984,
            "title": "Minority Oral Tradition in China",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Naran Bilik"
              }
            ],
            "files": []
          },
          {
            "id": 9982,
            "title": "Korean <em>p’ansori</em> Narrative",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Chan Park"
              }
            ],
            "files": []
          },
          {
            "id": 9980,
            "title": "Classical Persian",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Olga Merck Davidson"
              }
            ],
            "files": []
          },
          {
            "id": 9978,
            "title": "Turkic Oral Epic",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Karl Reichl"
              }
            ],
            "files": []
          },
          {
            "id": 9976,
            "title": "A Living Shamanistic Oral Tradition: Ifugao <em>hudhud</em>, the Philippines",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Maria V. Stanyukovich"
              }
            ],
            "files": []
          },
          {
            "id": 9974,
            "title": "Performing Off Stage: Oral Tradition Under the Radar",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Robert Cochran"
              }
            ],
            "files": []
          },
          {
            "id": 9972,
            "title": "Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas A. DuBois"
              }
            ],
            "files": []
          },
          {
            "id": 9970,
            "title": "Oral Theory and Medieval German Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Edward R. Haymes"
              }
            ],
            "files": []
          },
          {
            "id": 9968,
            "title": "Oral Tradition in Linguistics",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joshua T. Katz"
              }
            ],
            "files": []
          },
          {
            "id": 9966,
            "title": "Oral Traditions in Performance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Della Pollock"
              }
            ],
            "files": []
          },
          {
            "id": 9964,
            "title": "Poetics and Translation Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Burton Raffel"
              }
            ],
            "files": []
          },
          {
            "id": 9962,
            "title": "The Search for Wisdom in Native American Narratives and Classical Scholarship",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William Schneider"
              }
            ],
            "files": []
          },
          {
            "id": 9493,
            "title": "The Popular Ballad and Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mary Ellen  Brown"
              }
            ],
            "files": []
          },
          {
            "id": 9491,
            "title": "The Implicated Ballad",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William Bernard McCarthy"
              }
            ],
            "files": []
          },
          {
            "id": 9489,
            "title": "Ballads and Bad Quartos: Oral Tradition and the English Literary Historian",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Tom Pettitt"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 408,
            "issue_id": "290",
            "name": "Subtitle",
            "value": "Synopses of Oral Traditions (2)"
          },
          {
            "id": 409,
            "issue_id": "290",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18ii/18_2_cover.pdf"
          },
          {
            "id": 410,
            "issue_id": "290",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18ii/18_2_cover.jpg"
          },
          {
            "id": 411,
            "issue_id": "290",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18ii/18_2_front_matter.pdf"
          },
          {
            "id": 412,
            "issue_id": "290",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18ii/18_2_back_matter.pdf"
          },
          {
            "id": 413,
            "issue_id": "290",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18ii/editors_column_18_2.pdf"
          },
          {
            "id": 414,
            "issue_id": "290",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18ii/oraltradition_18_2.zip"
          },
          {
            "id": 415,
            "issue_id": "290",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18ii/18_2_complete.pdf"
          },
          {
            "id": 416,
            "issue_id": "290",
            "name": "Volume",
            "value": "18"
          },
          {
            "id": 417,
            "issue_id": "290",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 418,
            "issue_id": "290",
            "name": "Date",
            "value": "2003-10-01"
          }
        ]
      },
      {
        "id": 504,
        "name": "18i",
        "title": "Volume 18, Issue 1: Synopses of Oral Traditions (1)",
        "articles": [
          {
            "id": 10118,
            "title": "The People’s Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Steve Zeitlin"
              }
            ],
            "files": []
          },
          {
            "id": 10116,
            "title": "The Poem Performed",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Felice Belle"
              }
            ],
            "files": []
          },
          {
            "id": 10114,
            "title": "The Heike in Japan",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Elizabeth Oyler"
              }
            ],
            "files": []
          },
          {
            "id": 10112,
            "title": "Japanese Noh and Heike <em>katari</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Shelley Fenno Quinn"
              }
            ],
            "files": []
          },
          {
            "id": 10110,
            "title": "Japanese Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sybil Thornton"
              }
            ],
            "files": []
          },
          {
            "id": 10108,
            "title": "Performed Narratives and Music in Japan",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Alison Tokita"
              }
            ],
            "files": []
          },
          {
            "id": 10106,
            "title": "The Japanese Tale of the <em>Heike</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Yamashita Hiroaki"
              }
            ],
            "files": []
          },
          {
            "id": 10104,
            "title": "Oral Tradition in New Testament Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Richard A. Horsley"
              }
            ],
            "files": []
          },
          {
            "id": 10102,
            "title": "Oral Tradition and Rabbinic Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Martin S. Jaffee"
              }
            ],
            "files": []
          },
          {
            "id": 10100,
            "title": "Oral Tradition in Bible and New Testament Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Werner H. Kelber"
              }
            ],
            "files": []
          },
          {
            "id": 10098,
            "title": "Oral Tradition and Biblical Scholarship",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Susan Niditch"
              }
            ],
            "files": []
          },
          {
            "id": 10096,
            "title": "Performance Praxis and Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Elizabeth C. Fine"
              }
            ],
            "files": []
          },
          {
            "id": 10094,
            "title": "Tradition as Communication",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas A. McKean"
              }
            ],
            "files": []
          },
          {
            "id": 10092,
            "title": "Homer as Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Egbert J. Bakker "
              }
            ],
            "files": []
          },
          {
            "id": 10090,
            "title": "Oral Tradition and Hellenistic Epic: New Directions in Apollonius of Rhodes",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Michael Barnes"
              }
            ],
            "files": []
          },
          {
            "id": 10088,
            "title": "The Homeric Question: An Issue for the Ancients?",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "David Bouvier"
              }
            ],
            "files": []
          },
          {
            "id": 10086,
            "title": "Ancient Greek Oral Genres",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Casey Dué"
              }
            ],
            "files": []
          },
          {
            "id": 10084,
            "title": "Homer and the Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark W. Edwards"
              }
            ],
            "files": []
          },
          {
            "id": 10082,
            "title": "Neoanalysis and Oral Tradition in Homeric Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Margalit Finkelberg"
              }
            ],
            "files": []
          },
          {
            "id": 10080,
            "title": "The Grain of Greek Voices",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Richard Martin"
              }
            ],
            "files": []
          },
          {
            "id": 10078,
            "title": "Oral Poetics and Homeric Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Gregory Nagy"
              }
            ],
            "files": []
          },
          {
            "id": 10076,
            "title": "Homeric Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Steve Reece"
              }
            ],
            "files": []
          },
          {
            "id": 10074,
            "title": "The Reception of Homer as Oral Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "M. D. Usher"
              }
            ],
            "files": []
          },
          {
            "id": 10072,
            "title": "“Oral Tradition”: Weasel Words or Transdisciplinary Door to Multiplexity?",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ruth Finnegan"
              }
            ],
            "files": []
          },
          {
            "id": 10070,
            "title": "Zulu Oral Art",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "H.C. Groenewald"
              }
            ],
            "files": []
          },
          {
            "id": 10068,
            "title": "Oral Tradition in the Context of Verbal Art",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas A. Hale"
              }
            ],
            "files": []
          },
          {
            "id": 10066,
            "title": "The Global and the Local with a Focus on Africa",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Beverly Stoeltje"
              }
            ],
            "files": []
          },
          {
            "id": 10064,
            "title": "Orality in Tibet",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Anne Klein"
              }
            ],
            "files": []
          },
          {
            "id": 10062,
            "title": "The Metamorphosing Field of Chaoxianzu Oral Literature",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Peace B. Lee"
              }
            ],
            "files": []
          },
          {
            "id": 10060,
            "title": "Tibetan Oral Epic",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Yang Enhong"
              }
            ],
            "files": []
          },
          {
            "id": 10058,
            "title": "Oral Tradition in Lithuania",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lina Bugiene"
              }
            ],
            "files": []
          },
          {
            "id": 10056,
            "title": "Translating Lithuanian Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jonas Zdanys"
              }
            ],
            "files": []
          },
          {
            "id": 10054,
            "title": "The Perspective from Folklore Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Pertti Anttonen"
              }
            ],
            "files": []
          },
          {
            "id": 10052,
            "title": "Stumbling with/over Scripts: Vignettes",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Daniel Avorgbedor"
              }
            ],
            "files": []
          },
          {
            "id": 10050,
            "title": "Some Reflections on the “People’s Slam of Radivoje Ilić”: Thoughts on the Interplay of the Oral and Visual",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joel M. Halpern"
              }
            ],
            "files": []
          },
          {
            "id": 10048,
            "title": "Continual Morphing",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lee Haring"
              }
            ],
            "files": []
          },
          {
            "id": 10046,
            "title": "Frame Tales and Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bonnie D. Irwin"
              }
            ],
            "files": []
          },
          {
            "id": 10044,
            "title": "Oral Poetry in the Foreign Language Classroom",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Catharine Mason"
              }
            ],
            "files": []
          },
          {
            "id": 10042,
            "title": "Oral History",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Amy Shuman"
              }
            ],
            "files": []
          },
          {
            "id": 10040,
            "title": "A Plea for an Interdisciplinary Approach to the Study of Arab Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Saad A. Sowayan"
              }
            ],
            "files": []
          },
          {
            "id": 10038,
            "title": "“Oral Tradition” in a Technologically Advanced World",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Timothy R. Tangherlini"
              }
            ],
            "files": []
          },
          {
            "id": 10036,
            "title": "Oral Tradition and Folkloristics",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ülo Valk"
              }
            ],
            "files": []
          },
          {
            "id": 10034,
            "title": "Basque <em>Bertsolaritza</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Linda White"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 584,
            "issue_id": "504",
            "name": "Subtitle",
            "value": "Synopses of Oral Traditions (1)"
          },
          {
            "id": 585,
            "issue_id": "504",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18i/18_1_cover.pdf"
          },
          {
            "id": 586,
            "issue_id": "504",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18i/18_1_cover.jpg"
          },
          {
            "id": 587,
            "issue_id": "504",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18i/18_1_front_matter.pdf"
          },
          {
            "id": 588,
            "issue_id": "504",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18i/18_1_back_matter.pdf"
          },
          {
            "id": 589,
            "issue_id": "504",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18i/5_editor_column_18_1.pdf"
          },
          {
            "id": 590,
            "issue_id": "504",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18i/oraltradition_18_1.zip"
          },
          {
            "id": 591,
            "issue_id": "504",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/18i/18_1_complete.pdf"
          },
          {
            "id": 592,
            "issue_id": "504",
            "name": "Volume",
            "value": "18"
          },
          {
            "id": 593,
            "issue_id": "504",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 594,
            "issue_id": "504",
            "name": "Date",
            "value": "2003-03-01"
          }
        ]
      },
      {
        "id": 30,
        "name": "17ii",
        "title": "Volume 17, Issue 2",
        "articles": [
          {
            "id": 9662,
            "title": "No One Tells You This: Secondary Orality and Hypertextuality",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Michael Joyce"
              }
            ],
            "files": []
          },
          {
            "id": 9660,
            "title": "The Social and Dramatic Functions of Oral Recitation and Composition in <em>Beowulf</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John M. Hill"
              }
            ],
            "files": []
          },
          {
            "id": 9658,
            "title": "Rites of Passage and Oral Storytelling in Romanian Epic and the New Testament",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Margaret Hiebert Beissinger"
              }
            ],
            "files": []
          },
          {
            "id": 9656,
            "title": "The Minim-istic Imagination: Scribal Invention and the Word in the Early English Alliterative Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Johnathan Watson"
              }
            ],
            "files": []
          },
          {
            "id": 9654,
            "title": "Transforming Experience into Tradition: Two Theories of Proverb Use and Chaucer’s Practice",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Nancy Mason Bradbury"
              }
            ],
            "files": []
          },
          {
            "id": 9652,
            "title": "Cynewulf at the Interface of Literacy and Orality: The Evidence of the Puns in <em>Beowulf</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Samantha Zacher"
              }
            ],
            "files": []
          },
          {
            "id": 9650,
            "title": "Ubiquitous Format? What Ubiquitous Format? Chaucer’s <em>Tale of Melibee</em> as a Proverb Collection",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Betsy Bowden"
              }
            ],
            "files": []
          },
          {
            "id": 9648,
            "title": "Written on the Wind: An Introduction to Auralture",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Vladimir Guerrero"
              }
            ],
            "files": []
          },
          {
            "id": 8397,
            "title": "Oral Tradition and Contemporary Critical Theory. II",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark C. Amodio "
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 34,
            "issue_id": "30",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 35,
            "issue_id": "30",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17ii/17_2_cover.pdf"
          },
          {
            "id": 36,
            "issue_id": "30",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17ii/17_2_cover.jpg"
          },
          {
            "id": 37,
            "issue_id": "30",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17ii/17_2_front_matter.pdf"
          },
          {
            "id": 38,
            "issue_id": "30",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17ii/17_2_back_matter.pdf"
          },
          {
            "id": 39,
            "issue_id": "30",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17ii/editors_column.pdf"
          },
          {
            "id": 40,
            "issue_id": "30",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17ii/oraltradition_17_ii.zip"
          },
          {
            "id": 41,
            "issue_id": "30",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17ii/17_2_complete.pdf"
          },
          {
            "id": 42,
            "issue_id": "30",
            "name": "Volume",
            "value": "17"
          },
          {
            "id": 43,
            "issue_id": "30",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 44,
            "issue_id": "30",
            "name": "Date",
            "value": "2002-10-01"
          }
        ]
      },
      {
        "id": 32,
        "name": "17i",
        "title": "Volume 17, Issue 1",
        "articles": [
          {
            "id": 9674,
            "title": "The Case of the Gospels: Memory’s Desire and the Limits of Historical Criticism",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Werner H. Kelber"
              }
            ],
            "files": []
          },
          {
            "id": 9672,
            "title": "Homespun Homerics in the Kingdom of Aeolus: <em>Ninu Murina</em> in Stromboli",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "LindaAnn Loschiavo"
              }
            ],
            "files": []
          },
          {
            "id": 9670,
            "title": "On the Use and Abuse of “Orality” for Art: Reflections on Romantic and Late Twentieth-Century <em>Poiesis</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Maureen N. McLane"
              }
            ],
            "files": []
          },
          {
            "id": 9668,
            "title": "Interpreting Lyric Meaning in Irish Tradition: Love and Death in the Shadow of Tralee",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas A. DuBois"
              }
            ],
            "files": []
          },
          {
            "id": 9666,
            "title": "The Poetics of Arapaho Storytelling: From Salvage to Performance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Andrew Cowell"
              }
            ],
            "files": []
          },
          {
            "id": 9664,
            "title": "India’s “Hundred Voices”: Subaltern Oral Performance in Forsters <em>A Passage to India</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John McBratney"
              }
            ],
            "files": []
          },
          {
            "id": 8399,
            "title": "Oral Tradition and Contemporary Critical Theory. I",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark C. Amodio "
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 45,
            "issue_id": "32",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 46,
            "issue_id": "32",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17i/17_1_cover.pdf"
          },
          {
            "id": 47,
            "issue_id": "32",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17i/17_1_cover.jpg"
          },
          {
            "id": 48,
            "issue_id": "32",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17i/17_1_front_matter.pdf"
          },
          {
            "id": 49,
            "issue_id": "32",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17i/17_1_back_matter.pdf"
          },
          {
            "id": 50,
            "issue_id": "32",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17i/editors_column.pdf"
          },
          {
            "id": 51,
            "issue_id": "32",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17i/oraltradition_17_1.zip"
          },
          {
            "id": 52,
            "issue_id": "32",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/17i/17_1_complete.pdf"
          },
          {
            "id": 53,
            "issue_id": "32",
            "name": "Volume",
            "value": "17"
          },
          {
            "id": 54,
            "issue_id": "32",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 55,
            "issue_id": "32",
            "name": "Date",
            "value": "2002-03-01"
          }
        ]
      },
      {
        "id": 316,
        "name": "16ii",
        "title": "Volume 16, Issue 2: Chinese Oral Traditions",
        "articles": [
          {
            "id": 10138,
            "title": "A Preliminary Analysis of the Oral Shamanistic Songs of the Manchus",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Song Heping"
              }
            ],
            "files": []
          },
          {
            "id": 10136,
            "title": "The Bard Jusup Mamay ",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lang Ying"
              }
            ],
            "files": []
          },
          {
            "id": 10134,
            "title": "Nakhi Tiger Myth in its Context  ",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bai Gengsheng"
              }
            ],
            "files": []
          },
          {
            "id": 10132,
            "title": "A Brief Account of <em>Bensen Ülger</em> and <em>Ülgeren Bense</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": " Zhalgaa"
              }
            ],
            "files": []
          },
          {
            "id": 10130,
            "title": "<em>Bab Sgrung</em>: Tibetan Epic Singers  ",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Zhambei Gyaltsho"
              }
            ],
            "files": []
          },
          {
            "id": 10128,
            "title": "On the Study of the Narrative Structure of Tibetan Epic: <em>A Record of King Gesar</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Yang Enhong"
              }
            ],
            "files": []
          },
          {
            "id": 10126,
            "title": "History and the Tibetan Epic <em>Gesar</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Li Lianrong"
              }
            ],
            "files": []
          },
          {
            "id": 10124,
            "title": "The Mythology of Tibetan Mountain Gods: An Overview ",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Xie Jisheng"
              }
            ],
            "files": []
          },
          {
            "id": 10122,
            "title": "The Rhinoceros Totem and Pangu Myth: An Exploration of the Archetype of Pangu",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Wu Xiaodong"
              }
            ],
            "files": []
          },
          {
            "id": 10120,
            "title": "Mongolian-Turkic Epics: Typological Formation and Development ",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": " Rinchindorji"
              }
            ],
            "files": []
          },
          {
            "id": 9590,
            "title": "The Oirat Epic Cycle Jangar",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Chao Gejin"
              }
            ],
            "files": []
          },
          {
            "id": 9588,
            "title": "Dong Oral Poetry: <em>Kuant Cix</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Deng Minwen"
              }
            ],
            "files": []
          },
          {
            "id": 9586,
            "title": "Traditional Nuosu Origin Narratives: A Case Study of Ritualized Epos in <em>Bimo</em> Incantation Scriptures",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bamo Qubumo"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 452,
            "issue_id": "316",
            "name": "Subtitle",
            "value": "Chinese Oral Traditions"
          },
          {
            "id": 453,
            "issue_id": "316",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16ii/16_2_cover.pdf"
          },
          {
            "id": 454,
            "issue_id": "316",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16ii/16_2_cover.jpg"
          },
          {
            "id": 455,
            "issue_id": "316",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16ii/16_2_front_matter.pdf"
          },
          {
            "id": 456,
            "issue_id": "316",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16ii/16_2_back_matter.pdf"
          },
          {
            "id": 457,
            "issue_id": "316",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16ii/3_editors_column_16_2.pdf"
          },
          {
            "id": 458,
            "issue_id": "316",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16ii/oraltradition_16_2.zip"
          },
          {
            "id": 459,
            "issue_id": "316",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16ii/16_2_complete.pdf"
          },
          {
            "id": 460,
            "issue_id": "316",
            "name": "Volume",
            "value": "16"
          },
          {
            "id": 461,
            "issue_id": "316",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 462,
            "issue_id": "316",
            "name": "Date",
            "value": "2001-10-01"
          }
        ]
      },
      {
        "id": 543,
        "name": "16i",
        "title": "Volume 16, Issue 1",
        "articles": [
          {
            "id": 10152,
            "title": "Orality and Basque Nationalism: Dancing with the Devil or Waltzing into the Future?",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Linda White"
              }
            ],
            "files": []
          },
          {
            "id": 10150,
            "title": "I Control the Idioms: Creativity in Ndebele Praise Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "H.C. Groenewald"
              }
            ],
            "files": []
          },
          {
            "id": 10148,
            "title": "Milman Parry and A. L. Kroeber: Americanist Anthropology and the Oral Homer",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John F. García"
              }
            ],
            "files": []
          },
          {
            "id": 10146,
            "title": "Personal Favor and Public Influence: Arete, Arsinoë II, and the <em>Argonautica</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Anatole Mori"
              }
            ],
            "files": []
          },
          {
            "id": 10144,
            "title": "The Limits of Textuality: Mobility and Fire Production in Homer and <em>Beowulf</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Guillemette Bolens"
              }
            ],
            "files": []
          },
          {
            "id": 10142,
            "title": "Homer and Rhapsodic Competition in Performance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Derek Collins"
              }
            ],
            "files": []
          },
          {
            "id": 10140,
            "title": "Performance and Norse Poetry: The Hydromel of Praise and the Effluvia of Scorn",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Stephen Mitchell"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 595,
            "issue_id": "543",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 596,
            "issue_id": "543",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16i/16_1_cover.pdf"
          },
          {
            "id": 597,
            "issue_id": "543",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16i/16_1_cover.jpg"
          },
          {
            "id": 598,
            "issue_id": "543",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16i/16_1_front_matter.pdf"
          },
          {
            "id": 599,
            "issue_id": "543",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16i/16_1_back_matter.pdf"
          },
          {
            "id": 600,
            "issue_id": "543",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16i/editors_column.pdf"
          },
          {
            "id": 601,
            "issue_id": "543",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16i/oraltradition_16_1.zip"
          },
          {
            "id": 602,
            "issue_id": "543",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/16i/16_1_complete.pdf"
          },
          {
            "id": 603,
            "issue_id": "543",
            "name": "Volume",
            "value": "16"
          },
          {
            "id": 604,
            "issue_id": "543",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 605,
            "issue_id": "543",
            "name": "Date",
            "value": "2001-03-01"
          }
        ]
      },
      {
        "id": 548,
        "name": "15ii",
        "title": "Volume 15, Issue 2",
        "articles": [
          {
            "id": 10164,
            "title": "<em>Konodai senki</em>: Traditional Narrative and Warrior Ideology in Sixteenth-Century Japan",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sybil Thornton"
              }
            ],
            "files": []
          },
          {
            "id": 10162,
            "title": "On the Linguistic Properties of Formulaic Speech ",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Koenraad Kuiper"
              }
            ],
            "files": []
          },
          {
            "id": 10160,
            "title": "Body, Performance, and Agency in <em>Kalevala</em> Rune-Singing",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Anna-Leena Siikala"
              }
            ],
            "files": []
          },
          {
            "id": 10158,
            "title": "Collaborative Auto/biography: Notes on an Interview with Margaret McCord on <em>The Calling of Katie Makanya</em>: A Memoir of South Africa",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Stephan Meyer"
              }
            ],
            "files": []
          },
          {
            "id": 10156,
            "title": "Altai Oral Epic ",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lauri Harvilahti"
              }
            ],
            "files": []
          },
          {
            "id": 10154,
            "title": "Tradition, Performance, and Poetics in the Early Middle English Period",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark C. Amodio "
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 606,
            "issue_id": "548",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 607,
            "issue_id": "548",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15ii/15_2_cover.pdf"
          },
          {
            "id": 608,
            "issue_id": "548",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15ii/15_2_cover.jpg"
          },
          {
            "id": 609,
            "issue_id": "548",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15ii/15_2_front_matter.pdf"
          },
          {
            "id": 610,
            "issue_id": "548",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15ii/15_2_back_matter.pdf"
          },
          {
            "id": 611,
            "issue_id": "548",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15ii/1_editors_column.pdf"
          },
          {
            "id": 612,
            "issue_id": "548",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15ii/oraltradition_15_2.zip"
          },
          {
            "id": 613,
            "issue_id": "548",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15ii/15_2_complete.pdf"
          },
          {
            "id": 614,
            "issue_id": "548",
            "name": "Volume",
            "value": "15"
          },
          {
            "id": 615,
            "issue_id": "548",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 616,
            "issue_id": "548",
            "name": "Date",
            "value": "2000-10-01"
          }
        ]
      },
      {
        "id": 261,
        "name": "15i",
        "title": "Volume 15, Issue 1",
        "articles": [
          {
            "id": 10180,
            "title": "<em>Beowulf</em> as Epic",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joseph Harris"
              }
            ],
            "files": []
          },
          {
            "id": 10178,
            "title": "Thor’s Visit to Útgarðaloki",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Lindow"
              }
            ],
            "files": []
          },
          {
            "id": 10176,
            "title": "<em>Ex Ovo Omnia</em>: Where Does the Balto-Finnic Cosmogony Originate? ",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ülo Valk"
              }
            ],
            "files": []
          },
          {
            "id": 10174,
            "title": "The Narrator’s Voice in <em>Kalevala</em> and <em>Kalevipoeg</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas A. DuBois"
              }
            ],
            "files": []
          },
          {
            "id": 10172,
            "title": "Text, Orality, Literacy, Tradition, Dictation, Education, and Other Paradigms of Explication in Greek Literary Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Barry B. Powell"
              }
            ],
            "files": []
          },
          {
            "id": 10170,
            "title": "Cycle Construction and Character Development in Central Algonkian Trickster Tales",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Andrew Wiget"
              }
            ],
            "files": []
          },
          {
            "id": 10168,
            "title": "Dario Fo and Oral Tradition: Creating a Thematic Context ",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Antonio Scuderi"
              }
            ],
            "files": []
          },
          {
            "id": 10166,
            "title": "The “Trick” of Narratives: History, Memory, and Performance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Chiji Akoma"
              }
            ],
            "files": []
          },
          {
            "id": 9437,
            "title": "“O man do not scribble on the book”: Print and Counter-print in a Scottish Enlightenment University",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Matthew Simpson"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 331,
            "issue_id": "261",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 332,
            "issue_id": "261",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15i/15_1_cover.pdf"
          },
          {
            "id": 333,
            "issue_id": "261",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15i/15_1_cover.jpg"
          },
          {
            "id": 334,
            "issue_id": "261",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15i/15_1_front_matter.pdf"
          },
          {
            "id": 335,
            "issue_id": "261",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15i/15_1_back_matter.pdf"
          },
          {
            "id": 336,
            "issue_id": "261",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15i/1_editors_column_15_1.pdf"
          },
          {
            "id": 337,
            "issue_id": "261",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15i/oraltradition_15_1.zip"
          },
          {
            "id": 338,
            "issue_id": "261",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/15i/15_1_complete.pdf"
          },
          {
            "id": 339,
            "issue_id": "261",
            "name": "Volume",
            "value": "15"
          },
          {
            "id": 340,
            "issue_id": "261",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 341,
            "issue_id": "261",
            "name": "Date",
            "value": "2000-03-01"
          }
        ]
      },
      {
        "id": 309,
        "name": "14ii",
        "title": "Volume 14, Issue 2",
        "articles": [
          {
            "id": 10184,
            "title": "“Ah ain’t heard whut do tex’ wuz”: The (Il)legitimate Textuality of Old English and Black English",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Michael Saenger"
              }
            ],
            "files": []
          },
          {
            "id": 10182,
            "title": "A Treasury of Formulaic Narrative: the Persian Popular Romance <em>Hosein-e Kord</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ulrich Marzolph"
              }
            ],
            "files": []
          },
          {
            "id": 9584,
            "title": "<em>Epea Pteroenta</em> (“Winged Words”)",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Françoise Létoublon"
              }
            ],
            "files": []
          },
          {
            "id": 9582,
            "title": "Serial Repetition in Homer and the “Poetics of Talk”: A Case Study from the <em>Odyssey</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Elizabeth  Minchin"
              }
            ],
            "files": []
          },
          {
            "id": 9580,
            "title": "Oral-Formulaic Approaches to Coptic Hymnography",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Leslie  MacCoull"
              }
            ],
            "files": []
          },
          {
            "id": 9578,
            "title": "The Inscription of Charms in Anglo-Saxon Manuscripts",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lea Olsan"
              }
            ],
            "files": []
          },
          {
            "id": 9576,
            "title": "Speakerly Women and Scribal Men",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Christine  Neufeld"
              }
            ],
            "files": []
          },
          {
            "id": 9574,
            "title": "Writing as Relic: The Use of Oral Discourse to Interpret Written Texts in the Old French <em>La Queste del Saint</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lisa  Robeson"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 441,
            "issue_id": "309",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 442,
            "issue_id": "309",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14ii/14_2_cover.pdf"
          },
          {
            "id": 443,
            "issue_id": "309",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14ii/14_2_cover.jpg"
          },
          {
            "id": 444,
            "issue_id": "309",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14ii/14_2_front_matter.pdf"
          },
          {
            "id": 445,
            "issue_id": "309",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14ii/14_2_back_matter.pdf"
          },
          {
            "id": 446,
            "issue_id": "309",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14ii/1_editor_column_14_2.pdf"
          },
          {
            "id": 447,
            "issue_id": "309",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14ii/oraltradition_14_2.zip"
          },
          {
            "id": 448,
            "issue_id": "309",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14ii/14_2_complete.pdf"
          },
          {
            "id": 449,
            "issue_id": "309",
            "name": "Volume",
            "value": "14"
          },
          {
            "id": 450,
            "issue_id": "309",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 451,
            "issue_id": "309",
            "name": "Date",
            "value": "1999-10-01"
          }
        ]
      },
      {
        "id": 319,
        "name": "14i",
        "title": "Volume 14, Issue 1: Hebrew Oral Traditions",
        "articles": [
          {
            "id": 10192,
            "title": "Jewish Folk Literature",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Dan Ben-Amos"
              }
            ],
            "files": []
          },
          {
            "id": 10190,
            "title": "The Fixing of the Oral Mishnah and the Displacement of Meaning",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Elizabeth Shanks Alexander"
              }
            ],
            "files": []
          },
          {
            "id": 10188,
            "title": "Literary Composition and Oral Performance in Early Midrashim",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Steven D. Fraade"
              }
            ],
            "files": []
          },
          {
            "id": 10186,
            "title": "Oral Tradition in the Writings of Rabbinic Oral Torah: On Theorizing Rabbinic Orality",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Martin S. Jaffee"
              }
            ],
            "files": []
          },
          {
            "id": 9592,
            "title": "Orality and the Redaction of the Babylonian Talmud",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Yaakov Elman"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 463,
            "issue_id": "319",
            "name": "Subtitle",
            "value": "Hebrew Oral Traditions"
          },
          {
            "id": 464,
            "issue_id": "319",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14i/14_1_cover.pdf"
          },
          {
            "id": 465,
            "issue_id": "319",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14i/14_1_cover.jpg"
          },
          {
            "id": 466,
            "issue_id": "319",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14i/14_1_front_matter.pdf"
          },
          {
            "id": 467,
            "issue_id": "319",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14i/14_1_back_matter.pdf"
          },
          {
            "id": 468,
            "issue_id": "319",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14i/3_editor_column_14-1.pdf"
          },
          {
            "id": 469,
            "issue_id": "319",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14i/oraltradition_14_1.zip"
          },
          {
            "id": 470,
            "issue_id": "319",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/14i/14_1_complete.pdf"
          },
          {
            "id": 471,
            "issue_id": "319",
            "name": "Volume",
            "value": "14"
          },
          {
            "id": 472,
            "issue_id": "319",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 473,
            "issue_id": "319",
            "name": "Date",
            "value": "1999-03-01"
          }
        ]
      },
      {
        "id": 561,
        "name": "13ii",
        "title": "Volume 13, Issue 2",
        "articles": [
          {
            "id": 10210,
            "title": "Cultural Assimilation in <em>Njáls saga</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Craig R. Davis"
              }
            ],
            "files": []
          },
          {
            "id": 10208,
            "title": "The Creation of the Ancient Greek Epic Cycle",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ingrid Holmberg"
              }
            ],
            "files": []
          },
          {
            "id": 10206,
            "title": "A Comparative Study of the Singing Styles of Mongolian and Tibetan <em>Geser/Gesar</em> Artists",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Yang Enhong"
              }
            ],
            "files": []
          },
          {
            "id": 10204,
            "title": "Oral English in South African Theater in the 1980s",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Yvonne Banning"
              }
            ],
            "files": []
          },
          {
            "id": 10202,
            "title": "Translation and Orality in the Old English <em>Orosius</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Deborah VanderBilt"
              }
            ],
            "files": []
          },
          {
            "id": 10200,
            "title": "E-Texts: The Orality and Literacy Issue Revisited",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bruce Lionel Mason"
              }
            ],
            "files": []
          },
          {
            "id": 10198,
            "title": "Suzhou <em>Tanci</em> Storytelling in China: Contexts of Performance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark Bender"
              }
            ],
            "files": []
          },
          {
            "id": 10196,
            "title": "“Signs on a white field”: A Look at Orality <em>in</em> Literacy and James Joyce’s <em>Ulysses</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sabine Habermalz"
              }
            ],
            "files": []
          },
          {
            "id": 10194,
            "title": "Reflections on Myth and History: Tuareg Concepts of Truth, “Lies,” and “Children’s Tales”",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Susan J. Rasmussen"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 617,
            "issue_id": "561",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 618,
            "issue_id": "561",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13ii/13_2_cover.pdf"
          },
          {
            "id": 619,
            "issue_id": "561",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13ii/13_2_cover.jpg"
          },
          {
            "id": 620,
            "issue_id": "561",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13ii/13_2_front_matter.pdf"
          },
          {
            "id": 621,
            "issue_id": "561",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13ii/13_2_back_matter.pdf"
          },
          {
            "id": 622,
            "issue_id": "561",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13ii/1d_editor_column_13_2.pdf"
          },
          {
            "id": 623,
            "issue_id": "561",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13ii/oraltradition_13_2.zip"
          },
          {
            "id": 624,
            "issue_id": "561",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13ii/13_2_complete.pdf"
          },
          {
            "id": 625,
            "issue_id": "561",
            "name": "Volume",
            "value": "13"
          },
          {
            "id": 626,
            "issue_id": "561",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 627,
            "issue_id": "561",
            "name": "Date",
            "value": "1998-10-01"
          }
        ]
      },
      {
        "id": 569,
        "name": "13i",
        "title": "Volume 13, Issue 1: Native American Oral Traditions: Collaboration and Interpretation",
        "articles": [
          {
            "id": 10226,
            "title": "“There Are No More Words to the Story”",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Elsie P. Mather"
              },
              {
                "name": "Phyllis  Morrow"
              }
            ],
            "files": []
          },
          {
            "id": 10224,
            "title": "Coyote and the Strawberries: Cultural Drama and Intercultural Collaboration",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Barre Toelken"
              },
              {
                "name": "George B.  Wasson"
              }
            ],
            "files": []
          },
          {
            "id": 10222,
            "title": "Collaborative Sociolinguistic Research among the Tohono O’odham",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ofelia Zepeda"
              },
              {
                "name": "Jane Hill"
              }
            ],
            "files": []
          },
          {
            "id": 10220,
            "title": "“Wu-ches-erik (Loon Woman) and Ori-aswe (Wildcat)”",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Darryl Babe Wilson"
              },
              {
                "name": "Susan  Brandenstein Park"
              }
            ],
            "files": []
          },
          {
            "id": 10218,
            "title": "Reading Martha Lamont’s Crow Story Today",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Marya Moses"
              },
              {
                "name": "Toby C. S.  Langen"
              }
            ],
            "files": []
          },
          {
            "id": 10216,
            "title": "Tracking “Yuwaan Gagéets”: A Russian Fairy Tale in Tlingit Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Nora Marks Dauenhauer"
              },
              {
                "name": "Richard L. Dauenhauer"
              }
            ],
            "files": []
          },
          {
            "id": 10214,
            "title": "“Like this it stays in your hands”: Collaboration and Ethnopoetics",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Larry  Evers"
              },
              {
                "name": "Felipe S. Molina"
              }
            ],
            "files": []
          },
          {
            "id": 10212,
            "title": "Introduction: Collaboration in the translation and Interpretation of Native American Oral Traditions",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Barre Toelken"
              },
              {
                "name": "Larry  Evers"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 628,
            "issue_id": "569",
            "name": "Subtitle",
            "value": "Native American Oral Traditions: Collaboration and Interpretation"
          },
          {
            "id": 629,
            "issue_id": "569",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13i/13_1_cover.pdf"
          },
          {
            "id": 630,
            "issue_id": "569",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13i/13_1_cover.jpg"
          },
          {
            "id": 631,
            "issue_id": "569",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13i/13_1_front_matter.pdf"
          },
          {
            "id": 632,
            "issue_id": "569",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13i/13_1_back_matter.pdf"
          },
          {
            "id": 633,
            "issue_id": "569",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13i/5_intro.pdf"
          },
          {
            "id": 634,
            "issue_id": "569",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13i/oraltradition_13_1.zip"
          },
          {
            "id": 635,
            "issue_id": "569",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/13i/13_1_complete.pdf"
          },
          {
            "id": 636,
            "issue_id": "569",
            "name": "Volume",
            "value": "13"
          },
          {
            "id": 637,
            "issue_id": "569",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 638,
            "issue_id": "569",
            "name": "Date",
            "value": "1998-03-01"
          }
        ]
      },
      {
        "id": 584,
        "name": "12ii",
        "title": "Volume 12, Issue 2",
        "articles": [
          {
            "id": 10238,
            "title": "Annotated Bibliography 1986-1990",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Catherine Quick"
              }
            ],
            "files": []
          },
          {
            "id": 10236,
            "title": "Mongolian Oral Epic Poetry: An Overview",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Chao Gejin"
              }
            ],
            "files": []
          },
          {
            "id": 10234,
            "title": "Two Performances of the “Return of Alpamish”: Current Performance-Practice in the Uzbek Oral Epic of the Sherabad School",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Walter Feldman"
              }
            ],
            "files": []
          },
          {
            "id": 10232,
            "title": "“The Battle with the Monster”: Transformation of a Traditional Pattern in “The Dream of the Rood”",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Leslie Stratyner"
              }
            ],
            "files": []
          },
          {
            "id": 10230,
            "title": "Sink or Swim: On Associative Stucturing in Longer Latvian Folk Songs",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Vaira Vikis-Freibergs"
              }
            ],
            "files": []
          },
          {
            "id": 10228,
            "title": "From the <em>Griot</em> of Roots to the Roots of <em>Griot</em>: A New Look at the Origins of a Controversial African Term for Bard",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas A. Hale"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 639,
            "issue_id": "584",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 640,
            "issue_id": "584",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12ii/12_2_cover.pdf"
          },
          {
            "id": 641,
            "issue_id": "584",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12ii/12_2_cover.jpg"
          },
          {
            "id": 642,
            "issue_id": "584",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12ii/12_2_front_matter.pdf"
          },
          {
            "id": 643,
            "issue_id": "584",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12ii/12_2_back_matter.pdf"
          },
          {
            "id": 644,
            "issue_id": "584",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12ii/1d_editor_column_12_2.pdf"
          },
          {
            "id": 645,
            "issue_id": "584",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12ii/oraltradition_12_2.zip"
          },
          {
            "id": 646,
            "issue_id": "584",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12ii/12_2_complete.pdf"
          },
          {
            "id": 647,
            "issue_id": "584",
            "name": "Volume",
            "value": "12"
          },
          {
            "id": 648,
            "issue_id": "584",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 649,
            "issue_id": "584",
            "name": "Date",
            "value": "1997-10-01"
          }
        ]
      },
      {
        "id": 588,
        "name": "12i",
        "title": "Volume 12, Issue 1: South Asian Oral Traditions",
        "articles": [
          {
            "id": 10254,
            "title": "“A Flowering Tree”: A Woman’s Tale",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "A. K. Ramanujan"
              }
            ],
            "files": []
          },
          {
            "id": 10252,
            "title": "Negotiated Solidarities: Gendered Representations of Disruption and Desire in North Indian Oral Traditions and Popular Culture",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Gloria Goodwin Raheja"
              }
            ],
            "files": []
          },
          {
            "id": 10250,
            "title": "Two Houses and the Pain of Separation in Tamang Narratives from Highland Nepal",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Kathryn S. March"
              }
            ],
            "files": []
          },
          {
            "id": 10248,
            "title": "Outspoken Women: Representations of Female Voices in a Rajasthani Folklore Community",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ann Grodzins Gold"
              }
            ],
            "files": []
          },
          {
            "id": 10246,
            "title": "“There are Only Two Castes: Men and Women”: Negotiating Gender as a Female Healer in South Asian Islam",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joyce Burkhalter Flueckiger"
              }
            ],
            "files": []
          },
          {
            "id": 10244,
            "title": "Singing From Separation: Women’s Voices in and about Kangra Folksongs",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Kirin Narayan"
              }
            ],
            "files": []
          },
          {
            "id": 10242,
            "title": "The Beggared Mother: Older Women’s Narratives in West Bengal",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sarah Lamb"
              }
            ],
            "files": []
          },
          {
            "id": 10240,
            "title": "The Paradoxes of Power and Community: Women’s Oral Traditions and the Uses of Ethnography",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Gloria Goodwin Raheja"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 650,
            "issue_id": "588",
            "name": "Subtitle",
            "value": "South Asian Oral Traditions"
          },
          {
            "id": 651,
            "issue_id": "588",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12i/12_1_cover.pdf"
          },
          {
            "id": 652,
            "issue_id": "588",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12i/12_1_cover.jpg"
          },
          {
            "id": 653,
            "issue_id": "588",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12i/12_1_front_matter.pdf"
          },
          {
            "id": 654,
            "issue_id": "588",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12i/12_1_back_matter.pdf"
          },
          {
            "id": 655,
            "issue_id": "588",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12i/1e_Raheja_Introduction.pdf"
          },
          {
            "id": 656,
            "issue_id": "588",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12i/oraltradition_12_1.zip"
          },
          {
            "id": 657,
            "issue_id": "588",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/12i/12_1_complete.pdf"
          },
          {
            "id": 658,
            "issue_id": "588",
            "name": "Volume",
            "value": "12"
          },
          {
            "id": 659,
            "issue_id": "588",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 660,
            "issue_id": "588",
            "name": "Date",
            "value": "1997-03-01"
          }
        ]
      },
      {
        "id": 301,
        "name": "11ii",
        "title": "Volume 11, Issue 2",
        "articles": [
          {
            "id": 9572,
            "title": "The Mechanism of the Ancient Ballad: William Motherwell’s Explanation",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mary Ellen  Brown"
              }
            ],
            "files": []
          },
          {
            "id": 9570,
            "title": "Who Heard the Rhymes, and How: Shakespeare’s Dramaturgical Signals",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Burton Raffel"
              }
            ],
            "files": []
          },
          {
            "id": 9568,
            "title": "Orality and Literacy in the <em>Commedia dell’Arte</em> and the Shakespearean Clown",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Robert  Henke"
              }
            ],
            "files": []
          },
          {
            "id": 9566,
            "title": "A Furified Freestyle: Homer and Hip Hop",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Erik  Pihel"
              }
            ],
            "files": []
          },
          {
            "id": 9564,
            "title": "The <em>Kalevala</em> Received: From Printed Text to Oral Performance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas A. DuBois"
              }
            ],
            "files": []
          },
          {
            "id": 9562,
            "title": "Early Voice Recordings of Japanese Storytelling",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "J. Scott  Miller"
              }
            ],
            "files": []
          },
          {
            "id": 9560,
            "title": "“In Forme of Speche” is Anxiety: Orality in Chaucer’s <em>House of Fame</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Leslie K.  Arnovick"
              }
            ],
            "files": []
          },
          {
            "id": 9558,
            "title": "A Narrative Technique in <em>Beowulf</em> and Homeric Epic",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bruce  Louden"
              }
            ],
            "files": []
          },
          {
            "id": 9556,
            "title": "<em>Ei Pote</em>: A Note on Homeric Phraseology",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "R. Scott Garner"
              }
            ],
            "files": []
          },
          {
            "id": 9554,
            "title": "In Defense of Milman Parry: Renewing the Oral Theory",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William Merrit Sale"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 430,
            "issue_id": "301",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 431,
            "issue_id": "301",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11ii/11_2_cover.pdf"
          },
          {
            "id": 432,
            "issue_id": "301",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11ii/11_2_cover.jpg"
          },
          {
            "id": 433,
            "issue_id": "301",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11ii/11_2_front_matter.pdf"
          },
          {
            "id": 434,
            "issue_id": "301",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11ii/11_2_back_matter.pdf"
          },
          {
            "id": 435,
            "issue_id": "301",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11ii/4_editor_column_11_2.pdf"
          },
          {
            "id": 436,
            "issue_id": "301",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11ii/oraltradition_11_2.zip"
          },
          {
            "id": 437,
            "issue_id": "301",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11ii/11_2_complete.pdf"
          },
          {
            "id": 438,
            "issue_id": "301",
            "name": "Volume",
            "value": "11"
          },
          {
            "id": 439,
            "issue_id": "301",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 440,
            "issue_id": "301",
            "name": "Date",
            "value": "1996-10-01"
          }
        ]
      },
      {
        "id": 592,
        "name": "11i",
        "title": "Volume 11, Issue 1: Epics Along the Silk Roads",
        "articles": [
          {
            "id": 10276,
            "title": "Transformations of Epic Time and Space: Creating the World’s Creation in Kalevala-metric Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lotte Tarkka"
              }
            ],
            "files": []
          },
          {
            "id": 10274,
            "title": "<em>Kudaman</em>: An Oral Epic in the Palawan Highlands",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Nicole Revel"
              }
            ],
            "files": []
          },
          {
            "id": 10272,
            "title": "Epics in the Oral Genre System of Tulunadu",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "B.A. Viveka Rai"
              }
            ],
            "files": []
          },
          {
            "id": 10270,
            "title": "The Mechanisms of Epic Plot and the Mongolian Geseriad",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "S. Ju. Nekljudov"
              }
            ],
            "files": []
          },
          {
            "id": 10268,
            "title": "From Classical to Postclassical: Changing Ideologies and Changing Epics in India",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Petteri Koskikallio"
              }
            ],
            "files": []
          },
          {
            "id": 10266,
            "title": "Introduction<br />Epics along the Silk Roads: Mental Texts, Performance, and Written Codification",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lauri Honko"
              }
            ],
            "files": []
          },
          {
            "id": 10264,
            "title": "Epic and Identity: National, Regional, Communal, Individual",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lauri Honko"
              }
            ],
            "files": []
          },
          {
            "id": 10262,
            "title": "The Present State of the Mongolian Epic and Some Topics for Future Research",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Walter Heissig"
              }
            ],
            "files": []
          },
          {
            "id": 10260,
            "title": "Epos and National Identity: Transformations and Incarnations",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lauri Harvilahti"
              }
            ],
            "files": []
          },
          {
            "id": 10258,
            "title": "G.J. Ramstedt as a Recorder of Khalkha Epics",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Harry Halén"
              }
            ],
            "files": []
          },
          {
            "id": 10256,
            "title": "Caucasian Epics: Textualist Principles in Publishing",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Alla Alieva"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 661,
            "issue_id": "592",
            "name": "Subtitle",
            "value": "Epics Along the Silk Roads"
          },
          {
            "id": 662,
            "issue_id": "592",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11i/11_1_cover.pdf"
          },
          {
            "id": 663,
            "issue_id": "592",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11i/11_1_cover.jpg"
          },
          {
            "id": 664,
            "issue_id": "592",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11i/11_1_front_matter.pdf"
          },
          {
            "id": 665,
            "issue_id": "592",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 666,
            "issue_id": "592",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11i/5_Introduction_11_1.pdf"
          },
          {
            "id": 667,
            "issue_id": "592",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11i/oraltradition_11_1.zip"
          },
          {
            "id": 668,
            "issue_id": "592",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/11i/11_1_complete.pdf"
          },
          {
            "id": 669,
            "issue_id": "592",
            "name": "Volume",
            "value": "11"
          },
          {
            "id": 670,
            "issue_id": "592",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 671,
            "issue_id": "592",
            "name": "Date",
            "value": "1996-03-01"
          }
        ]
      },
      {
        "id": 257,
        "name": "10ii",
        "title": "Volume 10, Issue 2",
        "articles": [
          {
            "id": 10288,
            "title": "Word, Breath, and Vomit: Oral Competition in Old English and Old Norse Literature",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Robin Waugh"
              }
            ],
            "files": []
          },
          {
            "id": 10286,
            "title": "Generic and Racial Appropriation in Victoria Howard’s “The Honorable Milt”",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jarold Ramsey"
              }
            ],
            "files": []
          },
          {
            "id": 10284,
            "title": "Oral Register in the Biblical Libretto: Towards a Biblical Poetic",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Susan Niditch"
              }
            ],
            "files": []
          },
          {
            "id": 10282,
            "title": "Immanence and Immanent Truth",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John H. McDowell"
              }
            ],
            "files": []
          },
          {
            "id": 10280,
            "title": "Narrative Tradition in Early Greek Oral Poetry and Vase Painting",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "E.A. Mackay"
              }
            ],
            "files": []
          },
          {
            "id": 10278,
            "title": "<em>Chaucer New Painted</em> (1623):<br />Three Hundred Proverbs in Performance Context",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Betsy Bowden"
              }
            ],
            "files": []
          },
          {
            "id": 9433,
            "title": "Language, Memory, and Sense Perception in the Religious and Technological Culture of Antiquity and the Middle Ages",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Werner H. Kelber"
              }
            ],
            "files": []
          },
          {
            "id": 9431,
            "title": "Review Essay<br/>The <em>Fornaldarsögur</em>: Stephen Mitchell’s Contribution",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jesse Byock"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 309,
            "issue_id": "257",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 310,
            "issue_id": "257",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10ii/10_2_cover.pdf"
          },
          {
            "id": 311,
            "issue_id": "257",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10ii/10_2_cover.jpg"
          },
          {
            "id": 312,
            "issue_id": "257",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10ii/10_2_front_matter.pdf"
          },
          {
            "id": 313,
            "issue_id": "257",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10ii/10_2_back_matter.pdf"
          },
          {
            "id": 314,
            "issue_id": "257",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10ii/4_ed_col_10_2.pdf"
          },
          {
            "id": 315,
            "issue_id": "257",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10ii/oraltradition_10_2.zip"
          },
          {
            "id": 316,
            "issue_id": "257",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10ii/10_2_complete.pdf"
          },
          {
            "id": 317,
            "issue_id": "257",
            "name": "Volume",
            "value": "10"
          },
          {
            "id": 318,
            "issue_id": "257",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 319,
            "issue_id": "257",
            "name": "Date",
            "value": "1995-10-01"
          }
        ]
      },
      {
        "id": 604,
        "name": "10i",
        "title": "Volume 10, Issue 1",
        "articles": [
          {
            "id": 10306,
            "title": "Perspectives on Orality in African Cinema",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Keyan Tomaselli"
              },
              {
                "name": "Maureen Eke"
              }
            ],
            "files": []
          },
          {
            "id": 10304,
            "title": "The Three Circuits of the Suitors: A Ring Composition in <em>Odyssey</em> 17-22",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Steve Reece"
              }
            ],
            "files": []
          },
          {
            "id": 10302,
            "title": "Hermeneutic Forever: Voice, Text, Digitization, and the <em>‘I’</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Walter J. Ong"
              }
            ],
            "files": []
          },
          {
            "id": 10300,
            "title": "Mandela Comes Home: The Poets’ Perspective",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Russell H. Kaschula"
              }
            ],
            "files": []
          },
          {
            "id": 10298,
            "title": "What’s in a Frame? The Medieval Textualization of Traditional Storytelling",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bonnie D. Irwin"
              }
            ],
            "files": []
          },
          {
            "id": 10296,
            "title": "Narrating Saga Feud: <em>Tháttr</em> and the Fundamental Oral Progression",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jesse Byock"
              }
            ],
            "files": []
          },
          {
            "id": 10294,
            "title": "A Poet on the Achaean Wall",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Timothy W. Boyd"
              }
            ],
            "files": []
          },
          {
            "id": 10292,
            "title": "<em>Matigari</em>: An African Novel as Oral Narrative Performance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "F. Odun Balogun"
              }
            ],
            "files": []
          },
          {
            "id": 10290,
            "title": "Affective Criticism, Oral Poetics, and Beowulf’s Fight with the Dragon",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark C. Amodio "
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 672,
            "issue_id": "604",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 673,
            "issue_id": "604",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/10_1_cover.pdf"
          },
          {
            "id": 674,
            "issue_id": "604",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10i/10_1_cover.jpg"
          },
          {
            "id": 675,
            "issue_id": "604",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10i/10_1_front_matter.pdf"
          },
          {
            "id": 676,
            "issue_id": "604",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10i/10_1_back_matter.pdf"
          },
          {
            "id": 677,
            "issue_id": "604",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10i/4_editor_column_10_1.pdf"
          },
          {
            "id": 678,
            "issue_id": "604",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10i/oraltradition_10_1.zip"
          },
          {
            "id": 679,
            "issue_id": "604",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/10i/10_1_complete.pdf"
          },
          {
            "id": 680,
            "issue_id": "604",
            "name": "Volume",
            "value": "10"
          },
          {
            "id": 681,
            "issue_id": "604",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 682,
            "issue_id": "604",
            "name": "Date",
            "value": "1995-03-01"
          },
          {
            "id": 1828,
            "issue_id": "604",
            "name": "URN",
            "value": ""
          },
          {
            "id": 1829,
            "issue_id": "604",
            "name": "Thumbnail",
            "value": "http://journal.oraltradition.org/wp-content/uploads/placeholder.png"
          }
        ]
      },
      {
        "id": 259,
        "name": "9ii",
        "title": "Volume 9, Issue 2",
        "articles": [
          {
            "id": 9690,
            "title": "Performing <em>A Thousand and One Nights</em> In Egypt",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Susan Slyomovics"
              }
            ],
            "files": []
          },
          {
            "id": 9688,
            "title": "Homer’s Style: Non-Formulaic Features of an Oral Aesthetic",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joseph Russo"
              }
            ],
            "files": []
          },
          {
            "id": 9686,
            "title": "Forrest Spirits: Oral Echoes in Leon Forrest’s Prose",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bruce A. Rosenberg"
              }
            ],
            "files": []
          },
          {
            "id": 9684,
            "title": "Informing Performance: Producing the <em>Coloquio</em> in Tierra Blanca",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Richard Bauman"
              },
              {
                "name": "Pamela Ritch"
              }
            ],
            "files": []
          },
          {
            "id": 9682,
            "title": "Editing <em>Beowulf</em>: What Can Study of the Ballads Tell Us?",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John D. Niles"
              }
            ],
            "files": []
          },
          {
            "id": 9680,
            "title": "Oral Genres and the Art of Reading in Tibet",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Anne Klein"
              }
            ],
            "files": []
          },
          {
            "id": 9678,
            "title": "Ethnopoetics, Oral-Formulaic Theory, and Editing Texts",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Dell Hymes"
              }
            ],
            "files": []
          },
          {
            "id": 9676,
            "title": "The Ethnography of Scribal Writing and Anglo-Saxon Poetry: Scribe as Performer",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "A. Nicholas Doane"
              }
            ],
            "files": []
          },
          {
            "id": 9435,
            "title": "Symposium<br />The Study of the Orally Transmitted Ballad:<br/>Past Paradigms and a New Poetics",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Teresa Catarella"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 320,
            "issue_id": "259",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 321,
            "issue_id": "259",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9ii/9_2_cover.pdf"
          },
          {
            "id": 322,
            "issue_id": "259",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9ii/9_2_cover.jpg"
          },
          {
            "id": 323,
            "issue_id": "259",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9ii/9_2_front_matter.pdf"
          },
          {
            "id": 324,
            "issue_id": "259",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9ii/9_2_back_matter.pdf"
          },
          {
            "id": 325,
            "issue_id": "259",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9ii/4_editor_column.pdf"
          },
          {
            "id": 326,
            "issue_id": "259",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9ii/oraltradition_9_2.zip"
          },
          {
            "id": 327,
            "issue_id": "259",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9ii/9_2_complete.pdf"
          },
          {
            "id": 328,
            "issue_id": "259",
            "name": "Volume",
            "value": "9"
          },
          {
            "id": 329,
            "issue_id": "259",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 330,
            "issue_id": "259",
            "name": "Date",
            "value": "1994-10-01"
          }
        ]
      },
      {
        "id": 361,
        "name": "9i",
        "title": "Volume 9, Issue 1: African Oral Tradition",
        "articles": [
          {
            "id": 9710,
            "title": "On the Sense and Nonsense of Performance Studies Concerning Oral Literature of the Bulsa in Northern Ghana",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Rüediger Schott"
              }
            ],
            "files": []
          },
          {
            "id": 9708,
            "title": "Silent Voices: The Role of Somali Women’s Poetry in Social and Political Life",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Zainab Mohamed Jama"
              }
            ],
            "files": []
          },
          {
            "id": 9706,
            "title": "Introduction: The Search for Grounds in African Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lee Haring"
              }
            ],
            "files": []
          },
          {
            "id": 9704,
            "title": "Social Speech and Speech of the Imagination: Female Identity and Ambivalence in Bambara-Malinké Oral Literature",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Veronika Görög-Karady"
              }
            ],
            "files": []
          },
          {
            "id": 9702,
            "title": "Pattern, Interaction, and the Non-Dialogic in Performance by Hausa Rap Artists",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Graham Furniss"
              },
              {
                "name": "Sa’idu Babur  Ahmed"
              }
            ],
            "files": []
          },
          {
            "id": 9700,
            "title": "Through Ambiguous Tales: Women’s Voices in Chokwe Storytelling",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Rachel I. Fretz"
              }
            ],
            "files": []
          },
          {
            "id": 9698,
            "title": "Field of Life, Sowing of Speech, Harvest of Acts",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sory Camara"
              }
            ],
            "files": []
          },
          {
            "id": 9696,
            "title": "Oral Literary Criticism and the Performance of the Igbo Epic",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Chukwuma Azuonye"
              }
            ],
            "files": []
          },
          {
            "id": 9694,
            "title": "Freedom to Sing, License to Insult: The Influence of <em>Haló</em> Performance on Social Violence Among the Anlo Ewe",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Daniel K. Avorgbedor"
              }
            ],
            "files": []
          },
          {
            "id": 9692,
            "title": "Women’s Discourse on Social Change in Nzema (Ghanaian) Maiden Songs",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "K.E. Agovi"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 507,
            "issue_id": "361",
            "name": "Subtitle",
            "value": "African Oral Tradition"
          },
          {
            "id": 508,
            "issue_id": "361",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9i/9_1_cover.pdf"
          },
          {
            "id": 509,
            "issue_id": "361",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9i/9_1_cover.jpg"
          },
          {
            "id": 510,
            "issue_id": "361",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9i/9_1_front_matter.pdf"
          },
          {
            "id": 511,
            "issue_id": "361",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9i/9_1_back_matter.pdf"
          },
          {
            "id": 512,
            "issue_id": "361",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9i/1d_Introduction_9_1.pdf"
          },
          {
            "id": 513,
            "issue_id": "361",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9i/oraltradition_9_1.zip"
          },
          {
            "id": 514,
            "issue_id": "361",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/9i/9_1_complete.pdf"
          },
          {
            "id": 515,
            "issue_id": "361",
            "name": "Volume",
            "value": "9"
          },
          {
            "id": 516,
            "issue_id": "361",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 517,
            "issue_id": "361",
            "name": "Date",
            "value": "1994-03-01"
          }
        ]
      },
      {
        "id": 263,
        "name": "8ii",
        "title": "Volume 8, Issue 2",
        "articles": [
          {
            "id": 9722,
            "title": "Rap Music: An Interview with DJ Romeo",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Debra Wehmeyer-Shaw"
              }
            ],
            "files": []
          },
          {
            "id": 9720,
            "title": "<em>La fraticida por amor</em>: A Sixteenth-Century Spanish Ballad in the Modern Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Madeline Sutherland"
              }
            ],
            "files": []
          },
          {
            "id": 9718,
            "title": "Homer and <em>Roland</em>: The Shared Formular Technique, Part II",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William Merrit Sale"
              }
            ],
            "files": []
          },
          {
            "id": 9716,
            "title": "Isochrony in Old English Poetry: Two Performances of <em>Cædmon’s Hymn</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Miriam Youngerman Miller"
              }
            ],
            "files": []
          },
          {
            "id": 9714,
            "title": "From Maria to Marjatta: The Transformation of an Oral Poem in Elias Lönnrot’s <em>Kalevala</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas A. DuBois"
              }
            ],
            "files": []
          },
          {
            "id": 9712,
            "title": "“O Bride Light of My Eyes”: Bridal Songs of Arab Women in the Galilee",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mishael Maswari Caspi"
              },
              {
                "name": "Julia Ann Blessing"
              }
            ],
            "files": []
          },
          {
            "id": 9439,
            "title": "Symposium<br />Deafness and Orality: An Electronic Conversation",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": " ORTRAD-L"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 342,
            "issue_id": "263",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 343,
            "issue_id": "263",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8ii/8_2_cover.pdf"
          },
          {
            "id": 344,
            "issue_id": "263",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8ii/8_2_cover.jpg"
          },
          {
            "id": 345,
            "issue_id": "263",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8ii/8_2_front_matter.pdf"
          },
          {
            "id": 346,
            "issue_id": "263",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8ii/8_2_back_matter.pdf"
          },
          {
            "id": 347,
            "issue_id": "263",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8ii/1_editors_column_8_2.pdf"
          },
          {
            "id": 348,
            "issue_id": "263",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8ii/oraltradition_8_2.zip"
          },
          {
            "id": 349,
            "issue_id": "263",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8ii/8_2_complete.pdf"
          },
          {
            "id": 350,
            "issue_id": "263",
            "name": "Volume",
            "value": "8"
          },
          {
            "id": 351,
            "issue_id": "263",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 352,
            "issue_id": "263",
            "name": "Date",
            "value": "1993-10-01"
          }
        ]
      },
      {
        "id": 378,
        "name": "8i",
        "title": "Volume 8, Issue 1",
        "articles": [
          {
            "id": 9736,
            "title": "Alterities: On Methodology in Medieval Literary Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ursula Schaefer"
              }
            ],
            "files": []
          },
          {
            "id": 9734,
            "title": "Homer and <em>Roland</em>: The Shared Formular Technique, Part I",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William Merrit Sale"
              }
            ],
            "files": []
          },
          {
            "id": 9732,
            "title": "Theocritus and Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "James B. Pearce"
              }
            ],
            "files": []
          },
          {
            "id": 9730,
            "title": "The Interrelationship Between the Oral and the Written in the Works of Alexander Campbell",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Raymond F. Person, Jr."
              }
            ],
            "files": []
          },
          {
            "id": 9728,
            "title": "Strategies for the Presentation of Oral Tradition in Print",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Eric L. Montenyohl"
              }
            ],
            "files": []
          },
          {
            "id": 9726,
            "title": "Nestor Among the Sirens",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Keith Dickson"
              }
            ],
            "files": []
          },
          {
            "id": 9724,
            "title": "Activation and Preservation: The Interdependence of Text and Performance in an Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Egbert J. Bakker "
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 518,
            "issue_id": "378",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 519,
            "issue_id": "378",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8i/8_1_cover.pdf"
          },
          {
            "id": 520,
            "issue_id": "378",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8i/8_1_cover.jpg"
          },
          {
            "id": 521,
            "issue_id": "378",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8i/8_1_front_matter.pdf"
          },
          {
            "id": 522,
            "issue_id": "378",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8i/8_1_back_matter.pdf"
          },
          {
            "id": 523,
            "issue_id": "378",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8i/1d_editor_column_8_1.pdf"
          },
          {
            "id": 524,
            "issue_id": "378",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8i/oraltradition_8_1.zip"
          },
          {
            "id": 525,
            "issue_id": "378",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/8i/8_1_complete.pdf"
          },
          {
            "id": 526,
            "issue_id": "378",
            "name": "Volume",
            "value": "8"
          },
          {
            "id": 527,
            "issue_id": "378",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 528,
            "issue_id": "378",
            "name": "Date",
            "value": "1993-03-01"
          }
        ]
      },
      {
        "id": 266,
        "name": "7ii",
        "title": "Volume 7, Issue 2",
        "articles": [
          {
            "id": 9750,
            "title": "Narrative Proverbs in the African Novel",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Emmanuel Obiechina"
              }
            ],
            "files": []
          },
          {
            "id": 9748,
            "title": "Repetition as Invention in the Songs of Vuk Karadžić",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Svetozar Koljević"
              }
            ],
            "files": []
          },
          {
            "id": 9746,
            "title": "Homer and Oral Tradition: The Type-Scene",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark W. Edwards"
              }
            ],
            "files": []
          },
          {
            "id": 9744,
            "title": "<em>Beowulf</em>: The Monsters and the Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Marilynn Desmond"
              }
            ],
            "files": []
          },
          {
            "id": 9742,
            "title": "Storytelling in Medieval Wales",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Sioned Davies"
              }
            ],
            "files": []
          },
          {
            "id": 9740,
            "title": "On the Composition of Women’s Songs",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mary P. Coote"
              }
            ],
            "files": []
          },
          {
            "id": 9738,
            "title": "“Sound Shaping” of East Slavic <em>Zagovory</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Alla Astakhova"
              }
            ],
            "files": []
          },
          {
            "id": 9445,
            "title": "Symposium<br />Current State of Studies in Oral Tradition in Japan",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Hiroyuki Araki"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 353,
            "issue_id": "266",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 354,
            "issue_id": "266",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7ii/7_2_cover.pdf"
          },
          {
            "id": 355,
            "issue_id": "266",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7ii/7_2_cover.jpg"
          },
          {
            "id": 356,
            "issue_id": "266",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7ii/7_2_front_matter.pdf"
          },
          {
            "id": 357,
            "issue_id": "266",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7ii/7_2_back_matter.pdf"
          },
          {
            "id": 358,
            "issue_id": "266",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7ii/editor_column_7_2.pdf"
          },
          {
            "id": 359,
            "issue_id": "266",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7ii/oraltradition_7_2.zip"
          },
          {
            "id": 360,
            "issue_id": "266",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7ii/7_2_complete.pdf"
          },
          {
            "id": 361,
            "issue_id": "266",
            "name": "Volume",
            "value": "7"
          },
          {
            "id": 362,
            "issue_id": "266",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 363,
            "issue_id": "266",
            "name": "Date",
            "value": "1992-10-01"
          }
        ]
      },
      {
        "id": 268,
        "name": "7i",
        "title": "Volume 7, Issue 1",
        "articles": [
          {
            "id": 9766,
            "title": "Oral Poetry and the World of <em>Beowulf</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Paul Sorrell"
              }
            ],
            "files": []
          },
          {
            "id": 9764,
            "title": "Innervision and Innertext: Oral and Interpretive Modes of Storytelling Performance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joseph Sobol"
              }
            ],
            "files": []
          },
          {
            "id": 9762,
            "title": "The Combat of Lug and Balor: Discourses of Power in Irish Myth and Folktale",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joan N. Radner"
              }
            ],
            "files": []
          },
          {
            "id": 9760,
            "title": "Song, Text, and Cassette: Why We Need Authoritative Audio Editions of Medieval Literary Works",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ward Parks"
              }
            ],
            "files": []
          },
          {
            "id": 9758,
            "title": "Latin Charms of Medieval England: Verbal Healing in a Christian Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lea Olsan"
              }
            ],
            "files": []
          },
          {
            "id": 9756,
            "title": "A Gaelic Songmaker’s Response to an English-speaking Nation",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas A. McKean"
              }
            ],
            "files": []
          },
          {
            "id": 9754,
            "title": "The Production of Finnish Epic Poetry—Fixed Wholes or Creative Compositions?",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lauri Harvilahti"
              }
            ],
            "files": []
          },
          {
            "id": 9752,
            "title": "The Narrative Presentation of Orality in James Joyce’s <em>Finnegan’s Wake</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Willi Erzgräber"
              }
            ],
            "files": []
          },
          {
            "id": 9449,
            "title": "Symposium<br />Turkish Oral Tradition in Texas:<br />The Archive of Turkish Oral Narrative",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Warren S. Walker"
              }
            ],
            "files": []
          },
          {
            "id": 9447,
            "title": "Review",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Carolyn Higbie"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 364,
            "issue_id": "268",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 365,
            "issue_id": "268",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7i/7_1_cover.pdf"
          },
          {
            "id": 366,
            "issue_id": "268",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7i/7_1_cover.jpg"
          },
          {
            "id": 367,
            "issue_id": "268",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7i/7_1_front_matter.pdf"
          },
          {
            "id": 368,
            "issue_id": "268",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7i/7_1_back_matter.pdf"
          },
          {
            "id": 369,
            "issue_id": "268",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7i/3_editor_column_7_1.pdf"
          },
          {
            "id": 370,
            "issue_id": "268",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7i/oraltradition_7_1.zip"
          },
          {
            "id": 371,
            "issue_id": "268",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/7i/7_1_complete.pdf"
          },
          {
            "id": 372,
            "issue_id": "268",
            "name": "Volume",
            "value": "7"
          },
          {
            "id": 373,
            "issue_id": "268",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 374,
            "issue_id": "268",
            "name": "Date",
            "value": "1992-03-01"
          }
        ]
      },
      {
        "id": 396,
        "name": "6ii-iii",
        "title": "Volume 6, Issue 2-3: Serbo-Croatian Oral Tradition",
        "articles": [
          {
            "id": 9792,
            "title": "Macedonian Folk Poetry, Principally Lyric",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Tome Sazdov"
              }
            ],
            "files": []
          },
          {
            "id": 9790,
            "title": "The Legend of Kosovo",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jelka Ređep"
              }
            ],
            "files": []
          },
          {
            "id": 9788,
            "title": "Continuity and Change in Folk Prose Narrative",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Nada Milošević-Ɖorđević"
              }
            ],
            "files": []
          },
          {
            "id": 9786,
            "title": "Special Serbo-Croatian Issue Introduction",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John S. Miletich"
              }
            ],
            "files": []
          },
          {
            "id": 9784,
            "title": "The Folk Ballad in Slovenia",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Zmaga Kumer"
              }
            ],
            "files": []
          },
          {
            "id": 9782,
            "title": "Notes on the Poetics of Serbo-Croatian Folk Lyric",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Hatidža Krnjević"
              }
            ],
            "files": []
          },
          {
            "id": 9780,
            "title": "Concluding Formulas of Audience Address in Serbo-Croatian Oral Epic",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Marija Kleut"
              }
            ],
            "files": []
          },
          {
            "id": 9778,
            "title": "The Montenegrin Oral Epic in a New Perspective",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Novak Kilibarda"
              }
            ],
            "files": []
          },
          {
            "id": 9776,
            "title": "<em>Bugaršćice</em>: A Unique Type of Archaic Oral Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Josip Kekez"
              }
            ],
            "files": []
          },
          {
            "id": 9774,
            "title": "Enjambement as a Criterion for Orality in Homeric and South Slavic Epic Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Zdeslav Dukat"
              }
            ],
            "files": []
          },
          {
            "id": 9772,
            "title": "The Geographic Extent and Chronological Coordinates of South Slavic Moslem Oral Epic",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ɖenana Buturović"
              }
            ],
            "files": []
          },
          {
            "id": 9770,
            "title": "Yugoslav Oral Lyric, Primarily in Serbo-Croatian",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Vladimir Bovan"
              }
            ],
            "files": []
          },
          {
            "id": 9768,
            "title": "Balladic Forms of the <em>Bugarštica</em> and Epic Songs",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Maja Bošković-Stulli"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 529,
            "issue_id": "396",
            "name": "Subtitle",
            "value": "Serbo-Croatian Oral Tradition"
          },
          {
            "id": 530,
            "issue_id": "396",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6ii-iii/6_2-3_cover.pdf"
          },
          {
            "id": 531,
            "issue_id": "396",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6ii-iii/6_2-3_cover.jpg"
          },
          {
            "id": 532,
            "issue_id": "396",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6ii-iii/6_2-3_front_matter.pdf"
          },
          {
            "id": 533,
            "issue_id": "396",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6ii-iii/6_2-3_back_matter.pdf"
          },
          {
            "id": 534,
            "issue_id": "396",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6ii-iii/3_editors_column.pdf"
          },
          {
            "id": 535,
            "issue_id": "396",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6ii-iii/oraltradition_6_2-3.zip"
          },
          {
            "id": 536,
            "issue_id": "396",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6ii-iii/6_2-3_complete.pdf"
          },
          {
            "id": 537,
            "issue_id": "396",
            "name": "Volume",
            "value": "6"
          },
          {
            "id": 538,
            "issue_id": "396",
            "name": "Number",
            "value": "2-3"
          },
          {
            "id": 539,
            "issue_id": "396",
            "name": "Date",
            "value": "1991-05-01"
          }
        ]
      },
      {
        "id": 33,
        "name": "6i",
        "title": "Volume 6, Issue 1",
        "articles": [
          {
            "id": 9806,
            "title": "Serial Defamation in Two Medieval Tales: The Icelandic <em>Oelkofra Thattir</em> and the Irish <em>Scela Mucce Meic Datho</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William Sayers"
              }
            ],
            "files": []
          },
          {
            "id": 9804,
            "title": "Compound Diction and Traditional Style in <em>Beowulf</em> and <em>Genesis A</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jeffrey Alan Mazo"
              }
            ],
            "files": []
          },
          {
            "id": 9802,
            "title": "The Evolution of an Oral Tradition: Race-Calling in Canterbury, New Zealand",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Koenraad Kuiper"
              }
            ],
            "files": []
          },
          {
            "id": 9800,
            "title": "Folk Traditions in Serbo-Croatian Literary Culture",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Svetozar Koljević"
              }
            ],
            "files": []
          },
          {
            "id": 9798,
            "title": "Tradition, But What Tradition and For Whom?",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ruth Finnegan"
              }
            ],
            "files": []
          },
          {
            "id": 9796,
            "title": "Phemius’ Last Stand: The Impact of Occasion on Tradition in the <em>Odyssey</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Carol Dougherty"
              }
            ],
            "files": []
          },
          {
            "id": 9794,
            "title": "There’s Nothing Natural About Natural Conversation: A Look at Dialogue in Fiction and Drama",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ryan Bishop"
              }
            ],
            "files": []
          },
          {
            "id": 8405,
            "title": "Toward an Evolutionary Ontology of Beauty",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Frederick Turner"
              }
            ],
            "files": []
          },
          {
            "id": 8403,
            "title": "The Oral Aesthetic and the Bicameral Mind",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Carl Lindahl"
              }
            ],
            "files": []
          },
          {
            "id": 8401,
            "title": "Literary Aesthetics in Oral Art",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Robert Kellogg"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 56,
            "issue_id": "33",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 57,
            "issue_id": "33",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6i/6_1_cover.pdf"
          },
          {
            "id": 58,
            "issue_id": "33",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6i/6_1_cover.jpg"
          },
          {
            "id": 59,
            "issue_id": "33",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6i/6_1_front_matter.pdf"
          },
          {
            "id": 60,
            "issue_id": "33",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6i/6_1_back_matter.pdf"
          },
          {
            "id": 61,
            "issue_id": "33",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6i/editors_column_6_1.pdf"
          },
          {
            "id": 62,
            "issue_id": "33",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6i/oraltradition_6_1.zip"
          },
          {
            "id": 63,
            "issue_id": "33",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/6i/6_1_complete.pdf"
          },
          {
            "id": 64,
            "issue_id": "33",
            "name": "Volume",
            "value": "6"
          },
          {
            "id": 65,
            "issue_id": "33",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 66,
            "issue_id": "33",
            "name": "Date",
            "value": "1991-01-01"
          }
        ]
      },
      {
        "id": 415,
        "name": "5ii-iii",
        "title": "Volume 5, Issue 2-3: South Pacific Oral Traditions",
        "articles": [
          {
            "id": 9828,
            "title": "“Head” and “Tail”: The Shaping of Oral Traditions among the Binandere in Papua New Guinea",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John D. Waiko"
              }
            ],
            "files": []
          },
          {
            "id": 9826,
            "title": "Profile of a Composer: Ihaia Puka, a <em>Pulotu</em> of the Tokelau Islands",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ineleo Tuia"
              },
              {
                "name": "Allen Thomas"
              }
            ],
            "files": []
          },
          {
            "id": 9824,
            "title": "Wry Comment from the Outback: Songs of Protest from the Niva Islands, Tonga",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Wendy Pond"
              }
            ],
            "files": []
          },
          {
            "id": 9822,
            "title": "Winged Tangi’ia: A Mangaian Dramatic Performance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Teaea Parima"
              },
              {
                "name": "Marivee McMath"
              }
            ],
            "files": []
          },
          {
            "id": 9820,
            "title": "“My Summit Where I Sit”: Form and Content in Maori Women’s Love Songs",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Margaret Orbell"
              }
            ],
            "files": []
          },
          {
            "id": 9818,
            "title": "Fiction, Fact, and Imagination: A Tokelau Narrative",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Judith Huntsman"
              }
            ],
            "files": []
          },
          {
            "id": 9816,
            "title": "Sex and Slander in Tikopia Song: Public Antagonism and Private Intrigue",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Raymond Firth"
              }
            ],
            "files": []
          },
          {
            "id": 9814,
            "title": "Introduction: or, Why the Comparativist Should Take Account of the South Pacific",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ruth Finnegan"
              }
            ],
            "files": []
          },
          {
            "id": 9812,
            "title": "Wept Thoughts: The Voicing of Kaluli Memories",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Steven Feld"
              }
            ],
            "files": []
          },
          {
            "id": 9810,
            "title": "“That Isn’t Really a Pig”: Spirit Traditions in the Southern Cook Islands",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Christian Clerk"
              }
            ],
            "files": []
          },
          {
            "id": 9808,
            "title": "Every Picture Tells a Story: Visual Alternatives to Oral Tradition in Ponam Society",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "James Carrier"
              },
              {
                "name": "Achsah  Carrier"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 540,
            "issue_id": "415",
            "name": "Subtitle",
            "value": "South Pacific Oral Traditions"
          },
          {
            "id": 541,
            "issue_id": "415",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5ii-iii/5_2-3_cover.pdf"
          },
          {
            "id": 542,
            "issue_id": "415",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5ii-iii/5_2-3_cover.jpg"
          },
          {
            "id": 543,
            "issue_id": "415",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5ii-iii/5_2-3_front_matter.pdf"
          },
          {
            "id": 544,
            "issue_id": "415",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5ii-iii/5_2-3_back_matter.pdf"
          },
          {
            "id": 545,
            "issue_id": "415",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5ii-iii/2_finnegan.pdf"
          },
          {
            "id": 546,
            "issue_id": "415",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5ii-iii/oraltradition_5_2-3.zip"
          },
          {
            "id": 547,
            "issue_id": "415",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5ii-iii/5_2-3_complete.pdf"
          },
          {
            "id": 548,
            "issue_id": "415",
            "name": "Volume",
            "value": "5"
          },
          {
            "id": 549,
            "issue_id": "415",
            "name": "Number",
            "value": "2-3"
          },
          {
            "id": 550,
            "issue_id": "415",
            "name": "Date",
            "value": "1990-05-01"
          }
        ]
      },
      {
        "id": 37,
        "name": "5i",
        "title": "Volume 5, Issue 1",
        "articles": [
          {
            "id": 9840,
            "title": "Worlds Apart: Orality, Literacy, and the Rajasthani Folk-<em>Mahabharata</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John D. Smith"
              }
            ],
            "files": []
          },
          {
            "id": 9838,
            "title": "Marcel Jousse: The Oral Style and the Anthropology of Gesture",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Edgard Richard Sienaert"
              }
            ],
            "files": []
          },
          {
            "id": 9836,
            "title": "Preface to <em>The Dialect of the Kara-Kirgiz</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Wilhelm Radloff"
              }
            ],
            "files": []
          },
          {
            "id": 9834,
            "title": "King Solomon’s Magic: The Power of a Written Text",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Marie Nelson"
              }
            ],
            "files": []
          },
          {
            "id": 9832,
            "title": "The Singers and their Epic Songs",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Matija Murko"
              }
            ],
            "files": []
          },
          {
            "id": 9830,
            "title": "A Typology of Mediation in Homer",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Keith Dickson"
              }
            ],
            "files": []
          },
          {
            "id": 9429,
            "title": "Review<br/><em>Earnest Games: Folkloric Patterns in the Canterbury Tales</em>, Carl Lindahl",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ward Parks"
              }
            ],
            "files": []
          },
          {
            "id": 8407,
            "title": "The Effects of Oral and Written Transmission in the Exchange of Materials between Medieval Celtic and French Literatures: A Physiological View",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Annalee C. Rejhon"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 67,
            "issue_id": "37",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 68,
            "issue_id": "37",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5i/5_1_cover.pdf"
          },
          {
            "id": 69,
            "issue_id": "37",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5i/5_1_cover.jpg"
          },
          {
            "id": 70,
            "issue_id": "37",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5i/5_1_front_matter.pdf"
          },
          {
            "id": 71,
            "issue_id": "37",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5i/5_1_back_matter.pdf"
          },
          {
            "id": 72,
            "issue_id": "37",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5i/editors_column_5_1.pdf"
          },
          {
            "id": 73,
            "issue_id": "37",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5i/oraltradition_5_1.zip"
          },
          {
            "id": 74,
            "issue_id": "37",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/5i/5_1_complete.pdf"
          },
          {
            "id": 75,
            "issue_id": "37",
            "name": "Volume",
            "value": "5"
          },
          {
            "id": 76,
            "issue_id": "37",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 77,
            "issue_id": "37",
            "name": "Date",
            "value": "1990-01-01"
          }
        ]
      },
      {
        "id": 255,
        "name": "4iii",
        "title": "Volume 4, Issue 3",
        "articles": [
          {
            "id": 9850,
            "title": "Song, Ritual, and Commemoration in Early Greek Poetry and Tragedy",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Charles Segal"
              }
            ],
            "files": []
          },
          {
            "id": 9848,
            "title": "Oral Verse-Making in Homer’s <em>Odyssey</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William C. Scott"
              }
            ],
            "files": []
          },
          {
            "id": 9846,
            "title": "Formulaic Diction in Kazakh Epic Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Karl Reichl"
              }
            ],
            "files": []
          },
          {
            "id": 9844,
            "title": "Improvisation in Hungarian Ethnic Dancing: An Analog to Oral Verse Composition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Wayne Kraft"
              }
            ],
            "files": []
          },
          {
            "id": 9842,
            "title": "“Beowulf Was Not There”: Compositional Aspects of <em>Beowulf</em>, Lines 1299b-1301",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Michael D. Cherniss"
              }
            ],
            "files": []
          },
          {
            "id": 9427,
            "title": "Review<br/><em>The Wisdom of the Outlaw: The Boyhood Deeds of Finn in Gaelic Narrative Tradition</em>, Joseph Falaky Nagy",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Vincent A. Dunn"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 298,
            "issue_id": "255",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 299,
            "issue_id": "255",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4iii/4_3_cover.pdf"
          },
          {
            "id": 300,
            "issue_id": "255",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4iii/4_3_cover.jpg"
          },
          {
            "id": 301,
            "issue_id": "255",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4iii/4_3_front_matter.pdf"
          },
          {
            "id": 302,
            "issue_id": "255",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4iii/4_3_back_matter.pdf"
          },
          {
            "id": 303,
            "issue_id": "255",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4iii/1c_editor_column_4_3.pdf"
          },
          {
            "id": 304,
            "issue_id": "255",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4iii/oraltradition_4_3.zip"
          },
          {
            "id": 305,
            "issue_id": "255",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4iii/4_3_complete.pdf"
          },
          {
            "id": 306,
            "issue_id": "255",
            "name": "Volume",
            "value": "4"
          },
          {
            "id": 307,
            "issue_id": "255",
            "name": "Number",
            "value": "3"
          },
          {
            "id": 308,
            "issue_id": "255",
            "name": "Date",
            "value": "1989-10-01"
          }
        ]
      },
      {
        "id": 437,
        "name": "4i-ii",
        "title": "Volume 4, Issue 1-2: Arabic Oral Traditions",
        "articles": [
          {
            "id": 9878,
            "title": "Oral Traditions of the Prophet Muhammad:  A Formulaic Approach",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "R. Marston Speight"
              }
            ],
            "files": []
          },
          {
            "id": 9876,
            "title": "“Tonight My Gun is Loaded”: Poetic Dueling in Arabia",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Saad A. Sowayan"
              }
            ],
            "files": []
          },
          {
            "id": 9874,
            "title": "Palestinian Improvised-Sung Poetry: The Genres of <em>Hidā</em> and <em>Qarrādī</em> Performance and Transmission",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Dirghām H. Sbait"
              }
            ],
            "files": []
          },
          {
            "id": 9872,
            "title": "Oral Transmission in Arabic Music, Past and Present",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "George D. Sawa"
              }
            ],
            "files": []
          },
          {
            "id": 9870,
            "title": "<em>Sīrāt Banī Hilāl</em>: Introduction and Notes to an Arab Oral Epic Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Dwight F. Reynolds"
              }
            ],
            "files": []
          },
          {
            "id": 9868,
            "title": "Arabic Folk Epic and the Western <em>Chanson de Geste</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "H.T. Norris"
              }
            ],
            "files": []
          },
          {
            "id": 9866,
            "title": "Which Came First, the <em>Zajal</em> or the <em>Muwaššḥa</em>?<br />Some Evidence for the Oral Origins of Hispano-Arabic Strophic Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "James T. Monroe"
              }
            ],
            "files": []
          },
          {
            "id": 9864,
            "title": "Epic Splitting: An Arab Folk Gloss on the Meaning of the Hero Pattern",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Henry Massie"
              },
              {
                "name": "Bridget  Connelly"
              }
            ],
            "files": []
          },
          {
            "id": 9862,
            "title": "From History to Fiction: The Tale Told by the King’s Steward in the <em>Thousand and One Nights</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Muhsin Mahdi"
              }
            ],
            "files": []
          },
          {
            "id": 9860,
            "title": "Sung Poetry in the Oral Tradition of the Gulf Region and the Arabian Peninsula",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Simon Jargy"
              }
            ],
            "files": []
          },
          {
            "id": 9858,
            "title": "The Development of Lebanese <em>Zajal</em>: Genre, Meter, and Verbal Duel",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Adnan Haydar"
              }
            ],
            "files": []
          },
          {
            "id": 9856,
            "title": "<em>Qur’ān</em> Recitation: A Tradition of Oral Performance and Transmission",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Frederick M. Denny"
              }
            ],
            "files": []
          },
          {
            "id": 9854,
            "title": "Review",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Dwight F. Reynolds"
              }
            ],
            "files": []
          },
          {
            "id": 9852,
            "title": "Banī Halba Classification of Poetic Genres",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Teirab AshShareef"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 551,
            "issue_id": "437",
            "name": "Subtitle",
            "value": "Arabic Oral Traditions"
          },
          {
            "id": 552,
            "issue_id": "437",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4i-ii/4_1-2_cover.pdf"
          },
          {
            "id": 553,
            "issue_id": "437",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4i-ii/4_1-2_cover.jpg"
          },
          {
            "id": 554,
            "issue_id": "437",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4i-ii/4_1-2_front_matter.pdf"
          },
          {
            "id": 555,
            "issue_id": "437",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4i-ii/4_1-2_back_matter.pdf"
          },
          {
            "id": 556,
            "issue_id": "437",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4i-ii/4_1-2_editors_column.pdf"
          },
          {
            "id": 557,
            "issue_id": "437",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4i-ii/oraltradition_4_1-2.zip"
          },
          {
            "id": 558,
            "issue_id": "437",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/4i-ii/4_1-2_complete.pdf"
          },
          {
            "id": 559,
            "issue_id": "437",
            "name": "Volume",
            "value": "4"
          },
          {
            "id": 560,
            "issue_id": "437",
            "name": "Number",
            "value": "1-2"
          },
          {
            "id": 561,
            "issue_id": "437",
            "name": "Date",
            "value": "1989-01-01"
          }
        ]
      },
      {
        "id": 451,
        "name": "3iii",
        "title": "Volume 3, Issue 3",
        "articles": [
          {
            "id": 9890,
            "title": "Lord of the Singers",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jeff Opland"
              }
            ],
            "files": []
          },
          {
            "id": 9888,
            "title": "Before Textuality: Orality and Interpretation",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Walter J. Ong"
              }
            ],
            "files": []
          },
          {
            "id": 9886,
            "title": "Oral Life and Literary Death in Medieval Irish Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joseph Falaky Nagy"
              }
            ],
            "files": []
          },
          {
            "id": 9884,
            "title": "Ninna-nanna-nonsense? Fears, Dreams, and Falling in the Italian Lullaby",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Luisa Del Giudice"
              }
            ],
            "files": []
          },
          {
            "id": 9882,
            "title": "Incipient Literacy: From Involvement to Integration in Tojolabal Maya",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Jill Brody"
              }
            ],
            "files": []
          },
          {
            "id": 9880,
            "title": "Text and Music in Romanian Oral Epic",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Margaret Hiebert Beissinger"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 562,
            "issue_id": "451",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 563,
            "issue_id": "451",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3iii/3_3_cover.pdf"
          },
          {
            "id": 564,
            "issue_id": "451",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3iii/3_3_cover.jpg"
          },
          {
            "id": 565,
            "issue_id": "451",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3iii/3_3_front_matter.pdf"
          },
          {
            "id": 566,
            "issue_id": "451",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3iii/3_3_back_matter.pdf"
          },
          {
            "id": 567,
            "issue_id": "451",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3iii/1c_editor_column_3_3.pdf"
          },
          {
            "id": 568,
            "issue_id": "451",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3iii/oraltradition_3_3.zip"
          },
          {
            "id": 569,
            "issue_id": "451",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3iii/3_3_complete.pdf"
          },
          {
            "id": 570,
            "issue_id": "451",
            "name": "Volume",
            "value": "3"
          },
          {
            "id": 571,
            "issue_id": "451",
            "name": "Number",
            "value": "3"
          },
          {
            "id": 572,
            "issue_id": "451",
            "name": "Date",
            "value": "1988-10-01"
          }
        ]
      },
      {
        "id": 253,
        "name": "3i-ii",
        "title": "Volume 3, Issue 1-2",
        "articles": [
          {
            "id": 9902,
            "title": "Oral Text: A South Indian Instance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Richard M. Swiderski"
              }
            ],
            "files": []
          },
          {
            "id": 9900,
            "title": "Oral Tradition and Welsh Literature: A Description and Survey",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Brynley F. Roberts"
              }
            ],
            "files": []
          },
          {
            "id": 9898,
            "title": "Oral-Formulaic Research in Old English Studies: II",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Alexandra Hennessey Olsen"
              }
            ],
            "files": []
          },
          {
            "id": 9896,
            "title": "The Buddhist Tradition of Prosimetric Oral Narrative In Chinese Literature",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Victor H. Mair"
              }
            ],
            "files": []
          },
          {
            "id": 9894,
            "title": "Homer and Oral Tradition: The Formula, Part II",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark W. Edwards"
              }
            ],
            "files": []
          },
          {
            "id": 9892,
            "title": "A Formulaic Analysis of Samples taken from the <em>Shâhnâma</em> of Firdowsi",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Olga Merck Davidson"
              }
            ],
            "files": []
          },
          {
            "id": 9443,
            "title": "Annotated Bibliography to 1985",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Lee Edgar Tyler"
              }
            ],
            "files": []
          },
          {
            "id": 9441,
            "title": "Symposium<br />Oral, but Oral What? The Nomenclatures of Orality and Their Implications",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "David Henige"
              }
            ],
            "files": []
          },
          {
            "id": 9425,
            "title": "Reviews:<br/><em>The Spoken Word and the Work of Interpretation</em>, Dennis Tedlock",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Tullio Maranhão"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 287,
            "issue_id": "253",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 288,
            "issue_id": "253",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3i-ii/3_1-2_cover.pdf"
          },
          {
            "id": 289,
            "issue_id": "253",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3i-ii/3_1-2_cover.jpg"
          },
          {
            "id": 290,
            "issue_id": "253",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3i-ii/3_1-2_front_matter.pdf"
          },
          {
            "id": 291,
            "issue_id": "253",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3i-ii/3_1-2_back_matter.pdf"
          },
          {
            "id": 292,
            "issue_id": "253",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3i-ii/2_editor_column.pdf"
          },
          {
            "id": 293,
            "issue_id": "253",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3i-ii/oraltradition_3_1-2.zip"
          },
          {
            "id": 294,
            "issue_id": "253",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/3i-ii/3_1-2_complete.pdf"
          },
          {
            "id": 295,
            "issue_id": "253",
            "name": "Volume",
            "value": "3"
          },
          {
            "id": 296,
            "issue_id": "253",
            "name": "Number",
            "value": "1-2"
          },
          {
            "id": 297,
            "issue_id": "253",
            "name": "Date",
            "value": "1988-01-01"
          }
        ]
      },
      {
        "id": 199,
        "name": "2ii-iii",
        "title": "Volume 2, Issue 2-3: Hispanic Balladry",
        "articles": [
          {
            "id": 10322,
            "title": "The Judeo-Spanish Ballad Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Samuel G. Armistead "
              },
              {
                "name": "Joseph H. Silverman"
              }
            ],
            "files": []
          },
          {
            "id": 9920,
            "title": "Survival of the Traditional <em>Romancero</em>: Field Work",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ana Valenciano"
              }
            ],
            "files": []
          },
          {
            "id": 9918,
            "title": "Hunting for Rare <em>Romances</em> in the Canary Islands",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Maximiano Trapero"
              }
            ],
            "files": []
          },
          {
            "id": 9916,
            "title": "The Living Ballad in Brazil: Two Performances",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Judith Seeger"
              }
            ],
            "files": []
          },
          {
            "id": 9914,
            "title": "The Traditional <em>Romancero</em> in Mexico: Panorama",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mercedes Díaz Roig"
              }
            ],
            "files": []
          },
          {
            "id": 9912,
            "title": "Migratory Shepherds and Ballad Diffusion",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Antonio Sánchez Romeralo"
              }
            ],
            "files": []
          },
          {
            "id": 9910,
            "title": "In Defense of <em>Romancero</em> Geography",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Suzanne H. Petersen"
              }
            ],
            "files": []
          },
          {
            "id": 9908,
            "title": "The Structure and Changing Functions of Ballad Traditions",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Beatriz Mariscal de Rhett"
              }
            ],
            "files": []
          },
          {
            "id": 9906,
            "title": "Collecting Portuguese Ballads",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Manuel da Costa Fontes"
              }
            ],
            "files": []
          },
          {
            "id": 9904,
            "title": "The Artisan Poetry of the <em>Romancero</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Diego Catalán"
              }
            ],
            "files": []
          },
          {
            "id": 9144,
            "title": "References",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ruth House Webber"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 221,
            "issue_id": "199",
            "name": "Subtitle",
            "value": "Hispanic Balladry"
          },
          {
            "id": 222,
            "issue_id": "199",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2ii-iii/2_2-3_cover.pdf"
          },
          {
            "id": 223,
            "issue_id": "199",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2ii-iii/2_2-3_cover.jpg"
          },
          {
            "id": 224,
            "issue_id": "199",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2ii-iii/2_2-3_front_matter.pdf"
          },
          {
            "id": 225,
            "issue_id": "199",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2ii-iii/2_2-3_back_matter.pdf"
          },
          {
            "id": 226,
            "issue_id": "199",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2ii-iii/2_introduction.pdf"
          },
          {
            "id": 227,
            "issue_id": "199",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2ii-iii/oraltradition_2_2-3.zip"
          },
          {
            "id": 228,
            "issue_id": "199",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2ii-iii/2_2-3_complete.pdf"
          },
          {
            "id": 229,
            "issue_id": "199",
            "name": "Volume",
            "value": "2"
          },
          {
            "id": 230,
            "issue_id": "199",
            "name": "Number",
            "value": "2-3"
          },
          {
            "id": 231,
            "issue_id": "199",
            "name": "Date",
            "value": "1987-05-01"
          }
        ]
      },
      {
        "id": 470,
        "name": "2i",
        "title": "Volume 2, Issue 1: Festschrift for Walter J. Ong",
        "articles": [
          {
            "id": 9960,
            "title": "Rahner on <em>Sprachregelung</em>: Regulation of Language? Of Speech?",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Frans Jozef van Beeck"
              }
            ],
            "files": []
          },
          {
            "id": 9958,
            "title": "A Remark on Silence and Listening",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Paolo Valesio"
              }
            ],
            "files": []
          },
          {
            "id": 9956,
            "title": "Orality and Literacy in Matter and Form: Ben Franklin’s <em>Way to Wealth</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas J. Steele"
              }
            ],
            "files": []
          },
          {
            "id": 9954,
            "title": "Speech Is the Body of the Spirit: The Oral Hermeneutic in the Writings of Eugen Rosenstock-Huessy",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Harold M. Stahmer"
              }
            ],
            "files": []
          },
          {
            "id": 9952,
            "title": "Peter Ramus, Walter Ong, and the Tradition of Humanistic Learning",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Peter Sharratt"
              }
            ],
            "files": []
          },
          {
            "id": 9950,
            "title": "Orality and Textuality in Medieval Castilian Prose",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Dennis P. Seniff"
              }
            ],
            "files": []
          },
          {
            "id": 9948,
            "title": "The Complexity of Oral Tradition",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bruce A. Rosenberg"
              }
            ],
            "files": []
          },
          {
            "id": 9946,
            "title": "Two Functions of Social Discourse: From Lope de Vega to Miguel de Cervantes",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Elias L. Rivers"
              }
            ],
            "files": []
          },
          {
            "id": 9944,
            "title": "The Ramist Style of John Udall: Audience and Pictorial Logic in Puritan Sermon and Controversy",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John G. Rechtien"
              }
            ],
            "files": []
          },
          {
            "id": 9942,
            "title": "Orality-Literacy Studies and the Unity of the Human Race",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Walter J. Ong"
              }
            ],
            "files": []
          },
          {
            "id": 9940,
            "title": "Literacy, Commerce, and Catholicity: Two Contexts of Change and Invention",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Randolph F. Lumpp"
              }
            ],
            "files": []
          },
          {
            "id": 9938,
            "title": "Characteristics of Orality",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Albert B. Lord"
              }
            ],
            "files": []
          },
          {
            "id": 9936,
            "title": "“Voice” and “Address” in Literary Theory",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "William J. Kennedy"
              }
            ],
            "files": []
          },
          {
            "id": 9934,
            "title": "The Harmony of Time in <em>Paradise Lost</em>",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Robert Kellogg"
              }
            ],
            "files": []
          },
          {
            "id": 9932,
            "title": "The Authority of the Word in St. John’s Gospel: Charismatic Speech, Narrative Text, Logocentric Metaphysics",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Werner H. Kelber"
              }
            ],
            "files": []
          },
          {
            "id": 9930,
            "title": "The Cosmic Myths of Homer and Hesiod",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Eric A. Havelock"
              }
            ],
            "files": []
          },
          {
            "id": 9928,
            "title": "Man, Muse, and Story: Psychohistorical Patterns in Oral Epic Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Miles Foley"
              }
            ],
            "files": []
          },
          {
            "id": 9926,
            "title": "Early Christian Creeds and Controversies in the Light of the Orality-Literacy Hypothesis",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Thomas J. Farrell"
              }
            ],
            "files": []
          },
          {
            "id": 9924,
            "title": "The Making of the Novel and the Evolution of Consciousness",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ruth El Saffar"
              }
            ],
            "files": []
          },
          {
            "id": 9922,
            "title": "Coming of Age in the Global Village",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "James M. Curtis"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 573,
            "issue_id": "470",
            "name": "Subtitle",
            "value": "Festschrift for Walter J. Ong"
          },
          {
            "id": 574,
            "issue_id": "470",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2i/2_1_cover.pdf"
          },
          {
            "id": 575,
            "issue_id": "470",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2i/2_1_cover.jpg"
          },
          {
            "id": 576,
            "issue_id": "470",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2i/2_1_front_matter.pdf"
          },
          {
            "id": 577,
            "issue_id": "470",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads"
          },
          {
            "id": 578,
            "issue_id": "470",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2i/2_introduction.pdf"
          },
          {
            "id": 579,
            "issue_id": "470",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2i/oraltradition_2_1.zip"
          },
          {
            "id": 580,
            "issue_id": "470",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/2i/2_1_complete.pdf"
          },
          {
            "id": 581,
            "issue_id": "470",
            "name": "Volume",
            "value": "2"
          },
          {
            "id": 582,
            "issue_id": "470",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 583,
            "issue_id": "470",
            "name": "Date",
            "value": "1987-01-01"
          }
        ]
      },
      {
        "id": 195,
        "name": "1iii",
        "title": "Volume 1, Issue 3",
        "articles": [
          {
            "id": 10320,
            "title": "The Message of the American Folk Sermon",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Bruce A. Rosenberg"
              }
            ],
            "files": []
          },
          {
            "id": 10318,
            "title": "The Oral-Formulaic Theory in Middle English Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ward Parks"
              }
            ],
            "files": []
          },
          {
            "id": 10316,
            "title": "Oral-Formulaic Research in Old English Studies: I",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Alexandra Hennessey Olsen"
              }
            ],
            "files": []
          },
          {
            "id": 10314,
            "title": "Perspectives on Recent Work on the Oral Traditional Formula",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Albert B. Lord"
              }
            ],
            "files": []
          },
          {
            "id": 10312,
            "title": "The Oral Background of Byzantine Popular Poetry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Elizabeth and Michael Jeffreys"
              }
            ],
            "files": []
          },
          {
            "id": 10310,
            "title": "A Romanian Singer of Tales: Vasile Tetin",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Eliza Miruna Ghil"
              }
            ],
            "files": []
          },
          {
            "id": 10308,
            "title": "Social Functions of the Medieval Epic in the Romance Literatures",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joseph J. Duggan"
              }
            ],
            "files": []
          },
          {
            "id": 9142,
            "title": "Annotated Bibliography",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "John Miles Foley"
              },
              {
                "name": "Lee Edgar Tyler"
              },
              {
                "name": "Juris Dilevko"
              },
              {
                "name": "Patrick Gonder"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 210,
            "issue_id": "195",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 211,
            "issue_id": "195",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1iii/1_3_cover.pdf"
          },
          {
            "id": 212,
            "issue_id": "195",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1iii/1_3_cover.jpg"
          },
          {
            "id": 213,
            "issue_id": "195",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1iii/1_3_front_matter.pdf"
          },
          {
            "id": 214,
            "issue_id": "195",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1iii/1_3_back_matter.pdf"
          },
          {
            "id": 215,
            "issue_id": "195",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1iii/editors_column_1_3.pdf"
          },
          {
            "id": 216,
            "issue_id": "195",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1iii/oraltradition_1_3.zip"
          },
          {
            "id": 217,
            "issue_id": "195",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1iii/1_3_complete.pdf"
          },
          {
            "id": 218,
            "issue_id": "195",
            "name": "Volume",
            "value": "1"
          },
          {
            "id": 219,
            "issue_id": "195",
            "name": "Number",
            "value": "3"
          },
          {
            "id": 220,
            "issue_id": "195",
            "name": "Date",
            "value": "1986-12-01"
          }
        ]
      },
      {
        "id": 288,
        "name": "1ii",
        "title": "Volume 1, Issue 2",
        "articles": [
          {
            "id": 10336,
            "title": "Homer and Oral Tradition: The Formula, Part I",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Mark W. Edwards"
              }
            ],
            "files": []
          },
          {
            "id": 10334,
            "title": "Australian Aboriginal Oral Traditions",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Margaret Clunies Ross"
              }
            ],
            "files": []
          },
          {
            "id": 10332,
            "title": "Orality in Medieval Irish Narrative: An Overview",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Joseph Falaky Nagy"
              }
            ],
            "files": []
          },
          {
            "id": 10330,
            "title": "The Collection and Analysis of Oral Epic Tradition in South Slavic: An Instance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "David Bynum"
              }
            ],
            "files": []
          },
          {
            "id": 10328,
            "title": "Hispanic Oral Literature: Accomplishments and Perspectives",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Ruth House Webber"
              }
            ],
            "files": []
          },
          {
            "id": 10326,
            "title": "Exploring the Literate Blindspot: Alexander Pope’s Homer in Light of Milman Parry",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Elizabeth Hoffman"
              }
            ],
            "files": []
          },
          {
            "id": 10324,
            "title": "The Oral Tradition and Middle High German Literature",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Franz Bäuml"
              }
            ],
            "files": []
          },
          {
            "id": 9487,
            "title": "Two Aboriginal Oral Texts from Arnhem Land, North Australia",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Frank Gurrmanamana"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 397,
            "issue_id": "288",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 398,
            "issue_id": "288",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1ii/1_2_cover.pdf"
          },
          {
            "id": 399,
            "issue_id": "288",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1ii/1_2_cover.jpg"
          },
          {
            "id": 400,
            "issue_id": "288",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1ii/1_2_front_matter.pdf"
          },
          {
            "id": 401,
            "issue_id": "288",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1ii/1_2_back_matter.pdf"
          },
          {
            "id": 402,
            "issue_id": "288",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1ii/1b_editors_column.pdf"
          },
          {
            "id": 403,
            "issue_id": "288",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1ii/oraltradition_1_2.zip"
          },
          {
            "id": 404,
            "issue_id": "288",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1ii/1_2_complete.pdf"
          },
          {
            "id": 405,
            "issue_id": "288",
            "name": "Volume",
            "value": "1"
          },
          {
            "id": 406,
            "issue_id": "288",
            "name": "Number",
            "value": "2"
          },
          {
            "id": 407,
            "issue_id": "288",
            "name": "Date",
            "value": "1986-05-01"
          }
        ]
      },
      {
        "id": 617,
        "name": "1i",
        "title": "Volume 1, Issue 1",
        "articles": [
          {
            "id": 10346,
            "title": "The Manner of Boyan: Translating Oral Literature",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Burton Raffel"
              }
            ],
            "files": []
          },
          {
            "id": 10344,
            "title": "Oral Tradition and Biblical Studies",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Robert Culley"
              }
            ],
            "files": []
          },
          {
            "id": 10342,
            "title": "Performed Being: Word Art as a Human Inheritance",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Frederick Turner"
              }
            ],
            "files": []
          },
          {
            "id": 10340,
            "title": "The Oral Traditions of Modern Greece: A Survey",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Roderick Beaton"
              }
            ],
            "files": []
          },
          {
            "id": 10338,
            "title": "The Alphabetic Mind: A Gift of Greece to the Modern World",
            "content": "",
            "ecompanionhtml": null,
            "authors": [
              {
                "name": "Eric A. Havelock"
              }
            ],
            "files": []
          }
        ],
        "customFields": [
          {
            "id": 683,
            "issue_id": "617",
            "name": "Subtitle",
            "value": ""
          },
          {
            "id": 684,
            "issue_id": "617",
            "name": "Cover",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1i/1_1_cover.pdf"
          },
          {
            "id": 685,
            "issue_id": "617",
            "name": "Cover Image",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1i/1_1_cover.jpg"
          },
          {
            "id": 686,
            "issue_id": "617",
            "name": "Front Matter",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1i/1_1_front_matter.pdf"
          },
          {
            "id": 687,
            "issue_id": "617",
            "name": "About the Authors",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1i/1_1_back_matter.pdf"
          },
          {
            "id": 688,
            "issue_id": "617",
            "name": "Editor’s Column",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1i/1b_editors_column.pdf"
          },
          {
            "id": 689,
            "issue_id": "617",
            "name": "All Files",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1i/oraltradition_1_1.zip"
          },
          {
            "id": 690,
            "issue_id": "617",
            "name": "Complete Issue",
            "value": "http://journal.oraltradition.org/wp-content/uploads/files/articles/1i/1_1_complete.pdf"
          },
          {
            "id": 691,
            "issue_id": "617",
            "name": "Volume",
            "value": "1"
          },
          {
            "id": 692,
            "issue_id": "617",
            "name": "Number",
            "value": "1"
          },
          {
            "id": 693,
            "issue_id": "617",
            "name": "Date",
            "value": "1986-01-01"
          }
        ]
      }
    ]
  }
};

export default issues;
