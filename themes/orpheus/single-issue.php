<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

global $params;
global $wpdb;

$context         = Timber::context();
$term = $wpdb->get_results("select * from wp_terms where name = '" . $params['slug'] . "'");
$issue = new Timber\Term($term[0]->term_id);
$context['issue'] = $issue;
$context['body_class'] = "-issue";
$context['meta'] = array();
$context['attachments'] = array();
$meta_results = $wpdb->get_results("select * from wp_ccf_Value where term_id = " . $issue->ID);

foreach($meta_results as $meta_result) {
	if (strpos($meta_result->field_value, '@') != false) {
		$values = explode('@', $meta_result->field_value);
		$context['meta'][$meta_result->field_name] = $values[0];
	} else {
		$context['meta'][$meta_result->field_name] = $meta_result->field_value;
	}
}
$context['page_title'] = "Volume " . $context['meta']['Volume'] . ", Issue " . $context['meta']['Number'];
if ($context['meta']['Subtitle']) {
	$context['page_title'] = $context['page_title'] . ": " . $context['meta']['Subtitle'];
}

error_log(var_export($issue->ID, true));
$issue_posts = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "post", "cat" => $issue->ID));
$context['posts'] = array();
$posts = array();

foreach($issue_posts as $post) {
	$post->meta = get_post_meta($post->ID);
	if ($post->meta['pdf'] and count($post->meta['pdf'])) {
		$post->meta['pdf'] = wp_get_attachment_url(intval($post->meta['pdf'][0]));
	}
	if ($post->meta['attachments']) {
		$post->meta['attachments'] = JSON_decode($post->meta['attachments'][0]);
		$post->meta['attachments'] = $post->meta['attachments']->attachments;

		foreach($post->meta['attachments'] as $attachment) {
			$attachment->url = "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/" . $params['slug'] . "/full/" . $attachment->fields->title;
			$attachment->ecompanionLink = "/ecompanion/" . $post->ID . "/" . $post->post_name . "/";

			$file_is_image = false;
			$image_filename_endings = array("jpeg", "jpg", "png", "tiff", "tif");
			foreach ($image_filename_endings as $image_filename_ending) {
				$length = strlen($image_filename_ending);
				if (substr($attachment->fields->title, -$length) === $image_filename_ending){
					$file_is_image = true;
				}
			}
			if ($file_is_image) {
				$context['attachments'][] = $attachment;
			}
		}

	}

	if ($post->post_title == "Editors’ Column" || $post->post_title == "Editor's Column" || $post->post_title == "Editor’s Column" ) {
		$context['hasEditors'] = True;
	} else if ($post->post_title == "About the Authors") {
		$context['hasAboutAuthors'] = True;
	}

	$posts[] = $post;
}


function sortPosts($a, $b) {
    return intval($a->meta['issue_order'][0]) - intval($b->meta['issue_order'][0]);
}

usort($posts, "sortPosts");
$context['posts'] = $posts;


// error_log(var_export($context['meta'], true));

if ( post_password_required( $timber_post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array('single-issue.twig', 'single.twig' ), $context );
}
