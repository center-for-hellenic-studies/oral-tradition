function getQueryVariable(variable) {
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
     var pair = vars[i].split("=");
     if(pair[0] == variable){return pair[1];}
   }
   return(false);
}


(function ($) {
$( document ).ready(function() {

  // init headroom
  var headerEl = document.querySelector("header");
  var headroom  = new Headroom(headerEl);
  headroom.init();

  // initial page scroll state
  if (window.pageYOffset > 0) {
    headerEl.classList.add('headroom--not-top');
  }

  $('.showMenuButton').on('click', function(e) {
    $('header').addClass('-menuVisible');
  });

  $('.hideMenuButton').on('click', function(e) {
    $('header').removeClass('-menuVisible');
  });

  $('.headerExternalButtonSearchFocusInput').on('click', function(e) {
    $('#search_form input').focus();
  });


  $(document).keyup(function(e) {
    if (e.key === "Escape") {
      $('header').removeClass('-menuVisible');
    }
  });

  $('#search_form').submit(function(e) {
    e.preventDefault();
    window.location.replace(`/search/?s=${$('#header_external_search').val()}`);
  });

  var textSearchVal = getQueryVariable('s');
  if (textSearchVal && textSearchVal.length) {
    $('#header_external_search').val(textSearchVal);
  }

  $(".-categoriesMenu a").on("click", function(e) {
    var $target = $(e.target);
    var $parent = $target.closest('li');
    e.preventDefault();

    if (!$parent || $target.hasClass('active')) {
      return null;
    }

    var categoryClass = $parent[0].className.split('_')[1];
    $(".-horizontalList-category").addClass('-hidden');
    $(`.-horizontalList-category.-horizontalList_${categoryClass}`).removeClass('-hidden');
    $(".-categoriesMenu .active").removeClass('active');
    $parent.addClass('active');
  });

  $(".-volumesMenu a").on("click", function(e) {
    var $target = $(e.target);
    var $parent = $target.closest('li');
    e.preventDefault();

    if (!$parent) {
      return null;
    }

    var volumeClass = $parent[0].className.split('-')[2];
    $(".-horizontalList-volume").addClass('-hidden');
    $(`.-horizontalList-volume.-horizontalList_${volumeClass}`).removeClass('-hidden');
    $(".-volumesMenu .active").removeClass('active');
    $parent.addClass('active');
  });

  const initSidebarStick = () => {
    $("#sidebar").sticky({
      topSpacing: 30,
      bottomSpacing: 420,
    });
  };

  const removeSidebarStick = () => {
    $("#sidebar").unstick();
  };

  if (window.innerWidth > 990) {
    initSidebarStick();
  }

  // hide sidebar before sticky layout calculated; TODO: loading state
  $('#sidebar').removeClass('-hidden');

  $(window).resize(() => {
    if (window.innerWidth > 990) {
      initSidebarStick();
    } else {
      removeSidebarStick();
    }
  });

  window.downloadPDF = () => {
    html2pdf().set({
      margin: 1,
      filename: window.location.pathname.replace("/", "") + '.pdf',
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: { scale: 2 },
      jsPDF: {
        elementHandlers: {
          '#sidebar': function (element, renderer) {
              return true;
          }
        },
      },
    }).from(document.body).save();
  };
  if (window.location.hash === "#download") {
    window.downloadPDF();
  }
});
})(jQuery);
