<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context         = Timber::context();
$timber_post     = Timber::query_post();
$context['post'] = $timber_post;
$post = $timber_post;

if ($post->tags) {
	$context['byline'] = "By " . join(", ", $post->tags);
}

if (!strlen($context['post']->title)) {
	$context['post']->title = "eCompanion";
}

$post->meta = get_post_meta($post->ID);
if ($post->meta['attachments']) {
	$post->meta['attachments'] = JSON_decode($post->meta['attachments'][0]);
	$post->meta['attachments'] = $post->meta['attachments']->attachments;

	foreach($post->meta['attachments'] as $attachment) {

		//$attachment->url = "http://oraltradition.org/wp-content/uploads/files/ecompanions/" . $post->category . "/full/" . $attachment->fields->title;
		$attachment->url = wp_get_attachment_url($attachment->id);

		$attachment->ecompanionLink = "/ecompanion/" . $post->ID . "/" . $post->post_name . "/";
		$context['attachments'][] = $attachment;
	}
}

if ( post_password_required( $timber_post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $timber_post->ID . '.twig', 'single-' . $timber_post->post_type . '.twig', 'single-' . $timber_post->slug . '.twig', 'single-ecompanion.twig' ), $context );
}
