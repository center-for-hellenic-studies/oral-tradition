<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context          = Timber::context();
$templates        = array( 'index.twig' );
$issue = new Timber\Term('34i');
$context['issue'] = $issue;
$context['posts'] = array();
$context['meta'] = array();
$context['attachments'] = array();
$meta_results = $wpdb->get_results("select * from wp_ccf_Value where term_id = " . $issue->ID);

foreach($meta_results as $meta_result) {
	$context['meta'][$meta_result->field_name] = $meta_result->field_value;
}

$posts = array();
foreach($issue->posts as $post) {
	$post->meta = get_post_meta($post->ID);
	if ($post->meta['pdf'] and count($post->meta['pdf'])) {
		$post->meta['pdf'] = wp_get_attachment_url(intval($post->meta['pdf'][0]));
	}
	if ($post->meta['attachments']) {
		$post->meta['attachments'] = JSON_decode($post->meta['attachments'][0]);
		$post->meta['attachments'] = $post->meta['attachments']->attachments;

		foreach($post->meta['attachments'] as $attachment) {
			$attachment->url = "http://journal.oraltradition.org/wp-content/uploads/files/ecompanions/" . $params['slug'] . "/full/" . $attachment->fields->title;
			$attachment->ecompanionLink = "/ecompanion/" . $post->ID . "/" . $post->post_name . "/";

			$file_is_image = false;
			$image_filename_endings = array("jpeg", "jpg", "png", "tiff", "tif");
			foreach ($image_filename_endings as $image_filename_ending) {
				$length = strlen($image_filename_ending);
				if (substr($attachment->fields->title, -$length) === $image_filename_ending){
					$file_is_image = true;
				}
			}
			if ($file_is_image) {
				$context['attachments'][] = $attachment;
			}
		}
	}
	$posts[] = $post;
}

function sortPosts($a, $b) {
    return intval($a->meta['issue_order'][0]) - intval($b->meta['issue_order'][0]);
}

usort($posts, "sortPosts");
$context['posts'] = $posts;



if ( is_home() ) {
	array_unshift( $templates, 'front-page.twig', 'home.twig' );
}
Timber::render( $templates, $context );
